import 'package:flutter/material.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/screens/screen_delivery/components/design_cancelled_delivery.dart';
import 'package:ptp_application/screens/screen_delivery/components/design_delivered.dart';
import 'package:ptp_application/screens/screen_delivery/components/design_refuse.dart';
import 'package:ptp_application/screens/screen_delivery/components/design_waiting_confirm.dart';
import 'package:ptp_application/screens/screen_delivery/components/design_waiting_delivery.dart';

// ignore: must_be_immutable
class DeliveryScreen extends StatelessWidget {
  DeliveryScreen({Key? key}) : super(key: key);
  int currentIndex = 0;

  final List<Widget> _tabs = [
    const DesignWaitingConfirmScreen(),
    const DesignWaitingDeliveryScreen(),
    const DesignDeliveredScreen(),
    const DesignCancelledDeliveryScreen(),
    const DesignRefuseScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: _tabs.length,
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back,
              size: 30,
            ),
          ),
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: <Color>[
                  ColorCodes.colorPrimary,
                  const Color.fromARGB(255, 56, 227, 184).withOpacity(0.8),
                  ColorCodes.colorPrimary,
                ],
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
              ),
            ),
          ),
          title: const Text('THÔNG TIN ĐƠN HÀNG'),
          centerTitle: true,
          bottom: const TabBar(
              isScrollable: true,
              indicatorColor: Colors.white,
              tabs: [
                Tab(text: 'Chờ Xác Nhận'),
                Tab(text: 'Đang Giao'),
                Tab(text: 'Đã Giao'),
                Tab(text: 'Đã Huỷ'),
                Tab(text: 'Đã Từ Chối'),
              ],
              labelStyle: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w500,
              )),
        ),
        body: TabBarView(
          children: _tabs,
        ),
      ),
    );
  }
}
