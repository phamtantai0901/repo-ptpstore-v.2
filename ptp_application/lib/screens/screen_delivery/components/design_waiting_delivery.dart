import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/config.dart';
import 'package:ptp_application/models/bill.dart';
import 'package:ptp_application/providers/bill_provider.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/screens/screen_delivery/components/design_button_bill_detail.dart';
import 'package:ptp_application/screens/show_dialog.dart';

class DesignWaitingDeliveryScreen extends StatefulWidget {
  const DesignWaitingDeliveryScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _DesignWaitingDeliveryScreenState();
}

class _DesignWaitingDeliveryScreenState
    extends State<DesignWaitingDeliveryScreen> {
  String? memberId;
  late BillProvider billProvider;
  @override
  void initState() {
    super.initState();
    memberId =
        Provider.of<MemberProvider>(context, listen: false).member.memberId;
    billProvider = Provider.of<BillProvider>(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: FutureBuilder<List<Bill>>(
        future: billProvider.fetchAllBillByIdMemberAndStatus(memberId!, 0),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<Bill> bills = snapshot.data!;
            return bills.isEmpty
                ? const Center(
                    child: Text(
                      "Danh Sách Trống",
                      style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w500,
                          color: Colors.black,
                          letterSpacing: 0.5),
                    ),
                  )
                : Container(
                    padding:
                        const EdgeInsets.only(top: 20, left: 10, right: 10),
                    child: ListView.builder(
                      itemCount: bills.length,
                      itemBuilder: (context, index) {
                        return buildBiilOfProduct(bills[index]);
                      },
                    ),
                  );
          } else if (snapshot.hasError) {
            return Center(child: Text(snapshot.error.toString()));
          } else {
            return Center(
              child: Container(
                alignment: Alignment.center,
                child: Column(
                  children: const [
                    Text(
                      "Loading...",
                      style: TextStyle(
                        fontSize: 16,
                        letterSpacing: 0.5,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    CircularProgressIndicator(),
                  ],
                ),
              ),
            );
          }
        },
      ),
    );
  }

//Design Build Product
  Widget buildBiilOfProduct(Bill bill) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(color: Colors.grey.shade100, width: 2),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            alignment: Alignment.center,
            width: 100,
            height: 28,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4),
              color: ColorCodes.pinkColor.withOpacity(0.9),
            ),
            child: const Text(
              'PTP Maill +',
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          const SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: CachedNetworkImage(
                  imageUrl: Config.getIP('/invoices/invoice.png'),
                  height: 90,
                  fit: BoxFit.fill,
                  placeholder: (context, url) => const Center(
                    child: CircularProgressIndicator(),
                  ),
                  errorWidget: (context, url, error) => const Icon(Icons.error),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    width: 250,
                    height: 50,
                    child: Text(
                      bill.billId!,
                      style: const TextStyle(
                        fontSize: 18,
                        color: ColorCodes.textColorPrimary,
                        fontWeight: FontWeight.w600,
                      ),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  const SizedBox(height: 13),
                  SizedBox(
                    width: 240,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Tổng SL: ${bill.totalQuantity}',
                          style: const TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color: ColorCodes.textColorPrimary,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 13),
                  Text(
                    'Tổng Tiền: ${formatNumber(bill.totalPrice)} đ',
                    style: const TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      color: ColorCodes.textColorPrimary,
                    ),
                  ),
                  const SizedBox(height: 10),
                ],
              ),
            ],
          ),
          Container(
            // width: 260,
            margin: const EdgeInsets.only(left: 130),
            alignment: Alignment.centerRight,
            child: Text('Địa chỉ nhận: ${bill.shippingAddress}',
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: ColorCodes.textColorSecondary,
                )),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              buildButtonBillDetail(context, bill.billId!, bill.status!),
            ],
          )
        ],
      ),
    );
  }
}
