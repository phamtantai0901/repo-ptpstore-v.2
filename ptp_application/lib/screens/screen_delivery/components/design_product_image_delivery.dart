//Design product image
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

//Design Product Image
Widget buildProductImage(String image) {
  return Container(
    width: 100,
    height: 100,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      boxShadow: [
        BoxShadow(
          color: const Color.fromARGB(255, 199, 205, 205).withOpacity(0.3),
          blurRadius: 10,
          offset: const Offset(0, 3),
        )
      ],
    ),
    child: ClipRRect(
      borderRadius: BorderRadius.circular(20.0),
      child: CachedNetworkImage(
        imageUrl: image,
        // width: 150,
        height: 90,
        fit: BoxFit.fill,
        placeholder: (context, url) => const Center(
          child: CircularProgressIndicator(),
        ),
        errorWidget: (context, url, error) => const Icon(Icons.error),
      ),
    ),
  );
}
