import 'package:flutter/material.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/screens/screen_see_bill_detail_in_delivery/see_bill_detail_in_delivery.dart';

//Design Button Bill Detail
Widget buildButtonBillDetail(BuildContext context, String idBill, int status) {
  return Container(
    alignment: Alignment.centerRight,
    padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
    margin: const EdgeInsets.only(bottom: 8),
    child: Container(
      // width: 150,
      height: 40,
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          width: 1,
          color: ColorCodes.colorPrimary,
        ),
        borderRadius: const BorderRadius.only(
          // bottomRight: Radius.circular(12),
          topLeft: Radius.circular(12),
          topRight: Radius.circular(2),
          bottomLeft: Radius.circular(2),
        ),
      ),
      child: TextButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) =>
                  SeeBillDetailInDeliveryScreen(idBill: idBill, status: status),
            ),
          );
        },
        child: const Text(
          'Chi tiết đơn hàng',
          style: TextStyle(
            letterSpacing: 0.5,
            color: ColorCodes.colorPrimary,
            fontSize: 16,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    ),
  );
}
