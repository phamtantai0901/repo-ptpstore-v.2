import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/config.dart';
import 'package:ptp_application/models/bill.dart';
import 'package:ptp_application/models/bill_detail.dart';
import 'package:ptp_application/models/cart.dart';
import 'package:ptp_application/models/stock_detail.dart';
import 'package:ptp_application/providers/bill_detail_provider.dart';
import 'package:ptp_application/providers/bill_provider.dart';
import 'package:ptp_application/providers/cart_provider.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/providers/stock_detail_provider.dart';
import 'package:ptp_application/screens/screen_cart/cart.dart';
import 'package:ptp_application/screens/screen_delivery/components/design_button_bill_detail.dart';
import 'package:ptp_application/screens/show_dialog.dart';

class DesignCancelledDeliveryScreen extends StatefulWidget {
  const DesignCancelledDeliveryScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _DesignCancelledDeliveryScreenState();
}

class _DesignCancelledDeliveryScreenState
    extends State<DesignCancelledDeliveryScreen> {
  String? memberId;
  late BillProvider billProvider;
  @override
  void initState() {
    super.initState();
    memberId =
        Provider.of<MemberProvider>(context, listen: false).member.memberId;
    billProvider = Provider.of<BillProvider>(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: FutureBuilder<List<Bill>>(
        future: billProvider.fetchAllBillByIdMemberAndStatus(memberId!, -2),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<Bill> bills = snapshot.data!;
            return bills.isEmpty
                ? const Center(
                    child: Text(
                      "Danh Sách Trống",
                      style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w500,
                          color: Colors.black,
                          letterSpacing: 0.5),
                    ),
                  )
                : Container(
                    padding:
                        const EdgeInsets.only(top: 20, left: 10, right: 10),
                    child: ListView.builder(
                      itemCount: bills.length,
                      itemBuilder: (context, index) {
                        return buildBiilOfProduct(bills[index]);
                      },
                    ),
                  );
          } else if (snapshot.hasError) {
            return Center(child: Text(snapshot.error.toString()));
          } else {
            return Center(
              child: Container(
                alignment: Alignment.center,
                child: Column(
                  children: const [
                    Text(
                      "Loading...",
                      style: TextStyle(
                        fontSize: 16,
                        letterSpacing: 0.5,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    CircularProgressIndicator(),
                  ],
                ),
              ),
            );
          }
        },
      ),
    );
  }

//Design Build Product
  Widget buildBiilOfProduct(Bill bill) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(color: Colors.grey.shade100, width: 2),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            alignment: Alignment.center,
            width: 100,
            height: 28,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4),
              color: ColorCodes.pinkColor.withOpacity(0.9),
            ),
            child: const Text(
              'PTP Maill +',
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          const SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: CachedNetworkImage(
                  imageUrl: Config.getIP('/invoices/invoice.png'),
                  height: 90,
                  fit: BoxFit.fill,
                  placeholder: (context, url) => const Center(
                    child: CircularProgressIndicator(),
                  ),
                  errorWidget: (context, url, error) => const Icon(Icons.error),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    width: 250,
                    height: 50,
                    child: Text(
                      bill.billId!,
                      style: const TextStyle(
                        fontSize: 18,
                        color: ColorCodes.textColorPrimary,
                        fontWeight: FontWeight.w600,
                      ),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  const SizedBox(height: 13),
                  SizedBox(
                    width: 240,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Tổng SL: ${bill.totalQuantity}',
                          style: const TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color: ColorCodes.textColorPrimary,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 13),
                  Text(
                    'Tổng Tiền: ${formatNumber(bill.totalPrice)} đ',
                    style: const TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      color: ColorCodes.textColorPrimary,
                    ),
                  ),
                  const SizedBox(height: 10),
                ],
              ),
            ],
          ),
          Container(
            // width: 260,
            margin: const EdgeInsets.only(left: 130),
            alignment: Alignment.centerRight,
            child: Text('Địa chỉ nhận: ${bill.shippingAddress}',
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: ColorCodes.textColorSecondary,
                )),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              buildButtonBillDetail(context, bill.billId!, bill.status!),
              const SizedBox(width: 30),
              buildButtonBuyAgain(context, bill)
            ],
          )
        ],
      ),
    );
  }
}

//Design Build Button Confirm
Widget buildButtonBuyAgain(BuildContext context, Bill bill) {
  return Container(
    alignment: Alignment.centerRight,
    padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
    child: Container(
      // width: 150,
      height: 38,
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          width: 1,
          color: ColorCodes.pinkColor,
        ),
        borderRadius: const BorderRadius.only(
          bottomRight: Radius.circular(12),
          topLeft: Radius.circular(12),
          topRight: Radius.circular(2),
          bottomLeft: Radius.circular(2),
        ),
      ),
      child: TextButton(
        onPressed: () async {
          List<BillDetail> billDetails = [];
          showDialogLoading(context);
          await Provider.of<BillDetailProvider>(context, listen: false)
              .getAllBillDetailByIdBill(bill.billId!)
              .then((value) => billDetails = value);

          for (var billDetail in billDetails) {
            Cart cart = Cart(
              productDetail: billDetail.productDetail,
              member: bill.member,
              quantity: billDetail.quantity,
              pricePay: billDetail.price,
            );

            StockDetail? stockDetail =
                Provider.of<StockDetailProvider>(context, listen: false)
                    .getFirstProductDetailInStockDetailByIdProductDetail(
                        billDetail.productDetail.productDetailId);

            Provider.of<CartProvider>(context, listen: false)
                .addProductIntoCart(cart, stockDetail!.quantity)
                .then((value) {
              if (value == -1) {
                dismissDialog(context);
                showMessageDialog(
                    context,
                    "Sản phẩm trong kho không đủ, vui lòng thử lại sau",
                    Icons.error);
              } else if (value == 0) {
                dismissDialog(context);
                showMessageDialog(
                    context,
                    "Không thể mua sản phẩm này, vui lòng thử lại sau",
                    Icons.error);
              }
            });
          }
          dismissDialog(context);
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const CartScreen()));
        },
        child: const Text(
          'Mua Lại',
          style: TextStyle(
            // letterSpacing: 1,
            color: ColorCodes.pinkColor,
            fontSize: 14,
            fontWeight: FontWeight.normal,
          ),
        ),
      ),
    ),
  );
}
