import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/screens/screen_forgot_password/forgot_password.dart';
import 'package:ptp_application/screens/screen_login/login.dart';
import 'package:ptp_application/screens/screen_reset_password/reset_password.dart';
import 'package:ptp_application/screens/show_dialog.dart';

class DesignBodyForgotPassword extends StatefulWidget {
  const DesignBodyForgotPassword({Key? key}) : super(key: key);

  @override
  _DesignBodyForgotPassword createState() => _DesignBodyForgotPassword();
}

class _DesignBodyForgotPassword extends State<DesignBodyForgotPassword> {
  late TextEditingController phone;
  late TextEditingController inputOTP;

  @override
  void initState() {
    super.initState();
    phone = TextEditingController();
    inputOTP = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        buildTextAndTextField(),
        buildButtonSend(),
        buildButtonBackHome(context),
      ],
    );
  }

//Design Text And TextField
  Widget buildTextAndTextField() {
    return Container(
        margin: const EdgeInsets.only(top: 10),
        child: Column(children: [
          buildTextInstruct(),
          const SizedBox(
            height: 20,
          ),
          buildTextField(
              MdiIcons.phone, "Nhập Số điện thoại của bạn", "Số điện thoại"),
          const SizedBox(
            height: 10,
          ),
        ]));
  }

//Design Text Instruct
  Widget buildTextInstruct() {
    return const Text(
      "Xin vui lòng điền số điện thoại của bạn. Vui lòng kiểm tra mã sẽ được gửi tới phone này. Trân trọng!",
      style: TextStyle(
        fontSize: 16,
        color: ColorCodes.textColorSecondary,
        fontWeight: FontWeight.w500,
      ),
    );
  }

//Design TextField
  Widget buildTextField(IconData icon, String hintText, String labelText) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: TextField(
        autofocus: true,
        controller: phone,
        cursorColor: ColorCodes.colorPrimary,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          prefixIcon: Icon(
            icon,
            color: ColorCodes.colorPrimary,
          ),
          enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: ColorCodes.textColorSecondary),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: ColorCodes.colorPrimary),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          contentPadding: const EdgeInsets.all(10),
          labelText: labelText,
          labelStyle: const TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w500,
          ),
          hintText: hintText,
          hintStyle: const TextStyle(
            fontWeight: FontWeight.w500,
            letterSpacing: 0.5,
            fontSize: 15,
            color: ColorCodes.textColorSecondary,
          ),
        ),
      ),
    );
  }

//Design Button Send
  Widget buildButtonSend() {
    return Container(
      width: double.maxFinite,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: <Color>[
            ColorCodes.colorPrimary,
            const Color(0xFF6DDDCD).withOpacity(0.8),
            const Color(0xFF6DCFCF),
          ],
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      child: TextButton(
        onPressed: () async {
          showDialogLoading(context);
          if (phone.text.isEmpty) {
            dismissDialog(context);
            showMessageDialog(
                context, "Vui lòng nhập số điện thoại", Icons.error);
            Future.delayed(
                const Duration(seconds: 2), () => dismissDialog(context));
            return;
          }
          await Provider.of<MemberProvider>(context, listen: false)
              .authenticationPhone(phone.text)
              .then((member) {
            if (member == null) {
              dismissDialog(context);
              showMessageDialog(
                  context, "Số điện thoại không hợp lệ", Icons.error);
              Future.delayed(const Duration(seconds: 2),
                  () => const ForgotPasswordScreen());
              phone.clear();
            } else {
              dismissDialog(context);
              buildShowDialog(context);
            }
          }).catchError((e) {
            dismissDialog(context);
            showMessageDialog(context, e.toString(), Icons.error);
            Future.delayed(
                const Duration(seconds: 2), () => dismissDialog(context));
          });
        },
        child: const Text(
          'GỬI',
          style: TextStyle(
            letterSpacing: 1,
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }

//Design Button Back Home
  Widget buildButtonBackHome(context) {
    return Container(
      margin: const EdgeInsets.only(top: 20),
      decoration: BoxDecoration(
        gradient: const LinearGradient(
          colors: <Color>[
            ColorCodes.colorPrimary,
            Color(0xFF6DCFCF),
          ],
        ),
        color: ColorCodes.colorPrimary,
        borderRadius: BorderRadius.circular(30),
      ),
      child: IconButton(
        onPressed: () {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              // ignore: prefer_const_constructors
              builder: (ctx) => LoginScreen(),
            ),
            (route) => false,
          );
        },
        icon: const Icon(MdiIcons.arrowLeft),
        color: Colors.white,
        iconSize: 30,
      ),
    );
  }

//Design ShowDialog
  buildShowDialog(BuildContext context) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 6, sigmaY: 6),
          child: AlertDialog(
            title: const Text('Nhập mã code vừa nhận'),
            content: TextField(
              autofocus: true,
              controller: inputOTP,
              decoration: const InputDecoration(hintText: "Nhập mã code"),
            ),
            actions: [
              Container(
                // margin: const EdgeInsets.only(bottom: 10),
                width: 110,
                height: 45,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  gradient: LinearGradient(
                    colors: <Color>[
                      ColorCodes.colorPrimary.withOpacity(0.8),
                      ColorCodes.colorPrimary,
                    ],
                  ),
                ),
                child: TextButton(
                    child: const Text(
                      'HUỶ',
                      style: TextStyle(
                        letterSpacing: 1,
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                      inputOTP.clear();
                      phone.clear();
                    }),
              ),
              const SizedBox(width: 10),
              Container(
                width: 110,
                height: 45,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  gradient: LinearGradient(
                    colors: <Color>[
                      ColorCodes.colorPrimary.withOpacity(0.8),
                      ColorCodes.colorPrimary,
                    ],
                  ),
                ),
                child: TextButton(
                  child: const Text(
                    'XÁC NHẬN',
                    style: TextStyle(
                      letterSpacing: 1,
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  onPressed: () {
                    showDialogLoading(context);
                    if (inputOTP.text.isEmpty) {
                      dismissDialog(context);
                      showMessageDialog(
                          context, "vui lòng nhập mã OTP", Icons.error);
                      inputOTP.clear();
                      Future.delayed(
                        const Duration(seconds: 2),
                        () => dismissDialog(context),
                      );
                      return;
                    }
                    Provider.of<MemberProvider>(context, listen: false)
                        .confirmCode(phone.text, int.parse(inputOTP.text))
                        .then(
                      (value) {
                        if (value == -1) {
                          dismissDialog(context);
                          showMessageDialog(
                              context,
                              "OTP không hợp lệ, vui lòng kiểm tra lại!",
                              Icons.error);
                          inputOTP.clear();
                          Future.delayed(
                            const Duration(seconds: 2),
                            () => dismissDialog(context),
                          );
                        } else if (value == 0) {
                          dismissDialog(context);
                          showMessageDialog(
                              context, "Mã OTP đã hết hạn", Icons.error);
                          inputOTP.clear();
                          Future.delayed(const Duration(seconds: 2), () {
                            dismissDialog(context);
                            dismissDialog(context);
                          });
                        } else {
                          dismissDialog(context);
                          showDialogLoadingSuccessfully(
                              context, "Xác thực mã code thành công");
                          Future.delayed(
                            const Duration(seconds: 2),
                            () => Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                builder: (ctx) => ResetPasswordScreen(
                                  username: phone.text,
                                ),
                              ),
                              (route) => false,
                            ),
                          );
                        }
                      },
                    );
                  },
                ),
              ),
              const SizedBox(width: 8),
            ],
          ),
        );
      },
    );
  }
}
