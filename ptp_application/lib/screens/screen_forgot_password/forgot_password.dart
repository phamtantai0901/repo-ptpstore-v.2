import 'package:flutter/material.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/screens/screen_forgot_password/components/design_body_forgot_password.dart';

import 'components/design_haft_circle_forgot_password.dart';
import 'components/design_header_forgot_password.dart';

class ForgotPasswordScreen extends StatelessWidget {
  const ForgotPasswordScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          body: Stack(
            children: [
              DesignHeaderForgotPassword(),
              Positioned(
                top: 210,
                child: Container(
                  padding: const EdgeInsets.all(20),
                  height: 470,
                  width: MediaQuery.of(context).size.width - 50,
                  margin: const EdgeInsets.symmetric(horizontal: 25),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5),
                    boxShadow: [
                      BoxShadow(
                        color: ColorCodes.colorPrimary.withOpacity(0.1),
                        blurRadius: 10,
                        spreadRadius: 1,
                      ),
                    ],
                  ),
                  child: const DesignBodyForgotPassword(),
                ),
              ),
              const DesignHaftCircleForgotPassword(),
              //Add submit button
            ],
          ),
        ));
  }
}
