import 'package:flutter/material.dart';
import 'package:ptp_application/screens/screen_history_activity/components/design_appbar_history_activity.dart';
import 'package:ptp_application/screens/screen_history_activity/components/design_body_history_activity.dart';

class HistoryActivityScreen extends StatelessWidget {
  const HistoryActivityScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: const DesignAppbarHistoryActivity(),
        body: Container(
          color: Colors.white,
          child: const DesignBodyHistoryActivity(),
        ),
      ),
    );
  }
}
