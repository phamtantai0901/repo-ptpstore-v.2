import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/config.dart';
import 'package:ptp_application/models/activity_history.dart';
import 'package:ptp_application/models/member.dart';
import 'package:ptp_application/providers/activity_history_provider.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/providers/screen_provider.dart';

class DesignBodyHistoryActivity extends StatefulWidget {
  const DesignBodyHistoryActivity({Key? key}) : super(key: key);

  @override
  _DesignBodyHistoryActivityState createState() =>
      _DesignBodyHistoryActivityState();
}

class _DesignBodyHistoryActivityState extends State<DesignBodyHistoryActivity> {
  Map<int, String> opts = {
    0: "Tìm kiếm",
    1: "Tương tác & Đánh giá",
    2: "Giỏ hàng",
    3: "Yêu thích",
    4: "Thanh toán hoá đơn",
    5: "Phiên Hoạt Động"
  };
  bool isCheck = true;
  late ActivityHistoryProvider actHisProvider;
  late Member member;
  late int currentIndex;
  @override
  void initState() {
    super.initState();
    actHisProvider =
        Provider.of<ActivityHistoryProvider>(context, listen: false);
    member = Provider.of<MemberProvider>(context, listen: false).member;
    currentIndex =
        Provider.of<ScreenProvider>(context, listen: false).setCurrentIndex = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15),
      child: Column(
        children: [
          buildHeaderHistoryActivity(),
          Expanded(
            child: buildBorderHistoryActivity(),
          ),
        ],
      ),
    );
  }

  //Design Box Product Rate
  Widget buildHeaderHistoryActivity() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        buildButtonDeleteAll(),
        buildDivider(),
        buildFilterHistoryActivity(),
        buildDivider(),
        buildButtonTypeHistory(opts.length),
        buildDivider(),
      ],
    );
  }

  //Design Divider
  Widget buildDivider() {
    return Container(
      margin: const EdgeInsets.only(top: 5, bottom: 5),
      child: Divider(
          color: Colors.grey[350], thickness: 0.6, indent: 0, endIndent: 0),
    );
  }

  //Design Filter History Activity
  Widget buildFilterHistoryActivity() {
    return Container(
      width: 270,
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          width: 1,
          color: ColorCodes.pinkColor,
        ),
        borderRadius: BorderRadius.circular(30),
      ),
      child: Row(
        children: [
          const SizedBox(width: 10),
          const Icon(
            Icons.filter_alt_outlined,
            color: ColorCodes.pinkColor,
          ),
          TextButton(
            onPressed: () {},
            child: const Text(
              'Lọc lịch sử hoạt động',
              style: TextStyle(
                letterSpacing: 1,
                color: ColorCodes.pinkColor,
                fontSize: 14,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ],
      ),
    );
  }

  //Design Button Delete All
  Widget buildButtonDeleteAll() {
    return Container(
      width: 160,
      decoration: BoxDecoration(
        border: Border.all(
          width: 1,
          color: ColorCodes.pinkColor,
        ),
        borderRadius: BorderRadius.circular(30),
      ),
      child: Row(
        children: [
          const SizedBox(width: 10),
          Icon(
            Icons.delete_forever,
            color: Colors.red[300],
          ),
          TextButton(
            onPressed: () {},
            child: const Text(
              'Xoá Tất Cả',
              style: TextStyle(
                letterSpacing: 1,
                color: ColorCodes.pinkColor,
                fontSize: 14,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildBorderHistoryActivity() {
    currentIndex =
        Provider.of<ScreenProvider>(context, listen: true).currentIndex;
    return FutureBuilder<List<ActivityHistory>>(
      future: actHisProvider.fetchAllActivityHistoryByUserIdAndType(
          member.user.userId!, currentIndex),
      builder: (context, snapshot) {
        if (snapshot.connectionState != ConnectionState.waiting) {
          if (snapshot.hasData && snapshot.data!.isNotEmpty) {
            return ListView.builder(
                itemCount: snapshot.data!.length,
                itemBuilder: (context, index) {
                  return buildBodyHistoryActitity(snapshot.data![index]);
                });
          } else if (snapshot.hasError) {
            return Center(
              child: Text(
                snapshot.error.toString(),
              ),
            );
          } else {
            return const Center(
              child: Text(
                "Danh sách trống",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                  color: Colors.black,
                  letterSpacing: 0.5,
                ),
              ),
            );
          }
        } else {
          return Container(
            alignment: Alignment.center,
            child: Column(
              children: const [
                Text(
                  "Loading...",
                  style: TextStyle(
                    fontSize: 16,
                    letterSpacing: 0.5,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                CircularProgressIndicator(),
              ],
            ),
          );
        }
      },
    );
  }

  Widget buildButtonTypeHistory(int length) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        gradient: LinearGradient(
          colors: <Color>[
            ColorCodes.colorPrimary,
            const Color(0xFF26cba3).withOpacity(0.9),
            ColorCodes.colorPrimary,
          ],
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
        ),
      ),
      height: 80,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: length,
        itemBuilder: (context, index) {
          return Container(
            decoration: checkActive(index),
            width: 120,
            child: TextButton(
              onPressed: () {
                Provider.of<ScreenProvider>(context, listen: false)
                    .updateCurrentIndex(index);
              },
              child: Text(
                opts[index]!,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontSize: 14,
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          );
        },
      ),
    );
  }

//Design build Product
  Widget buildBodyHistoryActitity(ActivityHistory actHis) {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: const Color(0xFFF1F1F1).withOpacity(0.7),
            offset: const Offset(0.0, 1.0),
            spreadRadius: 2,
            blurRadius: 10,
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(100),
            child: CachedNetworkImage(
              imageUrl: Config.getIP("/avatar_users/${member.user.image}"),
              width: 50,
              height: 50,
              fit: BoxFit.cover,
              placeholder: (context, url) => const Center(
                child: CircularProgressIndicator(),
              ),
              errorWidget: (context, url, error) => const Icon(Icons.error),
            ),
          ),
          const SizedBox(width: 10),
          SizedBox(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  actHis.dateCreated,
                  style: const TextStyle(
                    fontSize: 16,
                    color: ColorCodes.textColorSecondary,
                    fontWeight: FontWeight.w500,
                    overflow: TextOverflow.ellipsis,
                    letterSpacing: 0.5,
                  ),
                ),
                const SizedBox(height: 3),
                Text(
                  actHis.user.fullName,
                  style: const TextStyle(
                    fontSize: 16,
                    color: ColorCodes.pinkColor,
                    fontWeight: FontWeight.w600,
                    overflow: TextOverflow.ellipsis,
                    letterSpacing: 0.5,
                  ),
                ),
                const SizedBox(height: 3),
                SizedBox(
                  width: 280,
                  child: Text(
                    actHis.activity,
                    maxLines: 2,
                    style: const TextStyle(
                      fontSize: 16,
                      color: ColorCodes.textColorPrimary,
                      fontWeight: FontWeight.w500,
                      overflow: TextOverflow.ellipsis,
                      letterSpacing: 0.5,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Decoration? checkActive(int indexActive) {
    Decoration? decoration;
    if (indexActive == currentIndex) {
      decoration = const BoxDecoration(
          border: Border(
        bottom: BorderSide(
            width: 5, color: ColorCodes.pinkColor, style: BorderStyle.solid),
      ));
    }
    return decoration;
  }
}
