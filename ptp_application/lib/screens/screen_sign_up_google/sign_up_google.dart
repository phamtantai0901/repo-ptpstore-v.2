import 'package:flutter/material.dart';
import 'package:ptp_application/custom.dart';
import 'components/design_container_signup.dart';
import 'components/design_header_signup.dart';

class SignUpGoogleScreen extends StatelessWidget {
  const SignUpGoogleScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          body: Stack(
            children: [
              const DesignHeaderSignup(),
              Positioned(
                bottom: -200,
                right: -200,
                child: Container(
                  padding: const EdgeInsets.all(20),
                  height: 400,
                  width: 400,
                  decoration: BoxDecoration(
                    color: ColorCodes.colorPrimary,
                    borderRadius: BorderRadius.circular(320),
                  ),
                ),
              ),
              //Container Signup
              DesignContainerSignup(),
              //Add submit button
            ],
          ),
        ));
  }
}
