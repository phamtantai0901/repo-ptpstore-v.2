import 'package:flutter/material.dart';
import 'package:ptp_application/custom.dart';

// ignore: use_key_in_widget_constructors
class DesignHaftCircleLogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: -200,
      right: -200,
      child: Container(
        padding: const EdgeInsets.all(20),
        height: 400,
        width: 400,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: <Color>[
              ColorCodes.colorPrimary,
              const Color(0xFF6DFFCD).withOpacity(0.9),
              const Color(0xFF6DCFCF),
            ],
          ),
          borderRadius: BorderRadius.circular(320),
        ),
      ),
    );
  }
}
