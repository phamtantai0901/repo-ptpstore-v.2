import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/main.dart';
import 'package:ptp_application/providers/google_provider.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/screens/screen_forgot_password/forgot_password.dart';
import 'package:ptp_application/screens/screen_sign_up_google/sign_up_google.dart';
import 'package:ptp_application/screens/screen_signup/signup.dart';
import 'package:ptp_application/screens/show_dialog.dart';

class DesignBodyLogin extends StatefulWidget {
  const DesignBodyLogin({Key? key}) : super(key: key);

  @override
  _DesignBodyLoginState createState() => _DesignBodyLoginState();
}

class _DesignBodyLoginState extends State<DesignBodyLogin> {
  bool _passwordVisible = false;

  late TextEditingController username;
  late TextEditingController password;

  @override
  void initState() {
    super.initState();
    _passwordVisible = false;
    username = TextEditingController();
    password = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        buildTextField(),
        buildButtonLogin(context),
        buildButtonForgotPassword(context),
        buildBottom(context),
      ],
    );
  }

  //Design TextField Password
  Widget buildTextFieldPassword(
      IconData icon, String hintText, String labelText, bool isChecked) {
    return Container(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: TextFormField(
        controller: password,
        cursorColor: ColorCodes.colorPrimary,
        keyboardType: isChecked ? TextInputType.text : TextInputType.number,
        obscureText: !_passwordVisible,
        decoration: InputDecoration(
          prefixIcon: Icon(
            icon,
            color: ColorCodes.colorPrimary,
          ),
          suffixIcon: IconButton(
            icon: Icon(
              _passwordVisible ? Icons.visibility : Icons.visibility_off,
              color: ColorCodes.colorPrimary,
            ),
            onPressed: () {
              setState(() {
                _passwordVisible = !_passwordVisible;
              });
            },
          ),
          enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: ColorCodes.textColorSecondary),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: ColorCodes.colorPrimary),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          contentPadding: const EdgeInsets.all(10),
          labelText: labelText,
          labelStyle: const TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w500,
          ),
          hintText: hintText,
          hintStyle: const TextStyle(
            fontWeight: FontWeight.w500,
            letterSpacing: 0.5,
            fontSize: 15,
            color: ColorCodes.textColorSecondary,
          ),
        ),
      ),
    );
  }

  //Design TextField
  Widget buildTextField() {
    return Container(
        margin: const EdgeInsets.only(top: 10),
        child: Column(children: [
          buildTextFieldPhoneNumber(
              MdiIcons.phone, "Nhập số điện thoại", "Số điện thoại", true),
          const SizedBox(
            height: 8,
          ),
          buildTextFieldPassword(
              MdiIcons.lock, "Nhập mật khẩu", "Mật khẩu", true),
          Row(children: const [
            SizedBox(
              height: 8,
            ),
          ]),
        ]));
  }

  //Design TextField PhoneNumber
  Widget buildTextFieldPhoneNumber(
      IconData icon, String hintText, String labelText, bool isChecked) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: TextField(
        controller: username,
        cursorColor: ColorCodes.colorPrimary,
        keyboardType: isChecked ? TextInputType.number : TextInputType.text,
        decoration: InputDecoration(
          prefixIcon: Icon(
            icon,
            color: ColorCodes.colorPrimary,
          ),
          enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: ColorCodes.textColorSecondary),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: ColorCodes.colorPrimary),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          contentPadding: const EdgeInsets.all(10),
          labelText: labelText,
          labelStyle: const TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w500,
          ),
          hintText: hintText,
          hintStyle: const TextStyle(
            fontWeight: FontWeight.w500,
            letterSpacing: 0.5,
            fontSize: 15,
            color: ColorCodes.textColorSecondary,
          ),
        ),
      ),
    );
  }

  //Design Button Login
  Widget buildButtonLogin(context) {
    return Container(
      width: double.maxFinite,
      decoration: BoxDecoration(
        gradient: const LinearGradient(
          colors: <Color>[
            Color(0xFF6DFFCD),
            Color(0xFF6DCFCF),
            ColorCodes.colorPrimary,
          ],
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      child: TextButton(
        onPressed: () async {
          showDialogLoading(context);
          await Provider.of<MemberProvider>(context, listen: false)
              .authentication(username.text, password.text, true)
              .then((member) async {
            if (member == null) {
              dismissDialog(context);
              showMessageDialog(
                  context, "Tài khoản hoặc mật khẩu không hợp lệ", Icons.error);
              Future.delayed(
                  const Duration(seconds: 2), () => dismissDialog(context));
            } else {
              if (member.status == 0) {
                dismissDialog(context);
                showMessageDialog(context, "Tài khoản đã bị khoá", Icons.error);
                Future.delayed(
                    const Duration(seconds: 2), () => dismissDialog(context));
                return;
              }
              dismissDialog(context);
              print(await FirebaseMessaging.instance.getToken());
              String? tokenDevice = await FirebaseMessaging.instance.getToken();
              Provider.of<MemberProvider>(context, listen: false)
                  .addOrUpdateTokenDevice(tokenDevice!)
                  .then(
                    (value) => {
                      if (value)
                        {
                          showDialogLoadingSuccessfully(
                              context, "Đăng nhập thành công"),
                          Future.delayed(
                            const Duration(seconds: 1),
                            () => Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                builder: (ctx) => BottomNavAppBar(
                                  currentIndex: 0,
                                ),
                              ),
                              (route) => false,
                            ),
                          ),
                        }
                      else
                        {
                          showMessageDialog(
                              context, "Cannot Connect Device", Icons.error)
                        }
                    },
                  );
            }
          }).catchError((e) {
            dismissDialog(context);
            showMessageDialog(context, e.toString(), Icons.error);
            Future.delayed(
                const Duration(seconds: 2), () => dismissDialog(context));
          });
        },
        child: const Text(
          'ĐĂNG NHẬP',
          style: TextStyle(
            letterSpacing: 1,
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
    );
  }
}

//Design Button Forgot Password
Widget buildButtonForgotPassword(context) {
  return Container(
    padding: const EdgeInsets.only(top: 5),
    child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
      TextButton(
        style: ButtonStyle(
          foregroundColor:
              MaterialStateProperty.all<Color>(ColorCodes.iconColor),
        ),
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => const ForgotPasswordScreen()));
        },
        child: const Text(
          'Quên mật khẩu?',
          style: TextStyle(
              fontSize: 16,
              color: ColorCodes.colorPrimary,
              fontWeight: FontWeight.w500),
        ),
      ),
    ]),
  );
}

//Design Title Suggestion
Widget buildTitleSuggestion() {
  return const Text(
    "hoặc đăng nhập bằng tài khoản",
    style: TextStyle(
      fontSize: 14,
      color: ColorCodes.textColorSecondary,
      fontWeight: FontWeight.w500,
    ),
  );
}

Widget buildButtonLoginWithGoogle(context, IconData icon, Color color) {
  return Container(
    alignment: Alignment.center,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(5),
    ),
    child: TextButton(
      onPressed: () async {
        await Provider.of<GoogleProvider>(context, listen: false)
            .signInWithGoogle()
            .then((value) {
          showDialogLoading(context);
          if (value == null) {
            dismissDialog(context);
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const SignUpGoogleScreen()));
          } else {
            Provider.of<MemberProvider>(context, listen: false).member = value;
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => BottomNavAppBar(currentIndex: 0),
                ),
                (route) => false);
          }
        }).catchError((error) {
          showMessageDialog(context, error.toString(), Icons.error);
          Future.delayed(
              const Duration(seconds: 2), () => dismissDialog(context));
        });
      },
      style: TextButton.styleFrom(
          side: const BorderSide(width: 0.05, color: ColorCodes.colorPrimary),
          minimumSize: const Size(45, 45),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          primary: Colors.white,
          backgroundColor: Colors.white),
      child: Icon(
        icon,
        color: color,
        size: 30,
      ),
    ),
  );
}

//Design Text Button + Bottom
Widget buildTextButtonBottom(context) {
  return Container(
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text(
          "Không có tài khoản?",
          style: TextStyle(
            fontSize: 14,
            color: ColorCodes.textColorSecondary,
            fontWeight: FontWeight.w500,
          ),
        ),
        TextButton(
          style: ButtonStyle(
            foregroundColor:
                MaterialStateProperty.all<Color>(ColorCodes.iconColor),
          ),
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => const SignUpScreen()));
          },
          child: const Text(
            "Đăng ký",
            style: TextStyle(
              fontSize: 14,
              color: ColorCodes.colorPrimary,
              fontWeight: FontWeight.w500,
              decoration: TextDecoration.underline,
            ),
          ),
        ),
      ],
    ),
  );
}

//Design Bottom
Widget buildBottom(context) {
  return Container(
    padding: const EdgeInsets.fromLTRB(20, 10, 20, 0),
    child: Column(children: [
      buildTitleSuggestion(),
      const SizedBox(height: 7),
      const Icon(
        MdiIcons.arrowDownDropCircle,
        color: ColorCodes.colorPrimary,
      ),
      const SizedBox(height: 7),
      // ignore: avoid_unnecessary_containers
      buildButtonLoginWithGoogle(
          context, MdiIcons.googlePlus, ColorCodes.googleColor),
      const SizedBox(height: 7),
      buildTextButtonBottom(context),
    ]),
  );
}
