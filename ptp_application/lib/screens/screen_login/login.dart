import 'package:flutter/material.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/screens/screen_login/components/design_body_login.dart';
import 'package:ptp_application/screens/screen_login/components/design_haft_circle_login.dart';
import 'package:ptp_application/screens/screen_login/components/design_header_login.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          body: Stack(
            children: [
              DesignHeaderLogin(),
              DesignHaftCircleLogin(),
              //Container Signup
              Positioned(
                top: 220,
                child: Container(
                  padding: const EdgeInsets.all(20),
                  height: 470,
                  width: MediaQuery.of(context).size.width - 50,
                  margin: const EdgeInsets.symmetric(horizontal: 25),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5),
                    boxShadow: [
                      BoxShadow(
                        color: ColorCodes.colorPrimary.withOpacity(0.1),
                        blurRadius: 10,
                        spreadRadius: 1,
                      ),
                    ],
                  ),
                  child: const DesignBodyLogin(),
                ),
              ),
              //Add submit button
            ],
          ),
        ));
  }
}
