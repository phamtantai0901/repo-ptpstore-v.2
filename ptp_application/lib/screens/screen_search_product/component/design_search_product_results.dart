import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/config.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/providers/product_provider.dart';
import 'package:ptp_application/screens/show_dialog.dart';
import 'package:ptp_application/models/product.dart';
import 'package:ptp_application/screens/screen_product_detail/product_detail.dart';

class DesignSearchProductResults extends StatefulWidget {
  final String? keyword;
  const DesignSearchProductResults({required this.keyword, Key? key})
      : super(key: key);

  @override
  _DesignSearchProductResultsState createState() =>
      _DesignSearchProductResultsState();
}

class _DesignSearchProductResultsState
    extends State<DesignSearchProductResults> {
  late List<Product> productsTmp;
  late List<Product> productsSave;
  late ProductProvider productProvider;
  String? dropdownValue;
  List<String> listItem = [
    'Tất cả',
    'Dưới 500k',
    '500K đến 1 triệu',
    '1 triệu đến 2 triệu',
    'Trên 2 triệu'
  ];
  String? userId;
  @override
  void initState() {
    super.initState();
    productsSave = [];
    userId =
        Provider.of<MemberProvider>(context, listen: false).member.user.userId;
    productProvider = Provider.of<ProductProvider>(context, listen: false);
    productProvider.getAllStockDetailBySearchKeyword(widget.keyword!, userId!);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width - 10,
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5),
        boxShadow: [
          BoxShadow(
            color: ColorCodes.colorPrimary.withOpacity(0.2),
            spreadRadius: 1,
            blurRadius: 2,
          ),
        ],
      ),
      child: Column(
        children: [
          Expanded(
            flex: 1,
            child: buildItemDropdown(),
          ),
          const SizedBox(height: 10),
          Expanded(
            flex: 10,
            child: Consumer<ProductProvider>(
                builder: (context, productProvider, child) {
              // stockDetailProvider
              //     .getAllStockDetailBySearchKeyword(widget.keyword!);
              if (productsSave.length < 1) {
                productsSave = productProvider.productsSearch;
              }
              productsTmp = productProvider.productsSearch;

              return productsTmp.isEmpty
                  ? Container(
                      alignment: Alignment.center,
                      child: Column(
                        children: const [
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            "Xin lỗi không tìm thấy...",
                            style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.w500,
                                color: Colors.black,
                                letterSpacing: 0.5),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          // CircularProgressIndicator(),
                        ],
                      ),
                    )
                  : GridView.builder(
                      // ignore: prefer_const_constructors
                      physics: ScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: productsTmp.length,
                      itemBuilder: (context, index) {
                        return buildProduct(productsTmp[index]);
                      },
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        mainAxisSpacing: 10,
                        mainAxisExtent: 220,
                      ),
                    );
            }),
          ),
        ],
      ),
    );
  }

  //Design show items in list dropdown -> use in buildDropdownSearch()
  Widget buildItemDropdown() {
    return Container(
        alignment: Alignment.center,
        height: 50,
        color: ColorCodes.colorPrimary.withOpacity(0.1),
        padding: const EdgeInsets.only(left: 10),
        child: DropdownButtonHideUnderline(
          child: DropdownButton<String>(
            style: const TextStyle(
              color: Colors.grey,
              fontSize: 16,
            ),
            isExpanded: true,
            iconSize: 25,
            icon: const Icon(
              Icons.filter_alt_outlined,
              color: ColorCodes.colorPrimary,
            ),
            value: dropdownValue,
            onChanged: (newValue) => setState(() {
              dropdownValue = newValue;
              productProvider.productsSearch = productProvider.sortPriceProduct(
                  productsSave, newValue, true);
            }),
            hint: const Text(
              'Lọc Theo Giá',
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 16,
                letterSpacing: 0.5,
              ),
            ),
            items: listItem
                .map((String item) => DropdownMenuItem<String>(
                      value: item,
                      child: Text(
                        item,
                        style: const TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                          letterSpacing: 0.5,
                        ),
                      ),
                    ))
                .toList(),
          ),
        ));
  }

  //Desgin product -> use builListProduct()
  Widget buildProduct(Product product) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ProductDetailScreen(
              productId: product.productId,
            ),
          ),
        );
      },
      child: Container(
        margin: const EdgeInsets.only(top: 2, left: 2, right: 2, bottom: 2),
        padding: const EdgeInsets.all(5),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Colors.white,
          boxShadow: const [
            BoxShadow(
              color: Color(0xFFF1F1FE),
              offset: Offset(0.0, 1.0),
              spreadRadius: 2,
              blurRadius: 10,
            ),
          ],
        ),
        child: Column(
          children: [
            Expanded(
              flex: 3,
              child: ClipRRect(
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(5),
                    topRight: Radius.circular(5),
                  ),
                  child: SizedBox(
                    height: 200,
                    child: CachedNetworkImage(
                      imageUrl: Config.getIP(
                          '/products/${product.productId}/${product.productImg}'),
                      fit: BoxFit.cover,
                      placeholder: (context, url) => const Center(
                        child: CircularProgressIndicator(),
                      ),
                      errorWidget: (context, url, error) =>
                          const Icon(Icons.error),
                    ),
                  )),
            ),
            Expanded(
              flex: 0,
              child: Container(
                padding: const EdgeInsets.only(left: 4, right: 4),
                child: Text(
                  product.productName,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                    color: ColorCodes.textColorPrimary,
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 0,
              child: Text(
                '${formatNumber(product.pricePay)} đ',
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: const TextStyle(
                    color: Color(0xFFF65E5E),
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                    letterSpacing: 0.5),
              ),
            ),
            Expanded(
              flex: 0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  buildRateProduct(product.quantityRate!, product.avgStar!),
                  buildSalesProduct(product.quantityPay!),
                ],
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            Expanded(
              flex: 0,
              child: Container(
                margin: const EdgeInsets.only(left: 80),
                alignment: Alignment.center,
                height: 40,
                width: double.maxFinite,
                decoration: BoxDecoration(
                  color: const Color(0xFFF65E5E),
                  borderRadius: const BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    topRight: Radius.circular(10),
                    topLeft: Radius.circular(4),
                  ),
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: [
                        Colors.green.withOpacity(0.7),
                        Colors.blue.withOpacity(0.8)
                      ]),
                ),
                child: const Text(
                  'Mua Ngay',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                    letterSpacing: 0.5,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  //Desgin sales product -> use buildProduct()
  Widget buildSalesProduct(int sales) {
    return Container(
      padding: const EdgeInsets.only(right: 4),
      child: Text(
        '$sales đã bán',
        style: const TextStyle(
          color: Colors.grey,
          fontWeight: FontWeight.w500,
          fontSize: 14,
        ),
      ),
    );
  }

  //Desgin rate product -> use buildProduct()
  Widget buildRateProduct(int rate, double avgStar) {
    return Row(
      children: [
        const SizedBox(width: 4),
        const Icon(
          Icons.star_outlined,
          size: 14,
          color: Colors.yellow,
        ),
        Text(
          avgStar.toStringAsFixed(1),
          style: const TextStyle(
            color: Colors.grey,
            fontSize: 14,
            letterSpacing: 1,
          ),
        ),
        Text(
          '($rate)',
          style: const TextStyle(
            color: Colors.grey,
            fontSize: 14,
            letterSpacing: 1,
          ),
        ),
      ],
    );
  }
}
