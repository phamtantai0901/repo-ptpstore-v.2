import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/models/cart.dart';
import 'package:ptp_application/providers/cart_provider.dart';
import 'package:ptp_application/screens/screen_cart/cart.dart';
import 'package:ptp_application/screens/screen_search_product/search_product.dart';
import 'package:ptp_application/screens/show_dialog.dart';

class DesignAppbarSearchProduct extends StatefulWidget
    implements PreferredSizeWidget {
  final String? keyword;
  const DesignAppbarSearchProduct({required this.keyword, Key? key})
      : super(key: key);

  @override
  _DesignAppbarSearchProductState createState() =>
      _DesignAppbarSearchProductState();

  @override
  Size get preferredSize => const Size.fromHeight(heighAppBarMd);
}

class _DesignAppbarSearchProductState extends State<DesignAppbarSearchProduct> {
  late TextEditingController keyContronller;
  @override
  Widget build(BuildContext context) {
    List<Cart> carts = Provider.of<CartProvider>(context, listen: true).carts;
    keyContronller = TextEditingController();
    keyContronller.text = widget.keyword!;
    return AppBar(
        // automaticallyImplyLeading: false,
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(colors: <Color>[
              ColorCodes.colorPrimary,
              const Color(0xFF43cea2).withOpacity(0.5),
              ColorCodes.colorPrimary,
            ], begin: Alignment.centerLeft, end: Alignment.centerRight),
          ),
        ),
        backgroundColor: ColorCodes.colorPrimary,
        elevation: 3.0,
        title: Container(
          padding: const EdgeInsets.only(top: 6),
          child: SizedBox(
            height: 50,
            width: double.maxFinite,
            child: TextField(
              controller: keyContronller,
              autofocus: false,
              cursorColor: ColorCodes.colorPrimary,
              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.white,
                prefixIcon: IconButton(
                  onPressed: () {
                    if (keyContronller.text == "") {
                      showMessageDialog(
                          context, "Vui lòng không bỏ trống", Icons.error);
                      Future.delayed(const Duration(seconds: 2),
                          () => dismissDialog(context));
                    } else {
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => SearchProductScreen(
                                  keyword: keyContronller.text)));
                    }
                  },
                  icon: const Icon(
                    MdiIcons.searchWeb,
                    color: ColorCodes.iconColor,
                    size: 30,
                  ),
                ),
                enabledBorder: const OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.all(
                    Radius.circular(5.0),
                  ),
                ),
                focusedBorder: const OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.all(
                    Radius.circular(5.0),
                  ),
                ),
                contentPadding: const EdgeInsets.all(10),
                labelStyle: const TextStyle(fontSize: 15),
                hintText: "Sản phẩm, thương hiệu và ...",
                hintStyle: const TextStyle(
                  fontWeight: FontWeight.w500,
                  letterSpacing: 0.5,
                  fontSize: 15,
                  color: ColorCodes.textColorSecondary,
                ),
              ),
            ),
          ),
        ),
        actions: [
          GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  // ignore: prefer_const_constructors
                  MaterialPageRoute(builder: (context) => CartScreen()));
            },
            child: SizedBox(
              width: 80,
              child: Stack(
                alignment: AlignmentDirectional.center,
                children: [
                  const Icon(Icons.shopping_cart_outlined,
                      color: Colors.white, size: 30),
                  // ignore: prefer_is_empty
                  carts.length == 0
                      ? Container()
                      : Positioned(
                          bottom: 18,
                          right: 15,
                          child: Container(
                            alignment: Alignment.center,
                            width: 25,
                            height: 25,
                            decoration: BoxDecoration(
                              borderRadius: const BorderRadius.all(
                                Radius.circular(50),
                              ),
                              color: Colors.red[400],
                            ),
                            child: Text(
                              carts.length >= 99 ? '99+' : '${carts.length}',
                              style: const TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                        ),
                ],
              ),
            ),
          ),
        ]);
  }
}
