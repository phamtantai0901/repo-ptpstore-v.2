import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/providers/product_provider.dart';
import 'package:ptp_application/providers/stock_detail_provider.dart';

class DesignHeaderSearchProduct extends StatefulWidget {
  final String? keyword;
  const DesignHeaderSearchProduct({required this.keyword, Key? key})
      : super(key: key);

  @override
  _DesignHeaderSearchProductState createState() =>
      _DesignHeaderSearchProductState();
}

class _DesignHeaderSearchProductState extends State<DesignHeaderSearchProduct> {
  int isActive = 0;
  String? userId;
  late ProductProvider productProvider;
  @override
  void initState() {
    super.initState();
    userId =
        Provider.of<MemberProvider>(context, listen: false).member.user.userId;
    productProvider = Provider.of<ProductProvider>(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      scrollDirection: Axis.horizontal,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            const SizedBox(
              width: 5,
            ),
            buildSearchResults("Liên Quan", 0),
            const SizedBox(
              width: 5,
            ),
            buildSearchResults("Mới Nhất", 1),
            const SizedBox(
              width: 5,
            ),
            buildSearchResults("Nổi Bậc", 2),
            const SizedBox(
              width: 5,
            ),
            buildSearchResults("Bán Chạy", 3),
            const SizedBox(
              width: 5,
            ),
          ],
        )
      ],
    );
  }

  //Design Button Related Results
  Widget buildSearchResults(String title, int isCheckCurrentIndex) {
    Color colorBorder = Colors.white;
    Color colorText = Colors.black.withOpacity(0.6);
    if (isActive == isCheckCurrentIndex) {
      colorBorder = ColorCodes.colorPrimary;
      colorText = ColorCodes.colorPrimary;
    }
    return Container(
      height: 45,
      width: 150,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5),
          border: Border.all(color: colorBorder),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.3),
              blurRadius: 10,
              spreadRadius: 1,
            )
          ]),
      child: TextButton(
        onPressed: () {
          setState(() {
            isActive = isCheckCurrentIndex;
            if (isCheckCurrentIndex == 0) {
              productProvider.getAllStockDetailBySearchKeyword(
                  widget.keyword!, userId!);
            } else if (isCheckCurrentIndex == 1) {
              productProvider
                  .getAllNewStockDetailBySearchKeyword(widget.keyword!);
            } else if (isCheckCurrentIndex == 2) {
              productProvider
                  .getAllStockDetailStandoutBySearchKeyword(widget.keyword!);
            } else {
              productProvider
                  .getAllStockDetailBestSellerBySearchKeyword(widget.keyword!);
            }
          });
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              title,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 16,
                letterSpacing: 0.5,
                color: colorText,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
