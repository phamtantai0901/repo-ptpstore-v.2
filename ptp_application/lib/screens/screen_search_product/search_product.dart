import 'package:flutter/material.dart';
import 'package:ptp_application/screens/screen_search_product/component/design_appbar_search_product.dart';
import 'package:ptp_application/screens/screen_search_product/component/design_header_search_product.dart';
import 'package:ptp_application/screens/screen_search_product/component/design_search_product_results.dart';

class SearchProductScreen extends StatefulWidget {
  final String? keyword;
  const SearchProductScreen({required this.keyword, Key? key})
      : super(key: key);

  @override
  State<SearchProductScreen> createState() => _SearchProductScreenState();
}

class _SearchProductScreenState extends State<SearchProductScreen>
    with AutomaticKeepAliveClientMixin<SearchProductScreen> {
  @override
  bool get wantKeepAlive => true;
  @override
  Widget build(BuildContext context) {
    //Notice the super-call
    super.build(context);
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: DesignAppbarSearchProduct(
            keyword: widget.keyword,
          ), //CUSTOM DESIGN
          body: Container(
            color: const Color(0xFFFFFEFA).withOpacity(0.5),
            child: Column(
              children: [
                Expanded(
                  flex: 1,
                  child: DesignHeaderSearchProduct(
                    keyword: widget.keyword,
                  ), //CUSTOM DESIGN
                ),
                // SizedBox(height: 5),
                Expanded(
                  flex: 5,
                  child: DesignSearchProductResults(
                    keyword: widget.keyword,
                  ), //CUSTOM DESIGN
                )
              ],
            ),
          ),
        ));
  }
}
