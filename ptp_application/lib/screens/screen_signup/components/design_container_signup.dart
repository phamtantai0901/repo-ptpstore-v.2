import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/screens/screen_login/login.dart';
import 'package:ptp_application/screens/show_dialog.dart';
import 'package:ptp_application/custom.dart';
import 'design_defferent_option_signup.dart';

// ignore: use_key_in_widget_constructors
class DesignContainerSignup extends StatefulWidget {
  @override
  _DesignContainerSignup createState() => _DesignContainerSignup();
}

class _DesignContainerSignup extends State<DesignContainerSignup> {
  bool _passwordVisibleT = false, _passwordVisibleC = false;
  late TextEditingController fullname;
  late TextEditingController username;
  late TextEditingController password;
  late TextEditingController confirmpassword;

  @override
  void initState() {
    super.initState();
    _passwordVisibleT = false;
    _passwordVisibleC = false;
    fullname = TextEditingController();
    username = TextEditingController();
    password = TextEditingController();
    confirmpassword = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 210,
      child: Container(
        padding: const EdgeInsets.all(20),
        height: 500,
        width: MediaQuery.of(context).size.width - 50,
        margin: const EdgeInsets.symmetric(horizontal: 25),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(
              color: ColorCodes.colorPrimary.withOpacity(0.1),
              blurRadius: 10,
              spreadRadius: 1,
            ),
          ],
        ),
        child: Column(
          children: [
            buildTextField(),
            buildButtonSignup(),
            const DesignDefferentOptionSignup(),
          ],
        ),
      ),
    );
  }

  //Design TextField
  Widget buildTextField() {
    return Column(children: [
      buildTextFieldName(MdiIcons.account, "Nhập tên của bạn", "Tên", true),
      const SizedBox(
        height: 5,
      ),
      buildTextFieldPhoneNumber(
          MdiIcons.phone, "Nhập số điện thoại", "Số điện thoại", true),
      const SizedBox(
        height: 5,
      ),
      buildTextFieldPassword(MdiIcons.lock, "Nhập mật khẩu", "Mật khẩu", true),
      const SizedBox(
        height: 5,
      ),
      buildTextFieldConfirmPassword(
          MdiIcons.lockCheck, "Nhập lại mật khẩu", "Xác Nhận Mật khẩu", true),
    ]);
  }

  //Design TextField Name
  Widget buildTextFieldName(
      IconData icon, String hintText, String labelText, bool isCheck) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 5.0),
      child: TextField(
        controller: fullname,
        cursorColor: ColorCodes.colorPrimary,
        keyboardType: isCheck ? TextInputType.text : TextInputType.number,
        decoration: InputDecoration(
          prefixIcon: Icon(
            icon,
            color: ColorCodes.colorPrimary,
          ),
          enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: ColorCodes.textColorSecondary),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: ColorCodes.colorPrimary),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          contentPadding: const EdgeInsets.all(10),
          labelText: labelText,
          labelStyle: const TextStyle(fontSize: 16),
          hintText: hintText,
          hintStyle: const TextStyle(
            fontWeight: FontWeight.w500,
            letterSpacing: 0.5,
            fontSize: 13,
            color: ColorCodes.textColorSecondary,
          ),
        ),
      ),
    );
  }

//Design TextField PhoneNumber
  Widget buildTextFieldPhoneNumber(
      IconData icon, String hintText, String labelText, bool isChecked) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 5),
      child: TextField(
        controller: username,
        cursorColor: ColorCodes.colorPrimary,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          prefixIcon: Icon(
            icon,
            color: ColorCodes.colorPrimary,
          ),
          enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: ColorCodes.textColorSecondary),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: ColorCodes.colorPrimary),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          contentPadding: const EdgeInsets.all(10),
          labelText: labelText,
          labelStyle: const TextStyle(fontSize: 16),
          hintText: hintText,
          hintStyle: const TextStyle(
            fontWeight: FontWeight.w500,
            letterSpacing: 0.5,
            fontSize: 13,
            color: ColorCodes.textColorSecondary,
          ),
        ),
      ),
    );
  }

  //Design TextField Password
  Widget buildTextFieldPassword(
      IconData icon, String hintText, String labelText, bool isChecked) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: TextField(
        controller: password,
        cursorColor: ColorCodes.colorPrimary,
        obscureText: !_passwordVisibleT,
        keyboardType: isChecked ? TextInputType.text : TextInputType.number,
        decoration: InputDecoration(
          prefixIcon: Icon(
            icon,
            color: ColorCodes.colorPrimary,
          ),
          suffixIcon: IconButton(
            icon: Icon(
              _passwordVisibleT ? Icons.visibility : Icons.visibility_off,
              color: ColorCodes.colorPrimary,
            ),
            onPressed: () {
              setState(
                () {
                  _passwordVisibleT = !_passwordVisibleT;
                },
              );
            },
          ),
          enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: ColorCodes.textColorSecondary),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: ColorCodes.colorPrimary),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          contentPadding: const EdgeInsets.all(10),
          labelText: labelText,
          labelStyle: const TextStyle(fontSize: 16),
          hintText: hintText,
          hintStyle: const TextStyle(
            fontWeight: FontWeight.w500,
            letterSpacing: 0.5,
            fontSize: 13,
            color: ColorCodes.textColorSecondary,
          ),
        ),
      ),
    );
  }

//Design TextField Confirm Password
  Widget buildTextFieldConfirmPassword(
      IconData icon, String hintText, String labelText, bool isChecked) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 16.0),
      child: TextField(
        controller: confirmpassword,
        cursorColor: ColorCodes.colorPrimary,
        obscureText: !_passwordVisibleC,
        keyboardType: isChecked ? TextInputType.text : TextInputType.number,
        decoration: InputDecoration(
          prefixIcon: Icon(
            icon,
            color: ColorCodes.colorPrimary,
          ),
          suffixIcon: IconButton(
            icon: Icon(
              _passwordVisibleC ? Icons.visibility : Icons.visibility_off,
              color: ColorCodes.colorPrimary,
            ),
            onPressed: () {
              setState(() {
                _passwordVisibleC = !_passwordVisibleC;
              });
            },
          ),
          enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: ColorCodes.textColorSecondary),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: ColorCodes.colorPrimary),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          contentPadding: const EdgeInsets.all(10),
          labelText: labelText,
          labelStyle: const TextStyle(fontSize: 16),
          hintText: hintText,
          hintStyle: const TextStyle(
            fontWeight: FontWeight.w500,
            letterSpacing: 0.5,
            fontSize: 13,
            color: ColorCodes.textColorSecondary,
          ),
        ),
      ),
    );
  }

  //Design Button Signup
  Widget buildButtonSignup() {
    return Container(
      width: double.maxFinite,
      decoration: BoxDecoration(
        gradient: const LinearGradient(
          colors: [
            Color(0xFF6DFFCD),
            Color(0xFF6DCFCF),
            ColorCodes.colorPrimary,
          ],
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      child: TextButton(
        onPressed: () async {
          showDialogLoading(context);
          if (password.text != confirmpassword.text) {
            dismissDialog(context);
            showMessageDialog(context, "Mật khẩu không khớp", Icons.error);
          } else {
            //Thực hiện 1 hàm cho tới khi kết thúc -> then(khi đó)
            await Provider.of<MemberProvider>(context, listen: false)
                .register(null, fullname.text, username.text, password.text,
                    confirmpassword.text)
                .then((isRegistered) {
              if (isRegistered == false) {
                dismissDialog(context);
                showMessageDialog(
                    context, "Đăng ký không thành công", Icons.error);
                Future.delayed(
                    const Duration(seconds: 2), () => dismissDialog(context));

                fullname.clear();
                username.clear();
                password.clear();
                confirmpassword.clear();
              } else {
                dismissDialog(context);
                showDialogLoadingSuccessfully(context, "Đăng ký thành công");
                Future.delayed(
                  const Duration(seconds: 1),
                  () => Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                      // ignore: prefer_const_constructors
                      builder: (ctx) => LoginScreen(),
                    ),
                    (route) => false,
                  ),
                );
              }
            }).catchError((error) {
              dismissDialog(context);
              showMessageDialog(context, error, Icons.check_circle_outlined);
              Future.delayed(
                  const Duration(seconds: 2), () => dismissDialog(context));
            });
          }
        },
        child: const Text(
          'ĐĂNG KÝ',
          style: TextStyle(
            letterSpacing: 1,
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }
}
