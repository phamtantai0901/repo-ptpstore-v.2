import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import 'package:ptp_application/custom.dart';

class DesignDefferentOptionSignup extends StatelessWidget {
  const DesignDefferentOptionSignup({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        buildButtonHaveAccount(context),
        buildBottom(),
      ],
    );
  }

  //Design Button Have An Account
  Widget buildButtonHaveAccount(context) {
    // ignore: avoid_unnecessary_containers
    return Container(
      child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
        TextButton(
          style: ButtonStyle(
            foregroundColor:
                MaterialStateProperty.all<Color>(ColorCodes.iconColor),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
          child: const Text(
            'Bạn đã có tài khoản?',
            style: TextStyle(
                fontSize: 16,
                color: ColorCodes.colorPrimary,
                fontWeight: FontWeight.w400),
          ),
        ),
      ]),
    );
  }

//Design Bottom Signup
  Widget buildBottom() {
    return Container(
      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
      child: Column(children: [
        const Text(
          "hoặc đăng ký bằng tài khoản",
          style: TextStyle(
            fontSize: 15,
            color: ColorCodes.textColorSecondary,
            fontWeight: FontWeight.w500,
          ),
        ),
        const SizedBox(height: 7),
        const Icon(
          MdiIcons.arrowDownDropCircle,
          color: ColorCodes.colorPrimary,
        ),
        const SizedBox(height: 7),
        // ignore: avoid_unnecessary_containers
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              buildGroupButtonBottom(
                  MdiIcons.googlePlus, ColorCodes.googleColor),
            ],
          ),
        ),
      ]),
    );
  }

//Design Group Button Bottom
  Widget buildGroupButtonBottom(IconData icon, Color color) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        boxShadow: [
          BoxShadow(
            color: ColorCodes.colorPrimary.withOpacity(0.1),
            blurRadius: 50,
            spreadRadius: 20,
          ),
        ],
      ),
      child: Row(children: [
        TextButton(
          onPressed: () {},
          style: TextButton.styleFrom(
              side:
                  const BorderSide(width: 0.05, color: ColorCodes.colorPrimary),
              minimumSize: const Size(55, 55),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              primary: Colors.white,
              backgroundColor: Colors.white),
          child: Row(children: [
            Icon(
              icon,
              color: color,
              size: 30,
            ),
          ]),
        )
      ]),
    );
  }
}
