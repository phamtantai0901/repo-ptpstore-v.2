import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:ptp_application/config.dart';

import 'package:ptp_application/custom.dart';

class DesignHeaderSignup extends StatelessWidget {
  const DesignHeaderSignup({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 0,
      right: 0,
      left: 0,
      child: Container(
        height: 240,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          gradient: LinearGradient(
            colors: <Color>[
              const Color(0xFF6DFFCD).withOpacity(0.6),
              const Color(0xFF6DCFCF),
              ColorCodes.colorPrimary,
              const Color(0xFF6DCFCF),
            ],
          ),
        ),
        child: Container(
          margin: const EdgeInsets.only(top: 20),
          color: const Color(0xFFFFFFFF).withOpacity(0.1),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
            Stack(
              children: [
                Container(
                  margin: const EdgeInsets.only(
                    right: 140,
                  ),
                  child: CachedNetworkImage(
                    imageUrl: Config.getIP("/logo/LogoF.png"),
                    fit: BoxFit.cover,
                    width: 250,
                    height: 150,
                    placeholder: (context, url) => const Center(
                      child: CircularProgressIndicator(),
                    ),
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.error),
                  ),
                ),
                Positioned(
                  left: 180,
                  top: 50,
                  child: SizedBox(
                    width: 180,
                    height: 150,
                    child: CachedNetworkImage(
                      imageUrl: Config.getIP("/logo/LogoE.png"),
                      fit: BoxFit.fill,
                      placeholder: (context, url) => const Center(
                        child: CircularProgressIndicator(),
                      ),
                      errorWidget: (context, url, error) =>
                          const Icon(Icons.error),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 5),
            const Text("Mua sắm an toàn-chất lượng",
                style: TextStyle(
                  fontSize: 19,
                  letterSpacing: 0.5,
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                ))
          ]),
        ),
      ),
    );
  }
}
