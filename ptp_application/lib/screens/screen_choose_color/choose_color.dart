import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/models/size.dart' as size_model;
import 'package:ptp_application/models/stock_detail.dart';
import 'package:ptp_application/providers/color_provider.dart';
import 'package:ptp_application/providers/size_provider.dart';
import 'package:ptp_application/providers/stock_detail_provider.dart';
import 'package:ptp_application/screens/screen_choose_color/components/design_appbar_choose_color.dart';
import 'package:ptp_application/screens/screen_choose_color/components/design_box_product.dart';
import 'package:ptp_application/screens/screen_choose_color/components/design_choose_color.dart';
import 'package:ptp_application/screens/screen_choose_color/components/design_choose_size.dart';

// ignore: use_key_in_widget_constructors
class ChooseColorScreen extends StatefulWidget {
  @override
  _ChooseColorScreenState createState() => _ChooseColorScreenState();
}

class _ChooseColorScreenState extends State<ChooseColorScreen> {
  // ignore: unused_field
  String? _dropDownValue;
  bool activeProduct = true;
  bool activeSize = true;
  @override
  void initState() {
    super.initState();
    StockDetail stockDetail =
        Provider.of<StockDetailProvider>(context, listen: false).stockDetail!;
    Provider.of<ColorProvider>(context, listen: false)
        .getAllColorByProductIdAndStatusInStock(
            stockDetail.productDetail.product.productId, 1);
  }

  @override
  Widget build(BuildContext context) {
    StockDetail stockDetailCurrent =
        Provider.of<StockDetailProvider>(context, listen: true).stockDetail!;
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: PreferredSize(
            preferredSize: const Size.fromHeight(60.0),
            child: buildAppBar(context),
          ),
          body: Center(
            child: Container(
              width: MediaQuery.of(context).size.width - 20,
              color: Colors.white,
              child: ListView(
                children: [
                  const SizedBox(height: 10),
                  buildBoxproduct(stockDetailCurrent),
                  const SizedBox(height: 10),
                  buildBoxColor(context,
                      stockDetailCurrent.productDetail.color.colorName),
                  const SizedBox(height: 15),
                  Column(
                    children: [
                      Consumer<ColorProvider>(
                        builder: (context, colorProvider, child) => colorProvider
                                .colors.isEmpty
                            ? Container(
                                alignment: Alignment.center,
                                child: Column(
                                  children: const [
                                    Text(
                                      "Loading...",
                                      style: TextStyle(
                                        fontSize: 16,
                                        letterSpacing: 0.5,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    CircularProgressIndicator(),
                                  ],
                                ),
                              )
                            : GridView.builder(
                                // ignore: prefer_const_constructors
                                physics: ScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: colorProvider.colors.length,
                                gridDelegate:
                                    const SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 2,
                                        mainAxisSpacing: 15,
                                        mainAxisExtent: 70,
                                        crossAxisSpacing: 15),
                                itemBuilder: (context, index) {
                                  return buildBoxItemColor(
                                      context,
                                      colorProvider.colors[index],
                                      stockDetailCurrent);
                                }),
                      ),
                    ],
                  ),
                  const SizedBox(height: 15),
                  buildBoxSize(
                      context, stockDetailCurrent.productDetail.size.sizeName),
                  const SizedBox(height: 15),
                  Column(
                    children: [
                      FutureBuilder<List<size_model.Size>>(
                          future:
                              Provider.of<SizeProvider>(context, listen: false)
                                  .getAllSizeByProductIdAndColorIdInStock(
                                      stockDetailCurrent
                                          .productDetail.product.productId,
                                      stockDetailCurrent
                                          .productDetail.color.colorId),
                          builder: (context, snapshot) {
                            if (snapshot.hasError) {
                              return Center(
                                  child: Text(snapshot.error.toString()));
                            } else if (snapshot.hasData) {
                              List<size_model.Size> sizes = snapshot.data!;
                              return GridView.builder(
                                physics: const ScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: sizes.length,
                                gridDelegate:
                                    const SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 4,
                                        mainAxisSpacing: 10,
                                        mainAxisExtent: 40,
                                        crossAxisSpacing: 15),
                                itemBuilder: (context, index) {
                                  return buildButtonSize(context, sizes[index],
                                      stockDetailCurrent);
                                },
                              );
                            } else if (snapshot.connectionState ==
                                ConnectionState.waiting) {
                              return SizedBox(
                                height: 350,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: const [
                                    Text(
                                      "Loading...",
                                      style: TextStyle(
                                        fontSize: 16,
                                        letterSpacing: 0.5,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    CircularProgressIndicator(),
                                  ],
                                ),
                              );
                            } else {
                              return const Center(
                                  child: Text(
                                "Danh sách trống",
                                style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.black,
                                    letterSpacing: 0.5),
                              ));
                            }
                          }),
                    ],
                  ),
                  const SizedBox(height: 25),
                  buildButtonConfirm(),
                  const SizedBox(height: 25),
                ],
              ),
            ),
          ),
        ));
  }

  //Design Button Buy Now
  Widget buildButtonConfirm() {
    return Container(
        height: 60,
        decoration: BoxDecoration(
          color: const Color(0xFFF65E5E),
          borderRadius: BorderRadius.circular(5),
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                Colors.green.withOpacity(0.8),
                Colors.blue.withOpacity(0.8)
              ]),
        ),
        child: TextButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: const Text(
            'Xác nhận',
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              letterSpacing: 0.5,
              fontWeight: FontWeight.w700,
            ),
          ),
        ));
  }
}
