//Design Box Size
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/models/size.dart' as size_model;
import 'package:ptp_application/models/stock_detail.dart';
import 'package:ptp_application/providers/stock_detail_provider.dart';

//Design Box Size
Widget buildBoxSize(BuildContext context, String sizeName) {
  return Container(
    padding: const EdgeInsets.only(left: 10),
    margin: const EdgeInsets.only(top: 10),
    alignment: Alignment.centerLeft,
    decoration: BoxDecoration(
      gradient: LinearGradient(
        colors: <Color>[
          const Color(0xFF02AAB0).withOpacity(0.5),
          const Color(0xFF74ebd5).withOpacity(0.5),
        ],
      ),
      borderRadius: BorderRadius.circular(5),
    ),
    child: Container(
      height: 40,
      margin: const EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: [
          const Text(
            "Kích thước:",
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
              fontWeight: FontWeight.w600,
            ),
          ),
          const SizedBox(width: 5),
          Text(
            sizeName,
            // stockDetail.productDetail.size.sizeName,
            style: const TextStyle(
              color: Colors.white,
              fontSize: 18,
              fontWeight: FontWeight.w600,
              letterSpacing: 0.3,
            ),
          ),
        ],
      ),
    ),
  );
}

//Design Button Size
Widget buildButtonSize(BuildContext context, size_model.Size size,
    StockDetail stockDetailCurrent) {
  bool isActive = (stockDetailCurrent.productDetail.size.sizeId == size.sizeId)
      ? true
      : false;
  return Container(
    decoration: BoxDecoration(
      color: isActive
          // ignore: prefer_const_constructors
          ? Color(0xffffe599).withOpacity(0.8)
          : ColorCodes.colorPrimary.withOpacity(0.1),
      borderRadius: BorderRadius.circular(5),
    ),
    child: TextButton(
      onPressed: () async {
        await Provider.of<StockDetailProvider>(context, listen: false)
            .updateSize(size);
      },
      child: Text(
        size.sizeName,
        style: const TextStyle(
          letterSpacing: 1,
          color: ColorCodes.textColorSecondary,
          fontSize: 18,
          fontWeight: FontWeight.w600,
        ),
      ),
    ),
  );
}
