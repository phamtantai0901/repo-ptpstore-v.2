import 'package:flutter/material.dart';
import 'package:ptp_application/custom.dart';

AppBar buildAppBar(BuildContext context) {
  return AppBar(
    backgroundColor: Colors.white,
    elevation: 0.5,
    centerTitle: true,
    automaticallyImplyLeading: false,
    leadingWidth: 70,
    title: const Text(
      "LỰA CHỌN THUỘC TÍNH",
      textAlign: TextAlign.center,
      style: TextStyle(
          color: ColorCodes.textColorPrimary,
          fontSize: FontSize.fontSizeAppBar,
          fontWeight: FontWeight.w600),
    ),
  );
}
