//Design Box Size
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/models/color.dart' as color_model;
import 'package:ptp_application/models/stock_detail.dart';
import 'package:ptp_application/providers/stock_detail_provider.dart';

//Design Box Color
Widget buildBoxColor(BuildContext context, String nameColor) {
  // StockDetail stockDetail =
  //     Provider.of<StockDetailProvider>(context, listen: true).stockDetail!;
  return Container(
    padding: const EdgeInsets.only(left: 10),
    margin: const EdgeInsets.only(top: 10),
    alignment: Alignment.centerLeft,
    decoration: BoxDecoration(
      gradient: LinearGradient(
        colors: <Color>[
          const Color(0xFF02AAB0).withOpacity(0.6),
          const Color(0xFF74ebd5).withOpacity(0.6),
        ],
      ),
      borderRadius: BorderRadius.circular(5),
    ),
    child: Container(
      height: 40,
      margin: const EdgeInsets.only(top: 15),
      child: Column(
        children: [
          Row(
            children: [
              const Text(
                "Màu:",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                ),
              ),
              const SizedBox(width: 5),
              Text(
                nameColor,
                // stockDetail.productDetail.color.colorName,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                  letterSpacing: 0.3,
                ),
              ),
            ],
          ),
        ],
      ),
    ),
  );
}

//Design Box Item Color
Widget buildBoxItemColor(BuildContext context, color_model.Color color,
    StockDetail stockDetailCurrent) {
  StockDetail stockDetailCurrent =
      Provider.of<StockDetailProvider>(context, listen: true).stockDetail!;
  bool isActive =
      (stockDetailCurrent.productDetail.color.colorId == color.colorId)
          ? true
          : false;

  return Container(
    decoration: BoxDecoration(
      border: Border.all(
        color: Colors.grey,
        width: 0.2,
      ),
      color: isActive
          // ignore: prefer_const_constructors
          ? Color(0xffffe599).withOpacity(0.8)
          : ColorCodes.colorPrimary.withOpacity(0.05),
      borderRadius: BorderRadius.circular(5),
    ),
    child: TextButton(
      onPressed: () async {
        await Provider.of<StockDetailProvider>(context, listen: false)
            .updateColor(color);
      },
      child: Row(
        children: [
          Container(
            width: 130,
            padding: const EdgeInsets.only(left: 10),
            child: Text(
              color.colorName,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(
                  fontSize: 16,
                  color: ColorCodes.textColorSecondary,
                  fontWeight: FontWeight.w600),
            ),
          ),
        ],
      ),
    ),
  );
}
