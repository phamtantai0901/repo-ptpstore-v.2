//Design Box Product
import 'package:flutter/material.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/models/product_detail.dart';
import 'package:ptp_application/models/stock_detail.dart';
import 'package:ptp_application/screens/show_dialog.dart';

//Design Box Product
Widget buildBoxproduct(StockDetail stockDetail) {
  ProductDetail productDetail = stockDetail.productDetail;
  return Container(
    padding: const EdgeInsets.all(15),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(10.0),
      color: Colors.white,
      boxShadow: [
        BoxShadow(
          color: ColorCodes.colorPrimary.withOpacity(0.1),
          blurRadius: 100,
          spreadRadius: 5,
        ),
      ],
    ),
    child: Row(
      children: [
        // Expanded(
        //   flex: 3,
        //   child: ClipRRect(
        //     borderRadius: const BorderRadius.only(
        //         bottomLeft: Radius.circular(10),
        //         topRight: Radius.circular(5),
        //         topLeft: Radius.circular(5),
        //         bottomRight: Radius.circular(10),),
        //     child: Image.network(
        //       "http://10.0.2.2/upload/products/${productDetail.product.productId}/${productDetail.productDetailId}.jpg",
        //       width: 105,
        //       height: 90,
        //       fit: BoxFit.cover,
        //     ),
        //   ),
        // ),
        const SizedBox(width: 15),
        Expanded(
          flex: 7,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                productDetail.product.productName,
                style: const TextStyle(
                  color: ColorCodes.textColorPrimary,
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                  letterSpacing: 0.5,
                ),
              ),
              const SizedBox(height: 5),
              Row(
                children: [
                  const Text(
                    "Màu, Kích thước: ",
                    style: TextStyle(
                      color: ColorCodes.textColorSecondary,
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  Text(
                    stockDetail.productDetail.color.colorName +
                        "/" +
                        stockDetail.productDetail.size.sizeName,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(
                      color: ColorCodes.textColorSecondary,
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  )
                ],
              ),
              const SizedBox(height: 5),
              Row(
                children: [
                  Text(
                    formatNumber(stockDetail.pricePay),
                    style: const TextStyle(
                      color: ColorCodes.pinkColor,
                      fontSize: 22,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  const SizedBox(width: 3),
                  const Text(
                    "đ",
                    style: TextStyle(
                      fontSize: 22,
                      color: ColorCodes.pinkColor,
                      fontWeight: FontWeight.w700,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ],
              ),
            ],
          ),
        )
      ],
    ),
  );
}
