import 'package:flutter/material.dart';
import 'package:ptp_application/custom.dart';

class DesignAppbarPaySuccessfully extends StatelessWidget
    implements PreferredSizeWidget {
  const DesignAppbarPaySuccessfully({Key? key}) : super(key: key);

  @override
  Size get preferredSize => const Size.fromHeight(heighAppBarMd);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 0.3,
      // leading: iconButtons(Icons.arrow_back, Colors.black, 25),
      centerTitle: true,
      title: Container(
        width: 150,
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            icons(
                Icons.add_shopping_cart_outlined, ColorCodes.colorPrimary, 22),
            const Text(
              '-',
              style: TextStyle(
                color: ColorCodes.colorPrimary,
              ),
            ),
            icons(Icons.where_to_vote_outlined, ColorCodes.colorPrimary, 22),
            const Text(
              '-',
              style: TextStyle(
                color: ColorCodes.colorPrimary,
              ),
            ),
            icons(Icons.verified_outlined, ColorCodes.colorPrimary, 22),
          ],
        ),
      ),
      actions: [
        iconButtons(Icons.shopping_cart_outlined, Colors.black, 25),
      ],
    );
  }

//Design Icon
  Widget icons(IconData icon, Color color, double sizeIcon) {
    return Icon(
      icon,
      color: color,
      size: sizeIcon,
    );
  }

//Design icon button
  Widget iconButtons(IconData icon, Color color, double sizeIcon) {
    return IconButton(
        onPressed: () {},
        icon: Icon(
          icon,
          color: color,
          size: sizeIcon,
        ));
  }
}
