import 'package:flutter/material.dart';
import 'package:ptp_application/main.dart';
import 'package:ptp_application/screens/screen_delivery/delivery.dart';
import 'package:ptp_application/custom.dart';

class DesignBodyPaySuccessfully extends StatelessWidget {
  // ignore: prefer_const_constructors_in_immutables
  DesignBodyPaySuccessfully({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Color(0xFFFFFFFF),
      ),
      padding: const EdgeInsets.only(top: 35),
      child: Column(
        children: [
          Container(
            alignment: Alignment.center,
            child: const Text(
              'THANH TOÁN THÀNH CÔNG',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: ColorCodes.textColorPrimary,
              ),
            ),
          ),
          buildTextBody(),
          buildThankYouText(),
          buildAssessNowButton(context),
          buildReturnHome(context),
        ],
      ),
    );
  }

  //Widget build text body
  Widget buildTextBody() {
    return Container(
      padding: const EdgeInsets.fromLTRB(40, 20, 30, 0),
      alignment: Alignment.center,
      child: Column(
        children: const [
          Text(
            'Vị khách hàng dễ thương kia ơi. Đơn hàng của bạn đã được thanh toán thành công. Cảm ơn bạn đã sử dụng sản phẩm đến từ PTP Store.',
            style: TextStyle(
              fontSize: 17,
              color: ColorCodes.textColorSecondary,
              letterSpacing: 0.2,
              fontWeight: FontWeight.w500,
            ),
          ),
        ],
      ),
    );
  }

  //Widget Build Thank You Text
  Widget buildThankYouText() {
    return Container(
        padding: const EdgeInsets.only(top: 20, bottom: 20),
        alignment: Alignment.center,
        child: Column(
          children: const [
            Text(
              'Xin chân thành cảm ơn!',
              style: TextStyle(
                fontSize: 22,
                color: ColorCodes.textColorPrimary,
              ),
            ),
            Icon(
              Icons.task_alt,
              size: 130,
              color: ColorCodes.colorPrimary,
            )
          ],
        ));
  }

  //Wiget Build Button Assess Now
  Widget buildAssessNowButton(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(25, 10, 25, 10),
      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        gradient: const LinearGradient(
          colors: <Color>[
            Color.fromARGB(255, 107, 214, 177),
            Color(0xFF6DCFCF),
            ColorCodes.colorPrimary,
          ],
        ),
      ),
      child: TextButton(
        onPressed: () {
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => DeliveryScreen(),
              ),
              (route) => false);
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: const [
            Text(
              'Vào danh sách đơn hàng',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.w500),
            ),
            Icon(Icons.arrow_forward_ios_outlined,
                color: Colors.white, size: 25),
          ],
        ),
      ),
    );
  }

  //Widget Build Return Home
  Widget buildReturnHome(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(25, 10, 25, 10),
      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        gradient: const LinearGradient(
          colors: <Color>[
            ColorCodes.colorPrimary,
            Color(0xFF6DCFCF),
            Color.fromARGB(255, 107, 214, 177),
          ],
        ),
      ),
      child: TextButton(
        onPressed: () {
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => BottomNavAppBar(currentIndex: 0),
              ),
              (route) => false);
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: const [
            Text(
              'Trở về trang chủ',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.w500),
            ),
            Icon(Icons.arrow_forward_ios_outlined,
                color: Colors.white, size: 25),
          ],
        ),
      ),
    );
  }
}
