import 'package:flutter/material.dart';
import 'package:ptp_application/screens/screen_pay_successfully/components/design_appbar_pay_successfully.dart';
import 'components/design_body_pay_successfully.dart';

class PaySuccessfullyScreen extends StatelessWidget {
  const PaySuccessfullyScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: const DesignAppbarPaySuccessfully(),
        body: DesignBodyPaySuccessfully(),
      ),
    );
  }
}
