import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/models/bill.dart';
import 'package:ptp_application/models/bill_detail.dart';
import 'package:ptp_application/models/cart.dart';
import 'package:ptp_application/models/member.dart';
import 'package:ptp_application/models/voucher.dart';
import 'package:ptp_application/providers/bill_detail_provider.dart';
import 'package:ptp_application/providers/bill_provider.dart';
import 'package:ptp_application/providers/cart_provider.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/providers/zalo_provider.dart';
import 'package:ptp_application/screens/screen_pay_successfully/pay_successfully.dart';
import 'package:ptp_application/screens/screen_select_address/address.dart';
import 'package:ptp_application/screens/screen_zalo_pay/zalo_pay.dart';
import 'package:ptp_application/screens/show_dialog.dart';

// ignore: must_be_immutable
class DesignBodyAddress2 extends StatefulWidget {
  Voucher? voucher;
  DesignBodyAddress2({this.voucher, Key? key}) : super(key: key);

  @override
  _DesignBodyAddressState createState() => _DesignBodyAddressState();
}

class _DesignBodyAddressState extends State<DesignBodyAddress2> {
  final textFieldController = TextEditingController();
  List itemPayment = [
    {
      'name': 'Thanh toán bằng tiền mặt',
      'value': 0,
    },
    {
      'name': 'Thanh toán bằng ZaloPay',
      'value': 1,
    }
  ];

  late String? dropdownAddress;
  int dropdownPayment = 0;

  late TextEditingController nameReceive;
  late TextEditingController houseNumber;
  late TextEditingController district;
  late TextEditingController ward;
  late TextEditingController shippingPhone;
  late TextEditingController shippingAddress;
  late Member member;

  bool isUpdated = true;
  bool isUpdateSelectPayment = true;

  @override
  void initState() {
    super.initState();
    member = Provider.of<MemberProvider>(context, listen: false).member;
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        buildTextNotification(),
        const SizedBox(height: 5),
        buildBorderAddress("Trần Minh Phường", "0309030933",
            "567, Đường Trần Duy Hưng, Phường Phùng Hưng Đạo, Chí Thọ, Hoàng Mai."),
        const SizedBox(height: 10),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // buildLabelText('Chọn hình thức thanh toán:'),
            const SizedBox(height: 2),
            buildDropdownPayment(),
          ],
        ),
        const SizedBox(height: 10),
        buildPayBill(),
      ],
    );
  }

  //Design Text Notification
  Widget buildTextNotification() {
    return Container(
        margin: const EdgeInsets.only(top: 5, bottom: 10),
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          gradient: LinearGradient(
            colors: <Color>[
              ColorCodes.colorPrimary.withOpacity(0.3),
              const Color(0xFF6DCFCF).withOpacity(0.1),
            ],
          ),
        ),
        child: const Text(
            "Đơn hàng có thể giao ngoài giờ làm việc hoặc được giao cuối tuần để đơn hàng có thể đến tay bạn nhanh nhất, nếu được hay chọn địa điểm giao hàng tránh vùng dịch!",
            style: TextStyle(
              color: ColorCodes.textColorPrimary,
              fontSize: 16,
              letterSpacing: 0.2,
              fontWeight: FontWeight.w600,
            )));
  }

  //Design Border Address
  Widget buildBorderAddress(
      String reveicerName, String phoneNumber, String address) {
    return Container(
      width: double.maxFinite,
      height: 150,
      margin: const EdgeInsets.only(
        left: 15,
        bottom: 10,
        top: 5,
        right: 15,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Colors.white,
        boxShadow: const [
          BoxShadow(
            color: ColorCodes.colorPrimary,
            spreadRadius: 1,
            blurRadius: 1,
          ),
        ],
      ),
      child: TextButton(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => SelectAddressScreen()));
          },
          child: Container(
            padding: const EdgeInsets.all(5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Expanded(
                  flex: 0,
                  child: Icon(
                    Icons.pin_drop,
                    color: ColorCodes.colorPrimary,
                    size: 35,
                  ),
                ),
                const SizedBox(width: 5),
                Expanded(
                  flex: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        reveicerName,
                        maxLines: 1,
                        style: const TextStyle(
                          overflow: TextOverflow.ellipsis,
                          color: ColorCodes.textColorPrimary,
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      Text(
                        phoneNumber,
                        maxLines: 1,
                        style: const TextStyle(
                          overflow: TextOverflow.ellipsis,
                          color: ColorCodes.textColorSecondary,
                          fontSize: 20,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      const SizedBox(height: 5),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            flex: 1,
                            child: Text(
                              address,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 3,
                              style: const TextStyle(
                                color: ColorCodes.textColorPrimary,
                                fontSize: 18,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                          const SizedBox(width: 10),
                          const Icon(
                            Icons.arrow_forward_ios_outlined,
                            color: ColorCodes.iconColor,
                            size: 23,
                          ),
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          )),
    );
  }

  //Design notification text
  Widget buildPayBill() {
    return Container(
      height: 55,
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        gradient: LinearGradient(
          colors: <Color>[
            ColorCodes.colorPrimary,
            const Color(0xFF6DCFCF),
            const Color(0xFF6DFFCD).withOpacity(0.7),
          ],
        ),
      ),
      child: TextButton(
        onPressed: () {
          CartProvider cartProvider =
              Provider.of<CartProvider>(context, listen: false);
          Member member =
              Provider.of<MemberProvider>(context, listen: false).member;
          double totalPrice = cartProvider.totalPrice;
          int totalQuantity = cartProvider.totalQuantity;
          Bill bill = Bill(
            member: member,
            shippingAddress: shippingAddress.text,
            shippingPhone: shippingPhone.text,
            receiver: nameReceive.text,
            totalPrice: totalPrice,
            totalQuantity: totalQuantity,
            voucher: widget.voucher,
            payment: dropdownPayment,
          );
          if (dropdownPayment == 0) {
            methodPay(bill, cartProvider.carts);
          } else {
            _methodZaloPay(bill, cartProvider.carts);
          }
          // await _methodPayCrash();
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: const [
            Text(
              'Thanh Toán',
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
                fontWeight: FontWeight.w600,
              ),
            ),
            Icon(
              Icons.arrow_forward_ios_outlined,
              color: Colors.white,
              size: 23,
            ),
          ],
        ),
      ),
    );
  } //Design Divider

  Widget buildDivider() {
    return Container(
      margin: const EdgeInsets.only(bottom: 5),
      child: Divider(
          color: Colors.grey[350], thickness: 0.6, indent: 0, endIndent: 0),
    );
  }

//Design dropdown button Payment
  Widget buildDropdownPayment() {
    return Container(
        width: MediaQuery.of(context).size.width - 10,
        height: 150,
        padding: const EdgeInsets.fromLTRB(10, 5, 0, 5),
        margin: const EdgeInsets.all(15),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          gradient: LinearGradient(
            colors: <Color>[
              ColorCodes.colorPrimary.withOpacity(0.2),
              const Color(0xFF6DCFCF).withOpacity(0.1),
            ],
          ),
        ),
        child: Column(
          children: [
            Container(
                alignment: Alignment.centerLeft,
                margin: const EdgeInsets.only(
                  top: 5,
                  bottom: 10,
                  right: 10,
                ),
                padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  gradient: LinearGradient(
                    colors: <Color>[
                      ColorCodes.colorPrimary.withOpacity(0.1),
                      const Color.fromARGB(255, 255, 255, 255).withOpacity(0.2),
                    ],
                  ),
                ),
                child: Row(
                  children: const [
                    Text(
                      "Hình thức thanh toán",
                      style: TextStyle(
                        color: ColorCodes.textColorPrimary,
                        fontSize: 18,
                        letterSpacing: 0.2,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    SizedBox(width: 20),
                    Icon(
                      Icons.payments,
                      size: 40,
                      color: ColorCodes.colorPrimary,
                    ),
                  ],
                )),
            buildDivider(),
            DropdownButton(
              // isDense: true,
              iconEnabledColor: ColorCodes.colorPrimary,
              value: dropdownPayment,
              underline: Container(
                height: 3,
                color: ColorCodes.pinkColor,
              ),
              icon: const Icon(
                Icons.expand_more_outlined,
                size: 35,
              ),
              elevation: 17,
              borderRadius: const BorderRadius.all(Radius.circular(5)),
              onChanged: (newValue) {
                setState(() {
                  dropdownPayment = newValue as int;
                });
              },
              items: itemPayment.map(
                (map) {
                  return DropdownMenuItem(
                    value: map['value'],
                    child: Text(
                      map['name'],
                      style: const TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                        color: ColorCodes.textColorSecondary,
                      ),
                    ),
                  );
                },
              ).toList(),
            ),
          ],
        ));
  }

  //Design Zalo pay
  void _methodZaloPay(Bill bill, List<Cart> carts) {
    Provider.of<ZaloProvider>(context, listen: false).createOrder(bill).then(
          (value) => {
            if (value != null)
              {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (ctx) => ZaloPayScreen(
                      zaloOrder: value,
                      bill: bill,
                      carts: carts,
                    ),
                  ),
                ),
              }
            else
              {}
          },
        );
  }

  Future<void> methodPay(Bill bill, List<Cart> carts) async {
    showDialogLoading(context);
    Provider.of<BillProvider>(context, listen: false)
        .saveBill(bill)
        .then((billTmp) {
      if (billTmp != null) {
        // ignore: avoid_function_literals_in_foreach_calls
        carts.forEach(
          (cart) async {
            if (cart.isChoosed) {
              BillDetail billDetail = BillDetail(
                productDetail: cart.productDetail,
                bill: billTmp,
                quantity: cart.quantity,
                price: cart.pricePay,
                priceDiscount: 0,
              );
              await Provider.of<BillDetailProvider>(context, listen: false)
                  .saveBillDetail(billDetail);
              print("Done");
            }
            //   .catchError((error) {
            // return showMessageDialog(context, error, Icons.error);
          },
        );
        dismissDialog(context);
        showDialogLoadingSuccessfully(context, "Đặt Hàng Thành Công");
        Provider.of<CartProvider>(context, listen: false)
            .removeAllCartByIdMember(member.memberId!)
            .then((isRemoved) {
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                // ignore: prefer_const_constructors
                builder: (context) => PaySuccessfullyScreen(),
              ),
              (route) => false);
        });
      } else {
        dismissDialog(context);
        showMessageDialog(context, "Đặt Hàng Thất Bại", Icons.error);
        Future.delayed(
          const Duration(seconds: 2),
          () => dismissDialog(context),
        );
      }
    }).catchError((error) {
      dismissDialog(context);
      showMessageDialog(context, error, Icons.error);
      Future.delayed(
        const Duration(seconds: 2),
        () => dismissDialog(context),
      );
    });
  }
}
