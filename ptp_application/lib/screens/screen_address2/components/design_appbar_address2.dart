import 'package:flutter/material.dart';
import 'package:ptp_application/custom.dart';

class DesignAppBarAddress2 extends StatelessWidget
    implements PreferredSizeWidget {
  const DesignAppBarAddress2({Key? key}) : super(key: key);
  @override
  Size get preferredSize => const Size.fromHeight(heighAppBarLg);

  @override
  AppBar build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 0.3,
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: const Icon(Icons.arrow_back),
        color: ColorCodes.textColorSecondary,
        iconSize: 25,
      ),
      centerTitle: true,
      title: Container(
        width: 150,
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            icons(
                Icons.add_shopping_cart_outlined, ColorCodes.colorPrimary, 22),
            const Text(
              '-',
              style: TextStyle(
                color: ColorCodes.colorPrimary,
              ),
            ),
            icons(Icons.where_to_vote_outlined, ColorCodes.colorPrimary, 22),
            const Text(
              '-',
              style: TextStyle(
                color: ColorCodes.textColorSecondary,
              ),
            ),
            icons(Icons.verified_outlined, Colors.black, 22),
          ],
        ),
      ),
      actions: [
        iconButtons(context, Icons.shopping_cart_outlined, Colors.black, 25),
      ],
      bottom: PreferredSize(
        child: Container(
          alignment: Alignment.center,
          child: const Text(
            'ĐỊA CHỈ',
            textAlign: TextAlign.center,
            style: TextStyle(
                color: ColorCodes.textColorPrimary,
                fontSize: FontSize.fontSizeAppBar,
                fontWeight: FontWeight.w600),
          ),
        ),
        preferredSize: const Size.fromHeight(50),
      ),
    );
  }

//Design Icon
  Widget icons(IconData icon, Color color, double sizeIcon) {
    return Icon(
      icon,
      color: color,
      size: sizeIcon,
    );
  }

//Design icon button
  Widget iconButtons(
      BuildContext context, IconData icon, Color color, double sizeIcon) {
    return IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          icon,
          color: color,
          size: sizeIcon,
        ));
  }
}
