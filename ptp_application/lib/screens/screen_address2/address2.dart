import 'package:flutter/material.dart';
import 'package:ptp_application/models/voucher.dart';
import 'package:ptp_application/screens/screen_address2/components/design_appbar_address2.dart';
import 'package:ptp_application/screens/screen_address2/components/design_body_address2.dart';

//ignore: must_be_immutable
class AddressScreen2 extends StatelessWidget {
  Voucher? voucher;
  AddressScreen2({this.voucher, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: const DesignAppBarAddress2(), //CUSTOM DESIGN
        body: Container(
          decoration: const BoxDecoration(
            color: Color(0xFFFFFFFF),
          ),
          padding: const EdgeInsets.only(top: 10),
          child: DesignBodyAddress2(
            voucher: voucher,
          ),
        ),
      ),
    );
  }
}
