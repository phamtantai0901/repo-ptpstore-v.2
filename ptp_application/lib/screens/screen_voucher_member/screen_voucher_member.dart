import 'package:flutter/material.dart';
import 'package:ptp_application/custom.dart';
import 'components/design_appbar_voucher_member.dart';
import 'components/list_voucher_member.dart';

// ignore: must_be_immutable
class VoucherMemberScreen extends StatefulWidget {
  int indexContext;

  VoucherMemberScreen({required this.indexContext, Key? key}) : super(key: key);
  @override
  _VoucherMemberScreenState createState() => _VoucherMemberScreenState();
}

class _VoucherMemberScreenState extends State<VoucherMemberScreen> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        // bottomNavigationBar: buildBottomNavigationBar(),
        resizeToAvoidBottomInset: false,
        appBar: const PreferredSize(
          preferredSize: Size.fromHeight(60.0),
          child: DesignAppbarVoucherMember(),
        ),
        body: Column(children: [
          const SizedBox(height: 10),
          designTextFieldSearch(context, Icons.search,
              'Nhập mã giảm giá của bạn', 'Tìm mã giảm giá'),
          Expanded(
            child: ListVoucherMember(indexContext: widget.indexContext),
          ),
        ]),
      ),
    );
  }

//Design TextField Search Voucher
  Widget designTextFieldSearch(
      BuildContext context, IconData icon, String hintText, String labelText) {
    return SizedBox(
      width: MediaQuery.of(context).size.width - 20,
      child: TextFormField(
        cursorColor: ColorCodes.colorPrimary,
        keyboardType: TextInputType.text,
        //obscureText: !_passwordVisible,
        decoration: InputDecoration(
          prefixIcon: Icon(
            icon,
            color: ColorCodes.colorPrimary,
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.grey.withOpacity(0.2),
            ),
            borderRadius: const BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: ColorCodes.colorPrimary),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          contentPadding: const EdgeInsets.all(10),
          labelText: labelText,
          labelStyle: const TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w500,
            letterSpacing: 0.5,
          ),
          hintText: hintText,
          hintStyle: const TextStyle(
            fontWeight: FontWeight.w500,
            letterSpacing: 0.5,
            fontSize: 16,
            color: ColorCodes.textColorSecondary,
          ),
        ),
      ),
    );
  }
}
