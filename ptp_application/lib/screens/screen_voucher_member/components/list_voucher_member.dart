import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/config.dart';
import 'package:ptp_application/models/voucher_member.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/providers/voucher_member_provider.dart';
import 'package:ptp_application/screens/show_dialog.dart';
import 'package:ptp_application/custom.dart';

// ignore: must_be_immutable
class ListVoucherMember extends StatefulWidget {
  int indexContext; //0 - tại vị trí personal, 1- tại vị trí cart
  ListVoucherMember({required this.indexContext, Key? key}) : super(key: key);

  @override
  _ListVoucherMemberState createState() => _ListVoucherMemberState();
}

class _ListVoucherMemberState extends State<ListVoucherMember> {
  String? id;
  @override
  void initState() {
    super.initState();
    id = Provider.of<MemberProvider>(context, listen: false).member.memberId;
    Provider.of<VoucherMemberProvider>(context, listen: false)
        .fetchAllVoucherMemberByIdMemberAndStatus(id!);
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<VoucherMemberProvider>(
        builder: (context, voucherMemberProvider, child) {
      return Container(
        child: voucherMemberProvider.voucherMembers.isEmpty
            ? Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Text(
                    "Danh sách ưu đãi trống",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Colors.black,
                        letterSpacing: 0.5),
                  ),
                ],
              )
            : ListView.builder(
                itemCount: voucherMemberProvider.voucherMembers.length,
                // itemCount: 1,
                padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                itemBuilder: (context, index) {
                  return designBoxVoucherOfMember(
                      voucherMemberProvider.voucherMembers[index]);
                }),
      );
    });
  }

  //Dessign Box Voucher Of Member
  Widget designBoxVoucherOfMember(VoucherMember voucherMember) {
    return Container(
        margin: const EdgeInsets.only(top: 8),
        width: MediaQuery.of(context).size.width - 20,
        decoration: BoxDecoration(
          color: const Color(0xFFF1F1FE),
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(
              color: const Color(0xFFF1F1F1).withOpacity(0.1),
              blurRadius: 2,
              spreadRadius: 4,
            ),
          ],
        ),
        child: Container(
          padding: const EdgeInsets.fromLTRB(10, 0, 5, 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(5.0),
                child: CachedNetworkImage(
                  imageUrl: Config.getIP('/vouchers/voucher.png'),
                  width: 100,
                  height: 100,
                  fit: BoxFit.fitWidth,
                  placeholder: (context, url) => const Center(
                    child: CircularProgressIndicator(),
                  ),
                  errorWidget: (context, url, error) => const Icon(Icons.error),
                ),
              ),
              designBoxInfoVoucherOfMember(voucherMember),
            ],
          ),
        ));
  }

  //Design Box Info Voucher Of Member
  Widget designBoxInfoVoucherOfMember(VoucherMember voucherMember) {
    return Container(
      padding: const EdgeInsets.fromLTRB(10, 10, 0, 0),
      child: Column(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: 200,
                child: Text(
                  voucherMember.code.code,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  style: const TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      color: ColorCodes.textColorPrimary,
                      letterSpacing: 0.5),
                ),
              ),
              const SizedBox(height: 5),
              Text(
                "Ưu đãi ${(voucherMember.code.saleOff * 100).toInt()}% tối đa ${formatNumber(voucherMember.code.maxPrice)}đ",
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  color: ColorCodes.textColorPrimary,
                  letterSpacing: 0.2,
                ),
              ),
              const SizedBox(height: 5),
              Row(
                children: [
                  const Text(
                    "Hạn đến",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: ColorCodes.textColorSecondary,
                        letterSpacing: 1),
                  ),
                  const SizedBox(width: 5),
                  Text(
                    voucherMember.code.dateEnd.toString(),
                    style: const TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w500,
                      color: ColorCodes.textColorSecondary,
                    ),
                  ),
                ],
              ),
            ],
          ),
          const SizedBox(height: 5),
          widget.indexContext == 0
              ? Container()
              : Container(
                  // width: 120,
                  margin: const EdgeInsets.only(left: 80),
                  height: 35,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: <Color>[
                        ColorCodes.colorPrimary,
                        const Color(0xFF6DCFCF),
                        const Color(0xFF6DDDCD).withOpacity(0.9),
                      ],
                    ),
                    borderRadius: const BorderRadius.only(
                      bottomRight: Radius.circular(5),
                      topLeft: Radius.circular(12),
                      topRight: Radius.circular(2),
                      bottomLeft: Radius.circular(2),
                    ),
                  ),
                  child: TextButton(
                    onPressed: () {
                      Navigator.pop(context, voucherMember.code);
                    },
                    child: const Text(
                      'Sử dụng ngay',
                      style: TextStyle(
                        letterSpacing: 0.5,
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
        ],
      ),
    );
  }
}
