import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/config.dart';
import 'package:ptp_application/models/bill_detail.dart';
import 'package:ptp_application/providers/bill_detail_provider.dart';
import 'package:ptp_application/screens/screen_delivery/components/design_product_image_delivery.dart';
import 'package:ptp_application/screens/screen_product_detail/product_detail.dart';
import 'package:ptp_application/screens/screen_rate/rate.dart';
import 'package:ptp_application/screens/show_dialog.dart';

class SeeBillDetailInDeliveryScreen extends StatefulWidget {
  final String? idBill;
  final int status;
  const SeeBillDetailInDeliveryScreen(
      {required this.idBill, required this.status, Key? key})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _SeeBillDetailInDeliveryScreenState();
}

class _SeeBillDetailInDeliveryScreenState
    extends State<SeeBillDetailInDeliveryScreen> {
  late Future<List<BillDetail>> billDetailProvider;
  @override
  void initState() {
    super.initState();
    billDetailProvider = Provider.of<BillDetailProvider>(context, listen: false)
        .getAllBillDetailByIdBill(widget.idBill!);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: buildAppbar(),
        body: FutureBuilder<List<BillDetail>>(
          future: billDetailProvider,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List<BillDetail> billDetails = snapshot.data!;
              return billDetails.isEmpty
                  ? const Center(
                      child: Text(
                        "Danh Sách Trống",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: Colors.black,
                            letterSpacing: 0.5),
                      ),
                    )
                  : Container(
                      padding:
                          const EdgeInsets.only(top: 20, left: 15, right: 15),
                      child: ListView.builder(
                        itemCount: billDetails.length,
                        itemBuilder: (context, index) {
                          return buildProduct(billDetails[index]);
                        },
                      ),
                    );
            } else if (snapshot.hasError) {
              return Center(child: Text(snapshot.error.toString()));
            } else {
              return Center(
                child: Container(
                  alignment: Alignment.center,
                  child: Column(
                    children: const [
                      Text(
                        "Loading...",
                        style: TextStyle(
                          fontSize: 16,
                          letterSpacing: 0.5,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      CircularProgressIndicator(),
                    ],
                  ),
                ),
              );
            }
          },
        ),
      ),
    );
  }

  AppBar buildAppbar() {
    return AppBar(
      elevation: 0.3,
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: const Icon(
          Icons.arrow_back,
          color: Colors.white,
          size: 30,
        ),
      ),
      flexibleSpace: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: <Color>[
              ColorCodes.colorPrimary,
              const Color.fromARGB(255, 56, 227, 184).withOpacity(0.8),
              ColorCodes.colorPrimary,
            ],
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
          ),
        ),
      ),
      title: const Text(
        'CHI TIẾT ĐƠN HÀNG',
        textAlign: TextAlign.center,
        style: TextStyle(
            fontSize: FontSize.fontSizeAppBar, fontWeight: FontWeight.w600),
      ),
      centerTitle: true,
    );
  }

//Design Build Product
  Widget buildProduct(BillDetail billDetail) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(color: Colors.grey.shade100, width: 2),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            alignment: Alignment.center,
            width: 100,
            height: 28,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(3),
              color: ColorCodes.pinkColor.withOpacity(0.9),
            ),
            child: const Text(
              'PTP Maill +',
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          const SizedBox(height: 10),
          TextButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ProductDetailScreen(
                          productId:
                              billDetail.productDetail.product.productId)));
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                buildProductImage(
                  Config.getIP(
                      "/products/${billDetail.productDetail.product.productId}/${billDetail.productDetail.product.productImg}"),
                ),
                builDescriptionProduct(
                    billDetail.productDetail.product.productName,
                    billDetail.price,
                    billDetail.quantity,
                    billDetail.productDetail.size.sizeName),
              ],
            ),
          ),
          const SizedBox(height: 5),
          buildTotalPriceOfProduct(billDetail.totalPrice!),
          widget.status == 1 ? buildButtonRate(billDetail) : Container(),
        ],
      ),
    );
  }

  //Design Build Button Confirm
  Widget buildButtonRate(BillDetail billDetail) {
    return Container(
      alignment: Alignment.centerRight,
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
      child: Container(
        // width: 150,
        height: 38,
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(
            width: 1,
            color: billDetail.rateStatus == 1
                ? ColorCodes.colorPrimary
                : ColorCodes.pinkColor,
          ),
          borderRadius: const BorderRadius.only(
            bottomRight: Radius.circular(12),
            topLeft: Radius.circular(12),
            topRight: Radius.circular(2),
            bottomLeft: Radius.circular(2),
          ),
        ),
        child: billDetail.rateStatus == 1
            ? TextButton(
                style: ElevatedButton.styleFrom(
                  splashFactory: NoSplash.splashFactory,
                ),
                onPressed: () {},
                child: const Text(
                  'Đã Đánh Giá',
                  style: TextStyle(
                    letterSpacing: 0.5,
                    color: ColorCodes.colorPrimary,
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              )
            : TextButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => RateScreen(
                                billDetail: billDetail,
                              )));
                },
                child: const Text(
                  'Đánh Giá',
                  style: TextStyle(
                    letterSpacing: 0.5,
                    color: ColorCodes.colorPrimary,
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
      ),
    );
  }

  //Design Build Total Price Of product
  Row buildTotalPriceOfProduct(int total) {
    return Row(
      // crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        const Text('Tổng : ',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w600,
              color: ColorCodes.textColorSecondary,
            )),
        Text(
          '${formatNumber(total)} đ',
          style: const TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w600,
            color: ColorCodes.pinkColor,
          ),
        ),
      ],
    );
  }

//Design Description Product
  Widget builDescriptionProduct(
      String name, int price, int quantity, String size) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SizedBox(
          width: 240,
          child: Text(
            name.toUpperCase(),
            style: const TextStyle(
              letterSpacing: 0.2,
              fontSize: 18,
              fontWeight: FontWeight.w600,
              color: ColorCodes.textColorPrimary,
            ),
            maxLines: 3,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        const SizedBox(height: 5),
        buildQuantityAndSize(quantity, size),
        const SizedBox(height: 5),
        Text(
          'đ: ${formatNumber(price)}',
          style: const TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w600,
            color: ColorCodes.textColorPrimary,
          ),
        ),
      ],
    );
  }

//Design quantity
  Widget buildQuantityAndSize(int quantity, String size) {
    return SizedBox(
      width: 240,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            'SL: $quantity',
            style: const TextStyle(
              fontSize: 16,
              color: ColorCodes.textColorSecondary,
              fontWeight: FontWeight.w600,
            ),
          ),
          Text(
            'K.thước: $size',
            style: const TextStyle(
              fontSize: 16,
              color: ColorCodes.textColorSecondary,
              fontWeight: FontWeight.w600,
            ),
          ),
        ],
      ),
    );
  }
}
