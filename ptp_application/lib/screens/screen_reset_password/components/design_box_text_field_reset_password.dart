import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/screens/screen_login/login.dart';
import 'package:ptp_application/screens/show_dialog.dart';

import 'package:ptp_application/custom.dart';

// ignore: must_be_immutable
class DesignBoxTextFieldResetPassword extends StatefulWidget {
  String username;

  DesignBoxTextFieldResetPassword({required this.username, Key? key})
      : super(key: key);
  @override
  _DesignBoxTextFieldResetPassword createState() =>
      _DesignBoxTextFieldResetPassword();
}

class _DesignBoxTextFieldResetPassword
    extends State<DesignBoxTextFieldResetPassword> {
  bool _passwordVisibleT = false, _passwordVisibleC = false;
  late TextEditingController password;
  late TextEditingController confirmPassword;
  @override
  // ignore: must_call_super
  void initState() {
    _passwordVisibleT = false;
    _passwordVisibleC = false;
    password = TextEditingController();
    confirmPassword = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 210,
      child: Container(
        padding: const EdgeInsets.all(20),
        height: 470,
        width: MediaQuery.of(context).size.width - 50,
        margin: const EdgeInsets.symmetric(horizontal: 25),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(
              color: ColorCodes.colorPrimary.withOpacity(0.1),
              blurRadius: 10,
              spreadRadius: 1,
            ),
          ],
        ),
        child: Column(
          children: [
            Column(children: [
              const SizedBox(height: 10),
              const Text(
                "ĐẶT LẠI MẬT KHẨU",
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                    color: ColorCodes.textColorSecondary),
              ),
              const SizedBox(
                height: 20,
              ),
              designTextFieldTypePassword(
                  MdiIcons.lockPlus, "Nhập mật khẩu mới", "Nhập mật khẩu mới"),
              const SizedBox(
                height: 15,
              ),
              designTextFieldConfirmPassword(
                  MdiIcons.lockCheck, "Nhập lại mật khẩu", "Xác nhận mật khẩu"),
              const SizedBox(
                height: 15,
              ),
              designButtonLoginNow(),
            ]),
          ],
        ),
      ),
    );
  }

  //Design TextField Type Password
  Widget designTextFieldTypePassword(
      IconData icon, String hintText, String labelText) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: TextField(
        cursorColor: ColorCodes.colorPrimary,
        keyboardType: TextInputType.text,
        obscureText: !_passwordVisibleT,
        controller: password,
        autofocus: true,
        decoration: InputDecoration(
          prefixIcon: Icon(
            icon,
            color: ColorCodes.colorPrimary,
          ),
          suffixIcon: IconButton(
            icon: Icon(
              _passwordVisibleT ? Icons.visibility : Icons.visibility_off,
              color: ColorCodes.colorPrimary,
            ),
            onPressed: () {
              setState(() {
                _passwordVisibleT = !_passwordVisibleT;
              });
            },
          ),
          enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: ColorCodes.textColorSecondary),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: ColorCodes.colorPrimary),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          contentPadding: const EdgeInsets.all(10),
          labelText: labelText,
          labelStyle: const TextStyle(fontSize: 16),
          hintText: hintText,
          hintStyle: const TextStyle(
            fontWeight: FontWeight.w500,
            letterSpacing: 0.5,
            fontSize: 13,
            color: ColorCodes.textColorSecondary,
          ),
        ),
      ),
    );
  }

  //Design TextField Confirm Password
  Widget designTextFieldConfirmPassword(
      IconData icon, String hintText, String labelText) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: TextField(
        cursorColor: ColorCodes.colorPrimary,
        keyboardType: TextInputType.text,
        obscureText: !_passwordVisibleC,
        controller: confirmPassword,
        decoration: InputDecoration(
          prefixIcon: Icon(
            icon,
            color: ColorCodes.colorPrimary,
          ),
          suffixIcon: IconButton(
            icon: Icon(
              _passwordVisibleC ? Icons.visibility : Icons.visibility_off,
              color: ColorCodes.colorPrimary,
            ),
            onPressed: () {
              setState(() {
                _passwordVisibleC = !_passwordVisibleC;
              });
            },
          ),
          enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: ColorCodes.textColorSecondary),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: ColorCodes.colorPrimary),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          contentPadding: const EdgeInsets.all(10),
          labelText: labelText,
          labelStyle: const TextStyle(fontSize: 16),
          hintText: hintText,
          hintStyle: const TextStyle(
            fontWeight: FontWeight.w500,
            letterSpacing: 0.5,
            fontSize: 13,
            color: ColorCodes.textColorSecondary,
          ),
        ),
      ),
    );
  }

  //Design Button Login Now
  Widget designButtonLoginNow() {
    return Container(
      width: double.maxFinite,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: <Color>[
            ColorCodes.colorPrimary,
            const Color(0xFF6DCFCF),
            const Color(0xFF6DDDCD).withOpacity(0.8),
          ],
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      child: TextButton(
        onPressed: () async {
          showDialogLoading(context);
          if (password.text.isEmpty && confirmPassword.text.isEmpty) {
            dismissDialog(context);
            showMessageDialog(context, "Vui lòng không để trống", Icons.error);
            Future.delayed(
              const Duration(seconds: 2),
              () => dismissDialog(context),
            );
          } else if (password.text != confirmPassword.text) {
            dismissDialog(context);
            showMessageDialog(
                context, "Mật khẩu không trùng khớp", Icons.error);
            Future.delayed(
              const Duration(seconds: 2),
              () => dismissDialog(context),
            );
          } else {
            await Provider.of<MemberProvider>(context, listen: false)
                .changePassword(
                    widget.username, password.text, confirmPassword.text)
                .then((isResetPassword) {
              if (isResetPassword == false) {
                dismissDialog(context);
                showMessageDialog(
                    context, "Lấy lại mật khẩu không thành công", Icons.error);
                Future.delayed(
                    const Duration(seconds: 2), () => dismissDialog(context));
                password.clear();
                confirmPassword.clear();
              } else {
                dismissDialog(context);
                showDialogLoadingSuccessfully(
                    context, "Lấy lại mật khẩu thành công");
                Future.delayed(
                  const Duration(seconds: 1),
                  () {
                    dismissDialog(context);
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        // ignore: prefer_const_constructors
                        builder: (ctx) => LoginScreen(),
                      ),
                    );
                  },
                );
              }
            }).catchError((error) {
              dismissDialog(context);
              showMessageDialog(context, error, Icons.check_circle_outlined);
              Future.delayed(
                  const Duration(seconds: 2), () => dismissDialog(context));
            });
          }
        },
        child: const Text(
          'ĐĂNG NHẬP NGAY',
          style: TextStyle(
            letterSpacing: 1,
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }
}
