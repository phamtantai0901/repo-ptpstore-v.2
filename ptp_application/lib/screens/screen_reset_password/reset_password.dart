import 'package:flutter/material.dart';
import 'package:ptp_application/custom.dart';

import 'components/design_box_text_field_reset_password.dart';
import 'components/design_header_reset_password.dart';

class ResetPasswordScreen extends StatelessWidget {
  final String username;
  const ResetPasswordScreen({required this.username, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          body: Stack(
            children: [
              const DesignHeaderResetPassword(),
              DesignBoxTextFieldResetPassword(username: username),
              Positioned(
                bottom: 0,
                right: 0,
                child: Container(
                  padding: const EdgeInsets.all(20),
                  height: 150,
                  width: 412,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: <Color>[
                        const Color(0xFF6DFFCD).withOpacity(0.7),
                        ColorCodes.colorPrimary.withOpacity(0.7),
                        const Color(0xFF6DCFCF).withOpacity(0.7),
                      ],
                    ),
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(80.0),
                      topRight: Radius.circular(80.0),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
