import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/models/discount_category.dart';
import 'package:ptp_application/providers/discount_category.dart';

class DesignPointAccumulation extends StatelessWidget {
  final String rankId;
  const DesignPointAccumulation({required this.rankId, Key? key})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<DiscountCategory>>(
        future: Provider.of<DiscountCategoryProvider>(context, listen: false)
            .getAllDiscountByRankIdAndStatus(rankId, 1),
        builder: (context, snapshot) {
          if (snapshot.connectionState != ConnectionState.done) {
            return Container(
              alignment: Alignment.center,
              child: Column(
                children: const [
                  Text(
                    "Loading...",
                    style: TextStyle(
                      fontSize: 16,
                      letterSpacing: 0.5,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  CircularProgressIndicator(),
                ],
              ),
            );
          } else {
            if (snapshot.hasData && snapshot.data!.isNotEmpty) {
              return ListView.builder(
                itemCount: snapshot.data!.length,
                itemBuilder: (context, index) {
                  return buildBoxVoucherWithRankMember(
                    Icons.local_offer,
                    snapshot.data![index],
                  );
                },
              );
            } else if (snapshot.hasError) {
              return Center(
                child: Text(
                  snapshot.error.toString(),
                ),
              );
            } else {
              return const Center(
                child: Text(
                  "Danh sách trống",
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w500,
                      color: Colors.black,
                      letterSpacing: 0.5),
                ),
              );
            }
          }
        });
  }

  Widget buildBoxVoucherWithRankMember(
      IconData? iconName, DiscountCategory discount) {
    return Container(
      margin: const EdgeInsets.only(top: 3, bottom: 3),
      padding: const EdgeInsets.fromLTRB(10, 10, 0, 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Colors.white,
        boxShadow: const [
          BoxShadow(
            color: Color(0xFFF1F1F1),
            offset: Offset(0.0, 1.0),
            spreadRadius: 2,
            blurRadius: 10,
          ),
        ],
      ),
      child: Row(
        children: [
          const SizedBox(
            width: 15,
          ),
          Icon(
            iconName,
            color: ColorCodes.colorPrimary,
            size: 20,
          ),
          const SizedBox(
            width: 15,
          ),
          SizedBox(
            width: 300,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Ưu đãi sản phẩm của loại ${discount.category.categoryName}",
                  style: const TextStyle(
                    color: ColorCodes.textColorPrimary,
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                Text(
                  "Giảm giá ${discount.percentPrice}% với các sản phẩm thuộc loại ${discount.category.categoryName}",
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                    color: ColorCodes.textColorSecondary,
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
