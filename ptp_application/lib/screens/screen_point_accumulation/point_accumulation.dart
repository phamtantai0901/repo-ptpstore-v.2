import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/config.dart';
import 'package:ptp_application/models/member.dart';
import 'package:ptp_application/models/rank.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/providers/rank_provider.dart';
import 'package:ptp_application/providers/screen_provider.dart';
import 'package:ptp_application/screens/screen_point_accumulation/components/design_point_accumulation.dart';

// ignore: must_be_immutable
class PointAccumulationScreen extends StatelessWidget {
  PointAccumulationScreen({Key? key}) : super(key: key);
  late int currentIndex;
  @override
  Widget build(BuildContext context) {
    Member member = Provider.of<MemberProvider>(context, listen: false).member;
    RankProvider rankProvider =
        Provider.of<RankProvider>(context, listen: false);
    currentIndex =
        Provider.of<ScreenProvider>(context, listen: false).setCurrentIndex = 0;
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(colors: <Color>[
              ColorCodes.colorPrimary,
              const Color(0xFF26cba3).withOpacity(0.9),
              ColorCodes.colorPrimary,
            ], begin: Alignment.centerLeft, end: Alignment.centerRight),
          ),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back),
        ),
        title: const Text(
          'BẢNG ĐIỂM TÍCH LUỸ',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: FontSize.fontSizeAppBar, fontWeight: FontWeight.w600),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.add_alert),
          ),
        ],
        centerTitle: true,
      ),
      body: FutureBuilder<List<Rank>>(
        future: rankProvider.getAllRankByStatus(1),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Column(
              children: [
                Container(
                  margin: const EdgeInsets.only(top: 10, bottom: 10),
                  child: Column(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100),
                          color: Colors.white,
                          boxShadow: const [
                            BoxShadow(
                              color: Color(0xFFF1F1F1),
                              offset: Offset(0.0, 1.0),
                              spreadRadius: 10,
                              blurRadius: 7,
                            ),
                          ],
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(100),
                          child: CachedNetworkImage(
                            imageUrl: Config.getIP(
                                "/avatar_users/${member.user.image}"),
                            width: 120,
                            height: 120,
                            fit: BoxFit.cover,
                            placeholder: (context, url) => const Center(
                              child: CircularProgressIndicator(),
                            ),
                            errorWidget: (context, url, error) =>
                                const Icon(Icons.error),
                          ),
                        ),
                      ),
                      const SizedBox(height: 10),
                      Text(
                        member.user.fullName,
                        style: const TextStyle(
                          fontSize: 20,
                          color: ColorCodes.pinkColor,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      const SizedBox(height: 5),
                      Text(
                        '#${member.memberId!}',
                        style: const TextStyle(
                          fontSize: 18,
                          color: ColorCodes.textColorSecondary,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      const SizedBox(height: 5),
                      FutureBuilder<Rank?>(
                          future: rankProvider
                              .getNextRankOfCurrentRankByMemberIdAndStatus(
                                  member.memberId!, 1),
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              return Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Column(
                                    children: [
                                      Text(
                                        '(${member.rank!.point})',
                                        style: const TextStyle(
                                          color: ColorCodes.textColorSecondary,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                      Text(
                                        member.rank!.rankName,
                                        style: const TextStyle(
                                          color: ColorCodes.textColorSecondary,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Slider(
                                    value: double.parse(
                                      member.currentPoint.toString(),
                                    ),
                                    max: double.parse(
                                      snapshot.data!.point.toString(),
                                    ),
                                    min: double.parse(
                                      member.rank!.point.toString(),
                                    ),
                                    thumbColor: ColorCodes.colorPrimary,
                                    activeColor: ColorCodes.colorPrimary,
                                    divisions: (snapshot.data!.point ~/ 100),
                                    autofocus: true,
                                    label: member.currentPoint.toString(),
                                    onChanged: (value) {},
                                  ),
                                  Column(
                                    children: [
                                      Text(
                                        '(${snapshot.data!.point})',
                                        style: const TextStyle(
                                          color: ColorCodes.textColorSecondary,
                                          fontSize: 18,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                      Text(
                                        snapshot.data!.rankName,
                                        style: const TextStyle(
                                          color: ColorCodes.textColorSecondary,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              );
                            } else if (snapshot.hasError) {
                              return Container(
                                alignment: Alignment.center,
                                child: Column(
                                  children: const [
                                    Text(
                                      "Loading...",
                                      style: TextStyle(
                                        fontSize: 16,
                                        letterSpacing: 0.5,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    CircularProgressIndicator(),
                                  ],
                                ),
                              );
                            } else {
                              return const Center(
                                child: Text("NONE"),
                              );
                            }
                          }),
                      const SizedBox(height: 10),
                      Text(
                        'Số điểm hiện tại: (${member.currentPoint})',
                        style: const TextStyle(
                          fontSize: 18,
                          color: ColorCodes.textColorSecondary,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                ),
                Consumer<ScreenProvider>(
                    builder: (context, screenProvider, child) {
                  currentIndex = screenProvider.currentIndex;
                  return Expanded(
                    child: Column(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                                colors: <Color>[
                                  ColorCodes.colorPrimary,
                                  const Color(0xFF26cba3).withOpacity(0.9),
                                  ColorCodes.colorPrimary,
                                ],
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight),
                          ),
                          height: 70,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: snapshot.data!.length,
                            itemBuilder: (context, index) {
                              return Container(
                                decoration: checkActive(index),
                                width: 110,
                                child: TextButton(
                                  onPressed: () {
                                    screenProvider.updateCurrentIndex(index);
                                  },
                                  child: Text(
                                    snapshot.data![index].rankName,
                                    textAlign: TextAlign.center,
                                    style: const TextStyle(
                                      fontSize: 15,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                        Expanded(
                          child: DesignPointAccumulation(
                              rankId: snapshot.data![currentIndex].rankId),
                        ),
                      ],
                    ),
                  );
                }),
              ],
            );
          } else if (snapshot.hasError) {
            return Center(child: Text(snapshot.error.toString()));
          } else {
            return Container(
              alignment: Alignment.center,
              child: Column(
                children: const [
                  Text(
                    "Loading...",
                    style: TextStyle(
                      fontSize: 16,
                      letterSpacing: 0.5,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  CircularProgressIndicator(),
                ],
              ),
            );
          }
        },
      ),
    );
  }

  Decoration? checkActive(int indexActive) {
    Decoration? decoration;
    if (indexActive == currentIndex) {
      decoration = const BoxDecoration(
          border: Border(
        bottom: BorderSide(
            width: 5, color: ColorCodes.pinkColor, style: BorderStyle.solid),
      ));
    }
    return decoration;
  }
}
