import 'package:flutter/material.dart';
import 'package:ptp_application/custom.dart';

class DesignBodyVersion extends StatelessWidget {
  const DesignBodyVersion({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Colors.white,
        boxShadow: const [
          BoxShadow(
            color: Color(0xFFF1F1F1),
            offset: Offset(0.0, 1.0),
            spreadRadius: 2,
            blurRadius: 10,
          ),
        ],
      ),
      width: double.maxFinite,
      child: ListView(
        children: [
          const SizedBox(height: 15),
          buildHeading("TEAM DEVELOPMENT"),
          const SizedBox(height: 15),
          buildVersionProfile("PHAM_TAN_TAI", "457"),
          const SizedBox(height: 5),
          buildVersionProfile("MINH_PHUONG", "452"),
          const SizedBox(height: 30),
          buildHeading("VERSION"),
          const SizedBox(height: 5),
          buildTextVersion(
            "19.22.452.457",
          ),
          const SizedBox(height: 15),
          buildImage(),
          buildTextVersion(
            "@copyright PTPStore",
          ),
          const SizedBox(height: 10),
        ],
      ),
      // height: 160,,
    );
  }
}

//Design Text Version
Widget buildTextVersion(String text) {
  return Center(
    child: Text(
      text,
      style: const TextStyle(
        fontSize: 20,
        color: ColorCodes.textColorSecondary,
        fontWeight: FontWeight.w600,
        letterSpacing: 0.8,
      ),
    ),
  );
}

//Design Box Image
Widget buildImage() {
  return ClipRRect(
    child: Image.asset(
      'images/logo.png',
      fit: BoxFit.cover,
      height: 350,
      width: 350,
    ),
  );
}

//Design Heading
Widget buildHeading(String titleHeading) {
  return Container(
    padding: const EdgeInsets.only(
      top: 20,
      right: 10,
    ),
    decoration: const BoxDecoration(
      border: Border(
        top: BorderSide(
          width: 1.0,
          color: Color(0xFFF1EEEE),
        ),
      ),
    ),
    alignment: Alignment.center,
    child: Text(
      titleHeading,
      style: TextStyle(
        color: ColorCodes.textColorSecondary.withOpacity(.8),
        fontWeight: FontWeight.w700,
        fontSize: 20,
        letterSpacing: 0.5,
      ),
    ),
  );
}

//Design Version Profile
Widget buildVersionProfile(String fullName, String mssv) {
  return Row(mainAxisAlignment: MainAxisAlignment.center, children: [
    Text(
      fullName,
      textAlign: TextAlign.left,
      style: TextStyle(
        color: ColorCodes.textColorSecondary.withOpacity(.8),
        fontWeight: FontWeight.w500,
        fontSize: 20,
        letterSpacing: 0.5,
      ),
    ),
    const SizedBox(width: 15),
    Text(
      "-",
      style: TextStyle(
          color: ColorCodes.textColorSecondary.withOpacity(.8),
          fontWeight: FontWeight.w700,
          fontSize: 20),
    ),
    const SizedBox(width: 15),
    Text(
      mssv,
      style: TextStyle(
        color: ColorCodes.textColorSecondary.withOpacity(.8),
        fontWeight: FontWeight.w500,
        fontSize: 20,
        letterSpacing: 0.5,
      ),
    ),
  ]);
}
