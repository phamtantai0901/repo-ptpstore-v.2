import 'package:flutter/material.dart';
import 'package:ptp_application/screens/screen_version/components/design_appbar_version.dart';
import 'package:ptp_application/screens/screen_version/components/design_body_version.dart';

class VersionScreen extends StatelessWidget {
  const VersionScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      appBar: DesignAppbarVersion(),
      body: DesignBodyVersion(),
    );
  }
}
