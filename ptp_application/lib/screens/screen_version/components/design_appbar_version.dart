import 'package:flutter/material.dart';

import 'package:ptp_application/custom.dart';

class DesignAppbarVersion extends StatelessWidget
    implements PreferredSizeWidget {
  const DesignAppbarVersion({Key? key}) : super(key: key);

  @override
  Size get preferredSize => const Size.fromHeight(heighAppBarMd);
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      toolbarHeight: 70,
      elevation: 0,
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: const Icon(
          Icons.arrow_back_outlined,
          color: ColorCodes.textColorSecondary,
          size: 30,
        ),
      ),
      title: const Text(
        'PHIÊN BẢN',
        textAlign: TextAlign.center,
        style: TextStyle(
            color: ColorCodes.textColorPrimary,
            fontSize: FontSize.fontSizeAppBar,
            fontWeight: FontWeight.w600),
      ),
      centerTitle: true,
    );
  }
}
