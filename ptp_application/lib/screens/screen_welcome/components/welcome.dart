import 'package:flutter/material.dart';
import 'package:ptp_application/custom.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          body: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: <Color>[
                  const Color(0xFF6DDDCD).withOpacity(0.3),
                  const Color(0xFF6DDDCD).withOpacity(0.25),
                  const Color(0xFF6DCFCF).withOpacity(0.3),
                ],
              ),
              borderRadius: BorderRadius.circular(5),
            ),
            child: Stack(
              children: [
                Positioned(
                  top: 80,
                  child: Container(
                    height: double.maxFinite,
                    width: MediaQuery.of(context).size.width - 0,
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(30),
                        topLeft: Radius.circular(30),
                      ),
                    ),
                    child: Column(
                      children: [
                        Image.asset(
                          'images/logo.png',
                          fit: BoxFit.cover,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: const [
                            CircularProgressIndicator(
                              strokeWidth: 4.0,
                              valueColor: AlwaysStoppedAnimation(
                                  ColorCodes.colorPrimary), //any color you want
                            ),
                            SizedBox(height: 15),
                            Text(
                              "Đợi tí nhé...!",
                              style: TextStyle(
                                color: ColorCodes.colorPrimary,
                                fontWeight: FontWeight.w600,
                                fontSize: 22,
                                letterSpacing: 0.6,
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
