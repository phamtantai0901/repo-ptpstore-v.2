import 'package:flutter/material.dart';
import 'package:ptp_application/custom.dart';

//Design Button Feature
Widget builButtonFeature(
    BuildContext context, String title, IconData icon, Widget widget) {
  return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Colors.white,
        boxShadow: const [
          BoxShadow(
            color: Color(0xFFF1F1F1),
            offset: Offset(0.0, 1.0),
            spreadRadius: 2,
            blurRadius: 10,
          ),
        ],
      ),
      child: TextButton(
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => widget));
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ShaderMask(
                shaderCallback: (rect) => LinearGradient(
                  colors: [
                    ColorCodes.colorPrimary.withOpacity(0.6),
                    const Color(0xff84dddf).withOpacity(0.8),
                  ],
                  begin: Alignment.bottomCenter,
                  // end: Alignment.bottomCenter,
                ).createShader(rect),
                child: Icon(
                  icon,
                  size: 35,
                ),
              ),
              Text(
                title,
                style: const TextStyle(
                  color: ColorCodes.textColorSecondary,
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  letterSpacing: 0.5,
                ),
              ),
              const Icon(
                Icons.chevron_right_outlined,
                size: 35,
              ),
            ],
          )));
}
