import 'package:flutter/material.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/screens/screen_favorite/favorites_list.dart';
import 'package:ptp_application/screens/screen_point_accumulation/point_accumulation.dart';
import 'package:ptp_application/screens/screen_voucher_member/screen_voucher_member.dart';

class DesignBorderRectangle extends StatelessWidget {
  const DesignBorderRectangle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: double.maxFinite,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Colors.white,
        boxShadow: const [
          BoxShadow(
            color: Color(0xFFF1F1F1),
            offset: Offset(0.0, 1.0),
            spreadRadius: 2,
            blurRadius: 20,
          ),
        ],
      ),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            buidBorderRectangle(
              context,
              'Mã Giảm Giá',
              Icons.card_giftcard_outlined,
              ColorCodes.colorPrimary,
              VoucherMemberScreen(indexContext: 0),
            ),
            buidBorderRectangle(
              context,
              'Yêu Thích',
              Icons.favorite_border_outlined,
              const Color(0xffF65E5E),
              const FavoritesListScreen(),
            ),
            buidBorderRectangle(
              context,
              'Tích Điểm',
              Icons.savings_outlined,
              const Color(0xfffdbb2d),
              PointAccumulationScreen(),
            ),
          ],
        ),
      ),
    );
  }
}

//Design Border Rectangle
Widget buidBorderRectangle(BuildContext context, String title, IconData icon,
    Color color, Widget widget) {
  return Container(
    width: 125,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      color: Colors.white,
    ),
    child: TextButton(
      onPressed: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => widget));
      },
      child: Column(
        // mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            icon,
            size: 40,
            color: color,
          ),
          const SizedBox(
            height: 5,
          ),
          Text(
            title,
            style: const TextStyle(
              color: ColorCodes.textColorSecondary,
              fontSize: 13,
              fontWeight: FontWeight.w600,
            ),
          ),
        ],
      ),
    ),
  );
}
