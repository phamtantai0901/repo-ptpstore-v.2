import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/providers/google_provider.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/screens/screen_login/login.dart';

//Design Button LogOut
Widget buildButtonLogOut(BuildContext context, String title, IconData icon) {
  return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Colors.white,
        boxShadow: const [
          BoxShadow(
            color: Color(0xFFF1F1F1),
            offset: Offset(0.0, 1.0),
            spreadRadius: 2,
            blurRadius: 10,
          ),
        ],
      ),
      child: TextButton(
          onPressed: () async {
            //Clear token
            await Provider.of<GoogleProvider>(context, listen: false)
                .signOutFromGoogle();

            await Provider.of<MemberProvider>(context, listen: false)
                .logout()
                .then((value) => {
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const LoginScreen(),
                          ),
                          (route) => false)
                    });
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ShaderMask(
                shaderCallback: (rect) => LinearGradient(
                  colors: [
                    ColorCodes.colorPrimary.withOpacity(0.6),
                    const Color(0xff84dddf).withOpacity(0.8),
                  ],
                  begin: Alignment.bottomCenter,
                  // end: Alignment.bottomCenter,
                ).createShader(rect),
                child: Icon(
                  icon,
                  size: 40,
                ),
              ),
              Text(
                title,
                style: const TextStyle(
                  color: ColorCodes.textColorSecondary,
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  letterSpacing: 0.5,
                ),
              ),
              const Icon(
                Icons.navigate_next,
                size: 35,
              ),
            ],
          )));
}
