import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/config.dart';
import 'package:ptp_application/models/cart.dart';
import 'package:ptp_application/models/member.dart';
import 'package:ptp_application/models/notification.dart';
import 'package:ptp_application/providers/cart_provider.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/providers/notification_provider.dart';
import 'package:ptp_application/screens/screen_cart/cart.dart';
import 'package:ptp_application/screens/screen_notification/notification.dart';

class DesignAppbarPersonal extends StatelessWidget
    implements PreferredSizeWidget {
  const DesignAppbarPersonal({Key? key}) : super(key: key);
  @override
  Size get preferredSize => const Size.fromHeight(heighAppBarMaxLg);
  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    List<Cart> carts = Provider.of<CartProvider>(context, listen: true).carts;
    Member member = Provider.of<MemberProvider>(context, listen: true).member;
    return AppBar(
      backgroundColor: Colors.white,
      toolbarHeight: 100,
      leadingWidth: 100,
      flexibleSpace: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color(0xffbeffff),
              Color(0xfff3ffff),
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
          boxShadow: [
            BoxShadow(
              color: Color.fromARGB(255, 228, 255, 255),
              offset: Offset(0.0, 1.0),
              spreadRadius: 2,
              blurRadius: 10,
            ),
          ],
        ),
      ),
      elevation: 0,
      leading: Container(
        margin: const EdgeInsets.only(left: 20, top: 10),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(100),
          child: CachedNetworkImage(
            imageUrl: Config.getIP("/avatar_users/${member.user.image}"),
            fit: BoxFit.cover,
            placeholder: (context, url) => const Center(
              child: CircularProgressIndicator(),
            ),
            errorWidget: (context, url, error) => const Icon(Icons.error),
          ),
        ),
      ),
      title:
          Consumer<MemberProvider>(builder: (context, memberProvider, child) {
        return Column(
          children: [
            Container(
              margin: const EdgeInsets.only(top: 20),
              alignment: Alignment.centerLeft,
              child: Text(
                memberProvider.member.user.fullName,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                  color: ColorCodes.textColorSecondary,
                  fontWeight: FontWeight.w700,
                  fontSize: 14,
                  letterSpacing: 0.1,
                ),
              ),
            ),
            Row(
              children: [
                const Text(
                  'Hạng:',
                  style: TextStyle(
                    color: ColorCodes.textColorSecondary,
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                Text(
                  memberProvider.member.rank?.rankName ?? "None",
                  style: const TextStyle(
                    color: ColorCodes.textColorSecondary,
                    fontSize: 14,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
              ],
            ),
            Row(
              children: [
                const Text(
                  'Điểm:',
                  style: TextStyle(
                    color: ColorCodes.textColorSecondary,
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                Text(
                  '(${memberProvider.member.currentPoint})',
                  style: const TextStyle(
                    color: ColorCodes.textColorSecondary,
                    fontSize: 14,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ],
            )
          ],
        );
      }),
      actions: [
        GestureDetector(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const ScreenNotification()));
          },
          child: Consumer<NotificationProvider>(
            builder: (context, notiProvider, child) {
              List<NotificationModel> notis = notiProvider.notificationsUnread;
              return SizedBox(
                width: 50,
                child: Stack(
                  alignment: AlignmentDirectional.center,
                  children: [
                    const Icon(
                      Icons.notifications_outlined,
                      color: ColorCodes.facebookColor,
                      size: 32,
                    ),
                    notis.isEmpty
                        ? Container()
                        : Positioned(
                            top: 18,
                            right: 5,
                            child: Container(
                              alignment: Alignment.center,
                              width: 28,
                              height: 28,
                              decoration: BoxDecoration(
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(50),
                                ),
                                color: Colors.red[400],
                              ),
                              child: Text(
                                notis.length >= 99 ? '99+' : '${notis.length}',
                                style: const TextStyle(
                                    color: Colors.white,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ),
                  ],
                ),
              );
            },
          ),
        ),
        GestureDetector(
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => const CartScreen()));
          },
          child: Consumer<CartProvider>(
            builder: (context, cartProvider, child) {
              List<Cart> carts = cartProvider.carts;
              return SizedBox(
                width: 50,
                child: Stack(
                  alignment: AlignmentDirectional.center,
                  children: [
                    const Icon(
                      Icons.shopping_cart_outlined,
                      color: ColorCodes.facebookColor,
                      size: 32,
                    ),
                    carts.isEmpty
                        ? Container()
                        : Positioned(
                            top: 18,
                            right: 5,
                            child: Container(
                              alignment: Alignment.center,
                              width: 28,
                              height: 28,
                              decoration: BoxDecoration(
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(50),
                                ),
                                color: Colors.red[400],
                              ),
                              child: Text(
                                carts.length >= 99 ? '99+' : '${carts.length}',
                                style: const TextStyle(
                                    color: Colors.white,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ),
                  ],
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
