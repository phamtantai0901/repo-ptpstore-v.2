import 'package:flutter/material.dart';
import 'package:ptp_application/main.dart';
import 'package:ptp_application/screens/screen_delivery/delivery.dart';
import 'package:ptp_application/screens/screen_history_activity/history_activity.dart';
import 'package:ptp_application/screens/screen_my_rate/my_rate.dart';
import 'package:ptp_application/screens/screen_personal/components/design_appbar_personal.dart';
import 'package:ptp_application/screens/screen_personal/components/design_border_rectangle.dart';
import 'package:ptp_application/screens/screen_personal/components/design_button_logout.dart';
import 'package:ptp_application/screens/screen_personal/components/design_feature_button.dart';
import 'package:ptp_application/screens/screen_profile/profile.dart';
import 'package:ptp_application/screens/screen_version/components/version.dart';

class PersonalScreen extends StatefulWidget {
  const PersonalScreen({Key? key}) : super(key: key);

  @override
  State<PersonalScreen> createState() => _PersonalScreenState();
}

class _PersonalScreenState extends State<PersonalScreen>
    with AutomaticKeepAliveClientMixin<PersonalScreen> {
  @override
  bool get wantKeepAlive => true;
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return RefreshIndicator(
      onRefresh: () async {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => BottomNavAppBar(currentIndex: 4)));
      },
      child: Scaffold(
        backgroundColor: const Color(0xfff3ffff),
        appBar: const DesignAppbarPersonal(),
        // bottomNavigationBar: buildBottomNavAppBar(),
        body: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.only(left: 15, right: 15),
            child: Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                const DesignBorderRectangle(),
                const SizedBox(
                  height: 30,
                ),
                builButtonFeature(
                  context,
                  'Thông tin cá nhân',
                  Icons.person_outline,
                  ProfileScreen(),
                ),
                const SizedBox(
                  height: 10,
                ),
                builButtonFeature(
                  context,
                  'Lịch sử hoạt động',
                  Icons.history_outlined,
                  const HistoryActivityScreen(),
                ),
                const SizedBox(
                  height: 10,
                ),
                builButtonFeature(
                  context,
                  'Đơn hàng của tôi',
                  Icons.add_shopping_cart_outlined,
                  DeliveryScreen(),
                ),
                const SizedBox(
                  height: 10,
                ),
                builButtonFeature(
                  context,
                  'Đánh giá của tôi',
                  Icons.thumb_up_outlined,
                  const MyRateScreen(),
                ),
                const SizedBox(
                  height: 10,
                ),
                builButtonFeature(
                  context,
                  'Phiên Bản',
                  Icons.handyman_outlined,
                  const VersionScreen(),
                ),
                const SizedBox(height: 25),
                buildButtonLogOut(context, 'Đăng Xuất', Icons.logout_outlined),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
