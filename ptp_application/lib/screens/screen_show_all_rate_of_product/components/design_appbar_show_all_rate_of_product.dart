import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:ptp_application/config.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/models/product.dart';

// ignore: must_be_immutable
class DesignAppbarShowAllRateOfProduct extends StatelessWidget
    implements PreferredSizeWidget {
  Product product;
  DesignAppbarShowAllRateOfProduct({required this.product, Key? key})
      : super(key: key);
  @override
  Size get preferredSize => const Size.fromHeight(heighAppBarMd);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 0.3,
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: const Icon(
          Icons.arrow_back,
          color: Colors.black,
          size: 25,
        ),
      ),
      centerTitle: true,
      title: const Text(
        'ĐÁNH GIÁ',
        style: TextStyle(
          color: ColorCodes.textColorPrimary,
          fontSize: 20,
        ),
      ),
      actions: [
        Container(
          width: 70,
          margin: const EdgeInsets.fromLTRB(0, 6, 8, 6),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(3),
            color: Colors.white,
            boxShadow: const [
              BoxShadow(
                color: Color(0xFFF1F1FE),
                offset: Offset(0.0, 1.0),
                spreadRadius: 1,
                blurRadius: 5,
              ),
            ],
          ),
          child: CachedNetworkImage(
            imageUrl: Config.getIP(
                "/products/${product.productId}/${product.productImg}"),
            fit: BoxFit.cover,
            placeholder: (context, url) => const Center(
              child: CircularProgressIndicator(),
            ),
            errorWidget: (context, url, error) => const Icon(Icons.error),
          ),
        ),
      ],
    );
  }

  //Design Product Image
  Widget buildProductImage(String link) {
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 6, 8, 6),
      padding: const EdgeInsets.all(4),
      width: 45,
      height: 20,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: ExactAssetImage(
            link,
          ),
          fit: BoxFit.fill,
        ),
        borderRadius: BorderRadius.circular(6),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.2),
            spreadRadius: 1,
            blurRadius: 2,
            offset: const Offset(0, 2),
          )
        ],
      ),
    );
  }
}
