import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/config.dart';
import 'package:ptp_application/models/product.dart';
import 'package:ptp_application/models/rate.dart';
import 'package:ptp_application/providers/rate_provider.dart';
import 'package:ptp_application/custom.dart';
import 'components/design_appbar_show_all_rate_of_product.dart';

// ignore: must_be_immutable
class ShowAllRateOfProductScreen extends StatefulWidget {
  Product product;

  ShowAllRateOfProductScreen({required this.product, Key? key})
      : super(key: key);
  @override
  _ShowAllRateOfProductScreenState createState() =>
      _ShowAllRateOfProductScreenState();
}

class _ShowAllRateOfProductScreenState
    extends State<ShowAllRateOfProductScreen> {
  int isActive = 0;

  late RateProvider rateProvider;
  List<Rate> rates = [];
  @override
  void initState() {
    super.initState();
    rateProvider = Provider.of<RateProvider>(context, listen: false);
    rates = rateProvider.rates;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: DesignAppbarShowAllRateOfProduct(
          product: widget.product,
        ),
        body: Consumer<RateProvider>(
          builder: (context, rateProvider, child) {
            return Column(
              //padding: const EdgeInsets.all(5),
              children: [
                buildButtonOptionViewer(rateProvider),
                Container(
                  decoration: const BoxDecoration(
                    border: Border(
                      left: BorderSide(
                        color: Color(0xFFED453A),
                        width: 3.0,
                      ),
                      top: BorderSide(
                        color: Color(0xFFED453A),
                        width: 3.0,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: buildListRate(rates),
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  //Design List Button Option Viewer
  Widget buildButtonOptionViewer(RateProvider rateProvider) {
    return Container(
      color: const Color(0xFFC4C4C4).withOpacity(0.1),
      padding: const EdgeInsets.fromLTRB(5, 15, 5, 15),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              buildOptionNormal('Tất cả', rateProvider.rates, 0),
              buildOptionNormal(
                  'Có bình luận',
                  rateProvider.getAllRateByProductIdAndComment(
                      widget.product.productId),
                  -1),
            ],
          ),
          const SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              buildOptionStar(
                  _fiveStar(),
                  rateProvider.getAllRateByProductIdAndStar(
                      widget.product.productId, 5),
                  5),
              buildOptionStar(
                  _fourStar(),
                  rateProvider.getAllRateByProductIdAndStar(
                      widget.product.productId, 4),
                  4),
              buildOptionStar(
                  _threeStar(),
                  rateProvider.getAllRateByProductIdAndStar(
                      widget.product.productId, 3),
                  3),
              buildOptionStar(
                  _twoStar(),
                  rateProvider.getAllRateByProductIdAndStar(
                      widget.product.productId, 2),
                  2),
              buildOptionStar(
                  _oneStar(),
                  rateProvider.getAllRateByProductIdAndStar(
                      widget.product.productId, 1),
                  1),
            ],
          ),
        ],
      ),
    );
  }

  //Design option normal
  Widget buildOptionNormal(
      String text, List<Rate> ratesTmp, int isCheckCurrentIndex) {
    Color colorBorder = Colors.grey.withOpacity(0.12);
    Color colorBackground = Colors.grey.withOpacity(0.12);
    Color colorText = Colors.black;
    if (isActive == isCheckCurrentIndex) {
      colorBorder = Colors.red;
      colorBackground = Colors.white;
      colorText = Colors.red;
    }

    return Container(
      width: 185,
      decoration: BoxDecoration(
        border: Border.all(
          color: colorBorder,
        ),
        color: colorBackground,
        borderRadius: BorderRadius.circular(5),
      ),
      child: TextButton(
        style: TextButton.styleFrom(
          padding: const EdgeInsets.all(16.0),
          primary: colorText,
          textStyle: const TextStyle(
            fontSize: 14,
            color: ColorCodes.textColorSecondary,
          ),
        ),
        onPressed: () {
          setState(() {
            isActive = isCheckCurrentIndex;
            rates = ratesTmp;
          });
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(text),
            Text('(${ratesTmp.length})'),
          ],
        ),
      ),
    );
  }

  //Design Option Star
  Widget buildOptionStar(
      Widget numberStar, List<Rate> rateStar, int isCheckCurrentIndex) {
    Color colorBorder = Colors.grey.withOpacity(0.12);
    Color colorBackground = Colors.grey.withOpacity(0.12);
    Color colorText = Colors.black;
    if (isActive == isCheckCurrentIndex) {
      colorBorder = Colors.red;
      colorBackground = Colors.white;
      colorText = Colors.red;
    }
    return Container(
      width: 75,
      decoration: BoxDecoration(
        border: Border.all(
          color: colorBorder,
        ),
        color: colorBackground,
        borderRadius: BorderRadius.circular(5),
      ),
      child: TextButton(
        style: TextButton.styleFrom(
          padding: const EdgeInsets.all(16.0),
          primary: colorText,
          textStyle: const TextStyle(
            fontSize: 14,
            color: ColorCodes.textColorSecondary,
          ),
        ),
        onPressed: () {
          setState(() {
            isActive = isCheckCurrentIndex;
            rates = rateStar;
          });
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            numberStar,
            Text('(${rateStar.length})'),
          ],
        ),
      ),
    );
  }

//Design _oneStar
  Widget _oneStar() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const [Icon(Icons.star, size: 9, color: Colors.orange)],
    );
  }

//Design _twoStar
  Widget _twoStar() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const [
        Icon(Icons.star, size: 9, color: Colors.orange),
        Icon(Icons.star, size: 9, color: Colors.orange),
      ],
    );
  }

//Design _threeStar
  Widget _threeStar() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const [
        Icon(Icons.star, size: 9, color: Colors.orange),
        Icon(Icons.star, size: 9, color: Colors.orange),
        Icon(Icons.star, size: 9, color: Colors.orange),
      ],
    );
  }

//Design _fourStar
  Widget _fourStar() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const [
        Icon(Icons.star, size: 9, color: Colors.orange),
        Icon(Icons.star, size: 9, color: Colors.orange),
        Icon(Icons.star, size: 9, color: Colors.orange),
        Icon(Icons.star, size: 9, color: Colors.orange),
      ],
    );
  }

//Design _fiveStar
  Widget _fiveStar() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const [
        Icon(Icons.star, size: 8, color: Colors.orange),
        Icon(Icons.star, size: 8, color: Colors.orange),
        Icon(Icons.star, size: 8, color: Colors.orange),
        Icon(Icons.star, size: 8, color: Colors.orange),
        Icon(Icons.star, size: 8, color: Colors.orange),
      ],
    );
  }

  //Design List Rate
  Widget buildListRate(List<Rate> rates) {
    return Container(
      padding: const EdgeInsets.fromLTRB(15, 10, 15, 0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5),
        boxShadow: [
          BoxShadow(
            color: ColorCodes.colorPrimary.withOpacity(0.05),
            blurRadius: 15,
            spreadRadius: 3,
          ),
        ],
      ),
      child: rates.isEmpty
          ? const Center(
              child: Text('Không có đánh giá nào!'),
            )
          : ListView.builder(
              itemCount: rates.length,
              itemBuilder: (context, index) {
                return buildBoxChat(rates[index]);
              }),
    );
  }

  //Design Box Chat
  Widget buildBoxChat(Rate rate) {
    return Container(
      padding: const EdgeInsets.only(top: 2, bottom: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          buildItemLeftChat(rate),
          buildItemCenterChat(rate.member.user.fullName, rate.comment!),
          buildItemRightChat(context, rate),
        ],
      ),
    );
  }

//Design Star
  Widget buildStar(int star) {
    Widget buildStar = Container();
    if (star == 1) {
      buildStar = _oneStar();
    } else if (star == 2) {
      buildStar = _twoStar();
    } else if (star == 3) {
      buildStar = _threeStar();
    } else if (star == 4) {
      buildStar = _fourStar();
    } else if (star == 5) {
      buildStar = _fiveStar();
    }
    return buildStar;
  }

  //Design Item Left Chat
  Widget buildItemLeftChat(Rate rate) {
    return Column(children: [
      //Box Image
      ClipRRect(
        borderRadius: BorderRadius.circular(30.0),
        child: CachedNetworkImage(
          imageUrl: Config.getIP("/avatar_users/${rate.member.user.image}"),
          width: 50,
          height: 50,
          fit: BoxFit.cover,
          placeholder: (context, url) => const Center(
            child: CircularProgressIndicator(),
          ),
          errorWidget: (context, url, error) => const Icon(Icons.error),
        ),
      ),
      const SizedBox(height: 5),
      //Box Star
      buildStar(rate.star),
    ]);
  }

  //Design Item Cente Chat
  Widget buildItemCenterChat(String name, String text) {
    return Container(
      padding: const EdgeInsets.only(left: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //Name User
          Container(
            padding: const EdgeInsets.only(left: 2),
            alignment: Alignment.topLeft,
            height: 25,
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                name,
                style: const TextStyle(
                  overflow: TextOverflow.ellipsis,
                  letterSpacing: 0.5,
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                  color: ColorCodes.textColorPrimary,
                ),
              ),
            ),
          ),
          //Comment
          Container(
            width: 210,
            padding: const EdgeInsets.all(5.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: [
                BoxShadow(
                  color: const Color(0xFFF1F1FE).withOpacity(0.8),
                  blurRadius: 10,
                  spreadRadius: 2,
                ),
              ],
            ),
            // child: Flexible(
            child: Text(
              text,
              style: const TextStyle(
                  letterSpacing: 0.5,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  color: ColorCodes.textColorPrimary),
            ),
          ),
          // )
        ],
      ),
    );
  }

  //Design Item Right Chat
  Widget buildItemRightChat(BuildContext context, Rate rate) {
    return Column(
      children: [
        IconButton(
          onPressed: () {
            Provider.of<RateProvider>(context, listen: false)
                .likeRate(rate.rateId)
                .then((isLiked) {
              if (isLiked) {
                rate.like += 1;
              }
            });
          },
          icon: const Icon(MdiIcons.heartOutline),
          iconSize: 30,
          color: ColorCodes.pinkColor.withOpacity(0.8),
        ),
        //Button Number Like
        Text(
          '${rate.like}',
          style: const TextStyle(
            fontSize: 16,
            color: ColorCodes.textColorPrimary,
            fontWeight: FontWeight.w400,
          ),
        )
      ],
    );
  }
}
