import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ptp_application/custom.dart';

void showMessageDialog(context, String tilte, IconData? icon) {
  showDialog(
    // barrierDismissible: false,
    context: context,
    builder: (context) {
      return AlertDialog(
        title: const Text(
          "Thông Báo",
          textAlign: TextAlign.center,
          style: TextStyle(
            color: ColorCodes.colorPrimary,
            fontSize: 18,
            letterSpacing: 0.5,
            fontWeight: FontWeight.w500,
          ),
        ),
        actions: [
          Container(
            padding: const EdgeInsets.only(
              left: 10,
              right: 10,
            ),
            alignment: Alignment.center,
            child: Column(
              children: [
                const SizedBox(height: 10),
                Text(
                  tilte,
                  style: const TextStyle(
                      color: ColorCodes.colorPrimary, fontSize: 16),
                ),
                const SizedBox(height: 10),
                // IconButton(
                //   icon: Icon(icon),
                //   onPressed: () {
                //     dismissDialog(context);
                //   },
                //   iconSize: 30,
                //   color: ColorCodes.googleColor,
                // ),
                Icon(
                  icon,
                  size: 35,
                  color: ColorCodes.googleColor,
                ),
                const SizedBox(height: 10),
              ],
            ),
          ),
        ],
      );
    },
  );
}

void showMessageDialogSuccess(context, String tilte, IconData? icon) {
  showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: const Text(
          "Thông Báo",
          textAlign: TextAlign.center,
          style: TextStyle(
            color: ColorCodes.colorPrimary,
            fontSize: 18,
            letterSpacing: 0.5,
            fontWeight: FontWeight.w500,
          ),
        ),
        actions: [
          Container(
            padding: const EdgeInsets.only(
              left: 10,
              right: 10,
            ),
            alignment: Alignment.center,
            child: Column(
              children: [
                const SizedBox(height: 10),
                Text(
                  tilte,
                  style: const TextStyle(
                    color: ColorCodes.colorPrimary,
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                const SizedBox(height: 10),
                Icon(
                  icon,
                  size: 35,
                  color: Colors.greenAccent,
                ),
                const SizedBox(height: 10),
              ],
            ),
          ),
        ],
      );
    },
  );
}

void showDialogLoading(context) {
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (context) {
      return AlertDialog(
        title: const Text(
          "Đang thực thi",
          style: TextStyle(
            color: ColorCodes.colorPrimary,
            fontSize: 18,
            letterSpacing: 0.5,
            fontWeight: FontWeight.w600,
          ),
        ),
        actions: [
          Container(
            alignment: Alignment.center,
            child: Column(
              children: const [
                Text(
                  "Loading...",
                  style: TextStyle(
                      color: ColorCodes.colorPrimary,
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 1),
                ),
                SizedBox(
                  height: 10,
                ),
                CircularProgressIndicator(),
              ],
            ),
          ),
        ],
      );
    },
  );
}

void showDialogLoadingSuccessfully(context, String message) {
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (context) {
      return AlertDialog(
        title: Text(
          message,
          textAlign: TextAlign.center,
          style: const TextStyle(
            color: ColorCodes.colorPrimary,
            fontSize: 18,
            letterSpacing: 0.5,
            fontWeight: FontWeight.w500,
          ),
        ),
        actions: [
          Container(
            alignment: Alignment.center,
            child: Column(
              children: const [
                Icon(
                  Icons.verified,
                  size: 50,
                  color: Colors.greenAccent,
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  "Loading...",
                  style: TextStyle(
                    color: ColorCodes.colorPrimary,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    letterSpacing: 1,
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                CircularProgressIndicator(),
              ],
            ),
          ),
        ],
      );
    },
  );
}

dismissDialog(context) {
  Navigator.pop(context);
}

String formatNumber(dynamic number) {
  NumberFormat formatter = NumberFormat('#,###', 'eu');
  return formatter.format(number);
}
