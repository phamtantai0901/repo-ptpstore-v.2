import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/models/bill.dart';
import 'package:ptp_application/models/bill_detail.dart';
import 'package:ptp_application/models/cart.dart';
import 'package:ptp_application/models/member.dart';
import 'package:ptp_application/models/zalo_order.dart';
import 'package:ptp_application/providers/bill_detail_provider.dart';
import 'package:ptp_application/providers/bill_provider.dart';
import 'package:ptp_application/providers/cart_provider.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/screens/screen_pay_successfully/pay_successfully.dart';
import 'package:ptp_application/screens/show_dialog.dart';

// ignore: must_be_immutable
class ZaloPayScreen extends StatefulWidget {
  ZaloOrder zaloOrder;
  Bill bill;
  List<Cart> carts;
  ZaloPayScreen(
      {required this.zaloOrder, required this.bill, required this.carts});

  @override
  _ZaloPayScreenState createState() => _ZaloPayScreenState();
}

class _ZaloPayScreenState extends State<ZaloPayScreen> {
  late Member member;
  // static const EventChannel eventChannel =
  //     EventChannel('flutter.native/eventPayOrder');
  static const MethodChannel platform =
      MethodChannel('flutter.native/channelPayOrder');

  @override
  void initState() {
    super.initState();
    // if (Platform.isIOS) {
    //   eventChannel.receiveBroadcastStream().listen(_onEvent, onError: _onError);
    // }
    member = Provider.of<MemberProvider>(context, listen: false).member;
  }

//   void _onEvent(dynamic event) {
//     print("_onEvent: '$event'.");
//     var res = Map<String, dynamic>.from(event);
//     setState(() {
//       if (res["errorCode"] == 1) {
//         payResult = "Thanh toán thành công";
//       } else if (res["errorCode"] == 4) {
//         payResult = "User hủy thanh toán";
//       } else {
//         payResult = "Giao dịch thất bại";
//       }
//     });
//   }

//   void _onError(Object error) {
//     print("_onError: '$error'.");
//     setState(() {
//       payResult = "Giao dịch thất bại";
//     });
//   }
  Future<void> methodPay(Bill bill, List<Cart> carts) async {
    Provider.of<BillProvider>(context, listen: false)
        .saveBill(bill)
        .then((billTmp) {
      if (billTmp != null) {
        // ignore: avoid_function_literals_in_foreach_calls
        carts.forEach(
          (cart) async {
            if (cart.isChoosed) {
              BillDetail billDetail = BillDetail(
                productDetail: cart.productDetail,
                bill: billTmp,
                quantity: cart.quantity,
                price: cart.pricePay,
                priceDiscount: 0,
              );
              await Provider.of<BillDetailProvider>(context, listen: false)
                  .saveBillDetail(billDetail);
              print("Done");
            }
            //   .catchError((error) {
            // return showMessageDialog(context, error, Icons.error);
          },
        );

        dismissDialog(context);
        showDialogLoadingSuccessfully(context, "Thanh Toán Thành Công");
        Provider.of<CartProvider>(context, listen: false)
            .removeAllCartByIdMember(member.memberId!)
            .then((isRemoved) {
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => const PaySuccessfullyScreen(),
              ),
              (route) => false);
        });
      } else {
        dismissDialog(context);
        showMessageDialog(context, "Thanh Toán Thất Bại", Icons.error);
        Future.delayed(
          const Duration(seconds: 2),
          () => dismissDialog(context),
        );
      }
    }).catchError((error) {
      dismissDialog(context);
      showMessageDialog(context, error, Icons.error);
      Future.delayed(
        const Duration(seconds: 2),
        () => dismissDialog(context),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    double totalPrice = widget.bill.totalPrice;
    if (widget.bill.voucher != null) {
      if (totalPrice * widget.bill.voucher!.saleOff >=
          widget.bill.voucher!.maxPrice) {
        totalPrice -= widget.bill.voucher!.maxPrice.toDouble();
      } else {
        totalPrice -= (totalPrice * widget.bill.voucher!.saleOff);
      }
    }
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(colors: <Color>[
                ColorCodes.colorPrimary,
                const Color(0xFF26cba3).withOpacity(0.9),
                ColorCodes.colorPrimary,
              ], begin: Alignment.centerLeft, end: Alignment.centerRight),
            ),
          ),
          elevation: 0,
          // backgroundColor: Colors.transparent,
          title: const Text(
            "THANH TOÁN BẰNG ZALO PAY",
            style: TextStyle(fontSize: 16),
          ),
        ),
        body: SafeArea(
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.only(top: 20, left: 10, right: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "Số tiền phải trả:",
                      style: TextStyle(fontSize: 16),
                    ),
                    Text(
                      'đ ${formatNumber(totalPrice)}',
                      style: const TextStyle(
                        color: ColorCodes.pinkColor,
                        fontSize: 20,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Text(
                      "Hình thức:",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    Text(
                      'Thanh toán bằng Zalo Pay',
                      style: TextStyle(
                        color: ColorCodes.textColorSecondary,
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            try {
              showDialogLoading(context);
              await platform.invokeMethod(
                  'payOrder', {"zptoken": widget.zaloOrder.zptranstoken}).then(
                (value) async => {
                  if (value == 'Payment Success')
                    {
                      await methodPay(widget.bill, widget.carts),
                    }
                  else
                    {
                      dismissDialog(context),
                      showMessageDialog(
                          context, value.toString(), Icons.cancel_outlined),
                    },
                },
              );
            } on PlatformException catch (e) {
              print("Failed to Invoke: '${e.message}'.");
            }
          },
          backgroundColor: Colors.green,
          child: const Icon(Icons.add_shopping_cart_outlined),
        ),
      ),
    );
  }
}
