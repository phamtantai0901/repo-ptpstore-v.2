import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/providers/notification_provider.dart';

import 'package:ptp_application/custom.dart';

class DesignAppbarNotification extends StatelessWidget
    implements PreferredSizeWidget {
  const DesignAppbarNotification({Key? key}) : super(key: key);

  @override
  Size get preferredSize => const Size.fromHeight(heighAppBarMd);
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      toolbarHeight: 70,
      elevation: 0,
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: const Icon(
          Icons.arrow_back_outlined,
          color: ColorCodes.textColorSecondary,
          size: 30,
        ),
      ),
      title: const Text(
        'THÔNG BÁO',
        textAlign: TextAlign.center,
        style: TextStyle(
            color: ColorCodes.textColorPrimary,
            fontSize: FontSize.fontSizeAppBar,
            fontWeight: FontWeight.w600),
      ),
      centerTitle: true,
      actions: [
        IconButton(
          onPressed: () {
            String memberId =
                Provider.of<MemberProvider>(context, listen: false)
                    .member
                    .memberId!;
            Provider.of<NotificationProvider>(context, listen: false)
                .updateAllStatusNotification(memberId);
          },
          icon: const Icon(
            Icons.done_all_outlined,
            color: ColorCodes.facebookColor,
          ),
        ),
      ],
    );
  }
}
