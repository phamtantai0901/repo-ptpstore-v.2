import 'package:flutter/material.dart';
import 'package:ptp_application/screens/screen_change_password/change_password.dart';
import 'package:ptp_application/screens/screen_profile/components/design_showdialog_profile.dart';
import 'package:ptp_application/custom.dart';

//Design Button Username
Widget designButtonUsername(
    BuildContext context, String title, TextEditingController fullName) {
  return Container(
    decoration: BoxDecoration(
      border: Border(
        bottom: BorderSide(color: Colors.grey.shade100, width: 2),
      ),
      color: Colors.white,
    ),
    child: TextButton(
      onPressed: () {
        showDialogChange(context, "Tên", "Nhập họ và tên của bạn", fullName, 0);
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            padding: const EdgeInsets.only(left: 20),
            child: Text(
              title,
              style: const TextStyle(color: Colors.black),
            ),
          ),
          Row(
            children: [
              Text(
                fullName.text,
                style: const TextStyle(color: ColorCodes.textColorSecondary),
              ),
              const Icon(Icons.chevron_right_outlined, color: Colors.black),
            ],
          )
        ],
      ),
    ),
  );
}

//Design Button Change Password
Widget designButtonChangePassword(
    BuildContext context, String title, String infor, Widget widget) {
  return Container(
    decoration: BoxDecoration(
      border: Border(
        bottom: BorderSide(color: Colors.grey.shade100, width: 2),
      ),
      color: Colors.white,
    ),
    child: TextButton(
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => const ChangePasswordScreen(),
          ),
        );
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            padding: const EdgeInsets.only(left: 20),
            child: Text(
              title,
              style: const TextStyle(color: Colors.black),
            ),
          ),
          Row(
            children: [
              Text(
                infor,
                style: const TextStyle(color: ColorCodes.textColorSecondary),
              ),
              const Icon(Icons.chevron_right_outlined, color: Colors.black),
            ],
          )
        ],
      ),
    ),
  );
}

//Design Button Email
Widget designButtonEmail(
    BuildContext context, String title, TextEditingController email) {
  return Container(
    decoration: BoxDecoration(
      border: Border(
        bottom: BorderSide(color: Colors.grey.shade100, width: 2),
      ),
      color: Colors.white,
    ),
    child: TextButton(
      onPressed: () {
        showDialogChange(context, "Email", "Nhập email của bạn", email, 1);
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            padding: const EdgeInsets.only(left: 20),
            child: Text(
              title,
              style: const TextStyle(color: Colors.black),
            ),
          ),
          Row(
            children: [
              Text(
                email.text,
                style: const TextStyle(color: ColorCodes.textColorSecondary),
              ),
              const Icon(Icons.chevron_right_outlined, color: Colors.black),
            ],
          )
        ],
      ),
    ),
  );
}

//Design Button PhoneNumber
Widget designButtonPhoneNumber(
    BuildContext context, String title, TextEditingController phoneNumber) {
  return Container(
    decoration: BoxDecoration(
      border: Border(
        bottom: BorderSide(color: Colors.grey.shade100, width: 2),
      ),
      color: Colors.white,
    ),
    child: SizedBox(
      height: 50,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            padding: const EdgeInsets.only(left: 28),
            child: Text(
              title,
              style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(right: 20),
            child: Row(
              children: [
                Text(
                  phoneNumber.text,
                  style: const TextStyle(
                    color: ColorCodes.textColorSecondary,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    ),
  );
}

Widget designButtonGender(
    BuildContext context, String title, TextEditingController gender) {
  return Container(
    decoration: BoxDecoration(
      border: Border(
        bottom: BorderSide(color: Colors.grey.shade100, width: 2),
      ),
      color: Colors.white,
    ),
    child: TextButton(
      onPressed: () {
        showDialogChange(context, "Giới tính", "Nhập giới tính", gender, null);
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            padding: const EdgeInsets.only(left: 20),
            child: Text(
              title,
              style: const TextStyle(color: Colors.black),
            ),
          ),
          // ignore: avoid_unnecessary_containers
          Container(
            child: Row(
              children: [
                Text(
                  gender.text,
                  style: const TextStyle(color: ColorCodes.textColorSecondary),
                ),
                const Icon(Icons.chevron_right_outlined, color: Colors.black),
              ],
            ),
          )
        ],
      ),
    ),
  );
}

//Design Button BirthDay
Widget designButtonBirthDay(
    BuildContext context, String title, TextEditingController birthDay) {
  return Container(
    decoration: BoxDecoration(
      border: Border(
        bottom: BorderSide(color: Colors.grey.shade100, width: 2),
      ),
      color: Colors.white,
    ),
    child: TextButton(
      onPressed: () {
        showDialogChange(
            context, "Ngày sinh", "Nhập ngày sinh của bạn", birthDay, 2);
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            padding: const EdgeInsets.only(left: 20),
            child: Text(
              title,
              style: const TextStyle(color: Colors.black),
            ),
          ),
          Row(
            children: [
              Text(
                birthDay.text,
                style: const TextStyle(color: ColorCodes.textColorSecondary),
              ),
              const Icon(Icons.chevron_right_outlined, color: Colors.black),
            ],
          )
        ],
      ),
    ),
  );
}

Widget designButtonAddress(
    BuildContext context, String title, TextEditingController address) {
  return Container(
    decoration: BoxDecoration(
      border: Border(
        bottom: BorderSide(color: Colors.grey.shade100, width: 2),
      ),
      color: Colors.white,
    ),
    child: TextButton(
      onPressed: () {
        showDialogChange(
            context, "Địa chỉ", "Nhập địa chỉ của bạn", address, 3);
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            padding: const EdgeInsets.only(left: 20),
            child: Text(
              title,
              style: const TextStyle(color: Colors.black),
            ),
          ),
          // ignore: avoid_unnecessary_containers
          Container(
            child: Row(
              children: [
                Text(
                  address.text,
                  style: const TextStyle(color: ColorCodes.textColorSecondary),
                ),
                const Icon(Icons.chevron_right_outlined, color: Colors.black),
              ],
            ),
          )
        ],
      ),
    ),
  );
}
