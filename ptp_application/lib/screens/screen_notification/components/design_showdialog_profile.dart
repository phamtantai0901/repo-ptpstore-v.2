import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/models/member.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/screens/show_dialog.dart';

//Design ShowDialog
showDialogChange(BuildContext context, String title, String hintText,
    TextEditingController value, int? isChecked) {
  return showDialog(
    context: context,
    barrierDismissible: false,
    builder: (context) {
      return BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 2, sigmaY: 2),
          child: AlertDialog(
            insetPadding: const EdgeInsets.symmetric(vertical: 10),
            title: Text(title),
            content: TextField(
              autofocus: true,
              controller: value,
              decoration: InputDecoration(hintText: hintText),
            ),
            actions: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  buildButtonCancel(context, value),
                  buildButtonSubmit(context, value, isChecked),
                ],
              )
            ],
          ));
    },
  );
}

//Design Button Reset
Widget buildButtonCancel(BuildContext context, TextEditingController value) {
  return Container(
    margin: const EdgeInsets.only(
      left: 10,
      right: 10,
    ),
    width: 120,
    height: 45,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      gradient: LinearGradient(
        colors: <Color>[
          const Color(0xFF6DCFCF).withOpacity(0.4),
          ColorCodes.colorPrimary,
        ],
      ),
    ),
    child: TextButton(
      child: const Text(
        "HUỶ",
        style: TextStyle(
          fontSize: 16,
          color: Colors.white,
          fontWeight: FontWeight.w600,
          letterSpacing: 0.5,
        ),
      ),
      onPressed: () {
        Navigator.pop(context);
        value.clear();
      },
    ),
  );
}

//Design Button Save
Widget buildButtonSubmit(
    BuildContext context, TextEditingController value, int? isChecked) {
  return Container(
    margin: const EdgeInsets.only(
      left: 10,
      right: 10,
    ),
    width: 120,
    height: 45,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      gradient: LinearGradient(
        colors: <Color>[
          const Color(0xFF6DFFCD).withOpacity(0.9),
          const Color(0xFF6DCFCF),
          ColorCodes.colorPrimary,
        ],
      ),
    ),
    child: TextButton(
      child: const Text(
        "LƯU",
        style: TextStyle(
          fontSize: 16,
          color: Colors.white,
          fontWeight: FontWeight.w600,
          letterSpacing: 0.5,
        ),
      ),
      onPressed: () async {
        if (isChecked != null) {
          Member member =
              Provider.of<MemberProvider>(context, listen: false).member;
          await Provider.of<MemberProvider>(context, listen: false)
              .updateInfor(member.user.username, value.text, isChecked)
              .then((element) {
            // ignore: unnecessary_null_comparison
            if (element != null) {
              dismissDialog(context);
            } else {
              showMessageDialog(context, "Cập nhật thất bại", Icons.error);
            }
          }).catchError((e) {
            showMessageDialog(context, e, Icons.error);
          });
        }
        return;
      },
    ),
  );
}
