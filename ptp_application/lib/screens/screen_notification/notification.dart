import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/models/notification.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/providers/notification_provider.dart';
import 'package:ptp_application/screens/screen_notification/components/design_appbar_notification.dart';

class ScreenNotification extends StatefulWidget {
  const ScreenNotification({Key? key}) : super(key: key);

  @override
  _ScreenNotificationState createState() => _ScreenNotificationState();
}

class _ScreenNotificationState extends State<ScreenNotification> {
  String? memberId;
  @override
  void initState() {
    super.initState();
    memberId =
        Provider.of<MemberProvider>(context, listen: false).member.memberId!;
    Provider.of<NotificationProvider>(context, listen: false)
        .fetchAllNotification(memberId!);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DesignAppbarNotification(),
      body:
          // FutureBuilder<List<NotificationModel>>(
          //   future: Provider.of<NotificationProvider>(context, listen: false)
          //       .fetchAllNotification(memberId!),
          //   builder: (context, snapshot) {
          //     if (snapshot.connectionState == ConnectionState.waiting) {
          //       return Container(
          //         alignment: Alignment.center,
          //         child: Column(
          //           children: const [
          //             Text("Loading..."),
          //             SizedBox(
          //               height: 10,
          //             ),
          //             CircularProgressIndicator(),
          //           ],
          //         ),
          //       );
          //     } else {
          //       if (snapshot.hasData && snapshot.data!.isNotEmpty) {
          //         return ListView.builder(
          //           itemCount: snapshot.data!.length,
          //           itemBuilder: (context, index) {
          //             return buildNotification(snapshot.data![index]);
          //           },
          //         );
          //       } else if (snapshot.hasError) {
          //         return Text(snapshot.error.toString());
          //       } else {
          //         return const Center(
          //           child: Text(
          //             "Hiện bạn không có thông báo nào!!!",
          //             style: TextStyle(
          //               color: Colors.black,
          //               fontSize: 18,
          //               fontWeight: FontWeight.w600,
          //             ),
          //           ),
          //         );
          //       }
          //     }
          //   },
          // )
          Consumer<NotificationProvider>(
        builder: (context, notificationProvider, child) {
          return Container(
            alignment: Alignment.center,
            // padding: const EdgeInsets.fromLTRB(15, 10, 15, 0),
            child: notificationProvider.isLoading
                ? Column(
                    children: const [
                      Text(
                        "Loading...",
                        style: TextStyle(
                          fontSize: 16,
                          letterSpacing: 0.5,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      CircularProgressIndicator(),
                    ],
                  )
                : notificationProvider.notifications.isEmpty
                    ? const Center(
                        child: Text(
                          "Hiện bạn không có thông báo nào!!!",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      )
                    : ListView.builder(
                        itemCount: notificationProvider.notifications.length,
                        itemBuilder: (context, index) {
                          return buildNotification(
                              notificationProvider.notifications[index]);
                        },
                      ),
          );
        },
      ),
    );
  }

  //Design Divider
  Widget buildDivider() {
    return Container(
      margin: const EdgeInsets.only(top: 5, bottom: 5),
      child: const Divider(
          color: Colors.black, thickness: 0.6, indent: 0, endIndent: 0),
    );
  }

  Widget buildNotification(NotificationModel noti) {
    String date = noti.createdAt!.split(' ')[0];
    String hour = noti.createdAt!.split(' ')[1];
    return GestureDetector(
      onTap: () {
        if (noti.status == 1) {
          return;
        }
        Provider.of<NotificationProvider>(context, listen: false)
            .updateStatusNotification(noti);
      },
      child: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: const Color(0xFFF1F1F1).withOpacity(0.7),
              offset: const Offset(0.0, 1.0),
              spreadRadius: 2,
              blurRadius: 10,
            ),
          ],
        ),
        child: Container(
          padding: const EdgeInsets.fromLTRB(4, 8, 4, 8),
          decoration: BoxDecoration(
            color: const Color.fromARGB(255, 250, 250, 255),
            borderRadius: BorderRadius.circular(5),
            boxShadow: [
              BoxShadow(
                color: const Color(0xFFF1F1F1).withOpacity(0.1),
                blurRadius: 2,
                spreadRadius: 4,
              ),
            ],
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: Icon(
                  Icons.notifications_outlined,
                  size: 35,
                  color: ColorCodes.pinkColor.withOpacity(0.5),
                ),
              ),
              const SizedBox(width: 5),
              SizedBox(
                width: 250,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      date,
                      style: const TextStyle(
                        fontSize: 14,
                        color: ColorCodes.textColorPrimary,
                        fontWeight: FontWeight.w500,
                        overflow: TextOverflow.ellipsis,
                        letterSpacing: 0.5,
                      ),
                    ),
                    Row(
                      children: [
                        Text(
                          '${noti.title}',
                          style: const TextStyle(
                            fontSize: 16,
                            color: ColorCodes.pinkColor,
                            fontWeight: FontWeight.w500,
                            overflow: TextOverflow.ellipsis,
                            letterSpacing: 0.5,
                          ),
                        ),
                        const SizedBox(width: 15),
                        noti.status == 0
                            ? const Icon(
                                Icons.check_circle,
                                size: 14,
                                color: Colors.red,
                              )
                            : Container(),
                      ],
                    ),
                    const SizedBox(height: 5),
                    Text(
                      '${noti.body}',
                      overflow: TextOverflow.ellipsis,
                      maxLines: 3,
                      style: const TextStyle(
                        fontSize: 15,
                        color: ColorCodes.textColorPrimary,
                        fontWeight: FontWeight.w500,
                        overflow: TextOverflow.ellipsis,
                        letterSpacing: 0.5,
                      ),
                    ),
                  ],
                ),
              ),
              Text(
                hour,
                style: const TextStyle(
                  fontSize: 15,
                  color: ColorCodes.textColorPrimary,
                  fontWeight: FontWeight.w500,
                  overflow: TextOverflow.ellipsis,
                  letterSpacing: 0.5,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
