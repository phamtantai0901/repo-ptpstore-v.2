import 'package:flutter/material.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/main.dart';

// ignore: must_be_immutable
class ChatScreen extends StatefulWidget {
  final urlAvatar =
      "https://scontent.fsgn5-11.fna.fbcdn.net/v/t1.6435-1/p200x200/110228472_2378970292405894_1333643626732894777_n.jpg?_nc_cat=111&ccb=1-5&_nc_sid=7206a8&_nc_ohc=jkAqLO6aG3kAX99p4NC&_nc_ht=scontent.fsgn5-11.fna&oh=51ff4dcfc490de0d2b1a4ece7841962f&oe=61C36560";
  List<ChatMessage> messages = [
    ChatMessage(
        messageContent:
            "Nếu bạn có thằc mắc vấn đề gì về sản phâm của mình có thể liên lạc qua hotline: 0123456789, hoặc sẽ có nhân viên trực tiếp tư vấn cho, vui lòng đợi trong ít phút xin chân thành cảm ơn",
        messageType: "receiver"),
    ChatMessage(messageContent: "Hú hú", messageType: "receiver"),
    ChatMessage(
        messageContent:
            "Sản phẩm mình mua bên bạn nó bị lỗi là áo đã bị rách tay bên trái, mình rất không hài lòng vui lòng xử lý trường hợp này cho mình",
        messageType: "sender"),
    ChatMessage(messageContent: "OK.", messageType: "receiver"),
    ChatMessage(messageContent: "Gúttt", messageType: "sender"),
    ChatMessage(
        messageContent:
            "Nếu bạn có thằc mắc vấn đề gì về sản phâm của mình có thể liên lạc qua hotline: 0123456789, hoặc sẽ có nhân viên trực tiếp tư vấn cho, vui lòng đợi trong ít phút xin chân thành cảm ơn",
        messageType: "receiver"),
    ChatMessage(messageContent: "Hú hú", messageType: "receiver"),
    ChatMessage(
        messageContent:
            "Sản phẩm mình mua bên bạn nó bị lỗi là áo đã bị rách tay bên trái, mình rất không hài lòng vui lòng xử lý trường hợp này cho mình",
        messageType: "sender"),
    ChatMessage(messageContent: "OK.", messageType: "receiver"),
    ChatMessage(messageContent: "Gúttt", messageType: "sender"),
    ChatMessage(messageContent: "OK.", messageType: "receiver"),
    ChatMessage(messageContent: "Gúttt", messageType: "sender"),
  ];

  ChatScreen({Key? key}) : super(key: key);

  @override
  _ChatScreen createState() => _ChatScreen();
}

class _ChatScreen extends State<ChatScreen>
    with AutomaticKeepAliveClientMixin<ChatScreen> {
  final messageController = TextEditingController();
  @override
  bool get wantKeepAlive => true;
  @override
  Widget build(BuildContext context) {
    //Notice the super-call
    super.build(context);
    return RefreshIndicator(
      onRefresh: () async {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => BottomNavAppBar(currentIndex: 3)));
      },
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircleAvatar(
                backgroundImage: NetworkImage(widget.urlAvatar),
                maxRadius: 20,
              ),
              const SizedBox(
                width: 12,
              ),
              const Text(
                "HỖ TRỢ TRỰC TUYẾN",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: ColorCodes.textColorPrimary,
                    fontSize: FontSize.fontSizeAppBar,
                    fontWeight: FontWeight.w600),
              ),
            ],
          ),
        ),
        body: Column(
          children: [
            Expanded(
              child: ListView.builder(
                // reverse: true,
                itemCount: widget.messages.length,
                shrinkWrap: true,
                padding: const EdgeInsets.only(top: 10, bottom: 10),
                // physics: NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return buildMessage(index);
                },
              ),
            ),
            buildInputText(),
          ],
        ),
      ),
    );
  }

  //Design Border Message
  Row buildMessage(int index) {
    return Row(
      mainAxisAlignment: (widget.messages[index].messageType == "receiver"
          ? MainAxisAlignment.start
          : MainAxisAlignment.end),
      children: [
        (widget.messages[index].messageType == "receiver"
            ? const Padding(
                padding: EdgeInsets.only(left: 10),
                child: CircleAvatar(
                  backgroundImage: NetworkImage(
                      "https://scontent.fsgn5-11.fna.fbcdn.net/v/t1.6435-1/p200x200/110228472_2378970292405894_1333643626732894777_n.jpg?_nc_cat=111&ccb=1-5&_nc_sid=7206a8&_nc_ohc=jkAqLO6aG3kAX99p4NC&_nc_ht=scontent.fsgn5-11.fna&oh=51ff4dcfc490de0d2b1a4ece7841962f&oe=61C36560"),
                  maxRadius: 20,
                ),
              )
            : Container()),
        Container(
          width: 300,
          padding:
              const EdgeInsets.only(left: 25, right: 25, top: 10, bottom: 10),
          child: Align(
            alignment: (widget.messages[index].messageType == "receiver"
                ? Alignment.topLeft
                : Alignment.topRight),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: (widget.messages[index].messageType == "receiver"
                    ? Colors.grey.shade200
                    : Colors.blue[200]),
              ),
              padding: const EdgeInsets.all(15),
              child: Text(
                widget.messages[index].messageContent,
                style: const TextStyle(
                  fontSize: 16,
                  color: ColorCodes.textColorPrimary,
                  fontWeight: FontWeight.w500,
                  letterSpacing: 0.25,
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  //Design Input Text
  Align buildInputText() {
    return Align(
      alignment: Alignment.bottomLeft,
      child: Container(
        padding: const EdgeInsets.only(left: 10, bottom: 10, top: 10),
        height: 60,
        width: double.infinity,
        color: Colors.white,
        child: Row(
          children: <Widget>[
            GestureDetector(
              onTap: () {},
              child: Container(
                height: 30,
                width: 30,
                decoration: BoxDecoration(
                  color: Colors.lightBlue,
                  borderRadius: BorderRadius.circular(30),
                ),
                child: const Icon(
                  Icons.add,
                  color: Colors.white,
                  size: 20,
                ),
              ),
            ),
            const SizedBox(
              width: 15,
            ),
            Expanded(
              child: TextField(
                controller: messageController,
                decoration: const InputDecoration(
                    hintText: "Nhập nội dung...",
                    hintStyle: TextStyle(
                      color: ColorCodes.textColorSecondary,
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                    border: InputBorder.none),
              ),
            ),
            const SizedBox(
              width: 15,
            ),
            FloatingActionButton(
              onPressed: () {
                setState(() {
                  widget.messages.add(ChatMessage(
                      messageContent: (messageController.text),
                      messageType: "sender"));
                  messageController.clear();
                });
              },
              child: const Icon(
                Icons.send,
                color: Colors.white,
                size: 18,
              ),
              backgroundColor: Colors.blue,
              elevation: 0,
            ),
          ],
        ),
      ),
    );
  }
}

class ChatMessage {
  String messageContent;
  String messageType;
  ChatMessage({required this.messageContent, required this.messageType});
}
