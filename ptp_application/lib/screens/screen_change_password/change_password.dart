import 'package:flutter/material.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/screens/screen_change_password/components/design_appbar_change_password.dart';
import 'package:ptp_application/screens/screen_change_password/components/design_body_change_password.dart';

class ChangePasswordScreen extends StatelessWidget {
  const ChangePasswordScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          appBar: const DesignAppBarChangePassword(), //CUSTOM DESIGN
          resizeToAvoidBottomInset: false,
          body: Container(
            color: Colors.white,
            child: Stack(
              children: [
                Positioned(
                  top: 10,
                  child: Container(
                    padding: const EdgeInsets.all(20),
                    height: 470,
                    width: MediaQuery.of(context).size.width - 40,
                    margin: const EdgeInsets.symmetric(horizontal: 20),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(5),
                      boxShadow: [
                        BoxShadow(
                          color: ColorCodes.colorPrimary.withOpacity(0.1),
                          blurRadius: 10,
                          spreadRadius: 1,
                        ),
                      ],
                    ),
                    child: const DesignBodyChangePassword(), //CUSTOM DESIGN
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
