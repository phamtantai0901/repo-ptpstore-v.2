import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/models/member.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/screens/screen_login/login.dart';
import 'package:ptp_application/screens/show_dialog.dart';

class DesignBodyChangePassword extends StatefulWidget {
  const DesignBodyChangePassword({Key? key}) : super(key: key);

  @override
  _DesignBodyChangePasswordState createState() =>
      _DesignBodyChangePasswordState();
}

class _DesignBodyChangePasswordState extends State<DesignBodyChangePassword> {
  bool _passwordVisible = false;
  late TextEditingController password;
  late TextEditingController newPassword;
  late TextEditingController confirmNewPassword;
  late Member member;

  @override
  // ignore: must_call_super
  void initState() {
    _passwordVisible = false;
    password = TextEditingController();
    newPassword = TextEditingController();
    confirmNewPassword = TextEditingController();
    member = Provider.of<MemberProvider>(context, listen: false).member;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.only(top: 10),
          child: Column(
            children: [
              const SizedBox(
                height: 20,
              ),
              buildTextFieldPassword(password, Icons.lock_clock,
                  "Nhập mật khẩu cũ", "Mật khẩu cũ", true),
              const SizedBox(
                height: 10,
              ),
              buildTextFieldPassword(newPassword, Icons.lock_clock,
                  "Nhập mật khẩu mới", "Mật khẩu mới", true),
              const SizedBox(
                height: 10,
              ),
              buildTextFieldPassword(confirmNewPassword, Icons.lock,
                  "Nhập lại mật khẩu mới", "Xác nhận mật khẩu mới", true),
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
        buildButtonSend(),
      ],
    );
  }

  //Design TextField Password
  Widget buildTextFieldPassword(TextEditingController controller, IconData icon,
      String hintText, String labelText, bool isChecked) {
    return Container(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: TextFormField(
        controller: controller,
        cursorColor: ColorCodes.colorPrimary,
        keyboardType: isChecked ? TextInputType.text : TextInputType.number,
        obscureText: !_passwordVisible,
        decoration: InputDecoration(
          prefixIcon: Icon(
            icon,
            color: ColorCodes.colorPrimary,
          ),
          suffixIcon: IconButton(
            icon: Icon(
              _passwordVisible ? Icons.visibility : Icons.visibility_off,
              color: ColorCodes.colorPrimary,
            ),
            onPressed: () {
              setState(() {
                _passwordVisible = !_passwordVisible;
              });
            },
          ),
          enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: ColorCodes.textColorSecondary),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: ColorCodes.colorPrimary),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          contentPadding: const EdgeInsets.all(10),
          labelText: labelText,
          labelStyle: const TextStyle(fontSize: 16),
          hintText: hintText,
          hintStyle: const TextStyle(
            fontWeight: FontWeight.w500,
            letterSpacing: 0.5,
            fontSize: 13,
            color: ColorCodes.textColorSecondary,
          ),
        ),
      ),
    );
  }

  //Design Button Send
  Widget buildButtonSend() {
    return Container(
      width: double.maxFinite,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: <Color>[
            ColorCodes.colorPrimary,
            const Color(0xFF6DDDCD).withOpacity(0.8),
            const Color(0xFF6DCFCF),
          ],
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      child: TextButton(
        onPressed: () async {
          showDialogLoading(context);
          await Provider.of<MemberProvider>(context, listen: false)
              .authentication(member.user.username, password.text, false)
              .then((memberr) async {
            if (newPassword.text.isEmpty ||
                confirmNewPassword.text.isEmpty ||
                password.text.isEmpty) {
              dismissDialog(context);
              showMessageDialog(
                  context, "Vui lòng không để trống thông tin", Icons.error);
              Future.delayed(
                  const Duration(seconds: 2), () => dismissDialog(context));
            } else if (memberr == null) {
              dismissDialog(context);
              showMessageDialog(
                  context, "Mật khẩu cũ không hợp lệ", Icons.error);
              Future.delayed(
                  const Duration(seconds: 2), () => dismissDialog(context));
            } else if (newPassword.text != confirmNewPassword.text) {
              dismissDialog(context);
              showMessageDialog(
                  context, "Mật khẩu mới không khớp", Icons.error);
              Future.delayed(
                  const Duration(seconds: 2), () => dismissDialog(context));
            } else {
              if (member.status == -1) {
                showMessageDialog(context, "Tài khoản đã bị khoá", Icons.error);
                Future.delayed(
                    const Duration(seconds: 2), () => dismissDialog(context));
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const LoginScreen(),
                    ),
                    (route) => false);
                return;
              }
              await Provider.of<MemberProvider>(context, listen: false)
                  .changePassword(member.user.username, newPassword.text,
                      confirmNewPassword.text)
                  .then((ischangepassword) {
                if (ischangepassword == false) {
                  dismissDialog(context);
                  showMessageDialog(
                      context, "Đổi mật khẩu không thành công", Icons.error);
                  Future.delayed(
                      const Duration(seconds: 2), () => dismissDialog(context));
                  password.clear();
                  newPassword.clear();
                  confirmNewPassword.clear();
                } else {
                  dismissDialog(context);
                  showDialogLoadingSuccessfully(
                      context, "Đổi mật khẩu thành công");
                  Future.delayed(
                    const Duration(seconds: 1),
                    () {
                      dismissDialog(context);
                      Navigator.pop(context);
                    },
                  );
                }
              });
            }
          }).catchError((error) {
            dismissDialog(context);
            showMessageDialog(context, error, Icons.check_circle_outlined);
            Future.delayed(
                const Duration(seconds: 2), () => dismissDialog(context));
          });
        },
        child: const Text(
          'Xác nhận thay đổi',
          style: TextStyle(
            letterSpacing: 1,
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }
}
