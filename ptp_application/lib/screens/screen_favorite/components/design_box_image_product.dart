import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:ptp_application/config.dart';
import 'package:ptp_application/models/favourite.dart';

//Design Box Image Product
Widget buildBoxImageProduct(Favourite favourite) {
  {
    // ignore: unnecessary_null_comparison
    return favourite == null
        ? Container(
            alignment: Alignment.center,
            child: Column(
              children: const [
                Text(
                  "Loading...",
                  style: TextStyle(
                    fontSize: 16,
                    letterSpacing: 0.5,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                CircularProgressIndicator(),
              ],
            ),
          )
        : SizedBox(
            width: 120,
            height: 120,
            child: CachedNetworkImage(
              imageUrl: Config.getIP(
                  '/products/${favourite.productDetail.product.productId}/${favourite.productDetail.product.productImg}'),
              placeholder: (context, url) => const Center(
                child: CircularProgressIndicator(),
              ),
              errorWidget: (context, url, error) => const Icon(Icons.error),
            ),
          );
  }
}
