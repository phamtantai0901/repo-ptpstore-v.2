import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/main.dart';
import 'package:ptp_application/models/cart.dart';
import 'package:ptp_application/models/favourite.dart';
import 'package:ptp_application/models/member.dart';
import 'package:ptp_application/models/product.dart';
import 'package:ptp_application/models/stock_detail.dart';
import 'package:ptp_application/providers/cart_provider.dart';
import 'package:ptp_application/providers/favourite_provider.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/providers/product_detail_provider.dart';
import 'package:ptp_application/providers/stock_detail_provider.dart';
import 'package:ptp_application/screens/screen_cart/cart.dart';
import 'package:ptp_application/screens/screen_product_detail/product_detail.dart';
import 'package:ptp_application/screens/show_dialog.dart';

import 'design_box_image_product.dart';

class DesignBodyFavouriteList extends StatefulWidget {
  const DesignBodyFavouriteList({Key? key}) : super(key: key);

  @override
  _DesignBodyFavouriteListState createState() =>
      _DesignBodyFavouriteListState();
}

class _DesignBodyFavouriteListState extends State<DesignBodyFavouriteList> {
  late FavouriteProvider favouriteProvider;
  late Member member;
  @override
  void initState() {
    super.initState();
    member = Provider.of<MemberProvider>(context, listen: false).member;
    favouriteProvider = Provider.of<FavouriteProvider>(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      const SizedBox(height: 5),
      Expanded(
        child: buildBoxProductFavoritesList(context),
      ),
      const SizedBox(height: 20),
      buildButtonBackHome(context),
      const SizedBox(height: 15),
    ]);
  }

  //Design Box Product Favorites List
  Widget buildBoxProductFavoritesList(BuildContext context) {
    return FutureBuilder<List<Favourite>>(
        future: favouriteProvider.fetchAllFavouriteByIdMember(member.memberId!),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Container(
              // padding: const EdgeInsets.fromLTRB(15, 10, 15, 0),
              child: snapshot.data!.isEmpty
                  ? const Center(
                      child: Text(
                        "Danh sách yêu thích trống",
                        style: TextStyle(
                          color: ColorCodes.textColorSecondary,
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    )
                  : ListView.builder(
                      padding: const EdgeInsets.all(5),
                      itemCount: snapshot.data!.length,
                      itemBuilder: (context, index) {
                        return buildBoxProductFavorites(snapshot.data![index]);
                      }),
            );
          } else if (snapshot.hasError) {
            return Center(child: Text(snapshot.error.toString()));
          } else {
            return Container(
              alignment: Alignment.center,
              child: Column(
                children: const [
                  Text(
                    "Loading...",
                    style: TextStyle(
                      fontSize: 16,
                      letterSpacing: 0.5,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  CircularProgressIndicator(),
                ],
              ),
            );
          }
        });
  }

  //Dessign Box Product Favorites
  Widget buildBoxProductFavorites(Favourite favourite) {
    Product product = favourite.productDetail.product;
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ProductDetailScreen(
                      productId: product.productId,
                    )));
      },
      child: Container(
        margin: const EdgeInsets.only(bottom: 10),
        width: MediaQuery.of(context).size.width - 50,
        decoration: BoxDecoration(
          color: const Color(0xFFF1F1FE),
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(
              color: const Color(0xFFF1F1F1).withOpacity(0.1),
              blurRadius: 2,
              spreadRadius: 4,
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const SizedBox(width: 20),
            buildBoxImageProduct(favourite),
            const SizedBox(width: 15),
            buildBoxItemFavoritesList(favourite),
          ],
        ),
      ),
    );
  }

//Design Box Product Favorites List
  Widget buildBoxItemFavoritesList(Favourite favourite) {
    // ignore: avoid_unnecessary_containers
    return Container(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: 200,
              child: Text(
                favourite.productDetail.product.productName,
                maxLines: 2,
                style: const TextStyle(
                  overflow: TextOverflow.ellipsis,
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: ColorCodes.textColorSecondary,
                  letterSpacing: 0.5,
                ),
              ),
            ),
            const SizedBox(width: 5),
            Text(
              'Màu: (${favourite.productDetail.color.colorName})',
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w600,
                color: ColorCodes.textColorSecondary,
                letterSpacing: 0.5,
              ),
            ),
            const SizedBox(width: 5),
            Text(
              'Kích thước: (${favourite.productDetail.size.sizeName})',
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w600,
                color: ColorCodes.textColorSecondary,
                letterSpacing: 0.5,
              ),
            ),
            const SizedBox(height: 10),
            Row(
              children: [
                IconButton(
                  onPressed: () async {
                    showDialogLoading(context);
                    await favouriteProvider
                        .removeFavourite(member.memberId!,
                            favourite.productDetail.productDetailId)
                        .then((value) {
                      if (value == true) {
                        dismissDialog(context);
                        showMessageDialog(context,
                            "Xoá thất bại, vui lòng thử lại sau", Icons.error);
                        Future.delayed(const Duration(seconds: 2),
                            () => dismissDialog(context));
                      } else {
                        dismissDialog(context);
                        setState(() {});
                      }
                    }).catchError((e) {
                      dismissDialog(context);
                      showMessageDialog(context, e.toString(), Icons.error);
                      Future.delayed(const Duration(seconds: 2),
                          () => dismissDialog(context));
                    });
                  },
                  icon: const Icon(MdiIcons.heart),
                  iconSize: 32,
                  color: ColorCodes.pinkColor.withOpacity(0.8),
                ),
                const SizedBox(width: 25),
                Container(
                  width: 130,
                  height: 35,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: <Color>[
                        ColorCodes.colorPrimary,
                        const Color(0xFF6DCFCF),
                        const Color(0xFF6DDDCD).withOpacity(0.9),
                      ],
                    ),
                    borderRadius: const BorderRadius.only(
                      bottomRight: Radius.circular(12),
                      topLeft: Radius.circular(12),
                      topRight: Radius.circular(2),
                      bottomLeft: Radius.circular(2),
                    ),
                  ),
                  child: TextButton(
                    onPressed: () {
                      StockDetail? stockDetail = Provider.of<
                              StockDetailProvider>(context, listen: false)
                          .getFirstProductDetailInStockDetailByIdProductDetail(
                              favourite.productDetail.productDetailId);
                      Cart cart = Cart(
                        productDetail: favourite.productDetail,
                        member: member,
                        quantity: 1,
                        pricePay: stockDetail!.pricePay,
                      );
                      addProductIntoCart(
                          context, cart, stockDetail.quantity, true);
                    },
                    child: const Text(
                      'Mua Ngay',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        letterSpacing: 0.5,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ));
  }

//Design Button Back Home
  Widget buildButtonBackHome(context) {
    return Container(
      width: MediaQuery.of(context).size.width - 20,
      decoration: BoxDecoration(
        gradient: const LinearGradient(
          colors: <Color>[
            ColorCodes.colorPrimary,
            Color(0xFF6DCFCF),
            Color(0xFF6DFFCD),
          ],
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      child: Row(
        children: [
          TextButton(
            style: TextButton.styleFrom(
              padding: const EdgeInsets.all(16.0),
              primary: Colors.white,
              textStyle:
                  const TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
            ),
            onPressed: () {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                    builder: (ctx) => BottomNavAppBar(
                      currentIndex: 0,
                    ),
                  ),
                  (route) => false);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const [
                Text(
                  'Trở về trang chủ',
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    letterSpacing: 1,
                    fontWeight: FontWeight.w500,
                    fontSize: 22,
                  ),
                ),
                SizedBox(width: 100),
                Icon(
                  Icons.arrow_forward_ios,
                  size: 25,
                  color: Colors.white,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void addProductIntoCart(
      context, Cart cart, int quantityInStock, bool isCheck) {
    showDialogLoading(context);
    CartProvider cartProvider =
        Provider.of<CartProvider>(context, listen: false);
    cartProvider.addProductIntoCart(cart, quantityInStock).then((returnType) {
      if (returnType == 1) {
        dismissDialog(context);
        showDialogLoadingSuccessfully(context, "Thêm Giỏ Hàng Thành Công");
        Provider.of<ProductDetailProvider>(context, listen: false)
            .updateQuantityOrder(1);
        Future.delayed(const Duration(seconds: 1), () {
          dismissDialog(context);
          if (isCheck) {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => const CartScreen()));
          }
        });
      } else if (returnType == 0) {
        dismissDialog(context);
        showMessageDialog(context, "Thêm Giỏ Hàng Thất Bại", Icons.error);
        Future.delayed(
            const Duration(seconds: 1), () => dismissDialog(context));
      } else {
        dismissDialog(context);
        showMessageDialog(
            context,
            "Bạn không thể thêm số lượng đã chọn vào giỏ hàng vì sẽ vượt quá số lượng tồn của sản phẩm",
            Icons.error);
        Future.delayed(
            const Duration(seconds: 4), () => dismissDialog(context));
      }
    }).catchError((error) {
      dismissDialog(context);
      showMessageDialog(context, error, Icons.error);
      Future.delayed(const Duration(seconds: 1), () => dismissDialog(context));
    });
  }
}
