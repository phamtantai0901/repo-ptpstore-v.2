import 'package:flutter/material.dart';
import 'package:ptp_application/custom.dart';

class DesignAppbarFavouriteList extends StatelessWidget
    implements PreferredSizeWidget {
  const DesignAppbarFavouriteList({Key? key}) : super(key: key);

  @override
  Size get preferredSize => const Size.fromHeight(heighAppBarMd);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 0.5,
      centerTitle: true,
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: const Icon(Icons.arrow_back),
        iconSize: 25,
        color: ColorCodes.textColorSecondary,
      ),
      title: const Text(
        "DANH SÁCH YÊU THÍCH",
        textAlign: TextAlign.center,
        style: TextStyle(
            color: ColorCodes.textColorPrimary,
            fontSize: FontSize.fontSizeAppBar,
            fontWeight: FontWeight.w600),
      ),
    );
  }
}
