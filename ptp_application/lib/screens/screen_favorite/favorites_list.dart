import 'package:flutter/material.dart';
import 'package:ptp_application/screens/screen_favorite/components/design_appbar_favourite_list.dart';
import 'package:ptp_application/screens/screen_favorite/components/design_body_favourite_list.dart';

class FavoritesListScreen extends StatelessWidget {
  const FavoritesListScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: const Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: DesignAppbarFavouriteList(),
          body: DesignBodyFavouriteList(),
        ));
  }
}
