import 'package:flutter/material.dart';
import 'package:ptp_application/custom.dart';

Widget designUploadAvatar() {
  return Center(
    child: Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(100),
            color: ColorCodes.pinkColor,
            boxShadow: const [
              BoxShadow(
                color: Color.fromARGB(255, 222, 255, 249),
                offset: Offset(0.0, 1.0),
                spreadRadius: 5,
                blurRadius: 10,
              ),
            ],
          ),
          child: const CircleAvatar(
            backgroundColor: Colors.white,
            backgroundImage: AssetImage(
              "images/logo.png",
            ), //NetworkImage
            radius: 100,
          ),
        ),
        Positioned(
          bottom: 0,
          right: 0,
          child: FloatingActionButton(
            backgroundColor: Colors.white,
            onPressed: () {},
            tooltip: "Pick Image form gallery",
            child: const Icon(
              Icons.add_a_photo,
              color: ColorCodes.colorPrimary,
            ),
          ),
        ),
      ],
    ),
  );
}
