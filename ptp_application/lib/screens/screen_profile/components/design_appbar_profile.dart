import 'package:flutter/material.dart';
import 'package:ptp_application/main.dart';
import 'package:ptp_application/custom.dart';

class DesignAppbarProfile extends StatelessWidget
    implements PreferredSizeWidget {
  const DesignAppbarProfile({Key? key}) : super(key: key);

  @override
  Size get preferredSize => const Size.fromHeight(heighAppBarMd);
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      toolbarHeight: 70,
      elevation: 0,
      leading: IconButton(
        onPressed: () {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (ctx) => BottomNavAppBar(
                currentIndex: 4,
              ),
            ),
            (route) => false,
          );
        },
        icon: const Icon(
          Icons.arrow_back_outlined,
          color: ColorCodes.textColorSecondary,
          size: 30,
        ),
      ),
      title: const Text(
        'THÔNG TIN CÁ NHÂN',
        textAlign: TextAlign.center,
        style: TextStyle(
            color: ColorCodes.textColorPrimary,
            fontSize: FontSize.fontSizeAppBar,
            fontWeight: FontWeight.w600),
      ),
      centerTitle: true,
      actions: [
        IconButton(
          onPressed: () {},
          icon: const Icon(
            Icons.shopping_cart_outlined,
            color: ColorCodes.facebookColor,
            size: 30,
          ),
        ),
      ],
    );
  }
}
