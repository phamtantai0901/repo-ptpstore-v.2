import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/models/member.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/screens/screen_change_password/change_password.dart';
import 'package:ptp_application/screens/screen_profile/components/design_appbar_profile.dart';
import 'package:ptp_application/screens/screen_profile/components/design_upload_avatar.dart';
import 'components/design_button_feature.dart';

// ignore: must_be_immutable
class ProfileScreen extends StatelessWidget {
  ProfileScreen({Key? key}) : super(key: key);

  late TextEditingController fullName;
  late TextEditingController email;
  late TextEditingController phoneNumber;
  late TextEditingController gender;
  late TextEditingController birthDay;
  late TextEditingController address;

  void initTextEditingController(Member member) {
    fullName = TextEditingController(text: member.user.fullName);
    email = TextEditingController(text: member.user.email);
    gender = TextEditingController(text: "Nam");
    birthDay = TextEditingController(text: member.user.birthDay);
    address = TextEditingController(text: member.user.address);
    String str =
        member.user.phone.substring(0, member.user.phone.length - 3) + "****";
    phoneNumber = TextEditingController(text: str);
  }

  @override
  Widget build(BuildContext context) {
    Member member = Provider.of<MemberProvider>(context, listen: false).member;
    initTextEditingController(member);
    return Scaffold(
      backgroundColor: const Color(0xfff3ffff),
      appBar: const DesignAppbarProfile(),
      body: Consumer<MemberProvider>(builder: (context, memberProvider, child) {
        return ListView(
          children: [
            Container(
              padding: const EdgeInsets.only(
                top: 30,
                bottom: 40,
              ),
              color: ColorCodes.colorPrimary.withOpacity(0.3),
              child: designUploadAvatar(),
            ),
            designButtonUsername(context, 'Tên', fullName),
            designButtonChangePassword(
              context,
              'Đổi mật khẩu',
              '* * * * * * * *',
              const ChangePasswordScreen(),
            ),
            const SizedBox(
              height: 10,
            ),
            designButtonEmail(context, 'Email', email),
            designButtonPhoneNumber(context, 'Số điện thoại', phoneNumber),
            designButtonGender(context, 'Giới tính', gender),
            designButtonBirthDay(context, 'Ngày sinh', birthDay),
            designButtonAddress(context, 'Địa chỉ', address),
          ],
        );
      }),
    );
  }
}
