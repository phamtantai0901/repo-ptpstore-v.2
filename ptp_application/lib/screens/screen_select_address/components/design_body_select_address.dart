import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:ptp_application/custom.dart';

// ignore: must_be_immutable
class DesignBodySelectAddress extends StatefulWidget {
  const DesignBodySelectAddress({Key? key}) : super(key: key);

  @override
  _DesignBodyAddressState createState() => _DesignBodyAddressState();
}

class _DesignBodyAddressState extends State<DesignBodySelectAddress> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        buildBorderAddress("Đi Đâu Cũng Được", "0909999999",
            "567, Đường Trần Duy Hưng, Phường Phùng Hưng Đạo, Chí Thọ, Hoàng Mai"),
        buildBorderAddress("Thế Thì Đi Tù", "0909999999",
            "567, Đường Trần Duy Hưng, Phường Phùng Hưng Đạo, Chí Thọ, Hoàng Mai"),
        buildBorderAddress("Cơm Ngày 3 Bữa", "0909999999",
            "567, Đường Trần Duy Hưng, Phường Phùng Hưng Đạo, Chí Thọ, Hoàng Mai"),
        buildButtonAddAddress(),
      ],
    );
  }

  Widget buildBorderAddress(
      String reveicerName, String phoneNumber, String address) {
    return Container(
      width: double.maxFinite,
      margin: const EdgeInsets.only(bottom: 10, top: 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Colors.white,
        boxShadow: const [
          BoxShadow(
            color: Color(0xFFF1F1F1),
            offset: Offset(0.0, 1.0),
            spreadRadius: 2,
            blurRadius: 10,
          ),
        ],
      ),
      child: TextButton(
          onPressed: () {},
          child: Container(
            padding: const EdgeInsets.fromLTRB(18, 10, 18, 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  reveicerName,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  style: const TextStyle(
                    color: ColorCodes.textColorSecondary,
                    fontSize: 20,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Text(
                  phoneNumber,
                  style: const TextStyle(
                    color: ColorCodes.textColorSecondary,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Text(
                  address,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 3,
                  style: const TextStyle(
                    color: ColorCodes.textColorSecondary,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                  ),
                )
              ],
            ),
          )),
    );
  }

  Widget buildButtonAddAddress() {
    return Container(
      width: double.maxFinite,
      margin: const EdgeInsets.only(bottom: 2, top: 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Colors.white,
        boxShadow: const [
          BoxShadow(
            color: Color(0xFFF1F1F1),
            offset: Offset(0.0, 1.0),
            spreadRadius: 2,
            blurRadius: 10,
          ),
        ],
      ),
      child: TextButton(
        onPressed: () {},
        child: Container(
          padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              Text(
                "Thêm địa chỉ mới",
                style: TextStyle(
                  color: ColorCodes.textColorPrimary,
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                ),
              ),
              Icon(
                MdiIcons.plus,
                color: ColorCodes.textColorSecondary,
                size: 32,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
