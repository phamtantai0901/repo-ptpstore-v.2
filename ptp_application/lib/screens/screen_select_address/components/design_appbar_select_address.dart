import 'package:flutter/material.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/screens/screen_address2/address2.dart';

class DesignAppBarSelectAddress extends StatelessWidget
    implements PreferredSizeWidget {
  const DesignAppBarSelectAddress({Key? key}) : super(key: key);
  @override
  Size get preferredSize => const Size.fromHeight(heighAppBarMd);

  @override
  AppBar build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 0.3,
      leading: IconButton(
        onPressed: () {
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) => AddressScreen2()));
        },
        icon: const Icon(Icons.arrow_back),
        color: ColorCodes.textColorSecondary,
        iconSize: 25,
      ),
      centerTitle: true,
      title: const Text(
        'ĐỊA CHỈ GIAO HÀNG',
        textAlign: TextAlign.center,
        style: TextStyle(
            color: ColorCodes.textColorPrimary,
            fontSize: FontSize.fontSizeAppBar,
            fontWeight: FontWeight.w600),
      ),
    );
  }
}
