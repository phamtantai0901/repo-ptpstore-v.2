import 'package:flutter/material.dart';
import 'package:ptp_application/models/voucher.dart';
import 'package:ptp_application/screens/screen_select_address/components/design_appbar_select_address.dart';
import 'package:ptp_application/screens/screen_select_address/components/design_body_select_address.dart';

//ignore: must_be_immutable
class SelectAddressScreen extends StatelessWidget {
  Voucher? voucher;
  SelectAddressScreen({this.voucher, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: const DesignAppBarSelectAddress(), //CUSTOM DESIGN
        body: Container(
          decoration: const BoxDecoration(
            color: Color(0xFFFFFFFF),
          ),
          padding: const EdgeInsets.only(top: 10),
          child: const DesignBodySelectAddress(),
        ),
      ),
    );
  }
}
