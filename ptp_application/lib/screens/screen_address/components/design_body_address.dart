import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/models/bill.dart';
import 'package:ptp_application/models/bill_detail.dart';
import 'package:ptp_application/models/cart.dart';
import 'package:ptp_application/models/member.dart';
import 'package:ptp_application/models/voucher.dart';
import 'package:ptp_application/providers/bill_detail_provider.dart';
import 'package:ptp_application/providers/bill_provider.dart';
import 'package:ptp_application/providers/cart_provider.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/providers/zalo_provider.dart';
import 'package:ptp_application/screens/screen_address2/address2.dart';
import 'package:ptp_application/screens/screen_pay_successfully/pay_successfully.dart';
import 'package:ptp_application/screens/screen_zalo_pay/zalo_pay.dart';
import 'package:ptp_application/screens/show_dialog.dart';

// ignore: must_be_immutable
class DesignBodyAddress extends StatefulWidget {
  Voucher? voucher;
  DesignBodyAddress({this.voucher, Key? key}) : super(key: key);

  @override
  _DesignBodyAddressState createState() => _DesignBodyAddressState();
}

class _DesignBodyAddressState extends State<DesignBodyAddress> {
  final textFieldController = TextEditingController();
  List<String> itemDropDownAddress = [
    'TP HCM',
    'Đồng Tháp',
    'Tiền Giang',
    'Hà Nội'
  ];
  List itemPayment = [
    {
      'name': 'Thanh toán khi nhận hàng',
      'value': 0,
    },
    {
      'name': 'Thanh toán bằng ZaloPay',
      'value': 1,
    }
  ];

  late String? dropdownAddress;
  int dropdownPayment = 0;

  late TextEditingController nameReceive;
  late TextEditingController houseNumber;
  late TextEditingController district;
  late TextEditingController ward;
  late TextEditingController shippingPhone;
  late TextEditingController shippingAddress;
  late Member member;

  bool isUpdated = true;
  bool isUpdateSelectPayment = true;

  @override
  void initState() {
    super.initState();
    member = Provider.of<MemberProvider>(context, listen: false).member;
    nameReceive = TextEditingController(text: member.user.fullName);
    houseNumber = TextEditingController();
    district = TextEditingController();
    ward = TextEditingController();
    shippingAddress = TextEditingController(text: member.user.address);
    dropdownAddress = itemDropDownAddress[0];
    shippingPhone = TextEditingController(text: member.user.phone);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(25, 5, 25, 0),
      child: ListView(
        children: [
          const SizedBox(height: 2),
          buildButtonSelectAddress(),
          const SizedBox(height: 5),
          buildLabelText('Tên người nhận *:'),
          const SizedBox(height: 2),
          buildInputTextField("Nhập tên người nhận", nameReceive, true),
          const SizedBox(height: 10),
          buildLabelText('Số nhà - Đường *:'),
          const SizedBox(height: 2),
          buildInputTextField("Nhập số nhà và đường", houseNumber, true),
          const SizedBox(height: 10),
          buildLabelText('Phường/Xã *:'),
          const SizedBox(height: 2),
          buildInputTextField("Nhập phường/ xã", ward, true),
          const SizedBox(height: 10),
          buildLabelText('Quận/Huyện *:'),
          const SizedBox(height: 2),
          buildInputTextField("Nhập quận/ huyện", district, true),
          const SizedBox(height: 10),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                buildLabelText('Nhập số điện thoại *:'),
                const SizedBox(height: 2),
                SizedBox(
                  width: 190,
                  child: buildInputTextField(
                      "Nhập số điện thoại người nhận", shippingPhone, false),
                ),
              ],
            ),
            Column(
              children: [
                buildLabelText('Chọn tỉnh thành*:'),
                const SizedBox(height: 2),
                buildDropdownProvinces(),
              ],
            ),
          ]),
          const SizedBox(height: 10),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              buildLabelText('Chọn hình thức thanh toán:'),
              const SizedBox(height: 2),
              buildDropdownPayment(),
            ],
          ),
          const SizedBox(height: 10),
          buildLabelText('Địa chỉ giao hàng của bạn là:'),
          const SizedBox(height: 2),
          buildAddressDetail(shippingAddress.text),
          const SizedBox(height: 10),
          buildButtonConfirm(),
          buildPayBill(),
        ],
      ),
    );
  }

//Design Bottom Confirm
  Widget buildButtonConfirm() {
    return Container(
      height: 45,
      margin: const EdgeInsets.only(left: 150),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        gradient: LinearGradient(
          colors: <Color>[
            ColorCodes.colorPrimary,
            const Color(0xFF6DCFCF),
            const Color(0xFF6DFFCD).withOpacity(0.7),
          ],
        ),
      ),
      child: TextButton(
        onPressed: () {
          if (isUpdated) {
            isUpdated = false;
          } else {
            isUpdated = true;
            shippingAddress.text =
                '${houseNumber.text} ${district.text}  ${ward.text} ${dropdownAddress!}';
          }
          setState(() {
            isUpdated;
          });
        },
        child: Text(
          isUpdated ? "Cập nhật thông tin" : "Xác nhận",
          maxLines: 2,
          style: const TextStyle(
            color: Colors.white,
            fontSize: 18,
          ),
        ),
      ),
    );
  }

//Design Label Text
  Widget buildLabelText(String text) {
    return Text(
      text,
      style: const TextStyle(
        fontSize: 15,
      ),
    );
  }

  //Design build input address
  Widget buildInputTextField(
      String text, TextEditingController? controller, bool isChecked) {
    return SizedBox(
      height: 40,
      child: TextField(
        readOnly: isUpdated,
        controller: controller,
        keyboardType: isChecked ? TextInputType.text : TextInputType.number,
        style: const TextStyle(fontSize: 14),
        decoration: InputDecoration(
          fillColor: isUpdated ? Colors.grey[300] : const Color(0xFFF5F5F5),
          filled: true,
          hintText: text,
        ),
      ),
    );
  }

  //Design build input address
  Widget buildAddressDetail(String address) {
    return SizedBox(
      height: 40,
      child: Text(address),
    );
  }

  //Design notification text
  Widget buildPayBill() {
    return Container(
      height: 55,
      margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        gradient: LinearGradient(
          colors: <Color>[
            ColorCodes.colorPrimary,
            const Color(0xFF6DCFCF),
            const Color(0xFF6DFFCD).withOpacity(0.7),
          ],
        ),
      ),
      child: TextButton(
        onPressed: () {
          CartProvider cartProvider =
              Provider.of<CartProvider>(context, listen: false);
          Member member =
              Provider.of<MemberProvider>(context, listen: false).member;
          double totalPrice = cartProvider.totalPrice;
          int totalQuantity = cartProvider.totalQuantity;
          Bill bill = Bill(
            member: member,
            shippingAddress: shippingAddress.text,
            shippingPhone: shippingPhone.text,
            receiver: nameReceive.text,
            totalPrice: totalPrice,
            totalQuantity: totalQuantity,
            voucher: widget.voucher,
            payment: dropdownPayment,
          );
          if (dropdownPayment == 0) {
            methodPay(bill, cartProvider.carts);
          } else {
            _methodZaloPay(bill, cartProvider.carts);
          }
          // await _methodPayCrash();
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: const [
            Text(
              'Thanh Toán',
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
              ),
            ),
            Icon(
              Icons.arrow_forward_ios_outlined,
              color: Colors.white,
              size: 23,
            ),
          ],
        ),
      ),
    );
  }

  //Design Button Select Address
  Widget buildButtonSelectAddress() {
    return Container(
      height: 45,
      margin: const EdgeInsets.all(0),
      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        gradient: LinearGradient(
          colors: <Color>[
            ColorCodes.colorPrimary,
            const Color(0xFF6DCFCF),
            const Color(0xFF6DFFCD).withOpacity(0.7),
          ],
        ),
      ),
      child: TextButton(
        onPressed: () {
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) => AddressScreen2()));
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: const [
            Text(
              'Địa chỉ giao hàng',
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
              ),
            ),
            Icon(
              Icons.arrow_forward_ios_outlined,
              color: Colors.white,
              size: 23,
            ),
          ],
        ),
      ),
    );
  }

  void _methodZaloPay(Bill bill, List<Cart> carts) {
    Provider.of<ZaloProvider>(context, listen: false).createOrder(bill).then(
          (value) => {
            if (value != null)
              {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (ctx) => ZaloPayScreen(
                      zaloOrder: value,
                      bill: bill,
                      carts: carts,
                    ),
                  ),
                ),
              }
            else
              {}
          },
        );
  }

  Future<void> methodPay(Bill bill, List<Cart> carts) async {
    showDialogLoading(context);
    Provider.of<BillProvider>(context, listen: false)
        .saveBill(bill)
        .then((billTmp) {
      if (billTmp != null) {
        // ignore: avoid_function_literals_in_foreach_calls
        carts.forEach(
          (cart) async {
            if (cart.isChoosed) {
              BillDetail billDetail = BillDetail(
                productDetail: cart.productDetail,
                bill: billTmp,
                quantity: cart.quantity,
                price: cart.pricePay,
                priceDiscount: 0,
              );
              await Provider.of<BillDetailProvider>(context, listen: false)
                  .saveBillDetail(billDetail);

              print("Done");
            }
            //   .catchError((error) {
            // return showMessageDialog(context, error, Icons.error);
          },
        );

        dismissDialog(context);
        showDialogLoadingSuccessfully(context, "Đặt Hàng Thành Công");
        Provider.of<CartProvider>(context, listen: false)
            .removeAllCartByIdMember(member.memberId!)
            .then((isRemoved) {
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                // ignore: prefer_const_constructors
                builder: (context) => PaySuccessfullyScreen(),
              ),
              (route) => false);
        });
      } else {
        dismissDialog(context);
        showMessageDialog(context, "Đặt Hàng Thất Bại", Icons.error);
        Future.delayed(
          const Duration(seconds: 2),
          () => dismissDialog(context),
        );
      }
    }).catchError((error) {
      dismissDialog(context);
      showMessageDialog(context, error, Icons.error);
      Future.delayed(
        const Duration(seconds: 2),
        () => dismissDialog(context),
      );
    });
  }

  //Design dropdown button provinces
  Widget buildDropdownProvinces() {
    return DropdownButton(
      // isDense: true,
      value: dropdownAddress,
      icon: const Icon(Icons.expand_more_outlined),
      elevation: 16,
      underline: Container(
        width: 200,
        height: 1,
        color: ColorCodes.colorPrimary,
      ),
      //borderRadius: BorderRadius.all(Radius.circular(20),),
      onChanged: isUpdated
          ? null
          : (String? newValue) {
              setState(() {
                dropdownAddress = newValue!;
              });
            },
      items: itemDropDownAddress.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(
            value,
            style: const TextStyle(
              fontWeight: FontWeight.w500,
            ),
          ),
        );
      }).toList(),
    );
  }

  //Design dropdown button Payment
  Widget buildDropdownPayment() {
    return DropdownButton(
      // isDense: true,
      iconEnabledColor: ColorCodes.colorPrimary,
      value: dropdownPayment,
      underline: Container(
        width: 200,
        height: 1,
        color: ColorCodes.colorPrimary,
      ),
      icon: const Icon(Icons.expand_more_outlined),
      elevation: 16,
      //borderRadius: BorderRadius.all(Radius.circular(20),),
      onChanged: isUpdated
          ? null
          : (newValue) {
              setState(() {
                dropdownPayment = newValue as int;
              });
            },
      items: itemPayment.map((map) {
        return DropdownMenuItem(
          value: map['value'],
          child: Text(
            map['name'],
            style: const TextStyle(
              fontWeight: FontWeight.w500,
            ),
          ),
        );
      }).toList(),
    );
  }
}
