import 'package:flutter/material.dart';
import 'package:ptp_application/models/voucher.dart';
import 'package:ptp_application/screens/screen_address/components/design_appbar_address.dart';
import 'package:ptp_application/screens/screen_address/components/design_body_address.dart';

//ignore: must_be_immutable
class AddressScreen extends StatelessWidget {
  Voucher? voucher;
  AddressScreen({this.voucher, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: const DesignAppBarAddress(), //CUSTOM DESIGN
        body: Container(
          decoration: const BoxDecoration(
            color: Color(0xFFFFFFFF),
          ),
          padding: const EdgeInsets.only(top: 10),
          child: DesignBodyAddress(
            voucher: voucher,
          ),
        ),
      ),
    );
  }
}
