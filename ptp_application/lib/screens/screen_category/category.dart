import 'package:flutter/material.dart';
import 'package:ptp_application/main.dart';
import 'package:ptp_application/screens/screen_category/components/design_appbar_category.dart';
import 'package:ptp_application/screens/screen_category/components/design_header_category.dart';
import 'package:ptp_application/screens/screen_category/components/design_list_product_belong_category.dart';

class CategoryScreen extends StatefulWidget {
  const CategoryScreen({Key? key}) : super(key: key);

  @override
  State<CategoryScreen> createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen>
    with AutomaticKeepAliveClientMixin<CategoryScreen> {
  @override
  bool get wantKeepAlive => true;
  @override
  Widget build(BuildContext context) {
    //Notice the super-call
    super.build(context);
    return RefreshIndicator(
      onRefresh: () async {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => BottomNavAppBar(currentIndex: 1)));
      },
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: const DesignAppbarCategory(), //CUSTOM DESIGN
          body: Container(
            color: const Color(0xFFFFFEFA).withOpacity(0.5),
            child: Column(
              children: const [
                Expanded(
                  flex: 2,
                  child: DesignHeaderCategory(), //CUSTOM DESIGN
                ),
                Expanded(
                  flex: 7,
                  child: DesignListProductBelongCategory(), //CUSTOM DESIGN
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
