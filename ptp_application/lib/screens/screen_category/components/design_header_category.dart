import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/config.dart';
import 'package:ptp_application/models/category.dart';
import 'package:ptp_application/providers/category_provider.dart';

class DesignHeaderCategory extends StatefulWidget {
  const DesignHeaderCategory({Key? key}) : super(key: key);

  @override
  _DesignHeaderCategoryState createState() => _DesignHeaderCategoryState();
}

class _DesignHeaderCategoryState extends State<DesignHeaderCategory> {
  @override
  void initState() {
    super.initState();
    Provider.of<CategoryProvider>(context, listen: false)
        .fetchAllCategoryInStock();
    // Provider.of<ImageProvider>(context, listen: false)
    //     .getImageByIdAndStatus();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(3, 3, 5, 5),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5.0),
        boxShadow: [
          BoxShadow(
            color: const Color(0xFFF1F1FE).withOpacity(0.8),
            blurRadius: 2,
            spreadRadius: 2,
          ),
        ],
      ),
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: buildBoxFixedItemHeader(),
          ),
          Expanded(
            flex: 8,
            child: buildScrollViewCategory(),
          ),
        ],
      ),
    );
  }

  //Design Box Fixed Item Header
  Widget buildBoxFixedItemHeader() {
    return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: const BorderRadius.only(
            topRight: Radius.circular(10),
            bottomRight: Radius.circular(10),
          ),
          boxShadow: [
            BoxShadow(
              color: ColorCodes.colorPrimary.withOpacity(0.2),
              spreadRadius: 1,
              blurRadius: 10,
            ),
          ],
        ),
        child: GestureDetector(
          onTap: () {
            Provider.of<CategoryProvider>(context, listen: false)
                .updateClick(null);
          },
          child: Column(
            children: const [
              SizedBox(height: 15),
              Icon(MdiIcons.star, size: 30, color: ColorCodes.colorPrimary),
              Text(
                "Gợi ý cho bạn",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    color: ColorCodes.textColorSecondary),
              ),
            ],
          ),
        ));
  }

  //Design Scroll View Category
  Widget buildScrollViewCategory() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: [
          Consumer<CategoryProvider>(
            builder: (context, categoryProvider, child) =>
                categoryProvider.categories.isEmpty
                    ? const Center(
                        child: Text("Danh sách loại trống"),
                      )
                    : ListView.builder(
                        scrollDirection: Axis.horizontal,
                        shrinkWrap: true,
                        // physics: ScrollPhysics(),
                        itemCount: categoryProvider.categories.length,
                        itemBuilder: (context, index) =>
                            buildCategory(categoryProvider.categories[index]),
                      ),
          ),
        ],
      ),
    );
  }

  //Design List Category
  Widget buildCategory(Category category) {
    return TextButton(
        style: TextButton.styleFrom(
          primary: Colors.blue[100],
          textStyle: const TextStyle(fontSize: 16),
        ),
        onPressed: () {
          Provider.of<CategoryProvider>(context, listen: false)
              .updateClick(category);
        },
        child: Container(
          width: 90,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                height: 45,
                child: ClipRRect(
                  // borderRadius: BorderRadius.circular(10.0),
                  child: CachedNetworkImage(
                    imageUrl: Config.getIP('/categories/${category.suffixImg}'),
                    width: 45,
                    height: 40,
                    fit: BoxFit.cover,
                    placeholder: (context, url) => const Center(
                      child: CircularProgressIndicator(),
                    ),
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.error),
                  ),
                ),
              ),
              Text(
                category.categoryName,
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: const TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.w400,
                  color: ColorCodes.textColorSecondary,
                  letterSpacing: 0.5,
                ),
              ),
            ],
          ),
        ));
  }
}
