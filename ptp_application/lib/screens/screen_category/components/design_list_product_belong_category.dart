import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/config.dart';
import 'package:ptp_application/models/category.dart';
import 'package:ptp_application/models/product.dart';
import 'package:ptp_application/models/stock_detail.dart';
import 'package:ptp_application/providers/category_provider.dart';
import 'package:ptp_application/screens/show_dialog.dart';
// ignore: unused_import
import 'package:ptp_application/providers/product_provider.dart';
// ignore: unused_import
import 'package:ptp_application/models/image.dart' as image;
// ignore: unused_import
import 'package:ptp_application/providers/image_provider.dart'
    as image_provider;
import 'package:ptp_application/providers/stock_detail_provider.dart';
import 'package:ptp_application/screens/screen_product_detail/product_detail.dart';

class DesignListProductBelongCategory extends StatefulWidget {
  const DesignListProductBelongCategory({Key? key}) : super(key: key);

  @override
  _DesignListProductBelongCategoryState createState() =>
      _DesignListProductBelongCategoryState();
}

class _DesignListProductBelongCategoryState
    extends State<DesignListProductBelongCategory> {
  @override
  Widget build(BuildContext context) {
    Category? category =
        Provider.of<CategoryProvider>(context, listen: true).category;

    return Container(
      width: MediaQuery.of(context).size.width - 10,
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5),
        boxShadow: [
          BoxShadow(
            color: ColorCodes.colorPrimary.withOpacity(0.2),
            spreadRadius: 1,
            blurRadius: 2,
          ),
        ],
      ),
      child: Column(
        children: [
          Expanded(
            flex: 1,
            child: buildTextTitle(category),
          ),
          const SizedBox(height: 10),
          Expanded(
            flex: 9,
            child: Consumer<ProductProvider>(
                builder: (context, productProvider, child) {
              List<Product> products = [];
              if (category != null) {
                products = productProvider
                    .getAllProductByCategoryId(category.categoryId);
              } else {
                products = productProvider.products;
              }
              return products.isEmpty
                  ? Container(
                      alignment: Alignment.center,
                      child: Column(
                        children: const [
                          Text(
                            "Loading...",
                            style: TextStyle(
                              fontSize: 16,
                              letterSpacing: 0.5,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          CircularProgressIndicator(),
                        ],
                      ),
                    )
                  : GridView.builder(
                      // ignore: prefer_const_constructors
                      physics: ScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: products.length,
                      itemBuilder: (context, index) {
                        return buildProduct(products[index]);
                      },
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        mainAxisSpacing: 10,
                        mainAxisExtent: 220,
                      ),
                    );
            }),
          ),
        ],
      ),
    );
  }

  //Design Text Title
  Widget buildTextTitle(Category? category) {
    return Container(
      alignment: Alignment.center,
      height: 50,
      color: ColorCodes.colorPrimary.withOpacity(0.1),
      padding: const EdgeInsets.only(left: 10),
      child: Container(
        alignment: Alignment.centerLeft,
        child: Row(
          children: [
            const Text(
              "Danh mục",
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w600,
                color: ColorCodes.textColorSecondary,
              ),
            ),
            category == null
                ? Container()
                : Row(
                    children: [
                      Icon(
                        Icons.navigate_next,
                        color: ColorCodes.textColorPrimary.withOpacity(0.5),
                      ),
                      Text(
                        category.categoryName,
                        style: const TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                            color: ColorCodes.textColorSecondary),
                      ),
                      Icon(
                        Icons.navigate_next,
                        color: ColorCodes.textColorPrimary.withOpacity(0.5),
                      ),
                    ],
                  ),
          ],
        ),
      ),
    );
  }

  //Desgin product -> use builListProduct()
  Widget buildProduct(Product product) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ProductDetailScreen(
                      productId: product.productId,
                    )));
      },
      child: Container(
        margin: const EdgeInsets.only(top: 5, left: 5, right: 5, bottom: 5),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
          boxShadow: const [
            BoxShadow(
              color: Color(0xFFF1F1F1),
              offset: Offset(0.0, 1.0),
              spreadRadius: 3,
              blurRadius: 10,
            ),
          ],
        ),
        child: Column(
          children: [
            Expanded(
              flex: 4,
              child: ClipRRect(
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                ),
                child: CachedNetworkImage(
                  imageUrl: Config.getIP(
                      '/products/${product.productId}/${product.productImg}'),
                  fit: BoxFit.cover,
                  placeholder: (context, url) => const Center(
                    child: CircularProgressIndicator(),
                  ),
                  errorWidget: (context, url, error) => const Icon(Icons.error),
                ),
              ),
            ),
            Expanded(
              flex: 0,
              child: Container(
                padding: const EdgeInsets.only(left: 10, right: 5),
                child: Text(
                  product.productName,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                    color: ColorCodes.textColorPrimary,
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 0,
              child: Text(
                '${formatNumber(product.pricePay)} đ',
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  color: Color(0xFFF65E5E),
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                  letterSpacing: 0.5,
                ),
              ),
            ),
            Expanded(
              flex: 0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  buildRateProduct(product.quantityRate!, product.avgStar!),
                  buildSalesProduct(product.quantityPay!),
                ],
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            Container(
              margin: const EdgeInsets.only(left: 70),
              alignment: Alignment.center,
              height: 35,
              width: double.maxFinite,
              decoration: BoxDecoration(
                color: const Color(0xFFF65E5E),
                borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                  topLeft: Radius.circular(4),
                ),
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [
                      Colors.green.withOpacity(0.7),
                      Colors.blue.withOpacity(0.8)
                    ]),
              ),
              child: const Text(
                'Mua Ngay',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                  letterSpacing: 0.5,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  //Desgin sales product -> use buildProduct()
  Widget buildSalesProduct(int sales) {
    return Container(
      padding: const EdgeInsets.only(right: 4),
      child: Text(
        '$sales đã bán',
        style: const TextStyle(
          color: ColorCodes.textColorSecondary,
          fontWeight: FontWeight.w500,
          fontSize: 12,
          letterSpacing: 0.5,
        ),
      ),
    );
  }

  //Desgin rate product -> use buildProduct()
  Widget buildRateProduct(int rate, double avgStar) {
    return Row(
      children: [
        const SizedBox(width: 4),
        const Icon(
          Icons.star_outlined,
          size: 13,
          color: Colors.yellow,
        ),
        Text(
          avgStar.toStringAsFixed(1),
          style: const TextStyle(
            color: ColorCodes.textColorSecondary,
            fontSize: 13,
            letterSpacing: 0.5,
          ),
        ),
        Text(
          '($rate)',
          style: const TextStyle(
            color: ColorCodes.textColorSecondary,
            fontSize: 13,
            letterSpacing: 0.5,
          ),
        ),
      ],
    );
  }
}
