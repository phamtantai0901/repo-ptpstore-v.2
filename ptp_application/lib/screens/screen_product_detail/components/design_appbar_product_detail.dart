import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/models/cart.dart';
import 'package:ptp_application/providers/cart_provider.dart';
import 'package:ptp_application/screens/screen_cart/cart.dart';

AppBar designAppBar(BuildContext context) {
  return AppBar(
    backgroundColor: Colors.white,
    elevation: 0.5,
    centerTitle: true,
    leading: IconButton(
      onPressed: () {
        Navigator.pop(context);
      },
      icon: const Icon(Icons.arrow_back),
      iconSize: 25,
      color: ColorCodes.textColorSecondary,
    ),
    leadingWidth: 70,
    title: const Text(
      "CHI TIẾT SẢN PHẨM",
      textAlign: TextAlign.center,
      style: TextStyle(
          color: ColorCodes.textColorPrimary,
          fontSize: 18,
          fontWeight: FontWeight.w600),
    ),
    actions: [
      GestureDetector(
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const CartScreen()));
        },
        child: Consumer<CartProvider>(
          builder: (context, cartProvider, child) {
            List<Cart> carts = cartProvider.carts;
            return SizedBox(
              width: 80,
              child: Stack(
                alignment: AlignmentDirectional.center,
                children: [
                  const Icon(Icons.shopping_cart_outlined,
                      color: Colors.black, size: 30),
                  // ignore: prefer_is_empty
                  carts.length == 0
                      ? Container()
                      : Positioned(
                          bottom: 25,
                          right: 15,
                          child: Container(
                            alignment: Alignment.center,
                            width: 25,
                            height: 25,
                            decoration: BoxDecoration(
                              borderRadius: const BorderRadius.all(
                                Radius.circular(50),
                              ),
                              color: Colors.red[400],
                            ),
                            child: Text(
                              carts.length >= 99 ? '99+' : '${carts.length}',
                              style: const TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w700),
                            ),
                          ),
                        ),
                ],
              ),
            );
          },
        ),
      ),
    ],
  );
}
