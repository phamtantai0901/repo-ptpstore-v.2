import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/models/cart.dart';
import 'package:ptp_application/models/member.dart';
import 'package:ptp_application/models/product_detail.dart';
import 'package:ptp_application/models/stock_detail.dart';
import 'package:ptp_application/providers/cart_provider.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/providers/product_detail_provider.dart';
import 'package:ptp_application/providers/stock_detail_provider.dart';
import 'package:ptp_application/screens/screen_cart/cart.dart';
import 'package:ptp_application/screens/show_dialog.dart';

//Design Button And Product Into Cart
Container designButtonAddProductIntoCart(BuildContext context, int pricePay) {
  return Container(
    height: 60,
    padding: const EdgeInsets.only(left: 10, right: 10),
    decoration: BoxDecoration(
      color: Colors.white,
      boxShadow: [
        BoxShadow(
          color: ColorCodes.colorPrimary.withOpacity(0.5),
          spreadRadius: 5,
          blurRadius: 7,
          offset: const Offset(0, 3), // changes position of shadow
        ),
      ],
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          flex: 2,
          child: Container(
            height: 45,
            // width: 205,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: <Color>[
                  ColorCodes.colorPrimary,
                  const Color(0xFF6DCFCF),
                  const Color(0xFF6DCFCF).withOpacity(0.7),
                ],
              ),
              borderRadius: BorderRadius.circular(5),
            ),
            child: TextButton(
              onPressed: () {
                int quantityOrder =
                    Provider.of<ProductDetailProvider>(context, listen: false)
                        .quantityOrder;
                ProductDetail productDetail =
                    Provider.of<StockDetailProvider>(context, listen: false)
                        .stockDetail!
                        .productDetail;
                Member member =
                    Provider.of<MemberProvider>(context, listen: false).member;
                Cart cart = Cart(
                  productDetail: productDetail,
                  member: member,
                  quantity: quantityOrder,
                  pricePay: pricePay,
                );
                StockDetail stockDetail =
                    Provider.of<StockDetailProvider>(context, listen: false)
                        .stockDetail!;
                // .getStockDetailByProductDetailId(
                //     cart.productDetail.productDetailId);
                addProductIntoCart(context, cart, stockDetail.quantity, false);
              },
              child: Row(
                children: const [
                  // SizedBox(width: 10),
                  Text(
                    "Thêm Vào Giỏ hàng",
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.5,
                    ),
                  ),
                  SizedBox(width: 5),
                  Icon(
                    Icons.add_shopping_cart,
                    size: 28,
                    color: Colors.white,
                  ),
                ],
              ),
            ),
          ),
        ),
        const SizedBox(width: 5),
        Expanded(
          flex: 1,
          child: Container(
            // width: 180,
            height: 45,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: <Color>[
                  ColorCodes.pinkColor.withOpacity(0.9),
                  ColorCodes.pinkColor.withOpacity(0.8),
                ],
              ),
              borderRadius: BorderRadius.circular(5.0),
            ),
            child: TextButton(
              onPressed: () {
                int quantityOrder =
                    Provider.of<ProductDetailProvider>(context, listen: false)
                        .quantityOrder;
                ProductDetail productDetail =
                    Provider.of<StockDetailProvider>(context, listen: false)
                        .stockDetail!
                        .productDetail;
                Member member =
                    Provider.of<MemberProvider>(context, listen: false).member;
                Cart cart = Cart(
                  productDetail: productDetail,
                  member: member,
                  quantity: quantityOrder,
                  pricePay: pricePay,
                );
                StockDetail stockDetail =
                    Provider.of<StockDetailProvider>(context, listen: false)
                        .stockDetail!;
                addProductIntoCart(context, cart, stockDetail.quantity, true);
              },
              child: const Text(
                'Mua Ngay',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  letterSpacing: 0.5,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          ),
        )
      ],
    ),
  );
}

void addProductIntoCart(context, Cart cart, int quantityInStock, bool isCheck) {
  showDialogLoading(context);
  CartProvider cartProvider = Provider.of<CartProvider>(context, listen: false);
  cartProvider.addProductIntoCart(cart, quantityInStock).then((returnType) {
    if (returnType == 1) {
      dismissDialog(context);
      showDialogLoadingSuccessfully(context, "Thêm Giỏ Hàng Thành Công");
      Provider.of<ProductDetailProvider>(context, listen: false)
          .updateQuantityOrder(1);
      Future.delayed(const Duration(seconds: 1), () {
        dismissDialog(context);
        if (isCheck) {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const CartScreen()));
        }
      });
    } else if (returnType == 0) {
      dismissDialog(context);
      showMessageDialog(context, "Thêm Giỏ Hàng Thất Bại", Icons.error);
      Future.delayed(const Duration(seconds: 1), () => dismissDialog(context));
    } else {
      dismissDialog(context);
      showMessageDialog(
          context,
          "Bạn không thể thêm số lượng đã chọn vào giỏ hàng vì sẽ vượt quá số lượng tồn của sản phẩm",
          Icons.error);
      Future.delayed(const Duration(seconds: 4), () => dismissDialog(context));
    }
  }).catchError((error) {
    dismissDialog(context);
    showMessageDialog(context, error, Icons.error);
    Future.delayed(const Duration(seconds: 1), () => dismissDialog(context));
  });
}
