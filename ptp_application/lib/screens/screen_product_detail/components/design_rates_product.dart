import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/config.dart';
import 'package:ptp_application/models/product.dart';
import 'package:ptp_application/models/rate.dart';
import 'package:ptp_application/providers/rate_provider.dart';
import 'package:ptp_application/screens/screen_show_all_rate_of_product/show_all_rate_of_product.dart';

//Design Rates Product
Widget designRatesProduct(BuildContext context, Product product) {
  return Consumer<RateProvider>(builder: (context, rateProvider, child) {
    List<Rate> rates = rateProvider.rates;
    double averageRates =
        Provider.of<RateProvider>(context, listen: false).averageRate(rates);
    return Column(
      children: [
        buildRowTextAndButtonSeeAll(context, rates.length, product),
        rates.isEmpty
            ? Container(
                padding: const EdgeInsets.only(top: 20),
                child: const Text(
                  "Hiện Không có đánh giá nào về sản phẩm",
                  style: TextStyle(
                    fontSize: 15,
                    color: ColorCodes.textColorSecondary,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              )
            : Column(
                children: [
                  buildTextAndStar(averageRates),
                  const SizedBox(height: 10),
                  buildContainerChat(context, rates),
                ],
              ),
        const SizedBox(height: 20),
      ],
    );
  });
}

//Design Row Text And Button Comment || include: buildTextTitleComment() & buildButtonSeeMore()
Widget buildRowTextAndButtonSeeAll(
    BuildContext context, int quantityRate, Product product) {
  return Container(
    height: 40,
    padding: const EdgeInsets.fromLTRB(25, 0, 10, 0),
    alignment: Alignment.centerLeft,
    decoration: const BoxDecoration(
      gradient: LinearGradient(
        colors: <Color>[
          Color(0xFFACB6E5),
          Color(0xFF74ebd5),
        ],
      ),
      border: Border(
        bottom: BorderSide(
          color: Colors.grey,
          width: 0.2,
        ),
        top: BorderSide(
          color: Colors.grey,
          width: 0.2,
        ),
      ),
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            const Text(
              "Đánh giá & nhận xét ",
              style: TextStyle(
                fontSize: 16,
                color: Colors.white,
                fontWeight: FontWeight.w600,
              ),
            ),
            const SizedBox(width: 5),
            Text(
              "($quantityRate)",
              style: const TextStyle(
                fontSize: 16,
                color: Colors.white,
                fontWeight: FontWeight.w600,
              ),
            )
          ],
        ),
        quantityRate == 0
            ? Container()
            : TextButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              ShowAllRateOfProductScreen(product: product)));
                },
                child: Row(
                  children: const [
                    Text(
                      "Xem tất cả",
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Icon(
                      Icons.arrow_forward_ios,
                      size: 18,
                      color: Colors.white,
                    ),
                  ],
                ),
              )
      ],
    ),
  );
}

//Design Text And Star Comment
Widget buildTextAndStar(double averageRate) {
  return Container(
    padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
    alignment: Alignment.center,
    // color: ColorCodes.colorPrimary.withOpacity(0.1),
    child: Align(
      alignment: Alignment.centerLeft,
      child: Row(
        children: [
          Row(
            children: [
              Text(
                averageRate.toStringAsFixed(1),
                style: const TextStyle(
                    letterSpacing: 1,
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: ColorCodes.textColorSecondary),
              ),
              const Text(
                "/5",
                style: TextStyle(
                    letterSpacing: 1,
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: ColorCodes.textColorPrimary),
              ),
            ],
          ),
          Container(
            padding: const EdgeInsets.only(left: 10),
            child: // //Box Star
                RatingBar.builder(
                    initialRating: averageRate,
                    ignoreGestures: true,
                    // minRating: 0,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    // itemCount: 5,
                    itemSize: 20.0,
                    itemPadding: const EdgeInsets.symmetric(horizontal: 3.0),
                    itemBuilder: (context, _) => Icon(
                          Icons.star,
                          color: Colors.amber[500],
                        ),
                    onRatingUpdate: (rating) {}),
          ),
        ],
      ),
    ),
  );
}

//Design Container Chat ||include: buildBoxChat()
Widget buildContainerChat(BuildContext context, List<Rate> rates) {
  return Container(
    decoration: BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(5),
      boxShadow: [
        BoxShadow(
          color: ColorCodes.colorPrimary.withOpacity(0.05),
          blurRadius: 15,
          spreadRadius: 3,
        ),
      ],
    ),
    child: ListView.builder(
        shrinkWrap: true,
        physics: const ScrollPhysics(),
        itemCount: rates.length < 5 ? rates.length : 5,
        itemBuilder: (context, index) {
          return buildBoxChat(context, rates[index]);
        }),
  );
}

//Design Chat Box || include: buildItemLeftChat() & buildItemCenterChat() & buildItemRightChat()
Widget buildBoxChat(BuildContext context, Rate rate) {
  return Container(
    padding: const EdgeInsets.only(top: 0, bottom: 15),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        buildItemLeftChat(
            rate.member.user.image, double.parse('${rate.star}'), "M - Vàng"),
        buildItemCenterChat(rate.member.user.fullName, rate.comment!),
        buildItemRightChat(context, rate),
      ],
    ),
  );
}

//Design Item Left Chat
Widget buildItemLeftChat(String image, double star, String size) {
  return Column(
    children: [
      //Box Image
      ClipRRect(
        borderRadius: BorderRadius.circular(25.0),
        child: CachedNetworkImage(
          imageUrl: Config.getIP("/avatar_users/$image"),
          width: 40,
          height: 40,
          fit: BoxFit.cover,
          placeholder: (context, url) => const Center(
            child: CircularProgressIndicator(),
          ),
          errorWidget: (context, url, error) => const Icon(Icons.error),
        ),
      ),
      const SizedBox(height: 5),
      // //Box Star
      RatingBar.builder(
        initialRating: star,
        direction: Axis.horizontal,
        allowHalfRating: false,
        itemSize: 15.0,
        itemPadding: const EdgeInsets.symmetric(horizontal: 0.0),
        itemBuilder: (context, _) => Icon(
          Icons.star,
          color: Colors.amber[500],
        ),
        onRatingUpdate: (double value) {},
      ),
      const SizedBox(height: 5),
      //Box Name Size
      Text(
        size,
        style: const TextStyle(
            fontSize: 16,
            color: ColorCodes.textColorSecondary,
            fontWeight: FontWeight.w600),
      )
    ],
  );
}

//Design Item Cente Chat
Widget buildItemCenterChat(String name, String text) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      //Name User
      Container(
        alignment: Alignment.topLeft,
        height: 25,
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            name,
            style: const TextStyle(
              overflow: TextOverflow.ellipsis,
              letterSpacing: 1,
              fontSize: 16,
              fontWeight: FontWeight.w600,
              color: ColorCodes.textColorSecondary,
            ),
          ),
        ),
      ),
      //Comment
      Container(
        width: 230,
        padding: const EdgeInsets.all(5.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          boxShadow: [
            BoxShadow(
              color: const Color.fromARGB(255, 244, 252, 254).withOpacity(0.8),
              blurRadius: 10,
              spreadRadius: 2,
            ),
          ],
        ),
        child: Text(
          text,
          style: const TextStyle(
              letterSpacing: 1,
              fontSize: 16,
              fontWeight: FontWeight.w500,
              color: ColorCodes.textColorSecondary),
        ),
      ),
    ],
  );
}

//Design Item Right Chat
Widget buildItemRightChat(BuildContext context, Rate rate) {
  return Column(
    children: [
      //Button Like
      IconButton(
        onPressed: () {
          Provider.of<RateProvider>(context, listen: false)
              .likeRate(rate.rateId)
              .then((isLiked) {
            if (isLiked) {
              rate.like += 1;
            }
          });
        },
        icon: const Icon(MdiIcons.heartOutline),
        iconSize: 28,
        color: ColorCodes.pinkColor.withOpacity(0.5),
      ),
      //Button Number Like
      Text(
        '${rate.like}',
        style: const TextStyle(
            fontSize: 18,
            color: ColorCodes.textColorSecondary,
            fontWeight: FontWeight.w600),
      )
    ],
  );
}
