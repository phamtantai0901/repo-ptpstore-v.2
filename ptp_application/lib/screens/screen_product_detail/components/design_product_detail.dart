import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/config.dart';
import 'package:ptp_application/models/product_detail.dart';
import 'package:ptp_application/models/stock_detail.dart';
import 'package:ptp_application/providers/favourite_provider.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/providers/product_detail_provider.dart';
import 'package:ptp_application/providers/stock_detail_provider.dart';
import 'package:ptp_application/screens/screen_choose_color/choose_color.dart';
import 'package:ptp_application/screens/show_dialog.dart';

class DesignProductDetail extends StatefulWidget {
  const DesignProductDetail({Key? key}) : super(key: key);

  @override
  _DesignProductDetailState createState() => _DesignProductDetailState();
}

class _DesignProductDetailState extends State<DesignProductDetail> {
  late String memberId;
  bool isChecked = false;
  late StockDetail stockDetail;
  @override
  void initState() {
    super.initState();

    memberId =
        Provider.of<MemberProvider>(context, listen: false).member.memberId!;
    // StockDetail stockDetailTmp =
    //     Provider.of<StockDetailProvider>(context, listen: false).stockDetail!;
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<StockDetailProvider>(
        builder: (context, stockDetailProvider, child) {
      stockDetail = stockDetailProvider.stockDetail!;
      Provider.of<FavouriteProvider>(context, listen: false)
          .isProductDetailInFavourites(
              stockDetail.productDetail.productDetailId, memberId);
      return Column(
        children: [
          Container(
            height: 50,
            padding: const EdgeInsets.only(left: 25),
            alignment: Alignment.centerLeft,
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                colors: <Color>[
                  Color(0xFFACB6E5),
                  Color(0xFF74ebd5),
                ],
              ),
              border: Border(
                bottom: BorderSide(
                  color: Colors.grey,
                  width: 0.2,
                ),
                top: BorderSide(
                  color: Colors.grey,
                  width: 0.2,
                ),
              ),
            ),
            child: const Text(
              "HOT SALES",
              style: TextStyle(
                color: Colors.white,
                fontSize: 22,
                fontWeight: FontWeight.w600,
                letterSpacing: 5,
              ),
            ),
          ),
          const SizedBox(height: 10),
          Container(
            alignment: Alignment.center,
            width: 100,
            height: 28,
            margin: const EdgeInsets.only(right: 265),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(3),
              color: ColorCodes.pinkColor.withOpacity(0.9),
            ),
            child: const Text(
              'PTP Maill +',
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          buildRowNameProduct(stockDetail.productDetail),
          buildRowPrice(context, stockDetail.pricePay),
          buildQuantityStockAndQuantityPay(
              stockDetail.quantity, stockDetail.quantityPay!),
          const SizedBox(height: 10),
          buildBoxChooseColorAndSize(context, stockDetail.productDetail),
          const SizedBox(height: 20),
          buildTitle("Chi tiết sản phẩm"),
          buildInfoProduct(stockDetail),
        ],
      );
    });
  }

//Build Title
  Widget buildTitle(String text) {
    return Container(
      height: 40,
      padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
      alignment: Alignment.centerLeft,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          colors: <Color>[
            Color(0xFFACB6E5),
            Color(0xFF74ebd5),
          ],
        ),
        border: Border(
          bottom: BorderSide(
            color: Colors.grey,
            width: 0.2,
          ),
          top: BorderSide(
            color: Colors.grey,
            width: 0.2,
          ),
        ),
      ),
      child: Text(
        text,
        style: const TextStyle(
            color: Colors.white,
            fontSize: 16,
            fontWeight: FontWeight.w600,
            letterSpacing: 0.5),
      ),
    );
  }

//Design Info Product
  Widget buildInfoProduct(StockDetail stockDetail) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
          // width: MediaQuery.of(context).size.width - 20,
          child: Column(
            children: [
              const SizedBox(height: 30),
              buildRowDataInfoProduct("Mã Kho", stockDetail.stock.stockId),
              const SizedBox(height: 30),
              buildRowDataInfoProduct("Tên Kho", stockDetail.stock.address),
              const SizedBox(height: 15),
              buildRowDataInfoProduct("Nhà Cung Cấp",
                  stockDetail.productDetail.product.producer.producerName),
              const SizedBox(height: 15),
              buildRowDataInfoProduct("Chất lượng sản phẩm", "100% hàng thật"),
              const SizedBox(height: 15),
            ],
          ),
        ),
        Container(
          // width: MediaQuery.of(context).size.width - 20,
          alignment: Alignment.centerLeft,
          padding: const EdgeInsets.fromLTRB(15, 25, 15, 25),
          decoration: const BoxDecoration(
            border: Border(
              top: BorderSide(
                color: Colors.grey,
                width: 0.2,
              ),
            ),
          ),
          child: Column(
            children: [
              const Text(
                "Mô tả sản phẩm",
                textAlign: TextAlign.start,
                style: TextStyle(
                  color: ColorCodes.textColorSecondary,
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                ),
              ),
              const SizedBox(height: 10),
              Text(
                stockDetail.productDetail.product.description,
                style: const TextStyle(
                  height: 1.3,
                  color: ColorCodes.textColorSecondary,
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  letterSpacing: 0.3,
                ),
              ),
              const SizedBox(height: 10),
              const Text(
                "[Đọc kỹ thông tin sản phâm trước khi mua]",
                style: TextStyle(
                  color: ColorCodes.textColorSecondary,
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  letterSpacing: 0.3,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

//Design Row Data Info Product
  Widget buildRowDataInfoProduct(String text, String stock) {
    return Row(children: [
      Expanded(
        flex: 5,
        child: Text(
          text,
          style: const TextStyle(
            color: ColorCodes.textColorSecondary,
            fontSize: 14,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
      Expanded(
        flex: 5,
        child: Text(
          stock,
          style: const TextStyle(
            color: ColorCodes.textColorSecondary,
            fontSize: 14,
            fontWeight: FontWeight.w500,
          ),
        ),
      )
    ]);
  }

//Design Row Name Product || include: buildTextNameProduct() &  buildButtonLike()
  Widget buildRowNameProduct(ProductDetail productDetail) {
    return Container(
      padding: const EdgeInsets.only(left: 25, top: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            width: 300,
            child: Text(
              productDetail.product.productName.toUpperCase(),
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(
                fontSize: 14,
                color: ColorCodes.textColorPrimary,
                fontWeight: FontWeight.w600,
                letterSpacing: 0.1,
              ),
            ),
          ),
          Expanded(
            child: Consumer<FavouriteProvider>(
              builder: (context, favouriteProvider, child) {
                isChecked = favouriteProvider.isChecked;
                return IconButton(
                  onPressed: () async {
                    if (isChecked == false) {
                      await favouriteProvider.addProductFavourite(
                          memberId, productDetail.productDetailId);
                    } else {
                      await favouriteProvider.removeFavourite(
                          memberId, productDetail.productDetailId);
                    }
                    //setState(() {});
                  },
                  icon: isChecked == true
                      ? const Icon(MdiIcons.heart, color: Colors.red)
                      : const Icon(MdiIcons.heartOutline),
                  iconSize: 35,
                  color: ColorCodes.pinkColor.withOpacity(0.7),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

//Design Text Quantity
  Widget buildQuantityStockAndQuantityPay(int quantity, int number) {
    return Container(
      padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              const Text(
                "Số lượng còn lại: ",
                style: TextStyle(
                  fontSize: 14,
                  color: ColorCodes.textColorSecondary,
                  fontWeight: FontWeight.w500,
                ),
              ),
              Text(
                '($quantity)',
                style: const TextStyle(
                    fontSize: 14,
                    color: ColorCodes.textColorPrimary,
                    fontWeight: FontWeight.w500),
              ),
            ],
          ),
          Row(children: [
            const Text(
              "Đã bán: ",
              style: TextStyle(
                  fontSize: 14,
                  color: ColorCodes.textColorSecondary,
                  fontWeight: FontWeight.w500),
            ),
            Text(
              '($number)',
              style: const TextStyle(
                  fontSize: 14,
                  color: ColorCodes.textColorPrimary,
                  fontWeight: FontWeight.w500),
            ),
          ]),
        ],
      ),
    );
  }

//Design Row Price And Buy
  Widget buildRowPrice(BuildContext context, int price) {
    return Consumer<ProductDetailProvider>(
      builder: (context, productDetailProvider, child) {
        int quantityOrder = productDetailProvider.quantityOrder;
        return Container(
            padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '${formatNumber(price)} đ',
                  style: const TextStyle(
                      fontSize: 22,
                      color: ColorCodes.pinkColor,
                      fontWeight: FontWeight.w700),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      'Số lượng: ',
                      style: TextStyle(
                          fontSize: 14,
                          color: ColorCodes.textColorSecondary,
                          fontWeight: FontWeight.w500),
                    ),
                    Row(
                      children: [
                        IconButton(
                          onPressed: () {
                            Provider.of<ProductDetailProvider>(context,
                                    listen: false)
                                .removeQuantityOrder();
                          },
                          icon: Container(
                            alignment: Alignment.center,
                            height: 30,
                            width: 30,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              gradient: LinearGradient(
                                colors: <Color>[
                                  ColorCodes.colorPrimary,
                                  const Color(0xFFb6fcd5).withOpacity(0.7),
                                ],
                              ),
                            ),
                            child: Icon(
                              Icons.remove,
                              size: 20,
                              color: Colors.grey[700],
                            ),
                          ),
                        ),
                        Text(
                          '$quantityOrder',
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                              color: Colors.grey[600]),
                        ),
                        IconButton(
                          onPressed: () {
                            if (quantityOrder >= stockDetail.quantity) {
                              showMessageDialog(
                                  context,
                                  "Rất tiếc, số lượng trong kho không đủ",
                                  Icons.error);
                              Future.delayed(const Duration(seconds: 1),
                                  () => dismissDialog(context));
                            } else {
                              Provider.of<ProductDetailProvider>(context,
                                      listen: false)
                                  .addQuantityOrder();
                            }
                          },
                          icon: Container(
                            alignment: Alignment.center,
                            height: 30,
                            width: 30,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              gradient: LinearGradient(
                                colors: <Color>[
                                  ColorCodes.colorPrimary,
                                  const Color(0xFFb6fcd5).withOpacity(0.7),
                                ],
                              ),
                            ),
                            child: Icon(
                              Icons.add,
                              size: 20,
                              color: Colors.grey[700],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ));
      },
    );
  }

//Desin Box Choose Color
  Widget buildBoxChooseColorAndSize(
      BuildContext context, ProductDetail productDetail) {
    return Container(
      width: MediaQuery.of(context).size.width - 45,
      height: 70,
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.grey,
          width: 0.2,
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      child: TextButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => ChooseColorScreen()));
        },
        child: Container(
          padding: const EdgeInsets.only(left: 15, right: 10),
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(5.0),
                  child: CachedNetworkImage(
                    imageUrl: Config.getIP(
                        "/products/${productDetail.product.productId}/${productDetail.product.productImg}"),
                    width: 60,
                    height: 60,
                    fit: BoxFit.cover,
                    placeholder: (context, url) => const Center(
                      child: CircularProgressIndicator(),
                    ),
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.error),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(left: 10),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Màu: ${productDetail.color.colorName}",
                          style: const TextStyle(
                              fontSize: 16,
                              color: ColorCodes.textColorSecondary,
                              fontWeight: FontWeight.w500),
                        ),
                        const SizedBox(height: 2),
                        Text(
                          "Kích thước: ${productDetail.size.sizeName}",
                          style: const TextStyle(
                              fontSize: 16,
                              color: ColorCodes.textColorSecondary,
                              fontWeight: FontWeight.w500),
                        ),
                      ]),
                ),
              ],
            ),
            Icon(
              Icons.arrow_forward_ios,
              size: 22,
              color: Colors.grey[400],
            ),
          ]),
        ),
      ),
    );
  }
}
