import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:ptp_application/config.dart';
import 'package:ptp_application/models/image.dart' as image_obj;

//Design Slider Image Product
Widget designSliderImageProduct(List<image_obj.Image> images) {
  return SizedBox(
    height: 350,
    child: CarouselSlider.builder(
      itemCount: images.length,
      itemBuilder: (context, indexItem, indexPage) {
        return CachedNetworkImage(
          imageUrl: Config.getIP(
              "/products/${images[indexItem].product.productId}/${images[indexItem].imgName}"),
          fit: BoxFit.cover,
          placeholder: (context, url) => const Center(
            child: CircularProgressIndicator(),
          ),
          errorWidget: (context, url, error) => const Icon(Icons.error),
        );
      },
      options: CarouselOptions(
        viewportFraction: 1,
        autoPlay: true,
        aspectRatio: 1,
        enlargeCenterPage: true,
      ),
    ),
  );
}
