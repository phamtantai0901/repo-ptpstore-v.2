import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/config.dart';
import 'package:ptp_application/models/product.dart';
import 'package:ptp_application/models/stock_detail.dart';
import 'package:ptp_application/providers/product_provider.dart';
import 'package:ptp_application/providers/stock_detail_provider.dart';
import 'package:ptp_application/screens/screen_product_detail/product_detail.dart';
import 'package:ptp_application/screens/show_dialog.dart';
import 'package:ptp_application/custom.dart';

//Design Similar Product
Widget designSimilarProduct(BuildContext context) {
  // ignore: prefer_const_constructors
  return ListView(shrinkWrap: true, physics: ScrollPhysics(), children: [
    Container(
      height: 40,
      padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
      margin: const EdgeInsets.only(top: 20),
      alignment: Alignment.centerLeft,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          colors: <Color>[
            Color(0xFFACB6E5),
            Color(0xFF74ebd5),
          ],
        ),
        border: Border(
          bottom: BorderSide(
            color: Colors.grey,
            width: 0.2,
          ),
          top: BorderSide(
            color: Colors.grey,
            width: 0.2,
          ),
        ),
      ),
      child: const Text(
        "Sản phẩm tương tự",
        style: TextStyle(
            color: Colors.white,
            fontSize: 16,
            fontWeight: FontWeight.w600,
            letterSpacing: 0.5),
      ),
    ),
    // const SizedBox(height: 22),
    Container(
      padding: const EdgeInsets.only(top: 10),
      alignment: Alignment.center,
      height: 200,
      child: Consumer<ProductProvider>(
        builder: (context, productProvider, child) {
          List<Product> products = productProvider.products;
          return ListView.builder(
            // shrinkWrap: true,
            // physics: ScrollPhysics(),
            scrollDirection: Axis.horizontal,
            itemCount: products.length,
            itemBuilder: (context, index) =>
                buildProduct(context, products[index]),
          );
        },
      ),
    ),
  ]);
}

//Desgin product -> use buildScrollViewProduct()
Widget buildProduct(BuildContext context, Product product) {
  return GestureDetector(
    onTap: () {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ProductDetailScreen(
                    productId: product.productId,
                  )));
    },
    child: Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Colors.white,
        boxShadow: const [
          BoxShadow(
            color: Color(0xFFF1F1F1),
            offset: Offset(0.0, 1.0),
            spreadRadius: 2,
            blurRadius: 10,
          ),
        ],
      ),
      margin: const EdgeInsets.fromLTRB(3, 3, 3, 10),
      width: 160,
      child: Column(
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 4,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(5),
              child: CachedNetworkImage(
                imageUrl: Config.getIP(
                    "/products/${product.productId}/${product.productImg}"),
                fit: BoxFit.fill,
                placeholder: (context, url) => const Center(
                  child: CircularProgressIndicator(),
                ),
                errorWidget: (context, url, error) => const Icon(Icons.error),
              ),
            ),
          ),
          Expanded(
              flex: 4,
              child: Container(
                padding: const EdgeInsets.fromLTRB(5, 5, 5, 0),
                child: Text(
                  // ignore: unnecessary_string_interpolations
                  "${product.productName}",
                  maxLines: 2,
                  style: const TextStyle(
                      overflow: TextOverflow.ellipsis,
                      color: ColorCodes.textColorSecondary,
                      fontSize: 14,
                      fontWeight: FontWeight.w600),
                ),
              )),
          Expanded(
            flex: 2,
            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Text(
                // ignore: unnecessary_string_interpolations
                "${formatNumber(product.pricePay)}",
                style: const TextStyle(
                  color: ColorCodes.pinkColor,
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                ),
              ),
              const Text(
                ' đ',
                style: TextStyle(
                  color: Color(0xFFF65E5E),
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                  decoration: TextDecoration.underline,
                ),
              ),
            ]),
          ),
          const SizedBox(
            height: 5,
          ),
          Expanded(
            flex: 3,
            child: Container(
              alignment: Alignment.center,
              height: 35,
              width: double.maxFinite,
              decoration: BoxDecoration(
                color: const Color(0xFFF65E5E),
                borderRadius: BorderRadius.circular(5),
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [
                      Colors.green.withOpacity(0.7),
                      Colors.blue.withOpacity(0.8)
                    ]),
              ),
              child: const Text(
                'Mua Ngay',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  letterSpacing: 0.5,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          ),
        ],
      ),
    ),
  );
}
