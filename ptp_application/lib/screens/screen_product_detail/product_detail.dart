import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/models/stock_detail.dart';
import 'package:ptp_application/models/image.dart' as img_obj;
import 'package:ptp_application/providers/product_detail_provider.dart';
import 'package:ptp_application/providers/rate_provider.dart';
import 'package:ptp_application/providers/image_provider.dart' as img_provider;
import 'package:ptp_application/providers/stock_detail_provider.dart';
import 'package:ptp_application/screens/screen_product_detail/components/design_appbar_product_detail.dart';
import 'package:ptp_application/screens/screen_product_detail/components/design_button_add_product_into_cart.dart';
import 'package:ptp_application/screens/screen_product_detail/components/design_product_detail.dart';
import 'package:ptp_application/screens/screen_product_detail/components/design_rates_product.dart';
import 'package:ptp_application/screens/screen_product_detail/components/design_similar_product.dart';
import 'package:ptp_application/screens/screen_product_detail/components/design_slider_image_product.dart';

// ignore: must_be_immutable
class ProductDetailScreen extends StatefulWidget {
  late String productId;
  ProductDetailScreen({required this.productId, Key? key}) : super(key: key);

  @override
  _ProductDetailScreenState createState() => _ProductDetailScreenState();
}

class _ProductDetailScreenState extends State<ProductDetailScreen> {
  late img_provider.ImageProvider imageProvider;
  late StockDetailProvider stockDetailProvider;
  late RateProvider rateProvider;
  // late StockDetail stockDetail;
  // late List<img_obj.Image> images;
  @override
  void initState() {
    super.initState();
    stockDetailProvider =
        Provider.of<StockDetailProvider>(context, listen: false);
    rateProvider = Provider.of<RateProvider>(context, listen: false);
    imageProvider =
        Provider.of<img_provider.ImageProvider>(context, listen: false);
    Provider.of<ProductDetailProvider>(context, listen: false)
        .updateQuantityOrder(1);
  }

  Future initInfoProductDetail() async {
    List<Future> futureCall = [];
    futureCall.add(stockDetailProvider
        .getFirstProductDetailInStockDetailByIdProduct(widget.productId));
    futureCall
        .add(imageProvider.getAllImageByProductIdAndStatus(widget.productId));

    futureCall
        .add(rateProvider.getAllRateByIdProductAndStatus(widget.productId, 1));

    await Future.wait(futureCall);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: designAppBar(context),
      body: Center(
        child: FutureBuilder(
          future: initInfoProductDetail(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return SizedBox(
                height: 350,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Text(
                      "Loading...",
                      style: TextStyle(
                        fontSize: 16,
                        letterSpacing: 0.5,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    CircularProgressIndicator(),
                  ],
                ),
              );
            } else {
              return Column(
                children: [
                  Expanded(
                    child: ListView(
                      children: [
                        designSliderImageProduct(imageProvider.images),
                        const DesignProductDetail(),
                        designSimilarProduct(context),
                        designRatesProduct(
                            context,
                            stockDetailProvider
                                .stockDetail!.productDetail.product),
                        const SizedBox(height: 5),
                      ],
                    ),
                  ),
                  designButtonAddProductIntoCart(
                      context, stockDetailProvider.stockDetail!.pricePay),
                ],
              );
            }
          },
        ),
      ),
    );
  }
}
