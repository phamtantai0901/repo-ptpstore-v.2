import 'package:flutter/material.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/screens/screen_my_rate/components/design_appbar_my_rate.dart';
import 'package:ptp_application/screens/screen_my_rate/components/design_body_my_rate.dart';

class MyRateScreen extends StatelessWidget {
  const MyRateScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: const DesignAppbarMyRate(),
        body: Container(
          padding: const EdgeInsets.all(5),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5),
            boxShadow: [
              BoxShadow(
                color: ColorCodes.colorPrimary.withOpacity(0.2),
                spreadRadius: 1,
                blurRadius: 2,
              ),
            ],
          ),
          child: const DesignBodyMyRate(),
        ),
      ),
    );
  }
}
