import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/config.dart';
import 'package:ptp_application/models/rate.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/providers/rate_provider.dart';

class DesignBodyMyRate extends StatefulWidget {
  const DesignBodyMyRate({Key? key}) : super(key: key);

  @override
  _DesignBodyMyRateState createState() => _DesignBodyMyRateState();
}

class _DesignBodyMyRateState extends State<DesignBodyMyRate> {
  double rating = 5;
  @override
  void initState() {
    super.initState();
    String? memberId =
        Provider.of<MemberProvider>(context, listen: false).member.memberId;
    Provider.of<RateProvider>(context, listen: false)
        .getAllRateByIdMemberAndStatus(memberId!, 1);
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<RateProvider>(builder: (context, rateProvider, child) {
      List<Rate> rates = rateProvider.rates;
      return ListView.builder(
          itemCount: rates.length,
          itemBuilder: (context, index) {
            return buildBoxProductMyRate(rates[index]);
          });
    });
  }

  //Design Box Product Rate
  Widget buildBoxProductMyRate(Rate rate) {
    return Container(
      padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white,
        boxShadow: const [
          BoxShadow(
            color: Color.fromARGB(255, 231, 228, 228),
            offset: Offset(0.0, 2.0),
            spreadRadius: 1,
            blurRadius: 10,
          ),
        ],
      ),
      child: Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          Container(
            padding: const EdgeInsets.all(5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(30.0),
                  child: CachedNetworkImage(
                    imageUrl:
                        Config.getIP('/avatar_users/${rate.member.user.image}'),
                    width: 35,
                    height: 35,
                    fit: BoxFit.cover,
                    placeholder: (context, url) => const Center(
                      child: CircularProgressIndicator(),
                    ),
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.error),
                  ),
                ),
                const SizedBox(width: 10),
                Text(
                  rate.member.user.fullName,
                  style: const TextStyle(
                    fontSize: 14,
                    color: ColorCodes.textColorSecondary,
                    fontWeight: FontWeight.w600,
                    letterSpacing: 0.5,
                  ),
                ),
                const SizedBox(width: 5),
                RatingBar.builder(
                    initialRating: double.parse('${rate.star}'),
                    ignoreGestures: true,
                    // minRating: 0,
                    direction: Axis.horizontal,
                    allowHalfRating: false,
                    // itemCount: 5,
                    itemSize: 18.0,
                    itemPadding: const EdgeInsets.symmetric(horizontal: 3.0),
                    itemBuilder: (context, _) => Icon(
                          Icons.star,
                          color: Colors.amber[500],
                        ),
                    onRatingUpdate: (rating) {}),
              ],
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5),
                    boxShadow: [
                      BoxShadow(
                        color: const Color.fromARGB(255, 230, 255, 255)
                            .withOpacity(0.2),
                        spreadRadius: 1,
                        blurRadius: 2,
                      ),
                    ],
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20.0),
                    child: CachedNetworkImage(
                      imageUrl: Config.getIP(
                          '/products/${rate.product.productId}/${rate.product.productImg}'),
                      fit: BoxFit.cover,
                      placeholder: (context, url) => const Center(
                        child: CircularProgressIndicator(),
                      ),
                      errorWidget: (context, url, error) =>
                          const Icon(Icons.error),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                width: 20,
              ),
              Expanded(
                flex: 7,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      rate.product.productName,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 3,
                      style: const TextStyle(
                        fontSize: 14,
                        overflow: TextOverflow.ellipsis,
                        color: ColorCodes.textColorSecondary,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        const Text(
                          "Ngày: ",
                          style: TextStyle(
                            fontSize: 14,
                            color: ColorCodes.textColorSecondary,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        Text(
                          rate.dateRate,
                          style: const TextStyle(
                            fontSize: 14,
                            color: ColorCodes.textColorSecondary,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          rate.comment!.isEmpty
              ? Container()
              : Container(
                  padding: const EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    color: ColorCodes.colorPrimary.withOpacity(0.2),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  width: MediaQuery.of(context).size.width - 20,
                  child: Text(
                    rate.comment!,
                    style: const TextStyle(
                      fontSize: 16,
                      letterSpacing: 0.5,
                      color: ColorCodes.textColorPrimary,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
          const SizedBox(
            height: 5,
          ),
        ],
      ),
    );
  }
}
