import 'package:flutter/material.dart';
import 'package:ptp_application/custom.dart';

class DesignAppbarMyRate extends StatelessWidget
    implements PreferredSizeWidget {
  const DesignAppbarMyRate({Key? key}) : super(key: key);

  @override
  Size get preferredSize => const Size.fromHeight(heighAppBarMd);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 0.3,
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: const Icon(
          Icons.arrow_back,
          color: ColorCodes.textColorSecondary,
          size: 25,
        ),
      ),
      // automaticallyImplyLeading: ,
      centerTitle: true,
      title: Container(
        padding: const EdgeInsets.only(right: 25),
        alignment: Alignment.center,
        child: const Text(
          "ĐƠN HÀNG ĐÁNH GIÁ",
          textAlign: TextAlign.center,
          style: TextStyle(
              color: ColorCodes.textColorPrimary,
              fontSize: FontSize.fontSizeAppBar,
              fontWeight: FontWeight.w600),
        ),
      ),
    );
  }
}
