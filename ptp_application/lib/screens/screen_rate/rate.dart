import 'package:flutter/material.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/models/bill_detail.dart';
import 'package:ptp_application/screens/screen_rate/components/design_appbar_rate.dart';
import 'components/design_box_product_rate.dart';

// ignore: must_be_immutable
class RateScreen extends StatelessWidget {
  final BillDetail billDetail;
  RateScreen({required this.billDetail, Key? key}) : super(key: key);
  double rating = 5;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: const DesignAppbarRate(),
        body: ListView(
          children: [
            Container(
              width: MediaQuery.of(context).size.width - 20,
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5),
                boxShadow: [
                  BoxShadow(
                    color: ColorCodes.colorPrimary.withOpacity(0.2),
                    spreadRadius: 1,
                    blurRadius: 2,
                  ),
                ],
              ),
              child: DesignBoxProductRate(billDetail: billDetail),
            ),
          ],
        ),
      ),
    );
  }
}
