import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/config.dart';
import 'package:ptp_application/main.dart';
import 'package:ptp_application/models/bill_detail.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/providers/rate_provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/screens/show_dialog.dart';

class DesignBoxProductRate extends StatefulWidget {
  final BillDetail billDetail;
  const DesignBoxProductRate({required this.billDetail, Key? key})
      : super(key: key);
  @override
  _DesignBoxProductRate createState() => _DesignBoxProductRate();
}

class _DesignBoxProductRate extends State<DesignBoxProductRate> {
  double rating = 5;
  late TextEditingController comment;
  String? id;
  @override
  void initState() {
    super.initState();
    id = Provider.of<MemberProvider>(context, listen: false).member.memberId;
    comment = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        Row(
          children: [
            Expanded(
              flex: 3,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20.0),
                  child: CachedNetworkImage(
                    imageUrl: Config.getIP(
                        "/products/${widget.billDetail.productDetail.product.productId}/${widget.billDetail.productDetail.product.productImg}"),
                    width: 100,
                    height: 100,
                    fit: BoxFit.scaleDown,
                    placeholder: (context, url) => const Center(
                      child: CircularProgressIndicator(),
                    ),
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.error),
                  ),
                ),
              ),
            ),
            const SizedBox(
              width: 20,
            ),
            Expanded(
              flex: 7,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.billDetail.productDetail.product.productName,
                    maxLines: 2,
                    style: const TextStyle(
                        fontSize: 16,
                        overflow: TextOverflow.ellipsis,
                        color: ColorCodes.textColorSecondary,
                        fontWeight: FontWeight.w600),
                  ),
                  const SizedBox(height: 5),
                  Row(
                    children: [
                      const Text(
                        "Số lượng: ",
                        style: TextStyle(
                          fontSize: 14,
                          color: ColorCodes.textColorSecondary,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      Text(
                        "${widget.billDetail.quantity}",
                        style: const TextStyle(
                          fontSize: 14,
                          color: ColorCodes.textColorSecondary,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 5),
                  Row(
                    children: [
                      const Text(
                        "Kích thước: ",
                        style: TextStyle(
                          fontSize: 14,
                          color: ColorCodes.textColorSecondary,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      Text(
                        widget.billDetail.productDetail.size.sizeName,
                        style: const TextStyle(
                          fontSize: 14,
                          color: ColorCodes.textColorSecondary,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 5),
                  Row(
                    children: [
                      Text(
                        // ignore: prefer_adjacent_string_concatenation
                        "${formatNumber(widget.billDetail.price)} ",
                        style: const TextStyle(
                          fontSize: 18,
                          color: ColorCodes.pinkColor,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      const Text(
                        "đ",
                        style: TextStyle(
                            fontSize: 18,
                            color: ColorCodes.pinkColor,
                            fontWeight: FontWeight.w600,
                            decoration: TextDecoration.underline),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 25,
        ),
        SizedBox(
          width: MediaQuery.of(context).size.width - 20,
          child: TextFormField(
            minLines: 5,
            maxLines: 5,
            controller: comment,
            decoration: InputDecoration(
              fillColor:
                  const Color.fromRGBO(158, 158, 158, 1).withOpacity(0.05),
              filled: true,
              hintText: 'Hãy chia sẻ những điều bạn thích về sản phẩm này nhé.',
              hintStyle: TextStyle(
                fontSize: 16,
                letterSpacing: 0.5,
                fontWeight: FontWeight.w500,
                color: ColorCodes.textColorSecondary.withOpacity(0.8),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: ColorCodes.colorPrimary.withOpacity(0.4),
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  color: ColorCodes.colorPrimary,
                ),
              ),
            ),
          ),
        ),
        const SizedBox(
          height: 15,
        ),
        Text(
          "ĐÁNH GIÁ DỊCH VỤ CỦA CHÚNG TÔI",
          style: TextStyle(
            fontSize: 18,
            color: ColorCodes.textColorSecondary.withOpacity(0.8),
            fontWeight: FontWeight.w600,
          ),
        ),
        const SizedBox(
          height: 15,
        ),
        designRatingBar(),
        const SizedBox(
          height: 30,
        ),
        designButtonRating(context),
        const SizedBox(
          height: 30,
        ),
      ],
    );
  }

  Widget designButtonRating(context) {
    return Container(
      width: MediaQuery.of(context).size.width - 10,
      height: 60,
      decoration: BoxDecoration(
        gradient: const LinearGradient(
          colors: <Color>[
            Color(0xFF6DFFCD),
            Color(0xFF6DCFCF),
            ColorCodes.colorPrimary,
          ],
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      child: TextButton(
        onPressed: () async {
          showDialogLoading(context);
          await Provider.of<RateProvider>(context, listen: false)
              .addRate(id!, widget.billDetail.productDetail.product.productId,
                  rating, comment.text, widget.billDetail.billDetailId!)
              .then((isRated) {
            if (isRated == false) {
              dismissDialog(context);
              showMessageDialog(
                  context, "Rất tiếc đã có lỗi xảy ra", Icons.error);
              Future.delayed(
                  const Duration(seconds: 2), () => dismissDialog(context));
            } else {
              dismissDialog(context);
              showDialogLoadingSuccessfully(context,
                  "Xin chân thành cảm ơn bạn vì đã mua sản phẩm của shop");
              Future.delayed(const Duration(seconds: 1), () {
                dismissDialog(context);
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                    // ignore: prefer_const_constructors
                    builder: (ctx) => BottomNavAppBar(currentIndex: 0),
                  ),
                  (route) => false,
                );
              });
            }
          }).catchError((error) {
            dismissDialog(context);
            showMessageDialog(context, error, Icons.check_circle_outlined);
            Future.delayed(
                const Duration(seconds: 2), () => dismissDialog(context));
          });
        },
        child: const Text(
          'ĐÁNH GIÁ',
          style: TextStyle(
            letterSpacing: 0.5,
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
    );
  }

//Design Rating Bar
  Widget designRatingBar() {
    return Column(children: [
      RatingBar.builder(
        initialRating: rating,
        // minRating: 0,
        direction: Axis.horizontal,
        allowHalfRating: false,
        // itemCount: 5,
        itemSize: 50.0,
        itemPadding: const EdgeInsets.symmetric(horizontal: 3.0),
        itemBuilder: (context, _) => Icon(
          Icons.star,
          color: Colors.amber[500],
        ),
        onRatingUpdate: (rating) => setState(
          () => this.rating = rating,
        ),
      ),
      const SizedBox(
        height: 10,
      ),
      Container(
        // padding: const EdgeInsets.all(10),
        alignment: Alignment.center,
        width: 50,
        height: 50,
        decoration: BoxDecoration(
          border: Border.all(
              color: ColorCodes.pinkColor.withOpacity(0.8), width: 2.0),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.1),
              spreadRadius: 2,
              blurRadius: 10,
            ),
          ],
          color: Colors.white,
          borderRadius: BorderRadius.circular(30),
        ),
        child: Text(
          '$rating',
          style: const TextStyle(
            fontSize: 20,
            color: ColorCodes.pinkColor,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    ]);
  }
}
