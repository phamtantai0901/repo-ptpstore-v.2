import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/config.dart';
import 'package:ptp_application/models/product.dart';
import 'package:ptp_application/models/stock_detail.dart';
import 'package:ptp_application/providers/product_provider.dart';
import 'package:ptp_application/providers/rate_provider.dart';
import 'package:ptp_application/providers/stock_detail_provider.dart';
import 'package:ptp_application/screens/screen_product_detail/product_detail.dart';
import 'package:ptp_application/screens/show_dialog.dart';

class DesignListProduct extends StatefulWidget {
  const DesignListProduct({Key? key}) : super(key: key);

  @override
  _DesignListProductState createState() => _DesignListProductState();
}

class _DesignListProductState extends State<DesignListProduct> {
  late StockDetailProvider stockDetailProvider;
  late Future<List<StockDetail>> futureStockDetail;
  List<StockDetail> listStock = [];
  late RateProvider rateProvider;
  late ProductProvider productProvider;
  late Future<List<Product>> futureProduct;
  String? dropdownValue;
  List<String> listItem = [
    'Tất cả',
    'Dưới 500k',
    '500K đến 1 triệu',
    '1 triệu đến 2 triệu',
    'Trên 2 triệu'
  ];

  @override
  void initState() {
    super.initState();
    stockDetailProvider =
        Provider.of<StockDetailProvider>(context, listen: false);
    rateProvider = Provider.of<RateProvider>(context, listen: false);
    futureStockDetail =
        stockDetailProvider.getAllProductInStockAndRateAndPayByStatus(1);
    productProvider = Provider.of<ProductProvider>(context, listen: false);
    futureProduct = productProvider.getAllProductInStock();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<StockDetail>>(
        future: futureStockDetail,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Container(
              alignment: Alignment.center,
              child: Column(
                children: const [
                  Text(
                    "Loading...",
                    style: TextStyle(
                      fontSize: 16,
                      letterSpacing: 0.5,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  CircularProgressIndicator(),
                ],
              ),
            );
          } else {
            if (snapshot.hasData && snapshot.data!.isEmpty) {
              return const Center(
                child: Text(
                  "Danh Sách Trống",
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w500,
                      color: Colors.black,
                      letterSpacing: 0.5),
                ),
              );
            } else {
              if (listStock.isEmpty) {
                listStock = snapshot.data!;
              }

              return Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(left: 10),
                        child: buildItemDropdown(),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10),
                  GridView.builder(
                    physics: const ScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: listStock.length,
                    itemBuilder: (context, index) {
                      StockDetail stockDetail = listStock[index];
                      return buildProduct(stockDetail);
                    },
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      mainAxisSpacing: 10,
                      mainAxisExtent: 270,
                    ),
                  ),
                ],
              );
            }
          }
        });
  }

  //Design show items in list dropdown -> use in buildDropdownSearch()
  Widget buildItemDropdown() {
    return Container(
      height: 40,
      width: 180,
      padding: const EdgeInsets.only(left: 20, right: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: DropdownButtonHideUnderline(
        child: DropdownButton<String>(
          style: const TextStyle(color: Colors.grey, fontSize: 13),
          isExpanded: true,
          iconSize: 25,
          icon: const Icon(
            Icons.filter_alt_outlined,
            color: ColorCodes.colorPrimary,
          ),
          value: dropdownValue,
          onChanged: (newValue) async {
            dropdownValue = newValue;
            listStock = Provider.of<StockDetailProvider>(context, listen: false)
                .sortPriceProduct(listStock, newValue, false);
            if (listStock.isEmpty) {
              showMessageDialog(
                  context, "Xin lỗi hiện không có sản phẩm nào", Icons.warning);
              await Future.delayed(
                const Duration(seconds: 2),
                () => dismissDialog(context),
              );
            }

            setState(() {});
          },
          hint: const Text(
            'Lọc Theo Giá',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 15,
              letterSpacing: 0.5,
            ),
          ),
          items: listItem
              .map((String item) => DropdownMenuItem<String>(
                    value: item,
                    child: Text(
                      item,
                      style: const TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 15,
                        letterSpacing: 0.5,
                      ),
                    ),
                  ))
              .toList(),
        ),
      ),
    );
  }

  //Desgin product -> use builListProduct()
  Widget buildProduct(StockDetail stockDetail) {
    Product product = stockDetail.productDetail.product;
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ProductDetailScreen(
                      productId: product.productId,
                    )));
      },
      child: Container(
        margin: const EdgeInsets.only(top: 7, left: 7, right: 7, bottom: 7),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
          boxShadow: const [
            BoxShadow(
              color: Color(0xFFCFDEE3),
              offset: Offset(0.0, 1.0),
              spreadRadius: 2,
              blurRadius: 10,
            ),
          ],
        ),
        child: Column(
          children: [
            Expanded(
              flex: 4,
              child: ClipRRect(
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                ),
                child: CachedNetworkImage(
                  imageUrl: Config.getIP(
                      '/products/${product.productId}/${product.productImg}'),
                  fit: BoxFit.cover,
                  placeholder: (context, url) => const Center(
                    child: CircularProgressIndicator(),
                  ),
                  errorWidget: (context, url, error) => const Icon(Icons.error),
                ),
                // Image.network(
                //   Config.getIP(
                //       '/products/${product.productId}/${product.productImg}'),

                // ),
              ),
            ),
            Expanded(
              flex: 0,
              child: Container(
                padding: const EdgeInsets.only(left: 10, right: 5),
                child: Text(
                  product.productName,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                    color: ColorCodes.textColorSecondary,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 0,
              child: Text(
                '${formatNumber(stockDetail.pricePay)} đ',
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: const TextStyle(
                    color: Color(0xFFF65E5E),
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                    letterSpacing: 0.5),
              ),
            ),
            Expanded(
              flex: 0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  buildRateProduct(
                      stockDetail.quantityRate!, stockDetail.avgStar!),
                  buildSalesProduct(stockDetail.quantityPay!),
                ],
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            Expanded(
              flex: 1,
              child: Container(
                margin: const EdgeInsets.only(left: 80),
                alignment: Alignment.center,
                height: 30,
                width: double.maxFinite,
                decoration: BoxDecoration(
                  color: const Color(0xFFF65E5E),
                  borderRadius: const BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    topRight: Radius.circular(10),
                    topLeft: Radius.circular(4),
                  ),
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: [
                        Colors.green.withOpacity(0.7),
                        Colors.blue.withOpacity(0.8)
                      ]),
                ),
                child: const Text(
                  'Mua Ngay',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    letterSpacing: 0.5,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  //Desgin sales product -> use buildProduct()
  Widget buildSalesProduct(int sales) {
    return Container(
      padding: const EdgeInsets.only(right: 4),
      child: Text(
        '$sales đã bán',
        style: const TextStyle(
          color: ColorCodes.textColorSecondary,
          fontWeight: FontWeight.w500,
          fontSize: 16,
        ),
      ),
    );
  }

  //Desgin rate product -> use buildProduct()
  Widget buildRateProduct(int rate, double avgStar) {
    return Row(
      children: [
        const SizedBox(width: 4),
        const Icon(
          Icons.star_outlined,
          size: 16,
          color: Colors.yellow,
        ),
        Text(
          avgStar.toStringAsFixed(1),
          style: const TextStyle(
            color: ColorCodes.textColorSecondary,
            fontSize: 16,
            letterSpacing: 0.5,
            fontWeight: FontWeight.w500,
          ),
        ),
        Text(
          '($rate)',
          style: const TextStyle(
            color: ColorCodes.textColorSecondary,
            fontSize: 16,
            letterSpacing: 0.5,
            fontWeight: FontWeight.w500,
          ),
        ),
      ],
    );
  }
}
