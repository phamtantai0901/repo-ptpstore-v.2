//Desgin border directory
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/config.dart';
import 'package:ptp_application/models/category.dart';
import 'package:ptp_application/providers/category_provider.dart';
import 'package:ptp_application/providers/product_provider.dart';
import 'package:ptp_application/screens/screen_category/category.dart';

class DesignCategory extends StatefulWidget {
  const DesignCategory({Key? key}) : super(key: key);

  @override
  _DesignCategoryState createState() => _DesignCategoryState();
}

class _DesignCategoryState extends State<DesignCategory> {
  late CategoryProvider categoryProvider;
  @override
  void initState() {
    super.initState();
    categoryProvider = Provider.of<CategoryProvider>(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      // ignore: prefer_const_constructors
      physics: ScrollPhysics(),
      children: [
        Container(
          padding: const EdgeInsets.only(left: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'DANH MỤC',
                style: TextStyle(
                  fontSize: 16,
                  letterSpacing: 0.5,
                  color: const Color(0xFF43cea2).withOpacity(0.9),
                  fontWeight: FontWeight.w700,
                ),
              ),
              TextButton.icon(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          // ignore: prefer_const_constructors
                          builder: (context) => CategoryScreen()));
                },
                icon: Text(
                  'Xem thêm',
                  style: TextStyle(
                    fontSize: 16,
                    letterSpacing: 0.5,
                    color: const Color(0xFF43cea2).withOpacity(0.9),
                    fontWeight: FontWeight.w600,
                  ),
                ),
                label: Icon(
                  Icons.navigate_next_outlined,
                  size: 28,
                  color: const Color(0xFF43cea2).withOpacity(0.9),
                ),
              )
            ],
          ),
        ),
        Container(
          alignment: Alignment.center,
          margin: const EdgeInsets.only(left: 5),
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          height: 130,
          width: double.maxFinite,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: const Color(0xFFE6F7FD),
            //(0xFFF1F1FE),
          ),
          child: FutureBuilder<List<Category>>(
            future: categoryProvider.fetchAllCategoryInStock(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Container(
                  alignment: Alignment.center,
                  child: Column(
                    children: const [
                      Text(
                        "Loading...",
                        style: TextStyle(
                          fontSize: 16,
                          letterSpacing: 0.5,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      CircularProgressIndicator(),
                    ],
                  ),
                );
              } else {
                if (snapshot.hasData && snapshot.data!.isNotEmpty) {
                  return ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: categoryProvider.categories.length,
                    itemBuilder: (context, index) =>
                        buildCategory(categoryProvider.categories[index]),
                  );
                } else if (snapshot.hasError) {
                  return Center(child: Text(snapshot.error.toString()));
                } else {
                  return const Center(
                    child: Text(
                      "Danh Sách Trống",
                      style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w500,
                          color: Colors.black,
                          letterSpacing: 0.5),
                    ),
                  );
                }
              }
            },
          ),
        ),
      ],
    );
  }

//Design directory -> use buildBorderDirectory()
  Widget buildCategory(Category category) {
    int quantityProduct = Provider.of<ProductProvider>(context, listen: false)
        .getAllProductByCategoryId(category.categoryId)
        .length;
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white,
      ),
      margin: const EdgeInsets.all(4),
      width: 110,
      height: double.maxFinite,
      child: TextButton(
        onPressed: () {
          // Navigator.push(context,
          //     MaterialPageRoute(builder: (context) => ProductDetailScreen()));
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: CachedNetworkImage(
                imageUrl: Config.getIP('/categories/${category.suffixImg}'),
                width: 50,
                height: 40,
                fit: BoxFit.scaleDown,
                placeholder: (context, url) => const Center(
                  child: CircularProgressIndicator(),
                ),
                errorWidget: (context, url, error) => const Icon(Icons.error),
              ),
            ),
            Text(
              category.categoryName,
              style: const TextStyle(
                color: ColorCodes.textColorSecondary,
                fontSize: 13,
              ),
            ),
            Container(
              alignment: Alignment.centerRight,
              child: Text(
                '($quantityProduct)+',
                style: const TextStyle(color: Colors.grey, fontSize: 10),
              ),
            )
          ],
        ),
      ),
    );
  }
}
