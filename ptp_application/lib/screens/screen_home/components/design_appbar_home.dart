import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/models/cart.dart';
import 'package:ptp_application/models/notification.dart';
import 'package:ptp_application/providers/cart_provider.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/providers/notification_provider.dart';
import 'package:ptp_application/screens/screen_cart/cart.dart';
import 'package:ptp_application/screens/screen_notification/notification.dart';
import 'package:ptp_application/screens/screen_search_product/search_product.dart';
import 'package:ptp_application/screens/show_dialog.dart';

class DesignAppBarHome extends StatefulWidget implements PreferredSizeWidget {
  // ignore: prefer_const_constructors_in_immutables
  DesignAppBarHome({Key? key}) : super(key: key);

  @override
  _DesignAppBarHomeState createState() => _DesignAppBarHomeState();

  @override
  Size get preferredSize => const Size.fromHeight(heighAppBarMaxLg);
}

class _DesignAppBarHomeState extends State<DesignAppBarHome> {
  late TextEditingController keyword;
  @override
  void initState() {
    super.initState();
    keyword = TextEditingController();
    String memberId =
        Provider.of<MemberProvider>(context, listen: false).member.memberId!;
    Provider.of<NotificationProvider>(context, listen: false)
        .fetchAllNotificationUnread(memberId, 0);
    Provider.of<CartProvider>(context, listen: false)
        .fetchAllCartByIdMember(memberId);
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      flexibleSpace: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(colors: <Color>[
            ColorCodes.colorPrimary,
            const Color(0xFF26cba3).withOpacity(0.9),
            ColorCodes.colorPrimary,
          ], begin: Alignment.centerLeft, end: Alignment.centerRight),
        ),
      ),
      elevation: 0,
      leading: Container(
        padding: const EdgeInsets.only(left: 10),
        alignment: Alignment.center,
        child: const Text(
          'PTPStore+',
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 12,
            color: Colors.white,
            letterSpacing: 1,
          ),
        ),
      ),
      leadingWidth: 100,
      centerTitle: true,
      title: Container(
        margin: const EdgeInsets.only(top: 5),
        padding: const EdgeInsets.only(top: 3, bottom: 3),
        alignment: Alignment.center,
        width: 120,
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(15),
            bottomLeft: Radius.circular(10),
          ),
        ),
        child: const Text(
          'PTPStore',
          style: TextStyle(
            color: ColorCodes.colorPrimary,
            fontSize: 16,
            fontWeight: FontWeight.w600,
            letterSpacing: 0.5,
          ),
        ),
      ),
      actions: [
        GestureDetector(
          onTap: () {
            Navigator.push(
                // ignore: prefer_const_constructors
                context,
                MaterialPageRoute(
                    builder: (context) => const ScreenNotification()));
          },
          child: Consumer<NotificationProvider>(
            builder: (context, notiProvider, child) {
              List<NotificationModel> notis = notiProvider.notificationsUnread;
              return SizedBox(
                width: 80,
                child: Stack(
                  alignment: AlignmentDirectional.center,
                  children: [
                    const Icon(Icons.notifications_outlined,
                        color: Colors.white, size: 30),
                    notis.isEmpty
                        ? Container()
                        : Positioned(
                            bottom: 18,
                            right: 15,
                            child: Container(
                              alignment: Alignment.center,
                              width: 25,
                              height: 25,
                              decoration: BoxDecoration(
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(50),
                                ),
                                color: Colors.red[400],
                              ),
                              child: Text(
                                notis.length >= 99 ? '99+' : '${notis.length}',
                                style: const TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                          ),
                  ],
                ),
              );
            },
          ),
        ),
        GestureDetector(
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => const CartScreen()));
          },
          child: Consumer<CartProvider>(
            builder: (context, cartProvider, child) {
              List<Cart> carts = cartProvider.carts;
              return SizedBox(
                width: 80,
                child: Stack(
                  alignment: AlignmentDirectional.center,
                  children: [
                    const Icon(Icons.shopping_cart_outlined,
                        color: Colors.white, size: 30),
                    carts.isEmpty
                        ? Container()
                        : Positioned(
                            bottom: 18,
                            right: 15,
                            child: Container(
                              alignment: Alignment.center,
                              width: 25,
                              height: 25,
                              decoration: BoxDecoration(
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(50),
                                ),
                                color: Colors.red[400],
                              ),
                              child: Text(
                                carts.length >= 99 ? '99+' : '${carts.length}',
                                style: const TextStyle(
                                    color: Colors.white,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ),
                  ],
                ),
              );
            },
          ),
        ),
      ],
      bottom: PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: buildTxtSearch(),
      ),
    );
  }

  //Design Text Field Search
  Widget buildTxtSearch() {
    return Container(
      padding: const EdgeInsets.all(10),
      child: TextField(
        controller: keyword,
        cursorColor: ColorCodes.colorPrimary,
        style: const TextStyle(
          fontSize: 18,
          color: ColorCodes.textColorSecondary,
          fontWeight: FontWeight.w600,
          letterSpacing: 0.5,
        ),
        decoration: InputDecoration(
          fillColor: Colors.white,
          filled: true,
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 25, vertical: 15),
          border: const OutlineInputBorder(),
          enabledBorder: const OutlineInputBorder(
            // width: 0.0 produces a thin "hairline" border
            borderSide: BorderSide(
              color: Colors.grey,
              width: 0.0,
            ),
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.grey,
              width: 0.0,
            ),
          ),
          hintText: 'Bạn tìm gì hôm nay?',
          hintStyle: const TextStyle(
            fontSize: 18,
            color: ColorCodes.textColorSecondary,
            letterSpacing: 0.5,
            fontWeight: FontWeight.w500,
          ),
          suffixIcon: IconButton(
            icon: const Icon(Icons.search_outlined),
            onPressed: () async {
              if (keyword.text == "") {
                showMessageDialog(
                    context, "Vui lòng không bỏ trống", Icons.error);
                Future.delayed(
                    const Duration(seconds: 2), () => dismissDialog(context));
              } else {
                await Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            SearchProductScreen(keyword: keyword.text)));
                keyword.clear();
              }
            },
          ),
        ),
      ),
    );
  }
}
