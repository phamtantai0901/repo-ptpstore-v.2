//Desgin border top search
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/config.dart';
import 'package:ptp_application/models/product.dart';
import 'package:ptp_application/providers/product_provider.dart';
import 'package:ptp_application/providers/stock_detail_provider.dart';
import 'package:ptp_application/screens/screen_product_detail/product_detail.dart';
import 'package:ptp_application/screens/show_dialog.dart';

class DesginTopSearchCategory extends StatefulWidget {
  const DesginTopSearchCategory({Key? key}) : super(key: key);

  @override
  _DesginTopSearchCategoryState createState() =>
      _DesginTopSearchCategoryState();
}

class _DesginTopSearchCategoryState extends State<DesginTopSearchCategory> {
  late ProductProvider productProvider;
  late Future<List<Product>> futureProduct;
  @override
  void initState() {
    super.initState();
    // stockDetailProvider =
    //     Provider.of<StockDetailProvider>(context, listen: false);
    productProvider = Provider.of<ProductProvider>(context, listen: false);
    futureProduct = productProvider.getProductHotPay();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      // ignore: prefer_const_constructors
      physics: ScrollPhysics(),
      children: [
        Container(
          padding: const EdgeInsets.only(left: 20, top: 15),
          alignment: Alignment.centerLeft,
          child: Text(
            'SẢN PHẨM NỔI BẬC',
            style: TextStyle(
                fontSize: 16,
                letterSpacing: 0.5,
                color: const Color(0xFF43cea2).withOpacity(0.9),
                fontWeight: FontWeight.w700),
          ),
        ),
        const SizedBox(height: 10),
        Container(
          padding: const EdgeInsets.only(left: 5, top: 10),
          alignment: Alignment.center,
          height: 200,
          child: FutureBuilder<List<Product>>(
            future: futureProduct,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Container(
                  alignment: Alignment.center,
                  child: Column(
                    children: const [
                      Text(
                        "Loading...",
                        style: TextStyle(
                          fontSize: 16,
                          letterSpacing: 0.5,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      CircularProgressIndicator(),
                    ],
                  ),
                );
              } else {
                if (snapshot.hasData && snapshot.data!.isNotEmpty) {
                  return ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: productProvider.productsHotPay.length,
                    itemBuilder: (context, index) => buildProduct(
                        context, productProvider.productsHotPay[index]),
                  );
                } else if (snapshot.hasError) {
                  return Center(child: Text(snapshot.error.toString()));
                } else {
                  return const Center(
                    child: Text(
                      "Danh Sách Trống",
                      style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w500,
                          color: Colors.black,
                          letterSpacing: 0.5),
                    ),
                  );
                }
              }
            },
          ),
        ),
      ],
    );
  }

//Desgin product -> use buildScrollViewProduct()
  Widget buildProduct(BuildContext context, Product product) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ProductDetailScreen(
                      productId: product.productId,
                    )));
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Colors.white,
          boxShadow: const [
            BoxShadow(
              color: Color(0xFFF1F1F1),
              offset: Offset(0.0, 1.0),
              spreadRadius: 1,
              blurRadius: 5,
            ),
          ],
        ),
        margin: const EdgeInsets.fromLTRB(3, 3, 3, 10),
        width: 150,
        child: Column(
          children: [
            Expanded(
              flex: 3,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(5),
                child: CachedNetworkImage(
                  imageUrl: Config.getIP(
                      "/products/${product.productId}/${product.productImg}"),
                  fit: BoxFit.fill,
                  placeholder: (context, url) => const Center(
                    child: CircularProgressIndicator(),
                  ),
                  errorWidget: (context, url, error) => const Icon(Icons.error),
                ),
              ),
            ),
            Expanded(
                flex: 0,
                child: Container(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: Text(
                    // ignore: unnecessary_string_interpolations
                    "${product.productName}",
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(
                      color: ColorCodes.textColorPrimary,
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                )),
            Expanded(
              flex: 0,
              child:
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Text(
                  // ignore: unnecessary_string_interpolations
                  "${formatNumber(product.pricePay)}",
                  style: const TextStyle(
                    color: Color(0xFFF65E5E),
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const Text(
                  ' đ',
                  style: TextStyle(
                    color: Color(0xFFF65E5E),
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    decoration: TextDecoration.underline,
                  ),
                ),
              ]),
            ),
            const SizedBox(
              height: 5,
            ),
            Expanded(
              flex: 0,
              child: Container(
                alignment: Alignment.center,
                height: 30,
                width: double.maxFinite,
                decoration: BoxDecoration(
                  color: const Color(0xFFF65E5E),
                  borderRadius: BorderRadius.circular(5),
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: [
                        Colors.green.withOpacity(0.7),
                        Colors.blue.withOpacity(0.8)
                      ]),
                ),
                child: const Text(
                  'Mua Ngay',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    letterSpacing: 0.5,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
