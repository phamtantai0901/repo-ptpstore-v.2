import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:ptp_application/config.dart';

class DesignSliderImage extends StatefulWidget {
  const DesignSliderImage({Key? key}) : super(key: key);

  @override
  _DesignSliderImageState createState() => _DesignSliderImageState();
}

class _DesignSliderImageState extends State<DesignSliderImage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        buildBanners(),
      ],
    );
  }

  CarouselSlider buildBanners() {
    return CarouselSlider.builder(
      itemCount: 8,
      itemBuilder: (context, indexItem, indexPage) {
        return Container(
          margin: const EdgeInsets.only(top: 0),
          child: CachedNetworkImage(
            imageUrl: Config.getIP('/banners/$indexItem.png'),
            fit: BoxFit.cover,
            placeholder: (context, url) => const Center(
              child: CircularProgressIndicator(),
            ),
            errorWidget: (context, url, error) => const Icon(Icons.error),
          ),
        );
      },
      options: CarouselOptions(
        viewportFraction: 1,
        autoPlay: true,
        enlargeCenterPage: true,
      ),
    );
  }
}
