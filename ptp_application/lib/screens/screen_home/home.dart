import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/main.dart';
import 'package:ptp_application/providers/notification_provider.dart';
import 'package:ptp_application/screens/screen_home/components/design_appbar_home.dart';
import 'package:ptp_application/screens/screen_home/components/design_border_top_search_category.dart';
import 'package:ptp_application/screens/screen_home/components/design_border_category.dart';
import 'package:ptp_application/screens/screen_home/components/design_list_product.dart';
import 'package:ptp_application/screens/screen_home/components/design_slider_image.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with AutomaticKeepAliveClientMixin<HomeScreen> {
  bool keepAlive = true;

  @override
  bool get wantKeepAlive => keepAlive;
  @override
  void initState() {
    super.initState();
    Provider.of<NotificationProvider>(context, listen: false)
        .registerNotification(context);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return RefreshIndicator(
      onRefresh: () async {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => BottomNavAppBar(currentIndex: 0)));
      },
      child: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Scaffold(
          appBar: DesignAppBarHome(),
          body: SafeArea(
            child: Container(
              decoration: BoxDecoration(
                color: const Color(0xFFeefaf6),
                borderRadius: BorderRadius.circular(5),
              ),
              child: ListView(
                keyboardDismissBehavior:
                    ScrollViewKeyboardDismissBehavior.onDrag,
                children: const [
                  DesignSliderImage(),
                  SizedBox(height: 5),
                  DesignCategory(),
                  SizedBox(height: 15),
                  DesginTopSearchCategory(),
                  SizedBox(height: 10),
                  DesignListProduct(),
                  SizedBox(height: 10),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
