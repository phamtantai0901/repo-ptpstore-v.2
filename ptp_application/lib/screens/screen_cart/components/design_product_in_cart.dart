import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/config.dart';
import 'package:ptp_application/models/cart.dart';
import 'package:ptp_application/providers/cart_provider.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/providers/stock_detail_provider.dart';
import 'package:ptp_application/screens/show_dialog.dart';

class DesignListProductInCart extends StatefulWidget {
  const DesignListProductInCart({Key? key}) : super(key: key);

  @override
  _DesignListProductInCartState createState() =>
      _DesignListProductInCartState();
}

class _DesignListProductInCartState extends State<DesignListProductInCart> {
  String dropdownValue = '36';
  @override
  void initState() {
    super.initState();
    String? id =
        Provider.of<MemberProvider>(context, listen: false).member.memberId;
    Provider.of<CartProvider>(context, listen: false)
        .fetchAllCartByIdMember(id);
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<CartProvider>(builder: (context, cartProvider, child) {
      return Container(
        alignment: Alignment.center,
        // padding: const EdgeInsets.fromLTRB(15, 10, 15, 0),
        child: cartProvider.carts.isEmpty
            ? const Center(
                child: Text(
                  "Giỏ Hàng Trống",
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      color: ColorCodes.textColorSecondary,
                      letterSpacing: 0.5),
                ),
              )
            : ListView.builder(
                itemCount: cartProvider.carts.length,
                itemBuilder: (context, index) {
                  return buildProductIntoCart(cartProvider.carts[index]);
                }),
      );
    });
  }

  //Design build Product
  Widget buildProductIntoCart(Cart cart) {
    return Container(
      margin: const EdgeInsets.only(bottom: 1, top: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Colors.white,
        boxShadow: const [
          BoxShadow(
            color: Color(0xFFF1F1F1),
            offset: Offset(0.0, 1.0),
            spreadRadius: 2,
            blurRadius: 10,
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            flex: 1,
            child: IconButton(
                onPressed: () {
                  CartProvider cartProvider =
                      Provider.of<CartProvider>(context, listen: false);
                  cartProvider.chooseCart(cart);
                  if (cartProvider.totalPrice == 0) {
                    Provider.of<CartProvider>(context, listen: false)
                        .setVoucher(null);
                  }
                },
                icon: cart.isChoosed
                    ? const Icon(
                        Icons.verified,
                        color: Colors.green,
                      )
                    : Icon(
                        Icons.verified_outlined,
                        color: Colors.grey[500],
                      ),
                iconSize: 25),
          ),
          // const SizedBox(width: 10),
          Expanded(
            flex: 4,
            child: Container(
              width: 70,
              height: 110,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: CachedNetworkImage(
                  imageUrl: Config.getIP(
                      '/products/${cart.productDetail.product.productId}/${cart.productDetail.product.productImg}'),
                  fit: BoxFit.cover,
                  placeholder: (context, url) => const Center(
                    child: CircularProgressIndicator(),
                  ),
                  errorWidget: (context, url, error) => const Icon(Icons.error),
                ),
                //   Config.getIP(
                //       '/products/${cart.productDetail.product.productId}/${cart.productDetail.product.productImg}'),
                //   fit: BoxFit.scaleDown,
                // ),
              ),
            ),
          ),
          const SizedBox(width: 5),
          Expanded(
            flex: 6,
            child: Container(
              padding: const EdgeInsets.only(top: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    cart.productDetail.product.productName,
                    maxLines: 1,
                    style: const TextStyle(
                      fontSize: 14,
                      color: ColorCodes.textColorPrimary,
                      fontWeight: FontWeight.w500,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  const SizedBox(height: 4),
                  Row(
                    children: [
                      Text(
                        cart.productDetail.color.colorName,
                        style: const TextStyle(
                          fontSize: 15,
                          color: ColorCodes.pinkColor,
                          fontWeight: FontWeight.w600,
                          overflow: TextOverflow.ellipsis,
                          letterSpacing: 0.5,
                        ),
                      ),
                      const SizedBox(width: 15),
                      Text(
                        cart.productDetail.size.sizeName,
                        style: const TextStyle(
                          fontSize: 16,
                          color: ColorCodes.pinkColor,
                          fontWeight: FontWeight.w600,
                          overflow: TextOverflow.ellipsis,
                          letterSpacing: 0.5,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      const Text(
                        'Số lượng:',
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      const SizedBox(width: 15),
                      Row(
                        children: [
                          IconButton(
                            onPressed: () {
                              Provider.of<CartProvider>(context, listen: false)
                                  .removeQuantityInCart(cart);
                            },
                            icon: Container(
                              alignment: Alignment.center,
                              height: 30,
                              width: 30,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                gradient: LinearGradient(
                                  colors: <Color>[
                                    ColorCodes.colorPrimary,
                                    const Color(0xFFb6fcd5).withOpacity(0.7),
                                  ],
                                ),
                              ),
                              child: Icon(
                                Icons.remove,
                                size: 20,
                                color: Colors.grey[700],
                              ),
                            ),
                          ),
                          const SizedBox(width: 5),
                          Text(
                            '${cart.quantity}',
                            style: const TextStyle(
                              color: ColorCodes.textColorSecondary,
                              fontSize: 22,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          const SizedBox(width: 5),
                          IconButton(
                            onPressed: () {
                              Provider.of<StockDetailProvider>(context,
                                      listen: false)
                                  .checkQuantityProductDetailInStock(
                                      cart.productDetail.productDetailId,
                                      cart.quantity)
                                  .then((value) {
                                if (!value) {
                                  showMessageDialog(
                                      context,
                                      "Rất tiếc, số lượng trong kho không đủ",
                                      Icons.error);
                                  Future.delayed(const Duration(seconds: 1),
                                      () => dismissDialog(context));
                                } else {
                                  Provider.of<CartProvider>(context,
                                          listen: false)
                                      .addQuantityInCart(cart);
                                }
                              });
                            },
                            icon: Container(
                              alignment: Alignment.center,
                              height: 30,
                              width: 30,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                gradient: LinearGradient(
                                  colors: <Color>[
                                    ColorCodes.colorPrimary,
                                    const Color(0xFFb6fcd5).withOpacity(0.7),
                                  ],
                                ),
                              ),
                              child: Icon(
                                Icons.add,
                                size: 20,
                                color: Colors.grey[700],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        '${formatNumber(cart.pricePay)} đ',
                        style: const TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w600,
                            color: ColorCodes.pinkColor),
                      ),
                      // ignore: avoid_unnecessary_containers
                      Container(
                        // margin: EdgeInsets.only(left: 15),
                        child: TextButton(
                          onPressed: () {
                            CartProvider cartProvider =
                                Provider.of<CartProvider>(context,
                                    listen: false);
                            cartProvider.removeCartById(cart).then((value) {
                              showDialogLoadingSuccessfully(
                                  context, "Xoá Thành Công");
                              Future.delayed(const Duration(seconds: 1),
                                  () => dismissDialog(context));
                              if (cartProvider.totalPrice == 0) {
                                // cartProvider.setVoucher(null);
                              }
                            }).catchError((error) =>
                                // ignore: invalid_return_type_for_catch_error
                                showMessageDialog(context, error, Icons.error));
                          },
                          child: const Text(
                            'Xoá',
                            style: TextStyle(
                              color: ColorCodes.pinkColor,
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
