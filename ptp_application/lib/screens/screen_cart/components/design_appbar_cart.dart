import 'package:flutter/material.dart';
import 'package:ptp_application/custom.dart';

class DesignAppBarCart extends StatelessWidget implements PreferredSizeWidget {
  const DesignAppBarCart({Key? key}) : super(key: key);
  @override
  Size get preferredSize => const Size.fromHeight(heighAppBarLg);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 0.3,
      leading: Navigator.canPop(context)
          ? IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(
                Icons.arrow_back,
                color: ColorCodes.textColorSecondary,
                size: 30,
              ))
          : null,
      // automaticallyImplyLeading: ,
      centerTitle: true,
      title: Container(
        width: 150,
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            icons(
                Icons.add_shopping_cart_outlined, ColorCodes.colorPrimary, 22),
            const Text(
              '-',
              style: TextStyle(
                fontWeight: FontWeight.w600,
                color: ColorCodes.textColorSecondary,
              ),
            ),
            icons(Icons.where_to_vote_outlined, Colors.black, 22),
            const Text(
              '-',
              style: TextStyle(
                fontWeight: FontWeight.w600,
                color: ColorCodes.textColorSecondary,
              ),
            ),
            icons(Icons.verified_outlined, Colors.black, 22),
          ],
        ),
      ),
      bottom: PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: Container(
          alignment: Alignment.center,
          child: const Text(
            'GIỎ HÀNG',
            textAlign: TextAlign.center,
            style: TextStyle(
                color: ColorCodes.textColorPrimary,
                fontSize: FontSize.fontSizeAppBar,
                fontWeight: FontWeight.w600),
          ),
        ),
      ),
    );
  }

//Design Icon
  Widget icons(IconData icon, Color color, double sizeIcon) {
    return Icon(
      icon,
      color: color,
      size: sizeIcon,
    );
  }
}
