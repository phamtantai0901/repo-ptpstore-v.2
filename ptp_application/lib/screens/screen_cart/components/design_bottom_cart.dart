import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/models/voucher.dart';
import 'package:ptp_application/providers/cart_provider.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/screens/screen_address/address.dart';
import 'package:ptp_application/screens/screen_voucher_member/screen_voucher_member.dart';
import 'package:ptp_application/screens/show_dialog.dart';

class DesignBottomCart extends StatefulWidget {
  const DesignBottomCart({Key? key}) : super(key: key);

  @override
  _DesignBottomCartState createState() => _DesignBottomCartState();
}

class _DesignBottomCartState extends State<DesignBottomCart> {
  bool isCheck = true;
  Voucher? voucher;
  double salePrice = 0;
  double totalPrice = 0;
  int totalQuantity = 0;
  double intoMoney = 0;

  @override
  void initState() {
    super.initState();
    Provider.of<CartProvider>(context, listen: false).setVoucher(null);
  }

  @override
  Widget build(BuildContext context) {
    voucher = Provider.of<CartProvider>(context, listen: true).voucher;

    totalPrice = Provider.of<CartProvider>(context, listen: true).totalPrice;

    totalQuantity =
        Provider.of<CartProvider>(context, listen: true).totalQuantity;
    if (voucher == null) {
      salePrice = 0;
    }
    intoMoney = totalPrice - salePrice;

    return ListView(
      children: [
        buildVoucherInCart(),
        buildTotalPrice(totalPrice),
        buildDiscountVoucher(salePrice),
        buildIntoMoney(intoMoney),
        buildBottomChooseAllAndRemoveAll(),
        buildBottomAddress(),
      ],
    );
  }

//Design Voucher In Cart
  Widget buildVoucherInCart() {
    return Container(
      padding: const EdgeInsets.fromLTRB(25, 5, 25, 0),
      child: totalPrice == 0
          ? Container()
          : Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  (voucher == null)
                      ? 'Ưu đãi chưa áp dụng!'
                      : 'Ưu đãi ${voucher!.saleOff * 100}%',
                  style: const TextStyle(
                    color: ColorCodes.textColorSecondary,
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                (voucher == null)
                    ? Container()
                    : IconButton(
                        onPressed: () {
                          Provider.of<CartProvider>(context, listen: false)
                              .setVoucher(null);
                          setState(() {});
                        },
                        icon: const Icon(
                          Icons.highlight_off,
                          size: 28,
                          color: ColorCodes.pinkColor,
                        ),
                      ),
                Container(
                  width: 120,
                  height: 45,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    gradient: LinearGradient(
                      colors: <Color>[
                        const Color(0xFF6DFFCD).withOpacity(0.9),
                        const Color(0xFF6DCFCF),
                        ColorCodes.colorPrimary,
                      ],
                    ),
                  ),
                  child: TextButton(
                    child: Text(
                      (voucher == null) ? 'Ưu đãi' : 'Đã áp dụng',
                      style: const TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5,
                      ),
                    ),
                    onPressed: () async {
                      voucher = await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  VoucherMemberScreen(indexContext: 1)));
                      Provider.of<CartProvider>(context, listen: false)
                          .setVoucher(voucher);
                      if (voucher != null && totalPrice != 0) {
                        if (totalPrice * voucher!.saleOff >=
                            voucher!.maxPrice) {
                          salePrice = voucher!.maxPrice.toDouble();
                          intoMoney = totalPrice - salePrice;
                        } else {
                          salePrice = totalPrice * voucher!.saleOff;
                          intoMoney = totalPrice - salePrice;
                        }
                      }
                      setState(() {});
                    },
                  ),
                ),
              ],
            ),
    );
  }

//Design Bottom Choose All And Remove All
  Widget buildBottomChooseAllAndRemoveAll() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 25),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Checkbox(
            value: isCheck,
            onChanged: (value) {
              isCheck = value!;
              Provider.of<CartProvider>(context, listen: false)
                  .chooseAllProduct(isCheck);
              Provider.of<CartProvider>(context, listen: false)
                  .setVoucher(null);
            },
            activeColor: Colors.green,
          ),
          Container(
            height: 42,
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                width: 1,
                color: ColorCodes.pinkColor,
              ),
              borderRadius: const BorderRadius.only(
                bottomRight: Radius.circular(10),
                topLeft: Radius.circular(10),
                topRight: Radius.circular(2),
                bottomLeft: Radius.circular(2),
              ),
            ),
            child: TextButton(
              onPressed: () {
                if (totalPrice == 0) return;
                String memberId =
                    Provider.of<MemberProvider>(context, listen: false)
                        .member
                        .memberId!;
                Provider.of<CartProvider>(context, listen: false)
                    .removeAllCartByIdMember(memberId)
                    .then((value) {
                  showDialogLoadingSuccessfully(context, "Xoá Thành Công");
                  Provider.of<CartProvider>(context, listen: false)
                      .setVoucher(null);
                  Future.delayed(
                      const Duration(seconds: 1), () => dismissDialog(context));
                });
              },
              child: const Text(
                'Xoá Tất Cả',
                style: TextStyle(
                  letterSpacing: 0.5,
                  color: ColorCodes.pinkColor,
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

//Design Bototm Address
  Widget buildBottomAddress() {
    return Container(
      height: 50,
      margin: const EdgeInsets.fromLTRB(25, 5, 25, 5),
      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        gradient: LinearGradient(
          colors: <Color>[
            ColorCodes.colorPrimary,
            const Color(0xFF6DCFCF),
            const Color(0xFF6DFFCD).withOpacity(0.7),
          ],
        ),
      ),
      child: TextButton(
        onPressed: () {
          if (totalPrice == 0) {
            showMessageDialog(context,
                "Giao dịch thất bại, vui lòng thêm sản phẩm", Icons.error);
            Future.delayed(
                const Duration(seconds: 2), () => dismissDialog(context));
            return;
          }
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => AddressScreen(
                        voucher: voucher,
                      )));
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text(
              'Địa chỉ',
              style: TextStyle(
                color: Colors.white,
                fontSize: 22,
                fontWeight: FontWeight.w500,
              ),
            ),
            IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.arrow_forward_ios_outlined,
                color: Colors.white,
                size: 23,
              ),
            ),
          ],
        ),
      ),
    );
  }

  //Design build Total Price
  Widget buildTotalPrice(double total) {
    return Container(
      padding: const EdgeInsets.fromLTRB(25, 10, 25, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text(
            'Tổng tiền',
            style: TextStyle(
              color: ColorCodes.textColorSecondary,
              fontSize: 16,
              fontWeight: FontWeight.w600,
            ),
          ),
          Text(
            '${formatNumber(total)} đ',
            style: const TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 18,
            ),
          ),
        ],
      ),
    );
  }

  //Design build Total Price
  Widget buildDiscountVoucher(double sale) {
    return Container(
      padding: const EdgeInsets.fromLTRB(25, 10, 25, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text(
            'Khuyến Mãi',
            style: TextStyle(
              color: ColorCodes.textColorSecondary,
              fontSize: 16,
              fontWeight: FontWeight.w600,
            ),
          ),
          Text(
            '${formatNumber(salePrice)} đ',
            style: const TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 18,
            ),
          ),
        ],
      ),
    );
  }

//Design Into Money
  Widget buildIntoMoney(double intoMoney) {
    return Container(
      padding: const EdgeInsets.fromLTRB(25, 10, 25, 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text(
            'Thành Tiền',
            style: TextStyle(
              color: ColorCodes.textColorSecondary,
              fontSize: 16,
              fontWeight: FontWeight.w600,
            ),
          ),
          Text(
            '${formatNumber(intoMoney)} đ',
            style: const TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 18,
            ),
          ),
        ],
      ),
    );
  }
}
