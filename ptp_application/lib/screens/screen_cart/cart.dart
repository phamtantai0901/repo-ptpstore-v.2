import 'package:flutter/material.dart';
import 'package:ptp_application/main.dart';
import 'package:ptp_application/screens/screen_cart/components/design_appbar_cart.dart';
import 'package:ptp_application/screens/screen_cart/components/design_bottom_cart.dart';
import 'package:ptp_application/screens/screen_cart/components/design_product_in_cart.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({Key? key}) : super(key: key);

  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen>
    with AutomaticKeepAliveClientMixin<CartScreen> {
  bool keepAlive = true;

  @override
  bool get wantKeepAlive => keepAlive;
  @override
  Widget build(BuildContext context) {
    //Notice the super-call
    super.build(context);

    return RefreshIndicator(
      onRefresh: () async {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => BottomNavAppBar(currentIndex: 2)));
      },
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: const DesignAppBarCart(), //CUSTOM DESIGN
          body: Column(
            children: const [
              Expanded(
                flex: 3,
                child: DesignListProductInCart(),
              ), //CUSTOM DESIGN
              Divider(
                  color: Color.fromARGB(255, 218, 218, 218),
                  thickness: 0.6,
                  indent: 50,
                  endIndent: 50),
              Expanded(
                flex: 2,
                child: DesignBottomCart(), //CUSTOM DESIGN
              ),
            ],
          ),
        ),
      ),
    );
  }
}
