import 'package:ptp_application/models/rank.dart';
import 'package:ptp_application/models/user.dart';

class Member {
  final String? memberId;
  final int? currentPoint;
  final String? dateStartRank;
  final String? dateEndRank;
  final User user;
  final Rank? rank;
  final int? status;
  final String? tokenDevices;

  Member(
      {this.memberId,
      this.currentPoint,
      this.dateStartRank,
      this.dateEndRank,
      required this.user,
      this.rank,
      this.status,
      this.tokenDevices});

  factory Member.fromJson(Map<String, dynamic> json) => Member(
        memberId: json['member_id'] ?? '',
        currentPoint: json['current_point'],
        dateStartRank: json['date_start_rank'] ?? '',
        dateEndRank: json['date_end_rank'] ?? '',
        rank: Rank.fromJson(json['rank']),
        user: User.fromJson(json['user']),
        tokenDevices: json['token_devices'] ?? '',
        status: json['status'],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['member_id'] = memberId;
    data['current_point'] = currentPoint;
    data['date_start_rank'] = dateStartRank;
    data['date_end_rank'] = dateEndRank;
    data['user_id'] = user;
    data['rank_id'] = rank;
    data['status'] = status;
    return data;
  }
}
