import 'package:ptp_application/models/category.dart';
import 'package:ptp_application/models/image.dart';
import 'package:ptp_application/models/producer.dart';

class Product {
  final String productId;
  final String productName;
  final Category category;
  final Producer producer;
  final String description;
  final int status;
  final String productImg;
  final String createdAt;
  final String updatedAt;
  final int? quantityPay;
  final int? quantityRate;
  final int? quantityFavourity;
  final double? avgStar;
  final int pricePay;

  Product(
      {required this.productId,
      required this.productName,
      required this.category,
      required this.producer,
      required this.status,
      required this.productImg,
      required this.description,
      required this.createdAt,
      required this.updatedAt,
      required this.pricePay,
      this.quantityPay,
      this.quantityRate,
      this.avgStar,
      this.quantityFavourity});

  factory Product.fromJson(Map<String, dynamic> json) => Product(
      productId: json['product_id'] ?? '',
      productName: json['product_name'] ?? '',
      category: Category.fromJson(json['category']),
      producer: Producer.fromJson(json['producer']),
      status: json['status'],
      description: json['description'] ?? '',
      productImg: json['product_img'] ?? '',
      createdAt: json['created_at'] ?? '',
      updatedAt: json['updated_at'] ?? '',
      pricePay: json['price_pay'] ?? 0,
      quantityPay:
          json['quantity_pay'] != null ? int.parse(json['quantity_pay']) : 0,
      quantityRate: json['quantity_rate'] ?? 0,
      quantityFavourity: json['quantity_favourites'] ?? 0,
      avgStar: json['avg_star'] != null ? double.parse(json['avg_star']) : 0);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['product_id'] = productId;
    data['product_name'] = productName;
    data['category'] = category;
    data['producer'] = producer;
    data['description'] = description;
    data['status'] = status;
    data['product_img'] = productImg;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}
