import 'package:ptp_application/models/bill.dart';
import 'package:ptp_application/models/product_detail.dart';

class BillDetail {
  String? billDetailId;
  ProductDetail productDetail;
  Bill bill;
  int quantity;
  int price;
  int? totalPrice;
  int? priceDiscount;
  int? rateStatus;

  BillDetail(
      {this.billDetailId,
      required this.productDetail,
      required this.bill,
      required this.quantity,
      required this.price,
      this.totalPrice,
      this.priceDiscount,
      this.rateStatus});

  factory BillDetail.fromJson(Map<String, dynamic> json) => BillDetail(
      billDetailId: json['bill_detail_id'] ?? '',
      productDetail: ProductDetail.fromJson(json['product_detail']),
      bill: Bill.fromJson(json['bill']),
      quantity: json['quantity'],
      price: json['price'],
      totalPrice: json['total_price'],
      priceDiscount: json['price_discount'],
      rateStatus: json['rate_status']);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['bill_detail_id'] = billDetailId;
    data['product_detail'] = productDetail.toJson();
    data['bill'] = bill.toJson();
    data['quantity'] = quantity;
    data['price'] = price;
    data['total_price'] = totalPrice;
    data['price_discount'] = priceDiscount;
    data['rate_status'] = rateStatus;
    return data;
  }
}
