class Category {
  final String categoryId;
  final String categoryName;
  final int status;
  final String suffixImg;
  final String createdAt;
  final String updatedAt;

  Category(
      {required this.categoryId,
      required this.categoryName,
      required this.status,
      required this.suffixImg,
      required this.createdAt,
      required this.updatedAt});

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        categoryId: json['category_id'] ?? '',
        categoryName: json['category_name'] ?? '',
        status: json['status'],
        suffixImg: json['suffix_img'] ?? '',
        createdAt: json['created_at'] ?? '',
        updatedAt: json['updated_at'] ?? '',
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['category_id'] = categoryId;
    data['category_name'] = categoryName;
    data['status'] = status;
    data['suffix_img'] = suffixImg;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}
