import 'package:ptp_application/models/member.dart';
import 'package:ptp_application/models/voucher.dart';

class Bill {
  String? billId;
  Member member;
  String shippingAddress;
  String shippingPhone;
  String receiver;
  Voucher? voucher;
  String? dateOrder;
  String? dateConfirm;
  String? dateDelivery;
  String? dateReceipt;
  String? dateCancel;
  double totalPrice;
  int totalQuantity;
  int? status;
  int payment;

  Bill({
    this.billId,
    required this.member,
    required this.shippingAddress,
    required this.shippingPhone,
    required this.receiver,
    this.voucher,
    this.dateOrder,
    this.dateConfirm,
    this.dateDelivery,
    this.dateReceipt,
    this.dateCancel,
    required this.totalPrice,
    required this.totalQuantity,
    this.status,
    required this.payment,
  });

  factory Bill.fromJson(Map<String, dynamic> json) => Bill(
        billId: json['bill_id'] ?? '',
        member: Member.fromJson(json['member']),
        shippingAddress: json['shipping_address'] ?? '',
        shippingPhone: json['shipping_phone'] ?? '',
        receiver: json['receiver'] ?? '',
        voucher:
            json['voucher'] != null ? Voucher.fromJson(json['voucher']) : null,
        dateOrder: json['date_order'] ?? '',
        dateConfirm: json['date_confirm'] ?? '',
        dateDelivery: json['date_delivery'] ?? '',
        dateReceipt: json['date_receipt'] ?? '',
        dateCancel: json['date_cancel'] ?? '',
        totalPrice: double.parse(
          json['total_price'].toString(),
        ),
        totalQuantity: json['total_quantity'],
        payment: json['payment'],
        status: json['status'],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['bill_id'] = billId;
    data['member'] = member;
    data['shipping_address'] = shippingAddress;
    data['shipping_phone'] = shippingPhone;
    data['code'] = voucher;
    data['date_order'] = dateOrder;
    data['total_price'] = totalPrice;
    data['total_quantity'] = totalQuantity;
    data['payment'] = payment;
    data['status'] = status;
    return data;
  }
}
