import 'package:ptp_application/models/color.dart';
import 'package:ptp_application/models/product.dart';
import 'package:ptp_application/models/size.dart';

class ProductDetail {
  final String productDetailId;
  final Product product;
  Size size;
  Color color;
  final int status;

  ProductDetail(
      {required this.productDetailId,
      required this.product,
      required this.size,
      required this.color,
      required this.status});

  factory ProductDetail.fromJson(Map<String, dynamic> json) => ProductDetail(
        productDetailId: json['product_detail_id'],
        product: Product.fromJson(json['product']),
        size: Size.fromJson(json['size']),
        color: Color.fromJson(json['color']),
        status: json['status'],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['product_detail_id'] = productDetailId;
    data['product'] = product.toJson();
    data['size'] = size.toJson();
    data['color'] = color.toJson();
    data['status'] = status;
    return data;
  }
}
