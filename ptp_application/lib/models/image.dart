import 'package:ptp_application/models/color.dart';
import 'package:ptp_application/models/product.dart';

class Image {
  final String imgId;
  final String imgName;
  final Product product;
  final Color color;
  final int status;
  final String createdAt;
  final String updatedAt;

  Image({
    required this.imgId,
    required this.imgName,
    required this.product,
    required this.color,
    required this.status,
    required this.createdAt,
    required this.updatedAt,
  });

  factory Image.fromJson(Map<String, dynamic> json) => Image(
        imgId: json['img_id'] ?? '',
        imgName: json['img_name'] ?? '',
        product: Product.fromJson(json['product']),
        color: Color.fromJson(json['color']),
        status: json['status'],
        createdAt: json['created_at'] ?? '',
        updatedAt: json['updated_at'] ?? '',
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['img_id'] = imgId;
    data['img_name'] = imgName;
    data['product'] = product.toJson();
    data['color'] = color.toJson();
    data['status'] = status;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }

  static List<Image> fromJsons(datas) {
    return List<Image>.from(datas.map((data) => Image.fromJson(data)));
  }
}
