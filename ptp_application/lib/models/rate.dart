import 'package:ptp_application/models/member.dart';
import 'package:ptp_application/models/product.dart';

class Rate {
  final String rateId;
  final Member member;
  final Product product;
  final int star;
  final int status;
  final String? comment;
  int like;
  final String dateRate;

  Rate(
      {required this.rateId,
      required this.member,
      required this.product,
      required this.star,
      required this.status,
      this.comment,
      required this.like,
      required this.dateRate});

  factory Rate.fromJson(Map<String, dynamic> json) => Rate(
        rateId: json['rate_id'] ?? '',
        member: Member.fromJson(json['member']),
        product: Product.fromJson(json['product']),
        star: json['star'],
        status: json['status'],
        comment: json['comment'] ?? '',
        like: json['like'],
        dateRate: json['date_rate'] ?? '',
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['rate_id'] = rateId;
    data['member'] = member.toJson();
    data['product'] = product.toJson();
    data['star'] = star;
    data['status'] = status;
    data['comment'] = comment;
    data['like'] = like;
    data['date_rate'] = dateRate;
    return data;
  }
}
