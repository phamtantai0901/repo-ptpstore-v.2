import 'package:ptp_application/models/category.dart';

class Size {
  final String sizeId;
  final String sizeName;
  final Category category;
  final int status;

  Size(
      {required this.sizeId,
      required this.sizeName,
      required this.category,
      required this.status});

  factory Size.fromJson(Map<String, dynamic> json) => Size(
        sizeId: json['size_id'] ?? '',
        sizeName: json['size_name'] ?? '',
        category: Category.fromJson(json['category']),
        status: json['status'],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['size_id'] = sizeId;
    data['size_name'] = sizeName;
    data['category'] = category;
    data['status'] = status;
    return data;
  }
}
