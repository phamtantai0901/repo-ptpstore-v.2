import 'package:ptp_application/models/role.dart';

class User {
  final String? userId;
  final Role? role;
  final String username;
  String fullName;
  String? birthDay;
  String? email;
  String? address;
  final String phone;
  String image;
  final String? createdAt;
  final String? updatedAt;
  final int? status;
  final String? token;

  User(
      {this.userId,
      this.role,
      required this.username,
      required this.fullName,
      this.birthDay,
      this.email,
      this.address,
      required this.phone,
      required this.image,
      this.createdAt,
      this.updatedAt,
      this.status,
      this.token});

  factory User.fromJson(Map<String, dynamic> json) => User(
        userId: json["user_id"] ?? '',
        role: Role.fromJson(json['role']),
        username: json["username"] ?? '',
        fullName: json["full_name"] ?? '',
        birthDay: json["birth_day"] ?? '',
        email: json["email"] ?? '',
        address: json["address"] ?? '',
        phone: json["phone"] ?? '',
        image: json["image"] ?? '',
        createdAt: json["created_at"] ?? '',
        updatedAt: json["updated_at"] ?? '',
        status: json["status"],
        token: json['token'] ?? '',
      );
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["user_id"] = userId;
    data["role"] = role;
    data["username"] = username;
    data["full_name"] = fullName;
    data["birth_day"] = birthDay;
    data["email"] = email;
    data["address"] = address;
    data["phone"] = phone;
    data["image"] = image;
    data["created_at"] = createdAt;
    data["updated_at"] = updatedAt;
    data["status"] = status;
    return data;
  }
}
