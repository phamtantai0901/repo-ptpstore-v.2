import 'package:ptp_application/models/member.dart';
import 'package:ptp_application/models/product_detail.dart';

class Favourite {
  final String? favouriteId;
  final ProductDetail productDetail;
  final Member member;

  Favourite({
    this.favouriteId,
    required this.productDetail,
    required this.member,
  });

  factory Favourite.fromJson(Map<String, dynamic> json) => Favourite(
        favouriteId: json['favourite_id'] ?? '',
        productDetail: ProductDetail.fromJson(json['product_detail']),
        member: Member.fromJson(json['member']),
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['favourite_id'] = favouriteId;
    data['product_detail'] = productDetail.toJson();
    data['member'] = member.toJson();
    return data;
  }
}
