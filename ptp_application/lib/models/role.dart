class Role {
  final String roleId;
  final String roleName;
  final int status;
  final String createdAt;
  final String updatedAt;

  Role(
      {required this.roleId,
      required this.roleName,
      required this.status,
      required this.createdAt,
      required this.updatedAt});

  factory Role.fromJson(Map<String, dynamic> json) {
    return Role(
      roleId: json['role_id'] ?? '',
      roleName: json['role_name'] ?? '',
      status: json['status'],
      createdAt: json['created_at'] ?? '',
      updatedAt: json['updated_at'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['role_id'] = roleId;
    data['role_name'] = roleName;
    data['status'] = status;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}
