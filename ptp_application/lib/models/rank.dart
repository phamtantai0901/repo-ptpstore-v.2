class Rank {
  final String rankId;
  final String rankName;
  final int point;
  final int status;
  final String createdAt;
  final String updatedAt;

  Rank(
      {required this.rankId,
      required this.rankName,
      required this.point,
      required this.status,
      required this.createdAt,
      required this.updatedAt});

  factory Rank.fromJson(Map<String, dynamic> json) => Rank(
        rankId: json['rank_id'] ?? '',
        rankName: json['rank_name'] ?? '',
        point: json['point'],
        status: json['status'],
        createdAt: json['created_at'] ?? '',
        updatedAt: json['updated_at'] ?? '',
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['rank_id'] = rankId;
    data['rank_name'] = rankName;
    data['point'] = point;
    data['status'] = status;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}
