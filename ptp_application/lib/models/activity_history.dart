import 'package:ptp_application/models/user.dart';

class ActivityHistory {
  final int id;
  final String activity;
  final String? objectId;
  final String dateCreated;
  final User user;
  final int type;

  ActivityHistory({
    required this.id,
    required this.activity,
    required this.objectId,
    required this.dateCreated,
    required this.user,
    required this.type,
  });

  factory ActivityHistory.fromJson(Map<String, dynamic> json) =>
      ActivityHistory(
        id: json['id'] ?? '',
        activity: json['activity'] ?? '',
        objectId: json['object_id'] ?? '',
        dateCreated: json['date_created'] ?? '',
        user: User.fromJson(json['user']),
        type: json['type'] ?? '',
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'activity': activity,
        'object_id': objectId,
        'date_created': dateCreated,
        'user': user.toJson(),
        'type': type,
      };
}
