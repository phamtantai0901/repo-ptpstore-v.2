import 'package:ptp_application/models/member.dart';

class NotificationModel {
  String? notificationId;
  String? title;
  String? body;
  dynamic data;
  Member? member;
  String? createdAt;
  String? updatedAt;
  int? status;

  NotificationModel(
      {this.notificationId,
      this.title,
      this.body,
      this.data,
      this.member,
      this.createdAt,
      this.updatedAt,
      this.status});

  factory NotificationModel.fromJson(Map<String, dynamic> json) =>
      NotificationModel(
        notificationId: json['notification_id'] ?? '',
        title: json['title'] ?? '',
        body: json['body'] ?? '',
        data: json['data'] ?? '',
        member: Member.fromJson(json['member']),
        createdAt: json['created_at'] ?? '',
        updatedAt: json['updated_at'] ?? '',
        status: json['status'],
      );
}
