import 'package:ptp_application/models/category.dart';
import 'package:ptp_application/models/rank.dart';

class DiscountCategory {
  final String discountId;
  final double percentPrice;
  final Rank rank;
  final Category category;
  final String createdAt;
  final String updatedAt;
  final int status;

  DiscountCategory(
      {required this.discountId,
      required this.percentPrice,
      required this.rank,
      required this.category,
      required this.createdAt,
      required this.updatedAt,
      required this.status});

  factory DiscountCategory.fromJson(Map<String, dynamic> json) =>
      DiscountCategory(
        discountId: json['discount_id'] ?? '',
        percentPrice: json['percent_price'],
        rank: Rank.fromJson(json['rank']),
        category: Category.fromJson(json['category']),
        createdAt: json['created_at'] ?? '',
        updatedAt: json['updated_at'] ?? '',
        status: json['status'],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['discount_id'] = discountId;
    data['percent_price'] = percentPrice;
    data['rank'] = rank.toJson();
    data['category'] = category.toJson();
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['status'] = status;
    return data;
  }
}
