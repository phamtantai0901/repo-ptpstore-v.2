import 'package:ptp_application/models/member.dart';
import 'package:ptp_application/models/product_detail.dart';

class Cart {
  String? cartId;
  final ProductDetail productDetail;
  final Member member;
  int quantity;
  int pricePay;
  bool isChoosed = true;

  Cart({
    this.cartId,
    required this.productDetail,
    required this.member,
    required this.quantity,
    required this.pricePay,
  });

  factory Cart.fromJson(Map<String, dynamic> json) => Cart(
        cartId: json['cart_id'] ?? '',
        productDetail: ProductDetail.fromJson(json['product_detail']),
        member: Member.fromJson(json['member']),
        quantity: json['quantity'],
        pricePay: json['price_pay'],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['cart_id'] = cartId;
    data['product_detail'] = productDetail.toJson();
    data['member'] = member.toJson();
    data['quantity'] = quantity;
    data['price_pay'] = pricePay;
    return data;
  }
}
