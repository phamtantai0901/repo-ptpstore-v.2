class Voucher {
  final String code;
  final double saleOff;
  final int maxPrice;
  final int maxUsed;
  final String dateStart;
  final String dateEnd;
  final int status;

  Voucher(
      {required this.code,
      required this.saleOff,
      required this.maxPrice,
      required this.maxUsed,
      required this.dateStart,
      required this.dateEnd,
      required this.status});

  factory Voucher.fromJson(Map<String, dynamic> json) => Voucher(
        code: json['code'] ?? '',
        saleOff: double.parse(
          json['sale_off'].toString(),
        ),
        maxPrice: json['max_price'],
        maxUsed: json['max_used'],
        dateStart: json['date_start'] ?? '',
        dateEnd: json['date_end'] ?? '',
        status: json['status'],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['code'] = code;
    data['sale_off'] = saleOff;
    data['max_price'] = maxPrice;
    data['max_used'] = maxUsed;
    data['date_start'] = dateStart;
    data['date_end'] = dateEnd;
    data['status'] = status;
    return data;
  }
}
