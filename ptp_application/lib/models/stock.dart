class Stock {
  final String stockId;
  final String address;
  final int status;

  Stock({required this.stockId, required this.address, required this.status});

  factory Stock.fromJson(Map<String, dynamic> json) => Stock(
        stockId: json['stock_id'] ?? '',
        address: json['address'] ?? '',
        status: json['status'],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['stock_id'] = stockId;
    data['address'] = address;
    data['status'] = status;
    return data;
  }
}
