import 'package:ptp_application/models/product_detail.dart';
import 'package:ptp_application/models/stock.dart';

class StockDetail {
  final String stockDetailId;
  final Stock stock;
  final ProductDetail productDetail;
  final int quantity;
  final int pricePay;
  final int totalPrice;
  final int status;
  final int? quantityPay;
  final int? quantityRate;
  final double? avgStar;

  StockDetail(
      {required this.stockDetailId,
      required this.stock,
      required this.productDetail,
      required this.quantity,
      required this.pricePay,
      required this.totalPrice,
      required this.status,
      this.quantityPay,
      this.quantityRate,
      this.avgStar});

  factory StockDetail.fromJson(Map<String, dynamic> json) => StockDetail(
        stockDetailId: json['stock_detail_id'] ?? '',
        stock: Stock.fromJson(json['stock']),
        productDetail: ProductDetail.fromJson(json['product_detail']),
        quantity: json['quantity'],
        pricePay: json['price_pay'],
        totalPrice: json['total_price'],
        status: json['status'],
        quantityPay: json['quantity_pay'],
        quantityRate: json['quantity_rate'],
        avgStar: double.parse(
          json['avg_star'].toString(),
        ),
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['stock_detail_id'] = stockDetailId;
    data['stock'] = stock;
    data['product_detail_id'] = productDetail;
    data['quantity'] = quantity;
    data['price_pay'] = pricePay;
    data['total_price'] = totalPrice;
    data['status'] = status;
    return data;
  }
}
