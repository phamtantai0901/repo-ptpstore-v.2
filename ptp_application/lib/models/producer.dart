class Producer {
  final String producerId;
  final String producerName;
  final String phone;
  final String address;
  final int status;

  Producer(
      {required this.producerId,
      required this.producerName,
      required this.phone,
      required this.address,
      required this.status});

  factory Producer.fromJson(Map<String, dynamic> json) => Producer(
      producerId: json['producer_id'] ?? '',
      producerName: json['producer_name'] ?? '',
      phone: json['phone'] ?? '',
      address: json['address'] ?? '',
      status: json['status']);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['producer_id'] = producerId;
    data['producer_name'] = producerName;
    data['phone'] = phone;
    data['address'] = address;
    data['status'] = status;
    return data;
  }
}
