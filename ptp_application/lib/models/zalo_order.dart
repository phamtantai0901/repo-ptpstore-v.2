class ZaloOrder {
  final String zptranstoken;
  final String orderurl;
  final int returncode;
  final String returnmessage;
  final String ordertoken;

  ZaloOrder(
      {required this.zptranstoken,
      required this.orderurl,
      required this.returncode,
      required this.returnmessage,
      required this.ordertoken});

  factory ZaloOrder.fromJson(Map<String, dynamic> json) {
    return ZaloOrder(
      zptranstoken: json['zp_trans_token'] as String,
      orderurl: json['order_url'] as String,
      returncode: json['return_code'] as int,
      returnmessage: json['return_message'] as String,
      ordertoken: json["order_token"] as String,
    );
  }
}
