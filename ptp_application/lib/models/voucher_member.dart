import 'package:ptp_application/models/member.dart';
import 'package:ptp_application/models/voucher.dart';

class VoucherMember {
  final int? id;
  final Member member;
  final Voucher code;
  int? status;

  VoucherMember(
      {required this.id,
      required this.member,
      required this.code,
      required this.status});

  factory VoucherMember.fromJson(Map<String, dynamic> json) => VoucherMember(
        id: json['id'],
        member: Member.fromJson(json['member']),
        code: Voucher.fromJson(json['code']),
        status: json['status'],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['member'] = member;
    data['code'] = code;
    data['status'] = status;
    return data;
  }
}
