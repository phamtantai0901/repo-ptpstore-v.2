class Color {
  final String colorId;
  final String colorName;
  final String createdAt;
  final String updatedAt;
  final int status;

  Color(
      {required this.colorId,
      required this.colorName,
      required this.createdAt,
      required this.updatedAt,
      required this.status});

  factory Color.fromJson(Map<String, dynamic> json) => Color(
        colorId: json['color_id'] ?? '',
        colorName: json['color_name'] ?? '',
        createdAt: json['created_at'] ?? '',
        updatedAt: json['updated_at'] ?? '',
        status: json['status'],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['color_id'] = colorId;
    data['color_name'] = colorName;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['status'] = status;
    return data;
  }
}
