import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/custom.dart';
import 'package:ptp_application/providers/activity_history_provider.dart';
import 'package:ptp_application/providers/bill_detail_provider.dart';
import 'package:ptp_application/providers/bill_provider.dart';
import 'package:ptp_application/providers/cart_provider.dart';
import 'package:ptp_application/providers/category_provider.dart';
import 'package:ptp_application/providers/discount_category.dart';
import 'package:ptp_application/providers/favourite_provider.dart';
import 'package:ptp_application/providers/google_provider.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/providers/notification_provider.dart';
import 'package:ptp_application/providers/product_detail_provider.dart';
import 'package:ptp_application/providers/product_provider.dart';
import 'package:ptp_application/providers/image_provider.dart'
    as image_provider;
import 'package:ptp_application/providers/color_provider.dart'
    as color_provider;
import 'package:ptp_application/providers/size_provider.dart' as size_provider;
import 'package:ptp_application/providers/rank_provider.dart';
import 'package:ptp_application/providers/rate_provider.dart';
import 'package:ptp_application/providers/screen_provider.dart';
import 'package:ptp_application/providers/stock_detail_provider.dart';
import 'package:ptp_application/providers/voucher_member_provider.dart';
import 'package:ptp_application/providers/zalo_provider.dart';
import 'package:ptp_application/screens/screen_cart/cart.dart';
import 'package:ptp_application/screens/screen_category/category.dart';
import 'package:ptp_application/screens/screen_chat/chat.dart';
import 'package:ptp_application/screens/screen_home/home.dart';
import 'package:ptp_application/screens/screen_login/login.dart';
import 'package:ptp_application/screens/screen_personal/personal.dart';
import 'package:ptp_application/screens/screen_welcome/components/welcome.dart';
// Add library firebase_core
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  print(await FirebaseMessaging.instance.getToken());
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  Future<void> getLoading() async {
    try {
      await Future.delayed(const Duration(seconds: 5), () => print("waiting"));
    } catch (e) {
      rethrow;
    }
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (ctx) => FavouriteProvider()),
        ChangeNotifierProvider(create: (ctx) => MemberProvider()),
        ChangeNotifierProvider(create: (ctx) => VoucherMemberProvider()),
        ChangeNotifierProvider(create: (ctx) => StockDetailProvider()),
        ChangeNotifierProvider(create: (ctx) => CartProvider()),
        ChangeNotifierProvider(create: (ctx) => CategoryProvider()),
        ChangeNotifierProvider(create: (ctx) => ProductProvider()),
        ChangeNotifierProvider(create: (ctx) => image_provider.ImageProvider()),
        ChangeNotifierProvider(create: (ctx) => size_provider.SizeProvider()),
        ChangeNotifierProvider(create: (ctx) => color_provider.ColorProvider()),
        ChangeNotifierProvider(create: (ctx) => BillDetailProvider()),
        ChangeNotifierProvider(create: (ctx) => RateProvider()),
        ChangeNotifierProvider(create: (ctx) => BillProvider()),
        ChangeNotifierProvider(create: (ctx) => ProductDetailProvider()),
        ChangeNotifierProvider(create: (ctx) => RankProvider()),
        ChangeNotifierProvider(create: (ctx) => ScreenProvider()),
        ChangeNotifierProvider(create: (ctx) => ActivityHistoryProvider()),
        ChangeNotifierProvider(create: (ctx) => DiscountCategoryProvider()),
        ChangeNotifierProvider(create: (ctx) => GoogleProvider()),
        ChangeNotifierProvider(create: (ctx) => NotificationProvider()),
        ChangeNotifierProvider(create: (ctx) => ZaloProvider()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "PTP Store",
        theme: ThemeData(fontFamily: 'Quicksand'),
        home: SafeArea(
          child: FutureBuilder(
            future: getLoading(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const WelcomeScreen();
              } else {
                return const LoginScreen();
              }
            },
          ),
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class BottomNavAppBar extends StatefulWidget {
  int currentIndex;
  BottomNavAppBar({required this.currentIndex, Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _BottomNavAppBar();
}

class _BottomNavAppBar extends State<BottomNavAppBar> {
  late PageController _pageController;
  List<Widget> pages = [];
  @override
  void initState() {
    super.initState();
    pages = [
      const HomeScreen(),
      const CategoryScreen(),
      const CartScreen(),
      ChatScreen(),
      const PersonalScreen()
    ];
    _pageController = PageController(initialPage: widget.currentIndex);
  }

  void updateIndex(int value) {
    setState(() {
      widget.currentIndex = value;
      _pageController.jumpToPage(widget.currentIndex);
    });
  }

  //Desgin bottom navbar
  Widget buildBottomNavAppBar() {
    return ClipRRect(
      borderRadius: const BorderRadius.only(
        topRight: Radius.circular(25),
        topLeft: Radius.circular(25),
      ),
      child: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.home_outlined),
            label: 'Trang Chủ',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.widgets_outlined),
            label: 'Danh Mục',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart_outlined),
            label: 'Giỏ Hàng',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.chat_outlined),
            label: 'Hộ Trợ',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Cá Nhân',
          ),
        ],
        showUnselectedLabels: true,
        selectedItemColor: const Color(0xFF43cea2).withOpacity(0.9),
        unselectedItemColor: Colors.grey,
        iconSize: 30,
        currentIndex: widget.currentIndex,
        onTap: updateIndex,
        backgroundColor: ColorCodes.facebookColor,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Provider.of<CategoryProvider>(context, listen: false).setCategory = null;
    return Scaffold(
      bottomNavigationBar: buildBottomNavAppBar(),
      body: PageView(
        controller: _pageController,
        physics: const NeverScrollableScrollPhysics(),
        children: pages,
      ),
    );
  }
}
