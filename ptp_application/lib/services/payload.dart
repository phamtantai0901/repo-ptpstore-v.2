class Payload {
  dynamic data;
  String message;
  int status;
  Payload({required this.data, required this.message, required this.status});

  factory Payload.fromJson(Map<String, dynamic> json) {
    return Payload(
      data: json['data'],
      message: json['message'] ?? '',
      status: int.parse(json['status']),
    );
  }
}
