import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:ptp_application/config.dart';

class Request {
  Map<String, String> _headers(token) => {
        'Content-Type': 'application/json;charset=UTF-8',
        "Charset": "utf-8",
        'Authorization': 'Bearer ' + token
      };

  Future<String> doGet(String subUrl) async {
    try {
      String token = await getToken();
      String mainUrl = Config.getAPI(subUrl);
      print(mainUrl);
      http.Response response =
          await http.get(Uri.parse(mainUrl), headers: _headers(token)).timeout(
                const Duration(seconds: 30),
              );
      if (response.statusCode == 200) {
        return jsonDecode(jsonEncode(response.body));
      } else {
        throw "Internal Server Error";
      }
    } on Exception catch (ex) {
      print(ex);
      if (ex.runtimeType is FormatException) {
        print("Re-call");
        return await doGet(subUrl);
      }
      throw ex;
    }
  }

  Future<String> doPost(dynamic body, String subUrl) async {
    try {
      String token = await getToken();
      String mainUrl = Config.getAPI(subUrl);
      http.Response response = await http
          .post(Uri.parse(mainUrl),
              headers: _headers(token), body: jsonEncode(body))
          .timeout(const Duration(seconds: 30));
      if (response.statusCode == 200 ||
          response.statusCode == 201 ||
          response.statusCode == 202) {
        return jsonDecode(jsonEncode(response.body));
      } else {
        throw "Internal Server Error";
      }
    } on Exception catch (ex) {
      print(ex);
      if (ex.runtimeType is FormatException) {
        print("Re-call");
        return await doGet(subUrl);
      }
      throw ex;
    }
  }

  Future<String> getToken() async {
    String token = "";
    if (await const FlutterSecureStorage().containsKey(key: 'token')) {
      await const FlutterSecureStorage().read(key: 'token').then((value) {
        token += value!;
      });
    }

    return token;
  }
}
