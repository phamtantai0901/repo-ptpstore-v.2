// import 'package:firebase_analytics/firebase_analytics.dart';
// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:firebase_core/firebase_core.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
// import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
// import 'package:ptp_application/models/notification.dart';
// import 'package:ptp_application/providers/notification_provider.dart';

// void main() async {
//   WidgetsFlutterBinding.ensureInitialized();
//   await Firebase.initializeApp();
//   print(await FirebaseMessaging.instance.getToken());

//   runApp(const MyApp());
// }

// class MyApp extends StatelessWidget {
//   const MyApp({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return ChangeNotifierProvider(
//       create: (ctx) => NotificationProvider(),
//       child: const MaterialApp(
//           // Remove the debug banner
//           debugShowCheckedModeBanner: false,
//           title: 'Demo',
//           home: HomePage(),),
//     );
//   }
// }

// class HomePage extends StatefulWidget {
//   const HomePage({Key? key}) : super(key: key);

//   @override
//   _HomePageState createState() => _HomePageState();
// }

// class _HomePageState extends State<HomePage> {
//   @override
//   void initState() {
//     super.initState();
//     Provider.of<NotificationProvider>(context, listen: false)
//         .registerNotification();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(title: const Text("Test Notification"),),
//       body: Center(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: [
//             const Text('Notification'),
//             Consumer<NotificationProvider>(
//                 builder: (context, notifiProvider, child) {
//               if (notifiProvider.notificationModel == null) {
//                 return Container();
//               } else {
//                 return Column(
//                   children: [
//                     Text(notifiProvider.notificationModel.title),
//                     Text(notifiProvider.notificationModel.body),
//                   ],
//                 );
//               }
//             }),
//           ],
//         ),
//       ),
//     );
//   }
// }
