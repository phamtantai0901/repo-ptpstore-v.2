import 'package:flutter/painting.dart';

class ColorCodes {
  static const Color colorPrimary = Color(0xFF65CFCF);
  static const Color iconColor = Color(0xFFA8A8A8);
  static const Color textColorPrimary = Color(0xFF494949);
  static const Color textColorSecondary = Color(0xFF787878);
  static const Color facebookColor = Color(0xFF609ABB);
  static const Color googleColor = Color(0xFFFC4248);
  static const Color twitterColor = Color(0xFF1DA7D2);
  static const Color backgroundColor = Color(0xFFECF3F9);
  static const Color pinkColor = Color(0xFFe45757);
}

class FontSize {
  static const double fontSizeAppBar = 18;
}

const double heighAppBarMd = 60;
const double heighAppBarLg = 80;
const double heighAppBarMaxLg = 120;
