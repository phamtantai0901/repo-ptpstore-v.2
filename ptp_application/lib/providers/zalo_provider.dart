import 'dart:convert';
import 'package:flutter/widgets.dart';
import 'package:ptp_application/config.dart';
import 'package:ptp_application/models/bill.dart';
import 'package:ptp_application/models/zalo_order.dart';
import 'package:sprintf/sprintf.dart';
import 'package:http/http.dart' as http;

class ZaloProvider with ChangeNotifier {
  Future<ZaloOrder?> createOrder(Bill bill) async {
    var header = <String, String>{};
    header["Content-Type"] = "application/x-www-form-urlencoded";

    var body = <String, String>{};
    body["app_id"] = Config.appId;
    body["app_user"] = Config.appUser;
    body["app_time"] = DateTime.now().millisecondsSinceEpoch.toString();
    body["amount"] = bill.totalPrice.toStringAsFixed(0);
    body["app_trans_id"] = Config.getAppTransId();
    body["embed_data"] = "{}";
    body["item"] = "[]";
    body["bank_code"] = Config.bankCode;
    body["description"] =
        Config.getDescription("PTP Store", body["app_trans_id"]!);

    var dataGetMac = sprintf("%s|%s|%s|%s|%s|%s|%s", [
      body["app_id"],
      body["app_trans_id"],
      body["app_user"],
      body["amount"],
      body["app_time"],
      body["embed_data"],
      body["item"]
    ]);
    body["mac"] = Config.getMacCreateOrder(dataGetMac);
    print("mac: ${body["mac"]}");

    http.Response response = await http.post(
      Uri.parse(Config.urlOrder),
      headers: header,
      body: body,
    );

    print("body_request: $body");
    if (response.statusCode != 200) {
      return null;
    }

    var data = jsonDecode(response.body);
    print("data_response: $data}");

    return ZaloOrder.fromJson(data);
  }
}
