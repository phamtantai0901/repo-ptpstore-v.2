import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:ptp_application/models/discount_category.dart';
import 'package:ptp_application/services/payload.dart';
import 'package:ptp_application/services/request.dart';

class DiscountCategoryProvider with ChangeNotifier {
  List<DiscountCategory> _discountsFromJson(dynamic datas) =>
      List.from(datas.map((data) => DiscountCategory.fromJson(data)));
  Future<List<DiscountCategory>> getAllDiscountByStatus(int status) async {
    String subUrl =
        "/discount-categories/get-all-discount-category-by-status/$status";
    List<DiscountCategory> discounts = [];
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);

      if (result.status == 200) {
        discounts = _discountsFromJson(result.data);
      }
      print(result.message);
    } catch (e) {
      rethrow;
    }
    return discounts;
  }

  Future<List<DiscountCategory>> getAllDiscountByRankIdAndStatus(
      String id, int status) async {
    String subUrl =
        "/discount-categories/get-all-discount-category-by-id-rank-and-status/$id&$status";
    List<DiscountCategory> discounts = [];
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 200) {
        discounts = _discountsFromJson(result.data);
      }
      print(result.message);
    } catch (e) {
      rethrow;
    }
    return discounts;
  }
}
