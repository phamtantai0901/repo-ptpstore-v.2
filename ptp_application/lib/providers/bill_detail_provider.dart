//  -4 từ chối phía shop
//  -3 chưa đánh giá
//  -2 đã huỷ
//  -1 chưa duyệt
//  0 đã duyệt và đang giao
//  1 đã duyệt và đã giao
//  2 đã đánh giá

import 'dart:convert';
import 'package:flutter/widgets.dart';
import 'package:ptp_application/models/bill_detail.dart';
import 'package:ptp_application/services/payload.dart';
import 'package:ptp_application/services/request.dart';

class BillDetailProvider with ChangeNotifier {
  List<BillDetail> _billDetailsFromJson(dynamic datas) =>
      List<BillDetail>.from(datas.map((data) => BillDetail.fromJson(data)));

  Future<List<BillDetail>> getAllBillDetailByIdMemberAndStatus(
      String? idMember, int status) async {
    String subUrl =
        "/bill-details/get-all-bill-detail-by-id-member-and-status/$idMember&$status";
    List<BillDetail> billDetailTmp = [];
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);

      if (result.status == 200) {
        billDetailTmp = _billDetailsFromJson(result.data);
      }
      print("Bill Detail: " + result.message);
    } catch (e) {
      print(e);
    }
    notifyListeners();
    return billDetailTmp;
  }

  Future<List<BillDetail>> getAllBillDetailByIdBill(String idBill) async {
    String subUrl = "/bill-details/get-all-bill-detail-by-id-bill/$idBill";
    List<BillDetail> billDetailTmp = [];
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 200) {
        billDetailTmp = _billDetailsFromJson(result.data);
      }
      print("Bill Detail: " + result.message);
    } catch (e) {
      print(e);
    }
    //notifyListeners();
    return billDetailTmp;
  }

  Future<bool> saveBillDetail(BillDetail billDetail) async {
    String subUrl = '/bill-details/save-bill-detail';
    Map<String, dynamic> body = {
      'product_detail_id': billDetail.productDetail.productDetailId,
      'bill_id': billDetail.bill.billId,
      'quantity': billDetail.quantity,
      'price': billDetail.price,
      'price_discount': billDetail.priceDiscount,
    };
    bool isCheck = false;
    try {
      Map<String, dynamic> json =
          jsonDecode(await Request().doPost(body, subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 201) {
        isCheck = true;
      }
      print("Bill Detail: " + result.message);
    } catch (e) {
      rethrow;
    }
    return isCheck;
  }
}
