import 'dart:convert';
import 'package:flutter/widgets.dart';
import 'package:ptp_application/models/bill.dart';
import 'package:ptp_application/services/payload.dart';
import 'package:ptp_application/services/request.dart';

class BillProvider with ChangeNotifier {
  final List<Bill> _bills = [];
  List<Bill> get bills => _bills;
  List<Bill> _billsFromJson(dynamic datas) =>
      List<Bill>.from(datas.map((data) => Bill.fromJson(data)));

  Future<List<Bill>> fetchAllBillByIdMemberAndStatus(
      String id, int status) async {
    String subUrl = "/bills/get-all-bill-by-id-member-and-status/$id&$status";
    List<Bill> billTmp = [];
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 200) {
        billTmp = _billsFromJson(result.data);
      }

      print("Bill: " + result.message);
    } catch (e) {
      rethrow;
    }
    notifyListeners();
    return billTmp;
  }

  Future<Bill?> saveBill(Bill bill) async {
    String subUrl = "/bills/save-bill";
    Map<String, dynamic> body = {
      'member_id': bill.member.memberId,
      'shipping_address': bill.shippingAddress,
      'shipping_phone': bill.shippingPhone,
      'receiver': bill.receiver,
      'total_price': bill.totalPrice,
      'total_quantity': bill.totalQuantity,
      'code': bill.voucher == null ? null : bill.voucher!.code,
      'payment': bill.payment,
    };
    Bill? billTmp;
    try {
      Map<String, dynamic> json =
          jsonDecode(await Request().doPost(body, subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 201) {
        billTmp = Bill.fromJson(result.data);
      }
      print("Bill: " + result.message);
    } catch (e) {
      rethrow;
    }
    return billTmp;
  }

  Future<bool> updateStatusBill(String billId, int status) async {
    Map<String, dynamic> body = {'bill_id': billId, 'status': status};
    String subUrl = '/bills/update-status-bill';
    var isCompleted = false;
    try {
      Map<String, dynamic> json =
          jsonDecode(await Request().doPost(body, subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 202) {
        isCompleted = true;
      }
      print("Update Status Bill: " + result.message);
    } catch (e) {
      rethrow;
    }
    notifyListeners();
    return isCompleted;
  }
}
