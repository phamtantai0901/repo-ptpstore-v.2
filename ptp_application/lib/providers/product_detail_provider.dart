import 'package:flutter/widgets.dart';

class ProductDetailProvider with ChangeNotifier {
  int _quantityOrder = 1;
  int get quantityOrder => _quantityOrder;

  void addQuantityOrder() {
    _quantityOrder++;
    notifyListeners();
  }

  void removeQuantityOrder() {
    if (_quantityOrder <= 1) {
      _quantityOrder = 1;
    } else {
      _quantityOrder--;
    }
    notifyListeners();
  }

  void updateQuantityOrder(int value) {
    _quantityOrder = value;
    // notifyListeners();
  }
}
