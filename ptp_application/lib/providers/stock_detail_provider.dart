import 'dart:convert';
import 'package:flutter/widgets.dart';
import 'package:ptp_application/models/stock_detail.dart';
import 'package:ptp_application/services/payload.dart';
import 'package:ptp_application/services/request.dart';
import 'package:ptp_application/models/color.dart' as color_model;
import 'package:ptp_application/models/size.dart' as size_model;

class StockDetailProvider with ChangeNotifier {
  List<StockDetail> _stockDetails = [];
  StockDetail? _stockDetailSelected;
  StockDetail? get stockDetailSelected => _stockDetailSelected;

  List<StockDetail> _stockDetailSearchResults = [];
  List<StockDetail> _stockDetailSearchResultsTmp = [];
  String _dropdownValue = 'Tất cả';

  List<StockDetail> get stockDetails => _stockDetails;
  String get dropdownValue => _dropdownValue;

  List<StockDetail> get stockDetailSearchResults => _stockDetailSearchResults;
  List<StockDetail> get stockDetailSearchResultsTmp =>
      _stockDetailSearchResultsTmp;

  StockDetail? _stockDetail;

  StockDetail? get stockDetail => _stockDetail;

  set stockDetail(stockDetail) => _stockDetail = stockDetail;

  List<StockDetail> _stockDetailsFromJson(dynamic datas) =>
      List<StockDetail>.from(datas.map((data) => StockDetail.fromJson(data)));

  // AsyncMemoizer<List<StockDetail>> memCache = AsyncMemoizer();

  // Future<List<StockDetail>> fetchDistinctAllProductDetailInStockDetailByStatus(
  //     int status) async {
  //   String subUrl =
  //       "/stock-details/get-all-product-detail-in-stock-by-status/$status";
  //   List<StockDetail> stockDetailsDistinct = [];
  //   try {
  //     Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
  //     Payload result = Payload.fromJson(json);
  //     if (result.status == 200) {
  //       _stockDetailsFromJson(result.data).forEach((stockDetail) {
  //         if (isDistinct(stockDetailsDistinct, stockDetail) == true) {
  //           stockDetailsDistinct.add(stockDetail);
  //         }
  //       });
  //       _stockDetails = stockDetailsDistinct;
  //     }
  //     print("Stock Detail: " + result.message);
  //   } catch (e) {
  //     print(e);
  //   }
  //   notifyListeners();
  //   return _stockDetails;
  // }

  Future<List<StockDetail>> getAllProductInStockAndRateAndPayByStatus(
      int status) async {
    String subUrl =
        "/stock-details/get-all-product-in-stock-and-rate-and-pay-by-status/$status";
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 200) {
        _stockDetails = _stockDetailsFromJson(result.data);
      }
      print("Stock Detail: " + result.message);
    } catch (e) {
      print(e);
    }
    notifyListeners();
    return _stockDetails;
  }

  Future<StockDetail> getFirstProductDetailInStockDetailByIdProduct(
      String productId) async {
    String subUrl =
        "/stock-details/get-first-stock-detail-by-product-id/$productId";
    late StockDetail stockDetailSelected;
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);

      if (result.status == 200) {
        stockDetailSelected = StockDetail.fromJson(result.data);
        _stockDetail = stockDetailSelected;
      }
      print("Stock Detail: " + result.message);
    } catch (e) {
      print(e);
    }
    return stockDetailSelected;
  }

  StockDetail? getFirstProductDetailInStockDetailByIdProductDetail(
      String productDetailId) {
    for (StockDetail stockDetail in stockDetails) {
      if (stockDetail.productDetail.productDetailId == productDetailId) {
        _stockDetail = stockDetail;
        break;
      }
    }
    return _stockDetail;
  }

  Future<bool> checkQuantityProductDetailInStock(
      String id, int quantity) async {
    String subUrl =
        "/stock-details/check-quantity-product-detail-in-stock/$id&$quantity";
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);
      print("Stock Detail: " + result.message);
      return result.data as bool;
    } catch (e) {
      rethrow;
    }
  }

  bool isDistinct(List<StockDetail> stockDetails, StockDetail stockDetail) {
    bool isCheck = true;
    // ignore: avoid_function_literals_in_foreach_calls
    stockDetails.forEach((stockDetailTmp) {
      if (stockDetail.productDetail.product.productId ==
          stockDetailTmp.productDetail.product.productId) {
        isCheck = false;
        return;
      }
    });
    return isCheck;
  }

  //Update Color When CLicked
  Future<void> updateColor(color_model.Color color) async {
    String subUrl =
        "/stock-details/get-all-stock-detail-by-product-id-and-color-id/" +
            stockDetail!.productDetail.product.productId +
            "&" +
            color.colorId;
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);

      if (result.status == 200) {
        _stockDetail = _stockDetailsFromJson(result.data)[0];
        notifyListeners();
      }
      print("Stock Detail: " + result.message);
    } catch (e) {
      rethrow;
    }
  }

  //Update Size When CLicked
  Future<void> updateSize(size_model.Size size) async {
    String subUrl =
        "/stock-details/get-stock-detail-by-product-id-and-color-id-and-size-id/" +
            stockDetail!.productDetail.product.productId +
            "&" +
            stockDetail!.productDetail.color.colorId +
            "&" +
            size.sizeId;
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);

      if (result.status == 200) {
        _stockDetail = StockDetail.fromJson(result.data);
        notifyListeners();
      }

      print("Stock Detail: " + result.message);
    } catch (e) {
      rethrow;
    }
  }

  List<StockDetail> sortPriceProduct(listStockDetail, item, bool withKey) {
    if (withKey == true) {
      listStockDetail = _stockDetailSearchResultsTmp;
    } else {
      listStockDetail = _stockDetails;
    }
    Map<String, dynamic> seleletPrice = {
      'Tất cả': [0, -1],
      'Dưới 500k': [0, 499999],
      '500K đến 1 triệu': [500000, 1000000],
      '1 triệu đến 2 triệu': [1000000, 2000000],
      'Trên 2 triệu': [2000000, -1],
    };
    List<StockDetail> stockDetailsTmp = [];
    if (listStockDetail == []) {
      return stockDetailsTmp;
    }
    if (seleletPrice[item][1] == -1) {
      for (int i = 0; i < listStockDetail.length; i++) {
        if (listStockDetail[i].pricePay > seleletPrice[item][0]) {
          stockDetailsTmp.add(listStockDetail[i]);
        }
      }
    } else {
      for (int i = 0; i < listStockDetail.length; i++) {
        if (listStockDetail[i].pricePay >= seleletPrice[item][0] &&
            listStockDetail[i].pricePay <= seleletPrice[item][1]) {
          stockDetailsTmp.add(listStockDetail[i]);
        }
      }
    }
    _stockDetailSearchResults = stockDetailsTmp;
    _dropdownValue = item;
    notifyListeners();
    return stockDetailsTmp;
  }
}
