import 'dart:convert';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:ptp_application/models/member.dart';
import 'package:ptp_application/services/payload.dart';
import 'package:ptp_application/services/request.dart';

class MemberProvider with ChangeNotifier {
  late Member _member;
  Member get member => _member;
  set member(member) => _member = member;
  final storage = const FlutterSecureStorage();

  Future<Member?> authentication(
      String username, String password, bool islogin) async {
    Map<String, dynamic> body = {
      'username': username,
      'password': password,
      'islogin': islogin
    };
    String subUrl = '/users/get-user-by-username-password';
    Member? member;
    try {
      Map<String, dynamic> json =
          jsonDecode(await Request().doPost(body, subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 202) {
        member = Member.fromJson(result.data);
        _member = member;

        // Write value
        await storage.write(key: 'token', value: member.user.token);
        print("JWT token: " + member.user.token!);
      }
      print("Member: " + result.message);
    } catch (e) {
      rethrow;
    }

    return member;
  }

  Future<Member> refeshMember() async {
    String subUrl =
        '/members/get-member-by-id-and-status/${_member.memberId}&1';
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 200) {
        _member = Member.fromJson(result.data);
      }
      print("Member: " + result.message);
      notifyListeners();
    } catch (e) {
      rethrow;
    }
    return _member;
  }

  Future<Member?> checkUserExist(String? email) async {
    Map<String, dynamic> body = {'email': email};
    String subUrl = '/users/check-email-user-exist';
    Member? member;
    try {
      Map<String, dynamic> json =
          jsonDecode(await Request().doPost(body, subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 202) {
        member = Member.fromJson(result.data);
        _member = member;

        // Write value
        await storage.write(key: 'token', value: member.user.token);
        print("JWT token: " + member.user.token!);
      }
      print("Member: " + result.message);
    } catch (e) {
      rethrow;
    }
    return member;
  }

  Future<Member?> authenticationPhone(String phone) async {
    Map<String, dynamic> body = {'username': phone};
    String subUrl = '/users/get-user-by-username-and-status/1';
    Member? member;
    try {
      Map<String, dynamic> json =
          jsonDecode(await Request().doPost(body, subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 202) {
        member = Member.fromJson(result.data);
        _member = member;
      }
      print("Username: " + result.message);
    } catch (e) {
      rethrow;
    }
    return member;
  }

  Future<int> confirmCode(String phone, int code) async {
    Map<String, dynamic> body = {'username': phone, 'code': code};
    String subUrl = '/users/confirm-code';
    late int status;
    try {
      Map<String, dynamic> json =
          jsonDecode(await Request().doPost(body, subUrl));
      Payload result = Payload.fromJson(json);
      status = result.data as int;
      print("Username: " + result.message);
    } catch (e) {
      rethrow;
    }
    return status;
  }

  Future<bool> register(String? email, String fullName, String phone,
      String password, String confirmPassword) async {
    if (password != confirmPassword) {
      throw "Invalid";
    }
    Map<String, dynamic> body = {
      'email': email,
      'full_name': fullName,
      'phone': phone,
      'password': password,
    };
    String subUrl = '/users/save-user';
    bool isCheck = false;
    try {
      Map<String, dynamic> json =
          jsonDecode(await Request().doPost(body, subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 201) {
        isCheck = true;
      }
      print("Member: " + result.message);
    } catch (e) {
      rethrow;
    }
    return isCheck;
  }

  Future<bool> changePassword(
      String username, String newPassword, String confirmNewPassword) async {
    if (newPassword != newPassword) {
      throw "Invalid";
    }
    Map<String, dynamic> body = {'username': username, 'password': newPassword};
    String subUrl = '/users/change-password';
    try {
      Map<String, dynamic> json =
          jsonDecode(await Request().doPost(body, subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 202) {
        print(result.message);
        return true;
      } else {
        print(result.message);
        return false;
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<Member> updateInfor(
      String username, String value, int isChecked) async {
    Map<String, dynamic> body = {};
    switch (isChecked) {
      case 0:
        {
          body = {
            'username': username,
            'full_name': value,
            'is_checked': isChecked
          };

          break;
        }
      case 1:
        {
          body = {
            'username': username,
            'email': value,
            'is_checked': isChecked
          };
          break;
        }
      case 2:
        {
          body = {
            'username': username,
            'birth_day': value,
            'is_checked': isChecked
          };
          break;
        }
      case 3:
        {
          body = {
            'username': username,
            'address': value,
            'is_checked': isChecked
          };
          break;
        }
    }
    String subUrl = '/users/update-user';
    try {
      Map<String, dynamic> json =
          jsonDecode(await Request().doPost(body, subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 202) {
        _member = Member.fromJson(result.data);
      }
      print("Member: " + result.message);
    } catch (e) {
      rethrow;
    }
    notifyListeners();
    return _member;
  }

  Future<bool> addOrUpdateTokenDevice(String tokenDevice) async {
    String subUrl = '/members/add-or-update-token-device';
    Map<String, dynamic> body = {
      'member_id': _member.memberId,
      'token_device': tokenDevice
    };
    bool isAddOrUpdate = false;
    try {
      Map<String, dynamic> json =
          jsonDecode(await Request().doPost(body, subUrl));
      Payload result = Payload.fromJson(json);
      isAddOrUpdate = result.data as bool;
      print("Member: " + result.message);
    } catch (e) {
      rethrow;
    }
    return isAddOrUpdate;
  }

  Future<void> logout() async {
    String subUrl = '/users/invalidate-token';
    Map<String, dynamic> body = {'token': await storage.read(key: 'token')};
    try {
      Map<String, dynamic> json =
          jsonDecode(await Request().doPost(body, subUrl));
      Payload result = Payload.fromJson(json);

      if (result.status == 202) {
        print(result.message);
        // Write value
        await storage.delete(key: 'token');

        print("Remove storage: " +
            await storage.containsKey(key: 'token').toString());
      }
    } catch (e) {
      rethrow;
    }
  }
}
