import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:ptp_application/models/image.dart' as image_model;
import 'package:ptp_application/services/payload.dart';
import 'package:ptp_application/services/request.dart';

class ImageProvider with ChangeNotifier {
  List<image_model.Image> _images = [];
  List<image_model.Image> get images => _images;
  List<image_model.Image> _imagesFromJson(dynamic datas) =>
      List<image_model.Image>.from(
          datas.map((data) => image_model.Image.fromJson(data)));

  Future<List<image_model.Image>> getAllImageByProductIdAndStatus(
      String productId) async {
    // ignore: unused_local_variable
    Map<String, dynamic> body = {'product_id': productId};
    String subUrl =
        "/images/get-all-image-by-id-product-and-status/$productId&1";
    List<image_model.Image> imagesTmps = [];
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 200) {
        imagesTmps = _imagesFromJson(result.data);
      }
      print("Image: " + result.message);
    } catch (e) {
      rethrow;
    }
    _images = imagesTmps;
    notifyListeners();
    return _images;
  }

  Future<List<image_model.Image>> getAllImageByProductIdAndColorIdAndStatus(
      String productId) async {
    // ignore: unused_local_variable
    Map<String, dynamic> body = {'product_id': productId};
    String subUrl =
        "/images/get-all-image-by-id-product-and-id-color-and-status/$productId&1";
    List<image_model.Image> imagesTmps = [];
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 200) {
        imagesTmps = _imagesFromJson(result.data);
      }
      print("Image: " + result.message);
    } catch (e) {
      rethrow;
    }
    _images = imagesTmps;
    notifyListeners();
    return _images;
  }
}
