import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:ptp_application/models/color.dart' as color_model;
import 'package:ptp_application/services/payload.dart';
import 'package:ptp_application/services/request.dart';

class ColorProvider with ChangeNotifier {
  List<color_model.Color> _colors = [];
  List<color_model.Color> get colors => _colors;

  List<color_model.Color> _colorsFromJson(dynamic datas) =>
      List<color_model.Color>.from(
          datas.map((data) => color_model.Color.fromJson(data)));

  Future<List<color_model.Color>> getAllColorByProductIdAndStatusInStock(
      String productId, int status) async {
    String subUrl = "/colors/get-all-color-by-product-id-and-status-in-stock/" +
        productId +
        "&1";
    List<color_model.Color> colorTmps = [];
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);

      if (result.status == 200) {
        colorTmps = _colorsFromJson(result.data);
        //Get product detail first in product
        // _stockDetails = stockDetailsTmp;
        _colors = colorTmps;
      }
      print("Stock Detail: " + result.message);
    } catch (e) {
      rethrow;
    }
    notifyListeners();
    return _colors;
  }
}
