import 'dart:convert';
import 'package:flutter/widgets.dart';
import 'package:ptp_application/models/category.dart';
import 'package:ptp_application/services/payload.dart';
import 'package:ptp_application/services/request.dart';

class CategoryProvider with ChangeNotifier {
  // ignore: avoid_init_to_null
  Category? _category = null;
  Category? get category => _category;
  set setCategory(Category? category) => _category = category;

  List<Category> _categories = [];
  List<Category> get categories => _categories;
  List<Category> _categoriesFromJson(dynamic datas) =>
      List<Category>.from(datas.map((data) => Category.fromJson(data)));

  Future<List<Category>> fetchAllCategoryInStock() async {
    String subUrl = "/categories/get-all-category-in-stock";
    List<Category> categoriesTmp = [];
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 200) {
        categoriesTmp = _categoriesFromJson(result.data);
      }
      print("Category: " + result.message);
    } catch (e) {
      print(e);
    }
    _categories = categoriesTmp;
    notifyListeners();
    return categoriesTmp;
  }

  //Update category when you click choose category
  void updateClick(Category? category) {
    _category = category;

    print(_category?.categoryName);
    notifyListeners();
  }
}
