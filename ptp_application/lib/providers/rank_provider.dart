import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:ptp_application/models/rank.dart';
import 'package:ptp_application/services/payload.dart';
import 'package:ptp_application/services/request.dart';

class RankProvider with ChangeNotifier {
  Rank? _rank;
  Rank? get rank => _rank;
  Future<Rank?> getNextRankOfCurrentRankByMemberIdAndStatus(
      String id, int status) async {
    String subUrl =
        "/ranks/get-next-rank-of-current-rank-by-member-id-and-status/$id&$status";
    Rank? rankTmp;
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 200) {
        rankTmp = Rank.fromJson(result.data);
      }
      print("Rank: " + result.message);
    } catch (e) {
      rethrow;
    }
    _rank = rankTmp;
    return rankTmp;
  }

  List<Rank> _ranksFromJson(dynamic datas) =>
      List<Rank>.from(datas.map((data) => Rank.fromJson(data)));

  Future<List<Rank>> getAllRankByStatus(int status) async {
    String subUrl = "/ranks/get-all-rank-by-status/$status";
    List<Rank> ranksTmp = [];
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 200) {
        ranksTmp = _ranksFromJson(result.data);
      }
      print("Rank: " + result.message);
    } catch (e) {
      rethrow;
    }
    return ranksTmp;
  }
}
