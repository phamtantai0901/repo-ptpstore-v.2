import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:ptp_application/models/voucher_member.dart';
import 'package:ptp_application/services/payload.dart';
import 'package:ptp_application/services/request.dart';

class VoucherMemberProvider with ChangeNotifier {
  List<VoucherMember> _voucherMembers = [];
  List<VoucherMember> get voucherMembers => _voucherMembers;

  List<VoucherMember> _voucherMemberFromJson(dynamic datas) =>
      List<VoucherMember>.from(
          datas.map((data) => VoucherMember.fromJson(data)));

  Future<List<VoucherMember>> fetchAllVoucherMemberByIdMemberAndStatus(
      String id) async {
    String subUrl =
        "/voucher-members/get-all-voucher-member-by-id-member-and-status/$id&1";
    List<VoucherMember> voucherMembersTmp = [];
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 200) {
        voucherMembersTmp = _voucherMemberFromJson(result.data);
      }
      print("Voucher Member: " + result.message);
    } catch (e) {
      rethrow;
    }
    _voucherMembers = voucherMembersTmp;
    notifyListeners();
    return _voucherMembers;
  }
}
