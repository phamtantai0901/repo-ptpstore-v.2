import 'dart:convert';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptp_application/models/member.dart';
import 'package:ptp_application/models/notification.dart';
import 'package:ptp_application/providers/member_provider.dart';
import 'package:ptp_application/screens/screen_notification/notification.dart';
import 'package:ptp_application/services/payload.dart';
import 'package:ptp_application/services/request.dart';

class NotificationProvider with ChangeNotifier {
  bool _isLoading = false;
  bool get isLoading => _isLoading;

  NotificationModel? _notificationModel;
  get notificationModel => _notificationModel;

  List<NotificationModel> _notifications = [];
  List<NotificationModel> get notifications => _notifications;

  List<NotificationModel> _notificationsUnread = [];
  List<NotificationModel> get notificationsUnread => _notificationsUnread;

  List<NotificationModel> _notificationsFromJson(dynamic datas) =>
      List<NotificationModel>.from(
          datas.map((data) => NotificationModel.fromJson(data)));

  void registerNotification(BuildContext context) {
    FirebaseMessaging.instance.getInitialMessage().then((message) {
      if (message == null) return;
      _notificationModel = NotificationModel(
        title: message.notification!.title.toString(),
        body: message.notification!.body.toString(),
        data: message.data,
      );
      Provider.of<MemberProvider>(context, listen: false).refeshMember();
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => const ScreenNotification()));
    });

    FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
      _notificationModel = NotificationModel(
        title: message.notification!.title.toString(),
        body: message.notification!.body.toString(),
        data: message.data,
      );
      late Member member;
      await Provider.of<MemberProvider>(context, listen: false)
          .refeshMember()
          .then((value) => member = value);
      fetchAllNotificationUnread(member.memberId!, 0);
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) async {
      _notificationModel = NotificationModel(
        title: message.notification!.title.toString(),
        body: message.notification!.body.toString(),
        data: message.data,
      );
      late Member member;
      await Provider.of<MemberProvider>(context, listen: false)
          .refeshMember()
          .then((value) => member = value);
      fetchAllNotificationUnread(member.memberId!, 0);
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => const ScreenNotification()));
    });
  }

  Future<List<NotificationModel>> fetchAllNotification(String memberId) async {
    String subUrl =
        "/notifications/get-all-notification-by-member-id/" + memberId;
    List<NotificationModel> notificationsTmp = [];
    _isLoading = true;
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 200) {
        notificationsTmp = _notificationsFromJson(result.data);
      }
      print("Notificaiton: " + result.message);
    } catch (e) {
      print(e);
    }
    _notifications = notificationsTmp;
    _isLoading = false;
    notifyListeners();
    return _notifications;
  }

  Future<List<NotificationModel>> fetchAllNotificationUnread(
      String memberId, int status) async {
    String subUrl =
        "/notifications/get-all-notification-by-member-id-and-status/$memberId&$status";
    List<NotificationModel> notificationsTmp = [];
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 200) {
        notificationsTmp = _notificationsFromJson(result.data);
      }
      print("Notificaiton: " + result.message);
    } catch (e) {
      print(e);
    }
    _notificationsUnread = notificationsTmp;
    notifyListeners();
    return _notificationsUnread;
  }

  Future<void> updateStatusNotification(NotificationModel noti) async {
    String subUrl = "/notifications/update-status-notification";
    Map<String, dynamic> body = {
      'notification_id': noti.notificationId,
      'member_id': noti.member!.memberId,
      'status': 1,
    };
    try {
      Map<String, dynamic> json =
          jsonDecode(await Request().doPost(body, subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 202) {
        removeNotificationInListNotificationUnread(noti);
        noti = NotificationModel.fromJson(result.data);
        updateListNotification(noti);
        notifyListeners();
      }
      print("Notificaiton: " + result.message);
    } catch (e) {
      print(e);
    }
  }

  void removeNotificationInListNotificationUnread(NotificationModel noti) {
    for (int i = 0; i < _notificationsUnread.length; i++) {
      if (_notificationsUnread[i].notificationId == noti.notificationId) {
        _notificationsUnread.remove(_notificationsUnread[i]);
        return;
      }
    }
  }

  void updateListNotification(NotificationModel noti) {
    for (int i = 0; i < _notifications.length; i++) {
      if (_notifications[i].notificationId == noti.notificationId) {
        _notifications[i] = noti;
        return;
      }
    }
  }

  Future<void> updateAllStatusNotification(String memberId) async {
    String subUrl = "/notifications/update-all-status-notification";
    Map<String, dynamic> body = {
      'member_id': memberId,
      'status': 1,
    };
    try {
      Map<String, dynamic> json =
          jsonDecode(await Request().doPost(body, subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 202) {
        _notifications = _notificationsFromJson(result.data);
        _notificationsUnread = [];
        notifyListeners();
      }
      print("Notificaiton: " + result.message);
    } catch (e) {
      print(e);
    }
  }
}
