import 'dart:convert';
import 'package:flutter/widgets.dart';
import 'package:ptp_application/models/cart.dart';
import 'package:ptp_application/models/voucher.dart';
import 'package:ptp_application/services/payload.dart';
import 'package:ptp_application/services/request.dart';

class CartProvider with ChangeNotifier {
  List<Cart> _carts = [];
  double _totalPrice = 0;
  bool _isCheck = true;
  bool get isCheck => _isCheck;
  double get totalPrice => _totalPrice;
  int _totalQuantity = 0;
  int get totalQuantity => _totalQuantity;
  Voucher? _voucher;
  Voucher? get voucher => _voucher;

  void setVoucher(Voucher? voucher) {
    _voucher = voucher;
  }

  List<Cart> get carts => _carts;

  List<Cart> _cartsFromJson(dynamic datas) =>
      List<Cart>.from(datas.map((data) => Cart.fromJson(data)));

  Future<List<Cart>> fetchAllCartByIdMember(String? id) async {
    String subUrl = "/carts/get-all-cart-by-id-member/$id";
    List<Cart> cartsTmp = [];
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 200) {
        cartsTmp = _cartsFromJson(result.data);
        getTotal(cartsTmp);
      }
      print("Cart: " + result.message);
    } catch (e) {
      print(e);
    }
    _carts = cartsTmp;
    notifyListeners();
    return _carts;
  }

  Future<int> addProductIntoCart(Cart cart, int quantityInStock) async {
    bool isExist = false; //flag exist
    int quantityExist = cart.quantity; //Get quantity exist
    //returnType = 0 add product in cart fail
    //returnType = 1 add product in cart successfully
    //returnType = -1 not enough quantity in stock
    int returnType = -1;

    for (int i = 0; i < _carts.length; i++) {
      if (_carts[i].productDetail.productDetailId ==
          cart.productDetail.productDetailId) {
        quantityExist +=
            _carts[i].quantity; //plus quantity old and quantity new

        if (quantityExist > quantityInStock) {
          //check if quantityOrder > quantityInStock return -1
          return returnType;
        }
        isExist = true; //update flag
        cart.cartId = _carts[i].cartId;
        _carts[i].quantity = quantityExist; //update quantity cart
        break;
      }
    }

    String subUrl;
    Map<String, dynamic> body;

    if (isExist) {
      subUrl = "/carts/update-quantity-in-cart";
      body = {
        'cart_id': cart.cartId,
        'quantity': quantityExist,
      };
    } else {
      subUrl = "/carts/save-cart";
      body = {
        'product_detail_id': cart.productDetail.productDetailId,
        'member_id': cart.member.memberId,
        'quantity': quantityExist,
        'price_pay': cart.pricePay,
      };
    }

    try {
      Map<String, dynamic> json =
          jsonDecode(await Request().doPost(body, subUrl));

      Payload result = Payload.fromJson(json);
      if (result.status == 201 || result.status == 202) {
        returnType = 1;
        if (!isExist) {
          _carts.add(Cart.fromJson(result.data));
          notifyListeners();
        }
      } else {
        returnType = 0;
      }

      print("Cart: " + result.message);
    } catch (e) {
      print(e);
      rethrow;
    }

    return returnType;
  }

  Future<bool> removeAllCartByIdMember(String memberId) async {
    String subUrl = "/carts/remove-all-cart";
    Map<String, dynamic> body = {
      'member_id': memberId,
    };
    bool isCheck = false;
    try {
      Map<String, dynamic> json =
          jsonDecode(await Request().doPost(body, subUrl));

      Payload result = Payload.fromJson(json);
      if (result.status == 202) {
        isCheck = true;
        _carts = [];
        _totalPrice = 0;
        _totalQuantity = 0;
      }
      print("Cart: " + result.message);
    } catch (e) {
      print(e);
      return false;
    }
    notifyListeners();
    return isCheck;
  }

  void chooseCart(Cart cart) {
    if (cart.isChoosed) {
      cart.isChoosed = false;
    } else {
      cart.isChoosed = true;
    }
    getTotal(_carts);
    notifyListeners();
  }

  Future<void> getTotal(List<Cart> carts) async {
    double totalPrice = 0;
    int totalQuantity = 0;
    for (var cart in carts) {
      if (cart.isChoosed) {
        totalPrice += (cart.pricePay * cart.quantity);
        totalQuantity += cart.quantity;
      }
    }
    _totalPrice = totalPrice;
    _totalQuantity = totalQuantity;
  }

  Future<bool> removeCartById(Cart cart) async {
    String subUrl = "/carts/remove-cart";
    Map<String, dynamic> body = {
      'cart_id': cart.cartId,
    };
    bool isCheck = false;
    try {
      Map<String, dynamic> json =
          jsonDecode(await Request().doPost(body, subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 202) {
        isCheck = true;
        _carts.remove(cart);
        getTotal(_carts);
      }
      print("Cart: " + result.message);
    } catch (e) {
      rethrow;
    }
    notifyListeners();
    return isCheck;
  }

  Future<void> removeQuantityInCart(Cart cart) async {
    if (cart.quantity <= 1) {
      cart.quantity == 1;
    } else {
      cart.quantity--;
    }

    getTotal(carts);
    notifyListeners();
    try {
      await updateQuantityInCart(cart.cartId!, cart.quantity);
    } catch (e) {
      rethrow;
    }
  }

  Future<void> addQuantityInCart(Cart cart) async {
    cart.quantity++;
    getTotal(carts);
    notifyListeners();
    try {
      await updateQuantityInCart(cart.cartId!, cart.quantity);
    } catch (e) {
      rethrow;
    }
  }

  Future<void> updateQuantityInCart(String id, int quantity) async {
    String subUrl = "/carts/update-quantity-in-cart";
    Map<String, dynamic> body = {
      'cart_id': id,
      'quantity': quantity,
    };

    try {
      // ignore: unused_local_variable
      Map<String, dynamic> json =
          jsonDecode(await Request().doPost(body, subUrl));
    } catch (e) {
      rethrow;
    }
  }

  void chooseAllProduct(bool value) {
    _isCheck = value;
    for (int i = 0; i < _carts.length; i++) {
      _carts[i].isChoosed = _isCheck;
    }
    getTotal(_carts);
    notifyListeners();
  }
}
