import 'dart:convert';
import 'package:flutter/widgets.dart';
import 'package:ptp_application/models/activity_history.dart';
import 'package:ptp_application/services/payload.dart';
import 'package:ptp_application/services/request.dart';

class ActivityHistoryProvider with ChangeNotifier {
  final List<ActivityHistory> _actHs = [];
  List<ActivityHistory> get actHs => _actHs;
  List<ActivityHistory> _actHsFromJson(dynamic datas) =>
      List<ActivityHistory>.from(
          datas.map((data) => ActivityHistory.fromJson(data)));

  Future<List<ActivityHistory>> fetchAllActivityHistoryByUserIdAndType(
      String id, int type) async {
    String subUrl =
        "/activity-histories/get-all-activity-history-by-id-user-and-type/$id&$type";
    List<ActivityHistory> actHisTmp = [];
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 200) {
        actHisTmp = _actHsFromJson(result.data);
      }
      print(result.message);
    } catch (e) {
      rethrow;
    }
    return actHisTmp;
  }

  Future<List<ActivityHistory>> getAllActivityHistoryByIdUserAndDateAndType(
      String userId, String date, int type) async {
    String subUrl =
        "/activity-histories/get-all-activity-history-by-id-user-and-date-and-type/$userId&$date&$type";
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 200) {
        print(result.message);
        notifyListeners();
        return _actHsFromJson(result.data);
      } else {
        throw result.message;
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<bool> saveActivityHistory(ActivityHistory actHis) async {
    String subUrl = "/activity-histories/save-activity-history";
    Map<String, dynamic> body = {
      'activity': actHis.activity,
      'object_id': actHis.objectId,
      'user_id': actHis.user.userId
    };
    bool isCheck = false;
    try {
      Map<String, dynamic> json =
          jsonDecode(await Request().doPost(body, subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 201) {
        isCheck = true;
      }
      print("Activity History: " + result.message);
    } catch (e) {
      rethrow;
    }
    notifyListeners();
    return isCheck;
  }
}
