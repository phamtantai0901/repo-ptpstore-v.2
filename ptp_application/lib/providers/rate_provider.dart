import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:ptp_application/models/rate.dart';
import 'package:ptp_application/services/payload.dart';
import 'package:ptp_application/services/request.dart';

class RateProvider with ChangeNotifier {
  List<Rate> _rates = [];
  final bool _isReviewed = false;
  bool get isReviewed => _isReviewed;

  List<Rate> get rates => _rates;
  List<Rate> _ratesFromJson(dynamic datas) =>
      List<Rate>.from(datas.map((data) => Rate.fromJson(data)));

  Future<List<Rate>> getAllRateByIdProductAndStatus(
      String id, int status) async {
    List<Rate> ratesTmp = [];
    String subUrl = "/rates/get-all-rate-by-id-product-and-status/$id&1";
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 200) {
        ratesTmp = _ratesFromJson(result.data);
      }
      print("Rate: " + result.message);
    } catch (e) {
      rethrow;
    }
    _rates = ratesTmp;
    notifyListeners();
    return ratesTmp;
  }

  List<Rate> getAllRateByProductIdAndStar(String id, int star) {
    List<Rate> ratesTmp = [];
    for (var rate in _rates) {
      if (rate.star == star && rate.product.productId == id) ratesTmp.add(rate);
    }
    //notifyListeners();
    return ratesTmp;
  }

  List<Rate> getAllRateByProductIdAndComment(String id) {
    List<Rate> ratesTmp = [];
    for (var rate in _rates) {
      if (rate.comment != null && rate.product.productId == id) {
        ratesTmp.add(rate);
      }
    }
    // notifyListeners();
    return ratesTmp;
  }

  Future<void> getAllRateByIdMemberAndStatus(String id, int status) async {
    List<Rate> ratesTmp = [];
    String subUrl = "/rates/get-all-rate-by-id-member-and-status/$id&$status";
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);

      if (result.status == 200) {
        ratesTmp = _ratesFromJson(result.data);
      }
      print("Rate: " + result.message);
    } catch (e) {
      rethrow;
    }
    _rates = ratesTmp;
    notifyListeners();
  }

  Future<bool> likeRate(String rateId) async {
    String subUrl = "/rates/like-rate";
    Map<String, dynamic> body = {'rate_id': rateId};
    bool isCheck = false;
    try {
      Map<String, dynamic> json =
          jsonDecode(await Request().doPost(body, subUrl));
      Payload result = Payload.fromJson(json);

      if (result.status == 202) {
        isCheck = true;
      }
      print("Rate: " + result.message);
    } catch (e) {
      rethrow;
    }
    notifyListeners();
    return isCheck;
  }

  double averageRate(List<Rate> rates) {
    int total = 0;
    for (var element in rates) {
      total += element.star;
    }
    // notifyListeners();
    return total / rates.length;
  }

  Future<bool> addRate(String idmember, String idproduct, double star,
      String comment, String idbilldetail) async {
    Map<String, dynamic> body = {
      'member_id': idmember,
      'product_id': idproduct,
      'star': star,
      'comment': comment,
      'bill_detail_id': idbilldetail
    };
    String subUrl = '/rates/save-rate';
    bool isCheck = false;
    try {
      Map<String, dynamic> json =
          jsonDecode(await Request().doPost(body, subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 201) {
        isCheck = true;
      }
      print("Rate: " + result.message);
    } catch (e) {
      rethrow;
    }
    return isCheck;
  }
}
