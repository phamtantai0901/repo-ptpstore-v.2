import 'dart:convert';
import 'package:flutter/widgets.dart';
import 'package:ptp_application/models/favourite.dart';
import 'package:ptp_application/services/payload.dart';
import 'package:ptp_application/services/request.dart';

class FavouriteProvider with ChangeNotifier {
  final List<Favourite> _favourites = [];
  List<Favourite> get favourites => _favourites;

  bool _isChecked = false;
  bool get isChecked => _isChecked;
  set isChecked(check) => _isChecked = check;

  List<Favourite> _favouritesFromJson(dynamic datas) =>
      List<Favourite>.from(datas.map((data) => Favourite.fromJson(data)));

  Future<List<Favourite>> fetchAllFavouriteByIdMember(String id) async {
    String subUrl = "/favourites/get-all-favourite-by-id-member/$id";
    List<Favourite> favourtiesTmp = [];
    try {
      // String json = await Request().doGet(subUrl);
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 200) {
        favourtiesTmp = _favouritesFromJson(result.data);
      }
      print("Favourites: " + result.message);
    } catch (e) {
      rethrow;
    }
    //notifyListeners();
    return favourtiesTmp;
  }

  Future<bool> addProductFavourite(
      String idMember, String idProductdetail) async {
    Map<String, dynamic> body = {
      'member_id': idMember,
      'product_detail_id': idProductdetail,
    };
    String subUrl = '/favourites/save-favourite';
    try {
      Map<String, dynamic> json =
          jsonDecode(await Request().doPost(body, subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 201) {
        isChecked = true;
      }

      print("Favourite: " + result.message);
    } catch (e) {
      rethrow;
    }
    notifyListeners();
    return isChecked;
  }

  Future<bool> removeFavourite(String idMember, String idProductdetail) async {
    Map<String, dynamic> body = {'favourite_id': idMember + idProductdetail};
    String subUrl = '/favourites/remove-favourite';
    try {
      Map<String, dynamic> json =
          jsonDecode(await Request().doPost(body, subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 202) {
        isChecked = false;
      }
      print("Remove Favourite: " + result.message);
    } catch (e) {
      rethrow;
    }
    notifyListeners();
    return isChecked;
  }

  Future<void> isProductDetailInFavourites(
      String productDetailId, String memberId) async {
    String subUrl = '/favourites/is-product-detail-in-favourites/' +
        productDetailId +
        '&' +
        memberId;
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);
      _isChecked = result.data as bool;
      notifyListeners();

      print("Checked Favourite: " + result.message);
    } catch (e) {
      rethrow;
    }
  }
}
