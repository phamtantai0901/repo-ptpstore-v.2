import 'dart:convert';
import 'package:flutter/widgets.dart';
import 'package:ptp_application/models/product.dart';
import 'package:ptp_application/services/payload.dart';
import 'package:ptp_application/services/request.dart';

class ProductProvider with ChangeNotifier {
  List<Product> _productsSearch = [];
  List<Product> get productsSearch => _productsSearch;
  set productsSearch(data) => _productsSearch = data;

  String _dropdownValue = 'Tất cả';
  String get dropdownValue => _dropdownValue;

  List<Product> _products = [];
  List<Product> get products => _products;
  List<Product> _productsHotPay = [];
  List<Product> get productsHotPay => _productsHotPay;

  List<Product> _productsFromJson(dynamic datas) =>
      List<Product>.from(datas.map((data) => Product.fromJson(data)));

  Future<void> fetchAllProductInStock() async {
    String subUrl = "/products/get-all-product-in-stock";

    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 200) {
        _products = _productsFromJson(result.data);
      }
      print("Product: " + result.message);
    } catch (e) {
      print(e);
    }
    notifyListeners();
  }

  Future<void> getProductByIdAndStatus(String productId) async {
    String subUrl = "/products/get-product-by-id-and-status/$productId&1";
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 200) {
        _products = _productsFromJson(result.data);
      }
      print("Product: " + result.message);
    } catch (e) {
      print(e);
    }
  }

  Future<List<Product>> getProductHotPay() async {
    String subUrl = "/products/get-all-product-hot-pay";
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 200) {
        _productsHotPay = _productsFromJson(result.data);
      }
      print("Product: " + result.message);
    } catch (e) {
      print(e);
    }
    notifyListeners();
    return _productsHotPay;
  }

  Future<List<Product>> getAllProductInStock() async {
    String subUrl = "/products/get-all-product-in-stock";
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);
      if (result.status == 200) {
        _products = _productsFromJson(result.data);
      }
      print("Product: " + result.message);
    } catch (e) {
      print(e);
    }
    notifyListeners();
    return _products;
  }

  List<Product> sortPriceProduct(
      List<Product> listProduct, item, bool withKey) {
    Map<String, dynamic> seleletPrice = {
      'Tất cả': [0, -1],
      'Dưới 500k': [0, 499999],
      '500K đến 1 triệu': [500000, 1000000],
      '1 triệu đến 2 triệu': [1000000, 2000000],
      'Trên 2 triệu': [2000000, -1],
    };

    if (listProduct == []) {
      return listProduct;
    }
    List<Product> listProductTmp = [];
    if (seleletPrice[item][1] == -1) {
      for (int i = 0; i < listProduct.length; i++) {
        if (listProduct[i].pricePay > seleletPrice[item][0]) {
          listProductTmp.add(listProduct[i]);
        }
      }
    } else {
      for (int i = 0; i < listProduct.length; i++) {
        if (listProduct[i].pricePay >= seleletPrice[item][0] &&
            listProduct[i].pricePay <= seleletPrice[item][1]) {
          listProductTmp.add(listProduct[i]);
        }
      }
    }
    notifyListeners();
    return listProductTmp;
  }

  List<Product> getAllProductByCategoryId(String categoryId) {
    List<Product> productTmp = [];
    for (var product in products) {
      if (product.category.categoryId == categoryId) {
        productTmp.add(product);
      }
    }
    return productTmp;
  }

  Future<List<Product>> getAllStockDetailBySearchKeyword(
      String keyword, String userId) async {
    if (keyword == '') {
      _productsSearch = products;
    } else {
      List<Product> productTmp = [];
      String subUrl =
          "/products/get-all-stock-detail-by-search-keyword?keyword=" +
              keyword +
              "&user_id=" +
              userId +
              "&status=all";
      try {
        Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
        Payload result = Payload.fromJson(json);

        if (result.status == 200) {
          productTmp = _productsFromJson(result.data);
        }
        print("Product Search: " + result.message);
      } catch (e) {
        rethrow;
      }
      _productsSearch = productTmp;
    }

    _dropdownValue = 'Tất cả';
    notifyListeners();
    return _productsSearch;
  }

  Future<List<Product>> getAllNewStockDetailBySearchKeyword(String key) async {
    List<Product> productTmp = [];
    String subUrl =
        "/products/get-all-stock-detail-new-product-by-search-keyword?keyword=" +
            key;
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);

      if (result.status == 200) {
        productTmp = _productsFromJson(result.data);
        _productsSearch = productTmp;
      }
      print("Product Search Create New: " + result.message);
    } catch (e) {
      rethrow;
    }
    _dropdownValue = 'Tất cả';
    notifyListeners();
    return _productsSearch;
  }

  Future<List<Product>> getAllStockDetailBestSellerBySearchKeyword(
      String key) async {
    List<Product> productTmp = [];
    String subUrl =
        "/products/get-all-stock-detail-best-seller-by-search-keyword?keyword=" +
            key;
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);

      if (result.status == 200) {
        productTmp = _productsFromJson(result.data);
        _productsSearch = productTmp;
      }
      print("Product Search Best Seller: " + result.message);
    } catch (e) {
      rethrow;
    }
    _dropdownValue = 'Tất cả';
    notifyListeners();
    return _productsSearch;
  }

  Future<List<Product>> getAllStockDetailStandoutBySearchKeyword(
      String key) async {
    List<Product> productTmp = [];
    String subUrl =
        "/products/get-all-stock-detail-hot-favourite-by-search-keyword?keyword=" +
            key;
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);

      if (result.status == 200) {
        productTmp = _productsFromJson(result.data);
        _productsSearch = productTmp;
      }
      print("Product Search Hot Favourites: " + result.message);
    } catch (e) {
      rethrow;
    }
    _dropdownValue = 'Tất cả';
    notifyListeners();
    return _productsSearch;
  }
}
