import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:ptp_application/models/size.dart' as size_model;
import 'package:ptp_application/services/payload.dart';
import 'package:ptp_application/services/request.dart';

class SizeProvider with ChangeNotifier {
  List<size_model.Size> _sizes = [];
  List<size_model.Size> get sizes => _sizes;

  List<size_model.Size> _sizesFromJson(dynamic datas) =>
      List<size_model.Size>.from(
          datas.map((data) => size_model.Size.fromJson(data)));

  Future<List<size_model.Size>> getAllSizeByProductIdAndColorIdInStock(
      String productId, String colorId) async {
    String subUrl =
        "/sizes/get-all-size-by-id-product-and-id-color-and-status/" +
            productId +
            "&" +
            colorId +
            "&1";
    List<size_model.Size> sizeTmps = [];
    try {
      Map<String, dynamic> json = jsonDecode(await Request().doGet(subUrl));
      Payload result = Payload.fromJson(json);

      if (result.status == 200) {
        sizeTmps = _sizesFromJson(result.data);
        // _stockDetails = stockDetailsTmp;
        _sizes = sizeTmps;
      }
      print("Stock Detail: " + result.message);
    } catch (e) {
      rethrow;
    }
    notifyListeners();
    return _sizes;
  }
}
