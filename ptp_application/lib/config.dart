import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:intl/intl.dart';
import 'package:sprintf/sprintf.dart';

class Config {
  //Config IP
  static const String addressIP = "http://192.168.1.38:80/upload";
  static const String addressAPI = "http://192.168.1.38:8000/api";
  static getIP(String url) => addressIP + url;
  static getAPI(String url) => addressAPI + url;
  //Config Zalo Pay
  static const String urlOrder = "https://sb-openapi.zalopay.vn/v2/create";
  static const String appId = "2554";
  static const String key1 = "sdngKKJmqEMzvh5QQcdD2A9XBSKUNaYn";
  static const String key2 = "trMrHtvjo6myautxDUiAcYsVtaeQ8nhf";
  static const String bankCode = "zalopayapp";
  static const String appUser = "zalopaydemo";
  static int transIdDefault = 1;

  /// Function Format DateTime to String with layout string
  static String formatDateTime(DateTime dateTime, String layout) {
    return DateFormat(layout).format(dateTime).toString();
  }

  static String getAppTransId() {
    if (transIdDefault >= 100000) {
      transIdDefault = 1;
    }
    transIdDefault += 1;
    var timeString = formatDateTime(DateTime.now(), "yyMMdd_hhmmss");
    return sprintf("%s%06d", [timeString, transIdDefault]);
  }

  static String getDescription(String name, String apptransid) =>
      "$name thanh toán cho đơn hàng  #$apptransid";

  static String getMacCreateOrder(String data) {
    var hmac = Hmac(sha256, utf8.encode(Config.key1));
    return hmac.convert(utf8.encode(data)).toString();
  }
}
