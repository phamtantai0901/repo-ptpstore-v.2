<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('images')->insert([
            'img_id' => 'IMG202201140456523',
            'img_name' => 'PROD2022011404565293.jpg',
            'product_id' => 'PROD20220114045652',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG202201140456524',
            'img_name' => 'PROD202201140456522.jpg',
            'product_id' => 'PROD20220114045652',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011404565253',
            'img_name' => 'PROD2022011404565280.jpg',
            'product_id' => 'PROD20220114045652',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011404565254',
            'img_name' => 'PROD2022011404565290.jpg',
            'product_id' => 'PROD20220114045652',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011404594816',
            'img_name' => 'PROD2022011404594856.jpg',
            'product_id' => 'PROD20220114045948',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011404594833',
            'img_name' => 'PROD2022011404594847.jpg',
            'product_id' => 'PROD20220114045948',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011404594869',
            'img_name' => 'PROD2022011404594813.jpg',
            'product_id' => 'PROD20220114045948',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011404594873',
            'img_name' => 'PROD2022011404594819.jpg',
            'product_id' => 'PROD20220114045948',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011404594891',
            'img_name' => 'PROD2022011404594897.jpg',
            'product_id' => 'PROD20220114045948',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011404594896',
            'img_name' => 'PROD2022011404594857.jpg',
            'product_id' => 'PROD20220114045948',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405075935',
            'img_name' => 'PROD2022011405075951.jpg',
            'product_id' => 'PROD20220114050759',
            'color_id' => 'CL20220114050515',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405075938',
            'img_name' => 'PROD2022011405075929.jpg',
            'product_id' => 'PROD20220114050759',
            'color_id' => 'CL20220114050515',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405075983',
            'img_name' => 'PROD20220114050759100.jpg',
            'product_id' => 'PROD20220114050759',
            'color_id' => 'CL20220114050515',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405075986',
            'img_name' => 'PROD202201140507594.jpg',
            'product_id' => 'PROD20220114050759',
            'color_id' => 'CL20220114050515',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405112513',
            'img_name' => 'PROD2022011405112580.jpg',
            'product_id' => 'PROD20220114051125',
            'color_id' => 'CL20220114050852',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405112532',
            'img_name' => 'PROD2022011405112578.jpg',
            'product_id' => 'PROD20220114051125',
            'color_id' => 'CL20220114050852',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405112538',
            'img_name' => 'PROD2022011405112523.jpg',
            'product_id' => 'PROD20220114051125',
            'color_id' => 'CL20220114050852',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405112543',
            'img_name' => 'PROD2022011405112561.jpg',
            'product_id' => 'PROD20220114051125',
            'color_id' => 'CL20220114050852',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405112544',
            'img_name' => 'PROD2022011405112524.jpg',
            'product_id' => 'PROD20220114051125',
            'color_id' => 'CL20220114050852',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405140243',
            'img_name' => 'PROD2022011405140250.jpg',
            'product_id' => 'PROD20220114051402',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405140249',
            'img_name' => 'PROD2022011405140264.jpg',
            'product_id' => 'PROD20220114051402',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405151443',
            'img_name' => 'PROD2022011405151489.jpg',
            'product_id' => 'PROD20220114051514',
            'color_id' => 'CL20220114051420',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405151447',
            'img_name' => 'PROD2022011405151447.jpg',
            'product_id' => 'PROD20220114051514',
            'color_id' => 'CL20220114051420',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405190634',
            'img_name' => 'PROD202201140519059.jpg',
            'product_id' => 'PROD20220114051905',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405190655',
            'img_name' => 'PROD2022011405190673.jpg',
            'product_id' => 'PROD20220114051905',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG202201140525171',
            'img_name' => 'PROD2022011405251775.jpg',
            'product_id' => 'PROD20220114052517',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG20220114052517100',
            'img_name' => 'PROD2022011405251740.jpg',
            'product_id' => 'PROD20220114052517',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405251711',
            'img_name' => 'PROD2022011405251729.jpg',
            'product_id' => 'PROD20220114052517',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405251734',
            'img_name' => 'PROD2022011405251750.jpg',
            'product_id' => 'PROD20220114052517',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405251787',
            'img_name' => 'PROD2022011405251780.jpg',
            'product_id' => 'PROD20220114052517',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405251789',
            'img_name' => 'PROD2022011405251769.jpg',
            'product_id' => 'PROD20220114052517',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405335132',
            'img_name' => 'PROD2022011405335135.jpg',
            'product_id' => 'PROD20220114053351',
            'color_id' => 'CL20220114033006',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405335139',
            'img_name' => 'PROD2022011405335124.jpg',
            'product_id' => 'PROD20220114053351',
            'color_id' => 'CL20220114033006',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405335149',
            'img_name' => 'PROD2022011405335152.jpg',
            'product_id' => 'PROD20220114053351',
            'color_id' => 'CL20220114033006',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG202201140533515',
            'img_name' => 'PROD2022011405335167.jpg',
            'product_id' => 'PROD20220114053351',
            'color_id' => 'CL20220114033006',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405335177',
            'img_name' => 'PROD2022011405335165.jpg',
            'product_id' => 'PROD20220114053351',
            'color_id' => 'CL20220114033006',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405411554',
            'img_name' => 'PROD2022011405411531.jpg',
            'product_id' => 'PROD20220114054031',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG202201140549002',
            'img_name' => 'PROD2022011405490040.jpg',
            'product_id' => 'PROD20220114054900',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405490024',
            'img_name' => 'PROD2022011405490066.jpg',
            'product_id' => 'PROD20220114054900',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405490030',
            'img_name' => 'PROD2022011405490085.jpg',
            'product_id' => 'PROD20220114054900',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405490050',
            'img_name' => 'PROD202201140549007.jpg',
            'product_id' => 'PROD20220114054900',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405490076',
            'img_name' => 'PROD2022011405490015.jpg',
            'product_id' => 'PROD20220114054900',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405490080',
            'img_name' => 'PROD2022011405490025.jpg',
            'product_id' => 'PROD20220114054900',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405490093',
            'img_name' => 'PROD2022011405490033.jpg',
            'product_id' => 'PROD20220114054900',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405535938',
            'img_name' => 'PROD202201140553591.jpg',
            'product_id' => 'PROD20220114055359',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405535967',
            'img_name' => 'PROD2022011405535922.jpg',
            'product_id' => 'PROD20220114055359',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405544861',
            'img_name' => 'PROD2022011405544870.jpg',
            'product_id' => 'PROD20220114055359',
            'color_id' => 'CL20220114033006',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405544897',
            'img_name' => 'PROD2022011405544881.jpg',
            'product_id' => 'PROD20220114055359',
            'color_id' => 'CL20220114033006',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011405544899',
            'img_name' => 'PROD202201140554482.jpg',
            'product_id' => 'PROD20220114055359',
            'color_id' => 'CL20220114033006',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG20220114060557100',
            'img_name' => 'PROD20220114060557100.jpg',
            'product_id' => 'PROD20220114060526',
            'color_id' => 'CL20220114033013',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406055722',
            'img_name' => 'PROD2022011406055787.jpg',
            'product_id' => 'PROD20220114060526',
            'color_id' => 'CL20220114033013',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406055730',
            'img_name' => 'PROD2022011406055752.jpg',
            'product_id' => 'PROD20220114060526',
            'color_id' => 'CL20220114033013',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406055733',
            'img_name' => 'PROD2022011406055791.jpg',
            'product_id' => 'PROD20220114060526',
            'color_id' => 'CL20220114033013',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406055776',
            'img_name' => 'PROD202201140605572.jpg',
            'product_id' => 'PROD20220114060526',
            'color_id' => 'CL20220114033013',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406055781',
            'img_name' => 'PROD2022011406055752.jpg',
            'product_id' => 'PROD20220114060526',
            'color_id' => 'CL20220114033013',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406055783',
            'img_name' => 'PROD2022011406055770.jpg',
            'product_id' => 'PROD20220114060526',
            'color_id' => 'CL20220114033013',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406055792',
            'img_name' => 'PROD2022011406055792.jpg',
            'product_id' => 'PROD20220114060526',
            'color_id' => 'CL20220114033013',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG20220114061056100',
            'img_name' => 'PROD2022011406105668.jpg',
            'product_id' => 'PROD20220114061056',
            'color_id' => 'CL20220114060757',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG202201140610562',
            'img_name' => 'PROD2022011406105645.jpg',
            'product_id' => 'PROD20220114061056',
            'color_id' => 'CL20220114060757',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406105645',
            'img_name' => 'PROD2022011406105699.jpg',
            'product_id' => 'PROD20220114061056',
            'color_id' => 'CL20220114060757',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406105652',
            'img_name' => 'PROD2022011406105661.jpg',
            'product_id' => 'PROD20220114061056',
            'color_id' => 'CL20220114060757',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406105654',
            'img_name' => 'PROD2022011406105610.jpg',
            'product_id' => 'PROD20220114061056',
            'color_id' => 'CL20220114060757',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406105660',
            'img_name' => 'PROD2022011406105674.jpg',
            'product_id' => 'PROD20220114061056',
            'color_id' => 'CL20220114060757',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406105697',
            'img_name' => 'PROD2022011406105615.jpg',
            'product_id' => 'PROD20220114061056',
            'color_id' => 'CL20220114060757',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406131034',
            'img_name' => 'PROD202201140613107.jpg',
            'product_id' => 'PROD20220114061056',
            'color_id' => 'CL20220114061200',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406131042',
            'img_name' => 'PROD2022011406131084.jpg',
            'product_id' => 'PROD20220114061056',
            'color_id' => 'CL20220114061200',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406131054',
            'img_name' => 'PROD2022011406131098.jpg',
            'product_id' => 'PROD20220114061056',
            'color_id' => 'CL20220114061200',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406131061',
            'img_name' => 'PROD2022011406131036.jpg',
            'product_id' => 'PROD20220114061056',
            'color_id' => 'CL20220114061200',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406131075',
            'img_name' => 'PROD2022011406131089.jpg',
            'product_id' => 'PROD20220114061056',
            'color_id' => 'CL20220114061200',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406131079',
            'img_name' => 'PROD2022011406131033.jpg',
            'product_id' => 'PROD20220114061056',
            'color_id' => 'CL20220114061200',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406131084',
            'img_name' => 'PROD2022011406131062.jpg',
            'product_id' => 'PROD20220114061056',
            'color_id' => 'CL20220114061200',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406131093',
            'img_name' => 'PROD2022011406131017.jpg',
            'product_id' => 'PROD20220114061056',
            'color_id' => 'CL20220114061200',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406193924',
            'img_name' => 'PROD2022011406193933.jpg',
            'product_id' => 'PROD20220114061939',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406193930',
            'img_name' => 'PROD2022011406193944.jpg',
            'product_id' => 'PROD20220114061939',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406193936',
            'img_name' => 'PROD2022011406193954.jpg',
            'product_id' => 'PROD20220114061939',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406193991',
            'img_name' => 'PROD2022011406193914.jpg',
            'product_id' => 'PROD20220114061939',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406221714',
            'img_name' => 'PROD2022011406221721.jpg',
            'product_id' => 'PROD20220114062217',
            'color_id' => 'CL20220114033006',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406221753',
            'img_name' => 'PROD2022011406221719.jpg',
            'product_id' => 'PROD20220114062217',
            'color_id' => 'CL20220114033006',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406221768',
            'img_name' => 'PROD2022011406221769.jpg',
            'product_id' => 'PROD20220114062217',
            'color_id' => 'CL20220114033006',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011406221782',
            'img_name' => 'PROD2022011406221716.jpg',
            'product_id' => 'PROD20220114062217',
            'color_id' => 'CL20220114033006',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011408332615',
            'img_name' => 'PROD20220114083326129.jpg',
            'product_id' => 'PROD20220114083326',
            'color_id' => 'CL20220114033013',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011408332640',
            'img_name' => 'PROD20220114083326389.jpg',
            'product_id' => 'PROD20220114083326',
            'color_id' => 'CL20220114033013',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG2022011408332655',
            'img_name' => 'PROD20220114083326527.jpg',
            'product_id' => 'PROD20220114083326',
            'color_id' => 'CL20220114033013',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
        DB::table('images')->insert([
            'img_id' => 'IMG202202101059191',
            'img_name' => 'PROD202202101059191.jpg',
            'product_id' => 'PROD20220114052517',
            'color_id' => 'CL20220114032959',
            'status' => 1,
            'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => Carbon::now('Asia/Ho_Chi_Minh'),
        ]);
    }
}
