<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductDetailIdAndBillOrderIdToBillOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bill_order_details', function (Blueprint $table) {
            $table->foreign('product_detail_id')->references('product_detail_id')->on('product_details');
            $table->foreign('bill_order_id')->references('bill_order_id')->on('bill_orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bill_order_details', function (Blueprint $table) {
            //
        });
    }
}
