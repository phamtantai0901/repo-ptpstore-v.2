@extends('user/layouts')

@section('title','Chính sách bảo mật')

@section('header')
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<meta charset="UTF-8" />
<meta content="Ch&#237;nh s&#225;ch bảo mật" name="description" />
<meta content="Ch&#237;nh s&#225;ch bảo mật" name="keywords" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta property="fb:app_id" content="227481454296289" />
<meta content="vi_VN" property="og:locale" />
<meta content="website" property="og:type" />
<meta content="Ch&#237;nh s&#225;ch bảo mật" property="og:title" />
<meta content="Ch&#237;nh s&#225;ch bảo mật" property="og:description" />
<meta property="og:image" />
<meta property="og:url" />
<meta content=" PTPSTORE" property="og:site_name" />
<link rel="stylesheet" href="{{ asset('user/static.ptpstore/PTPSTOREV2/policy.css') }}">

@endsection

@section('body')
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-9">

                <div class="breadcrumb clearfix">
                    <ul>
                        <li itemtype="http://shema.org/Breadcrumb" itemscope="" class="home">
                            <a title="Đến trang chủ" href="{{ route('user/index') }}" itemprop="url"><span
                                    itemprop="title">Trang
                                    chủ</span></a>
                        </li>
                        <li class="icon-li"><strong>Ch&#237;nh s&#225;ch bảo mật</strong> </li>
                    </ul>
                </div>
                <script type="text/javascript">
                    $(".link-site-more").hover(function () { $(this).find(".s-c-n").show(); }, function () { $(this).find(".s-c-n").hide(); });
                </script>
                <div class="page-option policy">
                    <h1 class="policy-title">
                        <span>Ch&#237;nh s&#225;ch bảo mật</span>
                    </h1>
                    <div class="page-option-block">
                        <p class="policy-desc"> PTPSTORE đang cố gắng nắm bắt mọi vấn đề gây trở ngại từ phía quý khách
                            hàng, không ngừng cải thiện website để quý khách có thể mua hàng an toàn, với những thứ an
                            toàn nhất - tốt nhất - chất lượng nhất có thể.
                            Hệ thống mua sắm an toàn, bảo mật tài khoản của khách hàng để tránh các tình huống đáng
                            tiếc.
                        </p>

                        <p class="policy-desc"><strong>1. Thu thập thông tin khách hàng</strong></p>

                        <p class="policy-desc">Để sử dụng được các dịch vụ của PTPSTORE, Quý Khách Hàng phải đăng ký tài
                            khoản và
                            cung cấp một số thông tin như: email, họ tên, số điện thoại, ngày sinh và địa chỉ và
                            một số thông tin khác. Đăng ký tài khoản nhằm nắm bắt thông tin khách hàng để giao đến
                            những đơn hàng nhanh nhất cho quý khácg, quý khách có thể không cần cung cấp đầy đủ thông
                            tin nhưng khi cung cấp đầy đủ thông tin bạn sẽ nhận được một số tiện ích từ shop.<br />
                            <br />
                            Ngoài ra, các thông tin giao dịch gồm: lịch sử mua hàng, các vấn đề về mua hàng, phương
                            thức vận chuyển và thanh toán cũng được PTPSTORE lưu trữ nhằm giải quyết những vấn đề
                            có thể phát sinh về sau.
                        </p>

                        <p class="policy-desc"><strong>2. Sử dụng thông tin</strong></p>

                        <p class="policy-desc">Mục đích của việc thu thập thông tin là nhằm xây dựng PTPSTORE trở thành
                            một website
                            thương mại điện tử mang lại nhiều tiện ích nhất cho khách hàng. Vì thế, việc sử dụng
                            thông tin sẽ phục vụ những hoạt động sau:</p>

                        <ul>
                            <li class="policy-subDesc">+ Gửi newsletter giới thiệu sản phẩm mới và những chương trình
                                khuyến mãi
                                của PTPSTORE</li>
                            <li class="policy-subDesc">+ Cung cấp một số tiện ích, dịch vụ hỗ trợ khách hàng</li>
                            <li class="policy-subDesc">+ Nâng cao chất lượng dịch vụ khách hàng của PTPSTORE</li>
                            <li class="policy-subDesc">+ Giải quyết các vấn đề, tranh chấp phát sinh liên quan đến việc
                                sử dụng website
                            </li>
                            <li class="policy-subDesc">+ Ngăn chặn những hoạt động vi phạm pháp luật Việt Nam</li>
                        </ul>
                        <p class="policy-desc"><strong>3. Chia sẻ thông tin</strong></p>
                        <p class="policy-desc">PTPSTORE biết rằng thông tin về khách hàng là một phần rất quan trọng
                            trong việc kinh
                            doanh và chúng sẽ không được bán, trao đổi cho một bên thứ ba nào khác. Chúng tôi sẽ
                            không chia sẻ thông tin khách hàng trừ những trường hợp cụ thể sau đây:</p>
                        <ul>
                            <li class="policy-subDesc">+ Để bảo vệ PTPSTORE và các bên thứ ba khác: Chúng tôi chỉ đưa ra
                                thông tin tài
                                khoản và những thông tin cá nhân khác khi tin chắc rằng việc đưa những thông tin
                                đó là phù hợp với luật pháp, bảo vệ quyền lợi, tài sản của người sử dụng dịch
                                vụ, của PTPSTORE và các bên thứ ba khác</li>
                            <li class="policy-subDesc">+ Theo yêu cầu pháp lý từ một cơ quan chính phủ hoặc khi chúng
                                tôi tin rằng
                                việc làm đó là cần thiết và phù hợp nhằm tuân theo các yêu cầu pháp lý</li>
                            <li class="policy-subDesc">+ Trong những trường hợp còn lại, chúng tôi sẽ có thông báo cụ
                                thể cho Quý Khách
                                Hàng khi phải tiết lộ thông tin cho một bên thứ ba và thông tin này chỉ được
                                cung cấp khi được sự phản hồi đồng ý từ phía Quý Khách Hàng. VD: các chương
                                trình khuyến mãi có sự hợp tác, tài trợ với các đối tác của MYDEAL.vn; cung cấp
                                các thông tin giao nhận cần thiết cho các đơn vị vận chuyển</li>
                        </ul>
                        <p class="policy-desc">Nó chắc chắn không bao gồm việc bán, chia sẻ dẫn đến việc làm lộ thông
                            tin cá nhân
                            của khách hàng vì mục đích thương mại vi phạm những cam kết được đặt ra trong quy
                            định Chính sách bảo mật thông tin khách hàng của PTPSTORE</p>
                        <p class="policy-desc"><strong>4. Bảo mật thông tin khách hàng</strong></p>
                        <p class="policy-desc">Một điều quan trọng đối với khách hàng là việc tự bảo vệ bạn trước sự
                            tiếp cận thông
                            tin về password khi bạn dùng chung máy tính với nhiều người. Khi đó, bạn phải chắc
                            chắn đã thoát khỏi tài khoản sau khi sử dụng dịch vụ của chúng tôi.</p>
                        <p class="policy-desc">Chúng tôi cũng cam kết không cố ý tiết lộ thông tin khách hàng, không
                            bán hoặc chia
                            sẻ thông tin khách hàng của PTPSTORE vì mục đích thương mại vi phạm những cam kết
                            giữa chúng tôi với Quý khách hàng chiếu theo Chính sách bảo mật thông tin khách
                            hàng của PTPSTORE</p>
                        <p class="policy-desc">PTPSTORE nhấn mạnh rằng chúng tôi rất quan tâm đến quyền lợi của Quý
                            Khách hàng trong
                            việc bảo vệ thông tin cá nhân nên trong trường hợp bạn có góp ý, thắc mắc liên quan
                            đến chính sách bảo mật của chúng tôi, vui lòng liên hệ:</p>
                        <p class="policy-desc">Điện thoại: 0306 191 452 – Email: ptpstore@gmail.com</p>
                    </div>
                </div>
            </div>
            <div class=" col-md-3">

                <script src="{{ asset('user/app/services/moduleServices.js') }}"></script>
                <script src="{{ asset('user/app/controllers/moduleController.js') }}"></script>
                <!--Begin-->
                <div class="box-support-online">
                    <h3><span>Hỗ trợ trực tuyến</span></h3>
                    <div class="support-online-block">
                        <div class="support-hotline">
                            HOTLINE<br><span>0306 191 4**</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $(".menu-quick-select ul").hide();
        $(".menu-quick-select").hover(function () { $(".menu-quick-select ul").show(); }, function () { $(".menu-quick-select ul").hide(); });
    });
</script>
@endsection

@section('script')
<script type="text/javascript">
    $(".header-content").css({ "background": '' });
    $("html").addClass('');
</script>
@endsection
