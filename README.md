# repo-ptpstore-v.2

PTP Store - Config Project

## Config Application

- Step 1: run "flutter pub get"

## Config Website

- Step 1: run "composer update"
- Step 2: run "cp .env.example .env"
  Change APP_URL = http://localhost Or "IP Device"
- Step 3: run "php artisan key:generate"
- Step 4: Open MYSQL and create DB => set value for your DATABASE
  example:
  DB_CONNECTION=mysql
  DB_HOST=127.0.0.1
  DB_PORT=3306
  DB_DATABASE=ptpstore
  DB_USERNAME=root
  DB_PASSWORD=
- Step 5: Set value for your mail
  example:
  MAIL*MAILER=smtp
  MAIL_HOST=smtp.gmail.com
  MAIL_PORT=465
  MAIL_USERNAME=ptpstore.050914@gmail.com
  MAIL_PASSWORD=ptpstore01
  MAIL_ENCRYPTION=ssl
  MAIL_FROM_ADDRESS=ptpstore.050914@gmail.com
  MAIL_FROM_NAME="${APP_NAME}"
  mailtrap: (\_MAIL_USERNAME & * MAIL_PASSWORD base on file config mailtrap in browser)
  MAIL_MAILER=smtp
  MAIL_HOST=smtp.mailtrap.io
  MAIL_PORT=2525
  MAIL_USERNAME=b9dde30b2c2ffb
  MAIL_PASSWORD=316dfada513027
  MAIL_ENCRYPTION=tls
- Step 6: run XAMPP and start Apache
  if MYSQL exist in local (Ex: MySQL Workbench) => don't need start MYSQL

- Step 7: run "php artisan migrate"

- Step 8: run "php artisan db:seed"

- Step 9: run:
  composer update

            ## Start JWT
            php artisan vendor:publish--provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"
            php artisan jwt:secret
            ## Start JWT

            ##Clear Data
            php artisan config:clear
            php artisan view:clear
            php artisan cache:clear
            php artisan route:clear
            ##Clear Data

            ## Start Cronjob
            php artisan schedule:run
            php artisan schedule:work
            ## End Cronjob

            php artisan serve

- Step 10: Open browser
  Run site User => 127.0.0.1:8000
  Run site Admin => 127.0.0.1:8000/admin => login (admin/admin)

## With ptp_website:

- You need add a folder upload from repo-ptpstore-v.2/upload into ..\xampp\htdocs\
  to be able to get photos of the products or product_details,...
